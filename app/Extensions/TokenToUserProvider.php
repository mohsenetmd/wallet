<?php namespace App\Extensions;

use App\Models\Token;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class TokenToUserProvider implements UserProvider
{
    private $token;
    private $user;

    public function __construct(User $user, Token $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    public function retrieveById($identifier)
    {
        return $this->user->find($identifier);
    }

    public function retrieveByToken($identifier, $token)
    {
        if ($token != 'debug') {
            $redis = Redis::get($token);
            if ($redis) {
                $redisObject = json_decode($redis);
                return $redisObject;
            }
            $user = User::where('api_token', $token)->first();
            Redis::setex($token, 600, $user);
            return $user ? $user : null;
        } else {
            $user = User::find(26);
            return $user ? $user : null;
        }
    }

    public
    function updateRememberToken(Authenticatable $user, $token)
    {
        // update via remember token not necessary
    }

    public
    function retrieveByCredentials(array $credentials)
    {
        // implementation upto user.
        // how he wants to implement -
        // let's try to assume that the credentials ['username', 'password'] given
        $user = $this->user;
        foreach ($credentials as $credentialKey => $credentialValue) {
            if (!Str::contains($credentialKey, 'password')) {
                $user->where($credentialKey, $credentialValue);
            }
        }

        return $user->first();
    }

    public
    function validateCredentials(Authenticatable $user, array $credentials)
    {
        $plain = $credentials['password'];

        return app('hash')->check($plain, $user->getAuthPassword());
    }
}
