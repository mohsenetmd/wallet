<?php

namespace App\Http\Controllers;

use App\Models\AirFareCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class AirFareController extends Controller
{

    public function listLocation()
    {
        $array = ['ApiSiteID' => '00000000-0000-0000-0000-000000000000'];
        $url = 'http://webapitest.safariran.ir/api/FlightReservationApi/ListLocation';
        $jsonArray = json_encode($array);
        $data = $this->curlListLocation($jsonArray, $url);
        $dataArray = json_decode($data);
        Redis::setex('listCityAirFare', 600000, $dataArray);
    }


    public function curlListLocation($jsonArray, $url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonArray,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }


    public function selectCity()
    {
        if (!Redis::get('listCityAirFare')) {
            $this->listLocation();
        }
        $city = Redis::get('listCityAirFare');
        $cityArray = json_decode($city);
        $cityCollect=collect(json_decode($city));
        $cityVue=json_encode($cityArray);
        $selectedCity=json_encode(AirFareCity::all(),false);
//        dd(json_encode($test,true),json_encode($test,false),json_encode($test,JSON_PRETTY_PRINT));
        return view('airfare.select-city', ['cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'cityVue'=>$cityVue]);
    }

    public function saveCity($Abbreviation)
    {
        $city = Redis::get('listCityAirFare');
        $cityCollect = collect(json_decode($city));
        $cityApi = $cityCollect->where('Abbreviation', $Abbreviation)->first();
        $checkAirFareCity = AirFareCity::whereAbbreviation($Abbreviation)->first();
        if (!$checkAirFareCity) {
            AirFareCity::create([
                'name_en' => $cityApi->NameEN,
                'name_fa' => $cityApi->NameFA,
                'full_name' => $cityApi->FullName,
                'Abbreviation' => $cityApi->Abbreviation,
                'city_name' => $cityApi->CityName,
                'state_name' => $cityApi->StateName,
                'country_name' => $cityApi->CountryName
            ]);
        }else{
            $checkAirFareCity->delete();
        }
    }

    public function editCity(){
        $listAirFareCity=AirFareCity::all();
        $listAirFareCityJson=\GuzzleHttp\json_encode($listAirFareCity);
        return view('airfare.edit-city',['listAirFareCityJson'=>$listAirFareCity]);
    }

    public function updateCity(Request $request){
        AirFareCity::find($request->form_id)->update([
           'name_en'=>$request->name_en,
           'name_fa'=>$request->name_fa,
           'full_name'=>$request->full_name
        ]);
    }

    public function deleteCity(Request $request){
        AirFareCity::find($request->form_id)->delete();
    }
}
