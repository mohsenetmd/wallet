<?php

namespace App\Http\Controllers;

use App\Http\Requests\CharityRequest;
use App\Http\Resources\CharityListResource;
use App\Models\Charity;
use Carbon\Carbon;
use Illuminate\Support\Facades;
use Illuminate\Http\Request;

class CharityController extends Controller
{
    public function index(Request $request){
        if($request->filled('orderBy')){
            $charity=CharityListWebResource::collection(Charity::all());
            return response()->json($charity);
        }else{
            return view('charity.index');
        }
    }

    public function create()
    {
        return view('charity.create');
    }

    public function store(CharityRequest $request){
        $icon=$this->saveFile($request);
        Charity::create([
            'name'=>$request->name,
            'account_number'=>$request->account_number,
            'icon'=>$icon
        ]);
        return redirect(route('charity.index'));
    }


    public function edit(Charity $charity){
        return view('charity.edit',['charity'=>$charity]);
        return redirect(route('charity.index'));
    }

    public function update(Charity $charity,Request $request){
        Facades\Validator::make($request->all(),[
            'name'=>'required',
            'account_number'=>'required'
        ],[
            'name.required'=>'وارد کردن نام خیریه الزامی است',
            'account_number.required'=>'وارد کردن شماره حساب الزامی است',
        ])->validate();
        $icon=$this->saveFile($request);
        $charity->update([
            'name'=>$request->name,
            'account_number'=>$request->account_number,
            'icon'=>$icon ? $icon:$charity->icon
        ]);
    }

    public function destroy(Charity $charity){
        $charity->delete();
    }



    public function saveFile(Request $request)
    {
        if ($request->file('icon')) {
            $year = Carbon::now()->year;
            $month = Carbon::now()->month;
            $day = Carbon::now()->day;
            $pathFile = 'img/charity' . '/' . $year . '/' . $month . '/' . $day;
            $icon = $request->file('icon')->store($pathFile, 'app');
            return $icon;
        }
        return null;
    }
}
