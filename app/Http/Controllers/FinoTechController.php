<?php

namespace App\Http\Controllers;

use App\Models\CodeFinotech;
use App\Models\TokenFinotech;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class FinoTechController extends Controller
{
    public $address = "https://apibeta.finnotech.ir/";
    public $url = "https://apibeta.finnotech.ir/dev/v2/oauth2/token";
    public $debug = false;
    public $clientId = "newcash1";
    public $clientSecret = "dadba7740bd538bbad1b";
    public $grantType = "client_credentials";
    public $scopes = "";
    public $nid = "0682364223";

    /**
     * Show View For Panel Finotech
     */
    public function panelFinotechCc()
    {
        return view('finotech.panel-finotech-cc');
    }

    public function panelFinotechAc()
    {
        return view('finotech.panel-finotech-ac');
    }

    /*
    |--------------------------------------------------------------------------
    | Client Credential
    |--------------------------------------------------------------------------
    */

    /**
     * Get the first token for when not need to authenticate users
     * @return mixed
     */
    public function getClientCredential($scopes)
    {
        $array = ['grant_type' => "client_credentials", 'nid' => $this->nid, 'scopes' => $scopes];
        $responseObject = json_decode($this->curlResponseCc(json_encode($array), $this->url));
        if ($responseObject->status != 'DONE') {
            return false;
        }
        $this->saveTokenCc($responseObject, $scopes);
    }

    /**
     * Refresh Token Client Credential
     * @return mixed
     */
    public function refreshTokenCc($refreshToken)
    {
        $array = ["grant_type" => "refresh_token", "token_type" => "CLIENT-CREDENTIAL", "refresh_token" => $refreshToken];
        $responseObject = json_decode($this->curlResponseCc(json_encode($array), $this->url));
        if ($responseObject->status != 'DONE') {
            return false;
        }
        $this->saveTokenCc($responseObject, implode(',', $responseObject->result->scopes));
    }


    /**
     * Delete Token Client Credential
     * @return mixed
     */
    public function deleteTokenCc($id)
    {
        $token = TokenFinotech::find($id);
        $array = ['token' => $token->token, "token_type" => "CLIENT-CREDENTIAL"];
        $responseObject = json_decode($this->curlResponseDeleteCc(json_encode($array), 'https://apibeta.finnotech.ir/dev/v2/clients/' . $this->clientId . '/token'));
        if ($responseObject->status == "DONE") {
            return true;
        }
        return false;
    }

    /**
     * Get List Token Client Credential
     * @return mixed
     */
    public function getListTokenCc()
    {
        $url = "https://apibeta.finnotech.ir/dev/v2/clients/{$this->clientId}/tokens";
        $response = $this->curlResponseGetListCc($url);
        $responseObject = json_decode($response);
        if ($responseObject->status == "DONE") {
            $clientCredentials = $responseObject->result->clientCredentials;
            foreach ($clientCredentials as $key => $clientCredential) {
                $clientCredentials[$key]->scopes = $this->persianScope($clientCredential->scopes);
            }
        }
        return $clientCredentials;
    }


    public function getWages()
    {
        $url = "https://apibeta.finnotech.ir/dev/v2/clients/{$this->clientId}/tokens";
        $response = $this->curlWages($url);
        $responseObject = json_decode($response);
        if ($responseObject->status == "DONE") {
            $clientCredentials = $responseObject->result->clientCredentials;
            foreach ($clientCredentials as $key => $clientCredential) {
                $clientCredentials[$key]->scopes = $this->persianScope($clientCredential->scopes);
            }
        }
        return $clientCredentials;
    }

    public function getBankInfo()
    {
        $url = $url = "https://apibeta.finnotech.ir/facility/v2/clients/{$this->clientId}/banksInfo";
        $response = $this->curlBankInfo($url);
        $responseObject = json_decode($response);
        return $responseObject->result;

    }


    /*
    |--------------------------------------------------------------------------
    | Authorization Type
    |--------------------------------------------------------------------------
    */
    public function bankConfirmation(Request $request)
    {
        if ($request->error) {
            return view('api.msg', ['msg' => 'در پاسخ دریافتی از فینوتک به خطا برخورد کردیم.'
                , 'type' => 'error']);
        }
        if ($request->code) {
            $checkUniqueCode = CodeFinotech::where('code', $request->code)->first();
            if ($checkUniqueCode) {
                return view('api.msg', ['msg' => 'کد ورودی تکراری می باشد'
                    , 'type' => 'error']);
            }
            CodeFinotech::create([
                'code' => $request->code,
            ]);
            $response = $this->getAuthorizationCode($request->code);
            $lifeTime = JalaliToTimeStamp($response->creationDate) + $response->lifeTime / 1000 - 200000;
            $checkUniqueToken = TokenFinotech::where('token', $response->value)->first();
            if (!$checkUniqueToken) {
                TokenFinotech::create([
                    'monthly_call_limitation' => $response->monthlyCallLimitation,
                    'max_amount_per_transaction' => $response->maxAmountPerTransaction,
                    'user_id' => $response->userId,
                    'deposits' => json_encode($response->deposits),
                    'scopes' => json_encode($response->scopes),
                    'token' => $response->value,
                    'refresh_token' => $response->refreshToken,
                    'life_time' => $lifeTime,
                    'creation_date' => $response->creationDate
                ]);
                return view('api.msg', ['msg' => 'توکن دریافتی با موفقیت ذخیره شد.'
                    , 'type' => 'success']);
            }
            return view('api.msg', ['msg' => 'توکن دریافتی تکراری است'
                , 'type' => 'error']);
        }
        return view('api.msg', ['msg' => 'پارامتری به این صفحه ارسال نشده است'
            , 'type' => 'error']);
    }


    public function getAuthorizationCode($code)
    {
        $array = ['grant_type' => "authorization_code", "code" => $code, "bank" => "062", "redirect_uri" => "https://new-cash.ir/bank-confirmation"];
        $jsonArray = json_encode($array);
        if ($this->debug) {
            $response = $this->fakeDoneResponseAuthorizationCode();
        } else {
            $response = $this->curlAuthorization($jsonArray, $this->url);
        }
        $responseObject = json_decode($response);
        if ($responseObject->status != 'DONE') {
            return response()->error('در اتصال با فینوتک به مشکل خوردیم');
        }
        return $responseObject->result;
    }

    public function refreshTokenAc($refreshToken)
    {
        $array = ["grant_type" => "refresh_token", "token_type" => "CODE", "bank" => "062", "refresh_token" => $refreshToken];
        $responseObject = json_decode($this->curlResponseAc(json_encode($array), $this->url));
        if ($responseObject->status != 'DONE') {
            return false;
        }
        $this->saveTokenAc($responseObject);
    }


    public function getauthenticateString()
    {
        $authenticationString = base64_encode($this->clientId . ':' . $this->clientSecret);
        return $authenticationString;
    }


    public function Debug(string $jsonArray, string $header)
    {
        if ($this->debug) {
            $response = $this->fakeDoneResponseClientCredential();
        } else {
            $response = $this->curlResponse($jsonArray, $this->url);
        }
        return $response;
    }

    public function depositToCard(Request $request)
    {
        $traceNumber=rand('1111111111111111111111111111111111111111111111111111111111111111111111111111','9111111111111111111111111111111111111111111111111111111111111111111111111111');
        $array=['card'=>$request->card,'amount'=>$request->amount,'traceNumber'=>$traceNumber];
        $jsonArray = json_encode($array);
        $response=$this->curlDepositToCard($jsonArray);
        Redis::setex('response',6000,$response);
    }


    /*
    |--------------------------------------------------------------------------
    | Curl Function
    |--------------------------------------------------------------------------
    */

    public function curlResponseCc(string $jsonArray, $sendUrl)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonArray,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Basic bmV3Y2FzaDE6ZGFkYmE3NzQwYmQ1MzhiYmFkMWI="
            ),
        ));
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $response;
    }

    public function curlResponseAc(string $jsonArray, $sendUrl)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonArray,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Basic bmV3Y2FzaDE6ZGFkYmE3NzQwYmQ1MzhiYmFkMWI="
            ),
        ));
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $response;
    }

    public function curlResponseDeleteCc(string $jsonArray, $sendUrl)
    {
        $token = TokenFinotech::where('scopes', "like", "%boomrang:token:delete%")->first();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_POSTFIELDS => $jsonArray,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Bearer " . $token->token,
            ),
        ));
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $response;
    }

    public function curlResponseGetListCc($sendUrl)
    {
        $token = TokenFinotech::where('scopes', "like", "%boomrang:tokens:get%")->first();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Bearer " . $token->token,
            ),
        ));
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $response;
    }

    public function curlWages($sendUrl)
    {
        $token = TokenFinotech::where('scopes', "like", "%boomrang:wages:get%")->first();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Bearer " . $token->token,
            ),
        ));
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $response;
    }

    public function curlBankInfo($sendUrl)
    {
        $token = TokenFinotech::where('scopes', "like", "%facility:cc-bank-info:get%")->first();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Bearer " . $token->token,
            ),
        ));
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $response;
    }

    public function curlAuthorization($jsonArray, $sendUrl)
    {
        $head = $this->getauthenticateString();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonArray,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Basic " . $head,
            ),
        ));
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $response;
    }

    public function curlDepositToCard($jsonArray)
    {
        $token = TokenFinotech::where('scopes', "like", "%refund:deposit-card:post%")->first();
        $curl = curl_init();
        $trackId=Str::random(40);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apibeta.finnotech.ir/cardrefund/v2/clients/newcash1/depositToCard?trackId=" .$trackId,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $jsonArray,
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json",
                    "Authorization: Bearer " . $token,
                ),
            ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    /*
    |--------------------------------------------------------------------------
    | Other Function
    |--------------------------------------------------------------------------
    */

    public function saveTokenCc($responseObject, $scopes): void
    {
        $lifeTime = JalaliToTimeStamp($responseObject->result->creationDate) + $responseObject->result->lifeTime / 1000 - 200000;
        $refreshToken = $responseObject->result->refreshToken;
        TokenFinotech::create([
            'token' => $responseObject->result->value,
            "refresh_token" => $refreshToken,
            "scopes" => $scopes,
            'life_time' => $lifeTime,
            'creation_date' => $responseObject->result->creationDate,
            'created_at' => JalaliToTimeStamp($responseObject->result->creationDate)
        ]);
    }

    public function saveTokenAC($responseObject): void
    {
        $lifeTime = JalaliToTimeStamp($responseObject->result->creationDate) + $responseObject->result->lifeTime / 1000 - 200000;
        $refreshToken = $responseObject->result->refreshToken;
        TokenFinotech::create([
            'monthly_call_limitation' => $responseObject->result->monthlyCallLimitation,
            'max_amount_per_transaction' => $responseObject->result->maxAmountPerTransaction,
            'user_id' => $responseObject->result->userId,
            'deposits' => $responseObject->result->deposits[0],
            'scopes' => $responseObject->result->scopes[0],
            'token' => $responseObject->result->value,
            'refresh_token' => $responseObject->result->refreshToken,
            'life_time' => $lifeTime,
            'creation_date' => $responseObject->result->creationDate,
            'created_at' => JalaliToTimeStamp($responseObject->result->creationDate)
        ]);
    }

    public function persianScope($scopes): string
    {
        if (!is_array($scopes)) {
            $scopesArray = explode(',', $scopes);
        } else {
            $scopesArray = $scopes;
        }
        foreach ($scopesArray as $keyScope => $scope) {
            switch ($scope) {
                case "boomrang:sms-send:execute":
                    $scopesArray[$keyScope] = 'ارسال پیامک';
                    break;
                case "boomrang:sms-verify:execute":
                    $scopesArray[$keyScope] = 'تایید کد یکبار رمز پیامکی';
                    break;
                case "boomrang:token:delete":
                    $scopesArray[$keyScope] = 'حذف توکن';
                    break;
                case "boomrang:tokens:get":
                    $scopesArray[$keyScope] = 'مشاهده توکن های برنامه';
                    break;
                case "boomrang:wages:get":
                    $scopesArray[$keyScope] = 'مشاهده کارمزد ها';
                    break;
                case "card:information:get":
                    $scopesArray[$keyScope] = 'استعلام کارت';
                    break;
                case "facility:cc-bank-info:get":
                    $scopesArray[$keyScope] = 'اطلاعات بانک ها';
                    break;
                case "oak:iban-inquiry:get":
                    $scopesArray[$keyScope] = 'دریافت اطلاعات شبا';
                    break;
                case "oak:balance:get":
                    $scopesArray[$keyScope] = 'مانده حساب';
                    break;
                case '["oak:inquiry-transfer:get"]':
                    $scopesArray[$keyScope] = 'استعلام واریز وجه';
                    break;
                case '["oak:statement:get"]':
                    $scopesArray[$keyScope] = 'مانده و 100 گردش آخر حساب';
                    break;
                case '["oak:transfer-to:execute"]':
                    $scopesArray[$keyScope] = 'واریز وجه به';
                    break;
                case '["refund:deposit-card:post"]':
                    $scopesArray[$keyScope] = 'انتقال از حساب به کارت شتابی';
                    break;
            }
        }
        $persianStringScopes = implode(',', $scopesArray);
        return $persianStringScopes;
    }


}

