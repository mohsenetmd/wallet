<?php

namespace App\Http\Controllers;

use App\Category;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\v1\PaymentController as PaymentController;
use Illuminate\Support\Facades\Validator;

class LinkController extends Controller
{
    public $categoryLink = '3';
    public $statusPre = '0';

    public function show($url)
    {
        $user = User::where('url', $url)->first();
        return view('link.show', ['user' => $user]);
    }

    public function pay(Request $request)
    {
        Validator::make($request->all(), [
            'amount' => 'required|min:3|max:10',
            'url'=>'required'
        ],[
                'amount.required' => 'وارد کردن مبلغ الزامی است.',
                'amount.min' => 'مقدار وارد شده کمتر از حد مجاز است',
                'amount.max' => 'مقدار وارد شده بیشتر از حد مجاز است',
            ])->validate();
        $user = User::where('url', $request->url)->first();
        $category = Category::find($this->categoryLink);
        $transaction = Transaction::create([
            'user_id' => $user->user_id,
            'category_id' => $this->categoryLink,
            'amount' => $request->amount,
            'type_of_transaction' => $category->type_of_transaction,
            'status' => $this->statusPre,
        ]);
        $payment = new PaymentController;
        $redirectUrl = $payment->getLink($request, $transaction->transaction_id);
        return redirect('https://ipg.vandar.io/v3/' . $redirectUrl);
    }

}
