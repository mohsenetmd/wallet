<?php

namespace App\Http\Controllers\Panel;
use App\Http\Requests\GroupPanelRequest;
use App\Models\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public function insert(){
        return view('panel.group.insert');
    }
    public function store(GroupPanelRequest $request){
        Group::create($request->all());
    }
    public function delete(Request $request){
        Group::find($request->group_id)->delete();
    }
    public function update(GroupPanelRequest $request){
        Group::update([
            'name'=>$request->name
        ]);
    }
    public function show(App\Group $grouppanel){
    }
}
