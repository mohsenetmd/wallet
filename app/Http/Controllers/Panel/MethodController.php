<?php

namespace App\Http\Controllers\Panel;
use App\Http\Requests\MethodPanelRequest;
use App\Models\Method;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MethodController extends Controller
{
    public function insert(){
        return view('panel.method');
    }

    public function store(MethodPanelRequest $request){
        Method::create($request->all());
    }
    public function delete(Request $request){
        Method::find($request->method_id)->delete();
    }
    public function update(MethodPanelRequest $request){
        Method::update([
            'title'=>$request->title
        ]);
    }
    public function show(App\Mehod $Method){
    }
}
