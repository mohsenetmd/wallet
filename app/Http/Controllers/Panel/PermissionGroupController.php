<?php

namespace App\Http\Controllers\Panel;

use App\Group;
use App\Http\Requests\PermissinGroupPanelRequest;
use App\Models\Method;
use App\Models\PermissionGroup;
use App\Models\PermissionUser;
use App\Models\UserPanel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionGroupController extends Controller
{
    public function insert(){

        $method=Method::all();
        $groups=Group::all();
        $all = PermissionGroup::select('*')
            ->rightJoin('pl_methods', function($join) {
                $join->on('pl_permission_groups.method_id', '=', 'pl_methods.method_id');})->get();
        return view('panel.permissiongroup.insert', ['groups' => $groups,'methods'=>$method,'all'=>$all]);
    }

    public function store(PermissinGroupPanelRequest $request){
        PermissionGroup::where('group_id',$request->group_id)->delete();
        if($request->status) {
            foreach ($request->status as $key => $index) {
                PermissionGroup::create([
                    'group_id' => $request->group_id,
                    'method_id' => $key,
                    'status' => $index
                ]);
            }
        }
    }
    public function ajaxStore()
    {
        PermissionGroup::where('group_id',request()->group_id)->delete();
        $method_id="";
        $formData = $_POST['myCheckboxes'];
        foreach ($formData as $key => $index) {
            if(str_contains($index,'myCheckboxes%5B%5D')) {
                $method_id =   str_replace("myCheckboxes%5B%5D=", "", $index);
                PermissionGroup::create([
                    'group_id' => request()->group_id,
                    'method_id' => $method_id,
                    'status' => 1
                ]);
            }
        }
        $returnHTML = view('panel.permissiongroup.success', ['group_id' => request()->group_id])->render();
        return response()->json($returnHTML);
        /* if (true) {
             $returnHTML = view('panel.permissiongroup.success', ['group_id' => request()->group_id])->render();

             return response()->json($returnHTML);
         } else {
             return false;
         }*/
        /*PermissionGroup::where('group_id',request()->group_id)->delete();
        if(request()->status) {
            foreach (request()->status as $key => $index) {
                PermissionGroup::create([
                    'group_id' => request()->group_id,
                    'method_id' => $key,
                    'status' => $index
                ]);
            }
        }
        if (true) {
            $returnHTML = view('panel.permissiongroup.success', ['group_id' => request()->group_id])->render();

            return response()->json($returnHTML);
        } else {
            return false;
        }*/
    }
    public function delete(Request $request){
        PermissionGroup::get($request->permission_group_id)->delete();
    }
    public function update(PermissinGroupPanelRequest $request){
        PermissionGroup::update([
            'status'=>$request->status
        ]);
    }

    public function show(App\PermissionGroup $PermissionGroup){
    }
    public function ajax()
    {
        $method=Method::all();
        $all_permission = PermissionGroup::select('*')
            -> rightJoin('pl_methods', function($join) {
                $join->on('pl_permission_groups.method_id', '=', 'pl_methods.method_id')
                    ->where('pl_permission_groups.group_id', '=' , request()->group_id);
            })
            ->get();
        if (true) {
            $returnHTML = view('panel.permissiongroup.ajax', ['group_id' => request()->group_id, 'all_permission'=> $all_permission,'methods'=>$method])->render();
            return response()->json($returnHTML);
        } else {
            return false;
        }
    }
    public function test(){
        $methodAll=Method::get();
        $method=Method::get('method_id');
        $all_permission = PermissionGroup::select('*')
            -> leftJoin('pl_methods', function($join) {
                $join->on('pl_permission_groups.method_id', '=', 'pl_methods.method_id');})->where('pl_permission_groups.group_id',1)->get();
        $methods=$method->toarray();

        foreach ($methods as $item){

            if ( in_array($item, $all_permission->toarray())){

            }

        }

    }

}
