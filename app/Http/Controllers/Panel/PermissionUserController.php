<?php

namespace App\Http\Controllers\Panel;
use App\Http\Requests\PermissionUserPanelRequest;
use App\Models\Method;
use App\Models\PermissionUser;
use App\Models\UserPanel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionUserController extends Controller
{
    public function insert(){
        $method=Method::all();
        $users=UserPanel::all();
        $permissionuser=PermissionUser::all();
        $all = PermissionUser::select('*')
            ->leftJoin('pl_methods', function($join) {
                $join->on('pl_permission_users.method_id', '=', 'pl_methods.method_id');})->get();
        return view('panel.permissionuser.insert', ['users' => $users,'methods'=>$method,'permissionusers'=>$permissionuser,'all'=>$all]);
    }
    public function store(PermissionUserPanelRequest $request){

        PermissionUser::where('user_id',$request->user_id)->delete();
        if($request->status) {
            foreach ($request->status as $key => $index) {
                PermissionUser::create([
                    'user_id' => $request->user_id,
                    'method_id' => $key,
                    'status' => $index
                ]);
            }
        }
    }
    public function delete(Request $request){
        PermissionUser::find($request->permission_user_id)->delete();
    }
    public function update(PermissionUserPanelRequest $request){
        PermissionUser::update([
            'status'=>$request->status
        ]);
    }
    public function show(App\PermissionUser $PermissionUser){
    }
    public function ajax()
    {   $method=Method::all();
        $all_permission = PermissionUser::select('*')
            -> rightJoin('pl_methods', function($join) {
                $join->on('pl_permission_users.method_id', '=', 'pl_methods.method_id')
                    ->where('pl_permission_users.user_id', '=' , request()->user_id);
            })
            ->get();
        if (true) {
            $returnHTML = view('panel.permissionuser.ajax', ['user_id' => request()->user_id, 'all_permission'=> $all_permission,'methods'=>$method])->render();
            return response()->json($returnHTML);
        } else {
            return false;
        }
    }
}
