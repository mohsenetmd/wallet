<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Models\categoryPlace;
use App\Models\selectedPlace;
use App\Models\typeCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Models\Taxi;
use function GuzzleHttp\Psr7\copy_to_string;
use function GuzzleHttp\Psr7\str;
use function HighlightUtilities\splitCodeIntoArray;

class TaxiController extends Controller
{
    public function request(){

        return  view('panel.taxi.request');
    }
    public function countRequest()
    {
        $countRequest=Taxi::where('status','=','0')->get()->count();
          return  '<p  style="background-color: #d1ecf1; font-size: 12px;">     درخواست درحال انتظار  [ '.$countRequest.'   ]  </p>';
    }

    public function currentRequest()
    {
        $countRequest=Taxi::where('status','=','0')->get()->count();
        $request=Taxi::where('status','=','0')->get();

        return view('panel.taxi.currentRequest',['listRequest'=>$request,'countRequest'=>$countRequest]);
    }    public function chooseSelectedPlace()
    {
         //return view('panel.taxi.request');//, ['origin' => request()->place_lat,'destination'=>request()->place_lng,'namePlace'=>request()->typePlace]);
    }
    public function calculatePrice(Request $request){
        return'123';
        $wait=(double)$request->waiting;
        if ($wait != -1) {
            if ($wait == 0)
                $wait = "0m-5m";
            elseif ($wait == 5)
                $wait = "5m-10m";
            elseif ($wait == 10)
                $wait = "10m-15m";
            elseif ($wait == 15)
                $wait = "15m-20m";
            elseif ($wait == 20)
                $wait = "20m-25m";
            elseif ($wait == 25)
                $wait = "25m-30m";
            elseif ($wait == 30)
                $wait = "30m-45m";
            elseif ($wait == 45)
                $wait = "45m-1h";
            elseif ($wait == 1)
                $wait = "1h-1h30m";
            elseif ($wait == 1.5)
                $wait = "1h30m-2h";
            elseif ($wait == 2)
                $wait = "2h-2h30m";
            elseif ($wait == 2.5)
                $wait = "2h30m-3h";
            elseif ($wait == 3)
                $wait = "3h-3h30m";
            elseif ($wait == 3.5)
                $wait = "3h30m-4h";
        }
         $origin_lat=(double)($request->origin_lat);
         $origin_lng= (double)$request->origin_lng;
         $destination_lat=(double)$request->destination_lat;
         $destination_lng=(double)$request->destination_lng;
         $extra_destination_lat=null;
         $extra_destination_lng=null;
         $round_trip=(double)$request->round_trip;
         $waiting= $wait;
        $curlTaxiPrice= 'https://corporate.snapp.site/api/v1/ride/price';
        $POSTFIELDS=json_encode(array(
            /*"origin_lat"=>  (double)request()->origin_lat//36.326687547372785
            ,"origin_lng"=> (double)request()->origin_lng//59.485340974875165
            ,"destination_lat"=>(double)request()->destination_lat//36.32684014801495
            ,"destination_lng"=>(double)request()->destination_lng//59.67220003380188*/
            "origin_lat"=>(double)$request->origin_lat//36.326687547372785
            ,"origin_lng"=> (double)$request->origin_lng//59.485340974875165
            ,"destination_lat"=>(double)$request->destination_lat//36.32684014801495
            ,"destination_lng"=>(double)$request->destination_lng//59.67220003380188
            ,"extra_destination_lat"=>null
            ,"extra_destination_lng"=>null
            ,"round_trip"=>(double)$request->round_trip
            ,"service_type"=> (double)$request->service_type
            ,"waiting"=> $wait
            ));
        $HTTPHEADER= array(
            'Accept:application/x-www-form-urlencoded',
            'Accept-Encoding:gzip, deflate, br',
            'Accept-Language:en-US,en;q=0.9',
            'Authorization:eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ',
            'Content-Type:application/json',
            'Origin:https://corporate.snapp.taxi',
            'Referer:https://corporate.snapp.taxi/ride-request',
            'sec-fetch-mode:cors',
            'sec-fetch-site:cross-site',
            'User-Agent:Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36'
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curlTaxiPrice,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $POSTFIELDS,
            CURLOPT_HTTPHEADER => $HTTPHEADER,
        ));
        $response = curl_exec($curl);
        $responseArray = json_decode($response);
        $r=$responseArray;
        if($r) {
            $finalPrice = ($responseArray->prices[0]->final);
            $returnHTML = $finalPrice;//view('panel.taxi.request',['user_id'=>$user->user_id,'username'=>$user->username,'mobile'=>$user->mobile,'password'=>$user->password])->render();
            return  ($returnHTML);
        }
        else{
            return null;
        }
    }
    public function requestService(Request $request){
        $wait=(double)$request->waiting;
        if ($wait != -1) {
            if ($wait == 0)
                $wait = "0m-5m";
            elseif ($wait == 5)
                $wait = "5m-10m";
            elseif ($wait == 10)
                $wait = "10m-15m";
            elseif ($wait == 15)
                $wait = "15m-20m";
            elseif ($wait == 20)
                $wait = "20m-25m";
            elseif ($wait == 25)
                $wait = "25m-30m";
            elseif ($wait == 30)
                $wait = "30m-45m";
            elseif ($wait == 45)
                $wait = "45m-1h";
            elseif ($wait == 1)
                $wait = "1h-1h30m";
            elseif ($wait == 1.5)
                $wait = "1h30m-2h";
            elseif ($wait == 2)
                $wait = "2h-2h30m";
            elseif ($wait == 2.5)
                $wait = "2h30m-3h";
            elseif ($wait == 3)
                $wait = "3h-3h30m";
            elseif ($wait == 3.5)
                $wait = "3h30m-4h";
        }
       $POSTFIELDS=
            json_encode(array(
            "origin_lat"=>(double)$request->origin_lat//36.326687547372785
            ,"origin_lng"=> (double)$request->origin_lng//59.485340974875165
            ,"destination_lat"=>(double)$request->destination_lat//36.32684014801495
            ,"destination_lng"=>(double)$request->destination_lng//59.67220003380188
            ,"extra_destination_lat"=>null
            ,"extra_destination_lng"=>null
            ,"round_trip"=>$request->round_trip
            ,"waiting"=> $wait
            ,"service_type"=>(double)$request->service_type
            ,"contact_mobile"=>$request->contact_mobile
            ,"contact_name"=>$request->contact_name
            ));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://corporate.snapp.site/api/v1/ride/request",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n    \"origin_lat\": ".(double)$request->origin_lat.",\r\n    \"origin_lng\": ".(double)$request->origin_lng.",\r\n    \"destination_lat\": ".(double)$request->destination_lat.",\r\n    \"destination_lng\": ".(double)$request->destination_lng.",\r\n   \"round_trip\": ".$request->round_trip.",\r\n    \"waiting\": \"".$wait."\",\r\n    \"contact_name\": \"".$request->contact_name."\",\r\n    \"contact_mobile\": \"".$request->contact_mobile."\",\r\n    \"contact_id\": \"\",\r\n    \"by_credit\": false,\r\n    \"service_type\": \"".(double)$request->service_type."\"\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ",
                "Content-Type: application/json",
                "Cookie: laravel_session=L2TX2vypI4lRwIV4u6rYFxvolpGKsEMUoMrzScI9"
            ),
        ));
       // $response = curl_exec($curl);


         Redis::setex('SNP-20200921-0743-594',3000,$POSTFIELDS);

        // return array(json_decode($response,JSON_PRETTY_PRINT))[0]['ride_id'];
        return '1441';
    }
    public function requestRefresh($rideId=0)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://corporate.snapp.site/api/v1/ride/SNP-20200921-0743-593/refresh",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ",
                "Content-Type: application/json;charset=UTF-8",
                "Cookie: laravel_session=L2TX2vypI4lRwIV4u6rYFxvolpGKsEMUoMrzScI9"
            ),
        ));
//---------------جهت تست----------
        if($rideId!=0)// //درخواست  api//
        {
            $returnHTML =  ['origin' => '$origin', 'destination' => '$destination', 'driver_name' =>'علی مرادی', 'cellphone' => '09154748958', 'vehicle_model' => 'ب', 'plate' => '234'];
            return $returnHTML;
        }
//-----------------------------
        $response = 'تستی';//curl_exec($curl);
        if($response=== false) {
            return false;
        }
        else{
            $plate_1 = (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["character"]; //.array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['character'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['part_a'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['part_b'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['part_a'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['iran_id'];
            $plate_2 = (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["part_a"];
            $plate_3 = (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["part_b"];
            $plate_4 = 'ایران' . (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["iran_id"];
            $plate = $plate_4 . '-' . $plate_3 . '-' . $plate_1 . '-' . $plate_2;
            $origin = array(json_decode($response, JSON_PRETTY_PRINT))[0]['origin']['details'];
            $destination = array(json_decode($response, JSON_PRETTY_PRINT))[0]['destination']['details'];
            if($rideId=0)// //درخواست  api//
            {
                $returnHTML =  ['origin' => '$origin', 'destination' => '$destination', 'driver_name' =>'علی مرادی', 'cellphone' => '09154748958', 'vehicle_model' => 'ب', 'plate' => '234'];

            }
            else
                $returnHTML = view('panel.taxi.resultRequest', ['origin' => $origin, 'destination' => $destination, 'driver_name' => array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['driver_name'], 'cellphone' => (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['cellphone']), 'vehicle_model' => array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['vehicle_model'], 'plate' => $plate])->render();

            return $returnHTML;
        }
    }
    public function search(Request $request)
    {
        $myInput= urlencode($request->myInput) ;
        $curl = curl_init();
        $url="https://gmaps.snapp.site/maps/api/place/autocomplete/json?input={$myInput}&location=36.289632,59.616130";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ",
                "scheme: https",
                "accept-language: en-US,en;q=0.9",
                "origin: https://corporate.snapp.taxi",
                "referer: https://corporate.snapp.taxi/",
                "sec-fetch-dest: empty",
                "sec-fetch-mode: cors",
                "sec-fetch-site: cross-site",
                "Cookie: d40ade857a13b5ba0f5db6715b8faa4f=70f8c2c86aa4579102a84aca2b2dc72f"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
       // return $response;
       // return array(json_decode($response,JSON_PRETTY_PRINT))[0]['predictions'];
        $latitude= array(json_decode($response,JSON_PRETTY_PRINT))[0]['predictions'][0]['location']['latitude'];
       $longitude= array(json_decode($response,JSON_PRETTY_PRINT))[0]['predictions'][0]['location']['longitude'];
       $name= array(json_decode($response,JSON_PRETTY_PRINT))[0]['predictions'][0]['name'];
       $array="";
        foreach (array(json_decode($response,JSON_PRETTY_PRINT))[0]['predictions'] as $part) {
            $array .= $part['name']."," .$part['location']['latitude']."," .$part['location']['longitude'].'#';

         }
       return $array;
      return  array(json_decode($response,JSON_PRETTY_PRINT))[0]['predictions'][0]['name']."," .$latitude."," .$longitude.'#'
            .array(json_decode($response,JSON_PRETTY_PRINT))[0]['predictions'][1]['name']."," .$latitude."," .$longitude
         ;
    }
    //--------------------------------------------------
    public function  storeTrip(){
        $name = Redis::get('SNP-20200921-0743-594');
        Taxi::create([
            'ride_id'=> 'SNP-20200921-0743-592',
            'mobile'=> '09154146825',
            'contact_name'=> 'تتا',
            'destination_lat'=> '36.262082',
            'destination_lng'=> 59.614877,
            'extra_destination_lat'=> 0,
            'extra_destination_lng'=>0,
            'origin_lat'=>36.290426,
            'origin_lng'=> 59.615709,
            'round_trip'=> "false",
            'service_type'=> 0,
            'waiting'=> "0m-5m",
            'status'=>0,

            ]
        );
        return (($name));
    }
    public function storeSelectedPlace(Request $request)
    {
      /* if ($request->hasFile('file')) {
            $filename = $request->file('file')->getClientOriginalName();

            $request->file->storeAs('public', $filename);
            $file = new File;
            $file->name = $filename;
            $file->save();
        }
           dd($request->file('file'));*/
        selectedPlace::create([
                'place_lat'=> request()->place_lat,
                'place_lng'=> request()->place_lng,
                'name'=>request()->name,
                'city_id'=>request()->cityId,
                'address'=>request()->address,
                'tel'=>request()->tel,
                'category_id'=>(request()->category_Id)? request()->category_Id : 1000,
                'fa_description'=>'test',
                'en_description'=>'test',
                'ohter_description'=>'test',
                'icon'=>'noImg.jpg',
            ]
        );
        return ('ok');
    }
    }

