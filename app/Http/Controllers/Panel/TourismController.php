<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Models\categoryPlace;
use App\Models\selectedPlace;
use App\Models\typeCategory;
use Illuminate\Http\Request;

class TourismController extends Controller
{
    public function  storeTypeCategory(Request $request)
    {
        $typeCategory = typeCategory::create($request->all());
        if ($request->title) {
            return view('panel.Tourism.typeCategory', ['typeCategory' => $typeCategory]);
        }
    }
    public function  storeCategoryPlace(Request $request)
    {
        $typeCategory = categoryPlace::create($request->all());
        return 'ok';
    }
    public function editCategoryPlace(Request $request)
    {
        categoryPlace::where('id', $request->id)->update([
            'name'=>$request->name,
            'icon'=>$request->icon,
        ]);

        return 'ok';
    }
    public function typeCategory()
    {
        return view('panel.Tourism.typeCategory');

    }
    public function  listTypeCategory()
    {

        $typeCategoris =typeCategory::latest()->get();

        return view('panel.Tourism.listTypeCategory',['list'=>$typeCategoris,'cityId'=>request()->cityId ? request()->cityId :1]);
    }
    public function  listCity()
    {
       $city=[0=>'انتخاب شهر',1=>'مشهد',2=>'تهران',3=>'اصفهان',4=>'شیراز'];

        return  $city ;
    }

    public function homeTourism($id=0,$cityId=1,Request $request){
      if($request->name){
          $place = selectedPlace::find($request->id);
          selectedPlace::find($place->id)->update([
            'tel' => $request['tel'],
            'name' => $request['name'],
            'category_id' => 3,//$request['inputCat'],
          ]);
      }
      if($id)
      {
        $typeCategory = typeCategory::latest()->get();
        $kind= typeCategory::where('id','=',$id)->first();
        $parent = categoryPlace::where('parent_id', '=', '0')->where('typecategory_id','=',$id)->get();
        $child =  categoryPlace::where('parent_id', '<>', '0')->get();
        $places = selectedPlace::where('city_id','=',$cityId)->get();
       }
      $city=$this->listCity();

      return view('panel.tourism.homeTourism', ['typeCategory' => $typeCategory,'categoryList' => $parent,'childs'=>$child,'places'=>$places,'cityId'=>$cityId,'cityName'=>$city[$cityId],'kind'=>$kind->title]);
    }
    public function placeInfo($id){
        $places= selectedPlace::where('id',$id)->first();
        $city=$this->listCity();
        $group =  categoryPlace::where('id', '=',  $places->category_id)->first();
        $groupAll =  categoryPlace::where('parent_id', '=',  $group->parent_id)->get();
        $parent = categoryPlace::where('id', '=', $groupAll[0]['parent_id'])->first();

        return  view('panel.tourism.placeInfo',['place'=>$places,'city'=>$city,'groupName'=>$group->name,'group'=>$groupAll,'typeCat'=>$parent->typecategory_id]);
    }


}
