<?php

namespace App\Http\Controllers\Panel;

use App\Models\Group;
use App\Http\Requests\UserGroupPanelRequest;
use App\Models\User;
use App\Models\UserGroup;
use App\Models\UserPanel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;

class UserGroupController extends Controller
{
    public function insert(){
        $users=UserPanel::all();
        $groups=Group::all();
        return view('panel.usergroup.insert', ['users' => $users,'groups'=>$groups]);
    }

    public function store(UserGroupPanelRequest $request){
        UserGroup::create($request->all());
    }

    public function delete(Request $request){
        UserGroup::find($request->user_group_id)->delete();
    }

    public function update(UserGroupPanelRequest $request){

    }


    public function show(App\UserGroup $UserGroup){
    }
}
