<?php

namespace App\Http\Controllers\Panel;
use Cookie;
use Session;
use App\Http\Requests\UserPanelRequest;
use App\Models\PermissionGroup;
use App\Models\UserPanel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class UserPanelController extends Controller
{
    private $pageTitle='کاربران';
    public $lengthStrToken = 100;
    public $timeCookie = 86400 * 30;
    public function insert()
    {
        return view('panel.user.insert');
    }
    public function insertajax()
    {
        if (true) {
            $returnHTML = view('panel.user.insertajax' )->render();
            return response()->json($returnHTML);
        } else {
            return false;
        }
    }
    public function update()
    {
        $user = UserPanel::find(request()->user_id);
        UserPanel::find($user->user_id)->update([
            'mobile'   => request()->mobile,
            'username' => request()->username,
            'password' => Hash::make(request()->password)
        ]);
        $returnHTML = view('user.success')->render();
        return response()->json($returnHTML);

    }
    public function updateAjax()
    {
        $user = UserPanel::find(request()->user_id);
        $returnHTML = view('panel.user.update',['user_id'=>$user->user_id,'username'=>$user->username,'mobile'=>$user->mobile,'password'=>$user->password])->render();
        return response()->json($returnHTML);
    }
    public function store(UserPanelRequest $request)
    {
        UserPanel::create([
            'mobile' =>  $request->mobile,
            'username' => $request->username,
            'password' => Hash::make( $request->password),
        ]);


        return  view('layouts.home');
    }
    public function storeUser()
    {
        UserPanel::create([
            'mobile' => request()->mobile,
            'username' =>  request()->username,
            'password' =>Hash::make( request()->password),
        ]);
        return  view('panel.user.list');
    }
    public function delete(Request $request)
    {
        UserPanel::find($request->user_id)->delete();
        return back();
    }
    public function updatePass(Request $request)
    {
        return  view('panel.updatePass');

    }
    public function RestorePass(Request $request)
    {
        $user = UserPanel::where('mobile', $request->mobile)->first();
        if (!$user) {
            return  view('panel.updatePass')->withErrors('شماره همراه وارد شده صحیح نمی باشد');
        } else {
            $user->update([
                'password' =>Hash::make($request->mobile)
            ]);
            return view('panel.login');
        }
    }
    public function show(Request $request)
    {
        $data = UserPanel::latest()->get();
        return Datatables::of($data)
            ->addColumn('action', function ($row) {
                return $this->actionButtonRow($row);
            })
            ->rawColumns(['action'])
            ->make(true);
        return view('layouts.home', ['username' => ""]);
    }

    public function list(Request $request)
    {
        if ($request->ajax()) {
            $data = UserPanel::latest()->get();

            return Datatables::of($data)
                ->addColumn('link_city', function ($row) {
                    $btn = $this->actionButtonRow($row);
                    return $btn;
                })
                ->rawColumns(['link_city'])
                ->make(true);
        }

        return  view('panel.user.list');

    }
    public function permission(){
        return view('welcome')->withErrors(['permission' => 'کاربر گرامی٬ دسترسی لازم وجود ندارد']);
    }
    public function home(Request $request)
    {

        $pageTitle='';
        if ( session('username') || $request->username || request()->username  ) {

            return view('layouts.home', ['username' => $request->username, 'pageTitle' => $pageTitle]);

        }
        else
            return view('panel.login')->withErrors(['nologin' => 'کاربر گرامی،لطفا جهت ورود به پرتال لاگین کنید']);
    }
    public function login(Request $request)
    {
        $pageTitle='';
        $user = UserPanel::where('username', $request->username)->first();
        if (!$user) {

            return view('panel.login')->withErrors(['username' => 'Incorrect  username']);
        }
        if (Hash::check($request->password, $user->password)) {
            Auth::guard('user_panel')->login($user);
            session(['username' => $request->username]);
            return view('layouts.home', ['username' => $request->username,'pageTitle'=> $pageTitle]);
        }
        return view('panel.login')
            ->withInput($request->only('password', 'remember'))
            ->withErrors(['password' => 'Incorrect  password']);
    }
    public function logout(Request $request)
    {

        Auth::guard('user_panel')->logout();
        $request->session()->forget('username');

        return view('panel.login')->withCookie(Cookie::forget('sadra_session'));
    }
    public function actionButtonRow($row): string
    {
        $csrf = csrf_token();
        $link = '
                     <form style="display:contents" method="post" action="">
                    <input type="hidden" name="_token" value="' . $csrf . '">
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" id="e_user_id" value="' . $row->user_id . '">
                    <a class="btn btn-edit" onclick="editrow('. $row->user_id.')"   >ویرایش</a>
                    </form>
                    <form style="display:contents" method="post" action="' . route("panel.user.delete") . '">
                    <input type="hidden" name="_token" value="' . $csrf . '">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="user_id" value="' . $row->user_id . '">
                    <button class="btn btn-danger" type="submit">حذف</button>
                    </form>
                    ';
        return $link;
    }

}
