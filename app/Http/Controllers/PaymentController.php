<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public $api='test';


    public function send( $amount, $redirect) {
        return $this->curl_post('https://pay.ir/pg/send', [
            'api'          => $this->api,
            'amount'       => $amount,
            'redirect'     => $redirect,
        ]);
    }

    public function curl_post($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    public function payment($amount,$redirect){
        $api = $this->api;;
        $result = send($api, $amount, $redirect);
        $result = json_decode($result);
        if($result->status) {
            return "https://pay.ir/pg/$result->token";
        } else {
            echo $result->errorMessage;
        }
    }
}
