<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Inertia\Inertia;

class SnapController extends Controller
{
    public function search($name = "حرم")
    {
        $name = urlencode($name);
        $url = "https://gmaps.snapp.site/maps/api/place/autocomplete/json?input={$name}&location=36.3235179,59.57232200000001";
        $search = $this->curlResponseGetSearch($url);
        $searchObject = json_decode($search, true);
        if ($searchObject) {
            return $searchObject['predictions'];
        }
        return array();
    }

    public function curlResponseGetSearch($url)
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ",
                "scheme: https",
                "accept-language: en-US,en;q=0.9",
                "origin: https://corporate.snapp.taxi",
                "referer: https://corporate.snapp.taxi/",
                "sec-fetch-dest: empty",
                "sec-fetch-mode: cors",
                "sec-fetch-site: cross-site",
                "Cookie: d40ade857a13b5ba0f5db6715b8faa4f=70f8c2c86aa4579102a84aca2b2dc72f"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function index()
    {
        $data = [
            'name' => 'mohsen',
            'family' => 'etemady',
        ];
        return Inertia::render('Users/Index', $data);
    }

    public function index2()
    {
        $data = [
            'name' => 'mohsen',
            'family' => 'etemady',
        ];
        return Inertia::render('Users/Index2', $data);
    }

}
