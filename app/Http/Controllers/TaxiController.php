<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use function HighlightUtilities\splitCodeIntoArray;

class TaxiController extends Controller
{
    public function request(){

        return  view('panel.taxi.request');

    }
    public function calculatePrice(){
        $curlTaxiPrice= 'https://corporate.snapp.site/api/v1/ride/price';
        // $r=\request()->origin_lat;
        $POSTFIELDS=
            json_encode(array(
             "origin_lat"=>36.312323076822515
            ,"origin_lng"=> 59.485340974875165
            ,"destination_lat"=>36.32684014801495
            ,"destination_lng"=>59.67220003380188
            ,"extra_destination_lat"=>null
            ,"extra_destination_lng"=>null
            ,"round_trip"=>false,
                "waiting"=>null

            ));
        $HTTPHEADER= array(
            'Accept:application/x-www-form-urlencoded',
            'Accept-Encoding:gzip, deflate, br',
            'Accept-Language:en-US,en;q=0.9',
            'Authorization:eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ',
            'Content-Length:229',
            'Content-Type:application/json',
            'Origin:https://corporate.snapp.taxi',
            'Referer:https://corporate.snapp.taxi/ride-request',
            'sec-fetch-mode:cors',
            'sec-fetch-site:cross-site',
            'User-Agent:Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36'
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curlTaxiPrice,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $POSTFIELDS,
            CURLOPT_HTTPHEADER => $HTTPHEADER,
        ));
        $response = curl_exec($curl);

        $responseArray = json_decode($response);
        $r=$responseArray->status===200;

        if($r) {
            $finalPrice = ($responseArray->prices[0]->final);
            $returnHTML = $finalPrice;//view('panel.taxi.request',['user_id'=>$user->user_id,'username'=>$user->username,'mobile'=>$user->mobile,'password'=>$user->password])->render();
            return response()->json($returnHTML);

        }
        else{
            return ('خطا در محاسبه قیمت!');
        }
    }

    public function requestService(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://corporate.snapp.site/api/v1/ride/request",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"{\r\n    \"origin_lat\": 36.29521491070731,\r\n    \"origin_lng\": 59.576573145344005,\r\n    \"destination_lat\": 36.29705991070731,\r\n    \"destination_lng\": 59.57773209735702,\r\n   \"round_trip\": true,\r\n    \"waiting\": \"0m-5m\",\r\n    \"contact_name\": \"کاملی\",\r\n    \"contact_mobile\": \"09154146825\",\r\n    \"contact_id\": \"\",\r\n    \"by_credit\": false,\r\n    \"service_type\": \"1\"\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ",
                "Content-Type: application/json",
                "Cookie: laravel_session=L2TX2vypI4lRwIV4u6rYFxvolpGKsEMUoMrzScI9"
            ),
        ));
        //$response = curl_exec($curl);
        //return array(json_decode($response,JSON_PRETTY_PRINT))[0]['ride_id'];
        return '1441';
    }
    public function requestRefresh()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://corporate.snapp.site/api/v1/ride/SNP-20200910-0748-286/refresh",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJ0Ijoib3BlcmF0b3IiLCJ1IjoiNEJYeFpOcmpCM05FNnYzIiwiYyI6InAyUVhKbnB2Z1JvTDYwdyIsImgiOiIwZGRhY2M5MTVkYWExNzgyOWI2NDc5ZWFlMWEyYjc3NSJ9.WeTzZVI6FN5SwD_y6FkBJRvUOHn9R1lzLSRH9gUUpnB-FAjAfr_Q4_HMKZ7DJqMF-7hjsuNdB1CUHe1EKUYIzxOkyowmT5AFexP2S3Kd5eexmbbvD6TVYCY3vAPNqWgmsC-Wn4MronErsalYhAuxc3baXtmDVx2wf9W3Ihm1p9isjQKgKEEkV_Y847ZCVVo71xD7O8y2bBYJqVYmEjMMKCFP8R6f2TpVJETfW-wrmh4ftj0xZgeyU-Y3w8_jgURuH0V8lvrYp1Q_kjG6wYtghfGE0yfYEfuPU2fCcyp0vfxRfwRHdR_hJnChsemi4vzGtB8g5tpyxlns0-IXTbpYUQ",
                "Content-Type: application/json;charset=UTF-8",
                "Cookie: laravel_session=L2TX2vypI4lRwIV4u6rYFxvolpGKsEMUoMrzScI9"
            ),
        ));

        $response = curl_exec($curl);
        if($response=== false) {
            return false;
        }
        else{
            $plate_1 = (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["character"]; //.array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['character'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['part_a'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['part_b'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['part_a'].array(json_decode($response,JSON_PRETTY_PRINT))[0]['driver']['plate']['iran_id'];
            $plate_2 = (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["part_a"];
            $plate_3 = (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["part_b"];
            $plate_4 = 'ایران' . (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['plate'])["iran_id"];
            $plate = $plate_4 . '-' . $plate_3 . '-' . $plate_1 . '-' . $plate_2;
            $origin = array(json_decode($response, JSON_PRETTY_PRINT))[0]['origin']['details'];
            $destination = array(json_decode($response, JSON_PRETTY_PRINT))[0]['destination']['details'];
            $returnHTML = view('panel.taxi.resultRequest', ['origin' => $origin, 'destination' => $destination, 'driver_name' => array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['driver_name'], 'cellphone' => (array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['cellphone']), 'vehicle_model' => array(json_decode($response, JSON_PRETTY_PRINT))[0]['driver']['vehicle_model'], 'plate' => $plate])->render();
            return $returnHTML;
        }

    }
}
