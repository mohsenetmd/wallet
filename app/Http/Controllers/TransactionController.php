<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionRequest;
use App\Method;
use App\PermissionUser;
use App\Transaction;
use App\User;
use App\UserPanel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class TransactionController extends Controller
{

    public function TransactionForMarketer(Request $request)
    {
        if ($request->ajax()) {
            /*$TransactionMarketer = Transaction::select('*')
                -> leftjoin('categories', function($join) {
                    $join->on('categories.category_id', '=', 'transactions.category_id')
                         ->where('transactions.type_of_transaction=1 and transactions.user_id', '=' , request()->user_id);
                })
                ->sum('amount');*/
            $TransactionMarketer = DB::table("transactions")
                ->select(DB::raw("SUM(amount) as amount"))
                -> leftjoin('categories', function($join) {
                    $join->on('categories.category_id', '=', 'transactions.category_id')
                        ->where('transactions.type_of_transaction=1 and transactions.user_id', '=' , request()->user_id);
                })
                ->groupBy(DB::raw("transactions.category_id"))
                ->get();
            $returnHTML = view('transaction.listMarketer', ['allTransaction' => $TransactionMarketer, 'user_id' => request()->user_id])->render();
            return response()->json($returnHTML);
        } else {
            $returnHTML = view('transaction.listMarketer')->render();
            return response()->json($returnHTML);
        }
    }

    public function list(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::select('SELECT  transactions.category_id,categories.name , sum(case transactions.type_of_transaction when 0 then amount else 0 end) as reduced, sum(case transactions.type_of_transaction when 1 then amount else 0 end) as added
                                       FROM transactions
                                       left join categories on categories.category_id=transactions.category_id where transactions.status=1 and   transactions.user_id=' . request()->user_id . ' group by  transactions.category_id,categories.name ');
            $returnHTML = view('transaction.list', ['allTransaction' => $data, 'user_id' => request()->user_id])->render();
            return response()->json($returnHTML);
        } else {
            $returnHTML = view('transaction.list')->render();
            return response()->json($returnHTML);
        }
    }

    public function detailList(Request $request)
    {
        if ($request->ajax()) {
            $data =  DB::select('SELECT   payer,updated_at,transactions.transaction_id  ,  (case transactions.type_of_transaction when 0 then amount else 0 end) as reduced,  (case transactions.type_of_transaction when 1 then amount else 0 end) as added
                                       FROM transactions
                                       where transactions.status=1 and  transactions.category_id='.request()->category_id.' and transactions.user_id=  '.request()->user_id);

            $returnHTML = view('transaction.detailList',['allTransaction'=>$data])->render();

            return response()->json($returnHTML);
        } else {
            $returnHTML = view('transaction.detailList')->render();
            return response()->json($returnHTML);
        }
    }



}
