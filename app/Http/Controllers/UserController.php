<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Marketer;
use App\Models\User;
use App\Models\UserPanel;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /*
    | Define Fixed Variable
    */
    public $statusPreVerify='0';
    public $statusAcceptVerify='1';
    public $minTokenRand='1234';
    public $maxTokenRand='9876';
    public $tokenTime=120;

    public function login()
    {
        return view('user.login');
    }


    /*
    |--------------------------------------------------------------------------
    | Create Or Update User With Random Token
    |--------------------------------------------------------------------------
    */
    public function createToken()
    {
        if (!$this->checkUserExist()) {
            $user_id = $this->createUser();
        }else{
            $user_id = $this->updateUser();
        }
        //$this->sendToken($user_id);
        return view('user.verify', ['user_id' => $user_id,'msg'=>'']);
    }

    /*
    |--------------------------------------------------------------------------
    | Check Token
    | If True Update Status User And Go To Marketer Insert
    | If False Update User With New Random Token ANd Refresh Page
    |--------------------------------------------------------------------------
    */
    public function checkToken(Request $request)
    {
        $userId=$request->user_id;
        $user = User::find($userId);
        if ($this->checkTime($userId)) {
            if ($user->token_code == $request->token_code) {
                $user->update([
                    'status' => $this->statusAcceptVerify,
                ]);
                Auth::loginUsingId($userId);
                return view('user.marketerInsert',['user_id'=>$userId]);
            }
        }
        return redirect(route('user.createToken', ['country_code'=>$user->country_code,'mobile' => $user->mobile]));
    }

    /*
    |--------------------------------------------------------------------------
    | Store Marketer
    | If User->Marketer Dosnt Exist Create New User
    | If Marketer Dosnt Exist Create New Marketer
    | Update User MarketerId
    |--------------------------------------------------------------------------
    */
    public function marketerStore(Request $request){
        $userId=$this->checkUserExist();
        if (!$userId) {
            $userId = $this->createUser();
        }
        $marketerId=$this->checkMarketerExist($userId);
        if(!$marketerId){
            $marketerId = $this->createMarketer($userId);
        }
        User::find($userId)->update([
            'marketer_id'=> $marketerId
        ]);
    }



    /*
    |--------------------------------------------------------------------------
    | Other Function
    |--------------------------------------------------------------------------
    */

    //Check Time For Token
    public function checkTime($user_id)
    {
        $user = User::find($user_id);
        $from = Carbon::now()->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $user->updated_at);
        $diff_in_seconds = $to->diffInSeconds($from);
        if ($diff_in_seconds < $this->tokenTime) {
            return $diff_in_seconds;
        }
        return false;
    }
    public function checkUserExist()
    {
        $user = $this->getUserWithMobile();
        if (empty($user)) {
            return false;
        }
        return $user->user_id;
    }
    public function checkMarketerExist($userId)
    {
        $marketer=Marketer::where('user_id',$userId)->first();
        if (empty($marketer)) {
            return false;
        }
        return $marketer->marketer_id;
    }
    public function createUser()
    {
        $token = rand($this->minTokenRand, $this->maxTokenRand);
        $user = User::create([
            'marketer_id' => 1,
            'country_code' => \request()->country_code,
            'mobile' => \request()->mobile,
            'token_code' => $token,
            'status' => $this->statusPreVerify,
            'password' => Hash::make($token)
        ]);
        return $user->user_id;
    }
    public function updateUser()
    {
        $token = rand($this->minTokenRand, $this->maxTokenRand);
        $user = $this->getUserWithMobile();
        User::find($user->user_id)->update([
            'token_code' => $token,
        ]);
        return $user->user_id;
    }
    public function editUser()
    {
        $user = User::find(request()->user_id);
        User::find($user->user_id)->update([
            'mobile' => request()->mobile,
            'country_code'=> request()->country_code
        ]);
        $returnHTML = view('user.success')->render();
        return response()->json($returnHTML);
    }
    public function delete(Request $request)
    {
        User::find($request->user_id)->delete();
        return back();
    }
    public function sendToken($user_id)
    {
        $user = User::find($user_id);
        SendSms($user->token_code,'9158438332');
    }
    public function getUserWithMobile()
    {
        return User::where(['mobile' => \request()->mobile, 'country_code' => \request()->country_code])->first();
    }
    public function createMarketer($userId)
    {
        $marketer = Marketer::create([
            'user_id' => $userId
        ]);
        return $marketer->Id;
    }
    public function update(Request $request)
    {
        $user = User::find($request->user_id);
        $returnHTML = view('user.update',['user_id'=>$request->user_id,'mobile'=>$user->mobile,'country_code'=>$user->country_code])->render();
        return response()->json($returnHTML);
    }
    public function listMarketer (Request $request)
    {
        if ($request->ajax()) {
            /* $data = \Illuminate\Support\Facades\/*DB::table('users as user')*/
            /*  -> join('users as marketer', function($join) {
                  $join->on('user.marketer_id', '=', 'marketer.user_id')
                      ->where('user.marketer_id','5');
              })*/
            /* -> join('transactions  ', function($join) {
                 $join->on('transactions.user_id', '=', 'user.user_id')
                     ->where('transactions.type_of_transaction','1');
             })
             ->get();*/
            // $data = User::latest()->get();*/



            $data =  DB::select('SELECT marketer.user_id user_id   ,marketer.mobile,marketer.created_at created_at ,sum(amount) Profit FROM `users` user join users marketer on user.marketer_id=marketer.user_id
                                           join transactions on transactions.type_of_transaction=1 and  transactions.user_id=marketer.user_id
                                           WHERE 1
                                           group by marketer.user_id,marketer.mobile,marketer.created_at,marketer.user_id  ');

            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    $btn = $this->actionButtonRow($row);
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return  view('user.listMarketer');
    }
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addColumn('country_img', function ($row) {
                    $btn = '<img src="../../assets/images/country/country-'.$row->country_code.'.jpg">'.$row->country_code.'</a>';
                    return $btn;
                })
                ->addColumn('link_edit', function ($row) {
                    $btn = $this->actionButtonRow($row);
                    return $btn;
                })
                ->rawColumns(['country_img','link_edit'])
                ->make(true);
        }
        return  view('user.list');
    }
    public function store(UserRequest $request)
    {
        UserPanel::create([
            'mobile' => $request->mobile,
            'countrry_code' =>  $request->country_code,
            'password' => Hash::make($request->password)
        ]);
        return  view('user.list');
    }
    public function actionButtonRow($row): string
    {
        $csrf = csrf_token();
        $link = ' <form style="display:contents" method="post" action="">
                    <input type="hidden" name="_token" value="' . $csrf . '">
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" id="e_user_id" value="' . $row->user_id . '">
                    <a class="btn btn-primary" onclick="showTransaction('. $row->user_id.')"   >مشاهده تراکنش ها</a>
                    </form>

                    <form style="display:contents" method="post" action="">
                    <input type="hidden" name="_token" value="' . $csrf . '">
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" id="e_user_id" value="' . $row->user_id . '">
                    <a class="btn btn-edit" onclick="editrow('. $row->user_id.')"   >ویرایش</a>
                    </form>
                     <form style="display:contents" method="post" action="' . route("user.delete") . '">
                    <input type="hidden" name="_token" value="' . $csrf . '">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="user_id" value="' . $row->user_id . '">
                    <button class="btn btn-danger" type="submit">حذف</button>
                    </form>

                    ';
        return $link;
    }
}
