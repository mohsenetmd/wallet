<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\AirFareCityResource;
use App\Models\AirFareCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class AirFareController extends Controller
{
    public function listCity()
    {
        $listAirFareCity = AirFareCityResource::collection(AirFareCity::all());
        return response()->json(["data" => [
            'airfare_city' => $listAirFareCity
        ]]);
    }

}
