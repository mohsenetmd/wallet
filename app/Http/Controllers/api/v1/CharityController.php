<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CharityResource;
use App\Http\Resources\ListDonateResource;
use App\Models\Charity;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CharityController extends Controller
{
    public function list()
    {
        $charity = CharityResource::collection(Charity::all());
        return response()->json(["data" => [
            "charity" => $charity
        ]]);
    }

    public function donate(Request $request)
    {
        $checkCharityExist = Charity::find($request->id);
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return response()->error($validator->errors());
        }
        $user = $request->user();
        if ($checkCharityExist) {
            Transaction::create([
                'user_id' => $user->user_id,
                'amount' => $request->amount,
                'type_of_transaction' => 0,
                'category_id' => 6,
                'status' => 1,
                'payer' => $request->id
            ]);
            return response()->json([
                'data' => ['msg' => 'پرداخت با موفقیت انجام شد'
                ]
            ]);
        }
        return response()->error('Charity Not Exist');
    }

    public function getValidator(Request $request)
    {
        return Validator::make($request->all(), [
            'id' => 'required',
            'amount' => 'required|min:3|max:8'
        ], [
            'amount.min' => 'مبلغ وارد شده کمتر از حد مجاز است',
            'amount.max' => 'مبلغ وارد شده بیشتر از حد مجاز است'
        ]);
    }

    public function getListDonate(Request $request)
    {
        $user = $request->user();
        $transaction = DB::select("SELECT transactions.amount, charities.name ,  charities.icon FROM transactions INNER JOIN charities ON transactions.payer = charities.id where transactions.user_id = $user->user_id AND category_id = 6; ");
        if ($transaction) {
            $listDonate = ListDonateResource::collection($transaction);
            return response()->json(["data" => [
                "list_donate" => $listDonate
            ]]);
        } else {
            return response()->json(["data" => [
            ]]);
        }
    }
}
