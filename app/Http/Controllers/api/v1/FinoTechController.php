<?php

namespace App\Http\Controllers\api\v1;


use App\Models\CardInformation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;

class FinoTechController extends Controller
{
    use Notifiable;
    public $address = "https://apibeta.finnotech.ir/";
    public $url = "https://apibeta.finnotech.ir/dev/v2/oauth2/token";
    public $debug = false;
    public $clientId = "newcash1";
    public $scopes = "";
    public $nid = "0682364223";


    public function cardInformation(Request $request)
    {
        $url = 'https://apibeta.finnotech.ir/mpg/v2/clients/' . $this->clientId . '/cards/' . $request->card;
        $response = $this->curlResponseCardInfo($url, $request->tokenMiddleware);
        $responseObject = json_decode($response);
        if ($responseObject->status == 'DONE' && $responseObject->result->name) {
            CardInformation::create([
                'user_id' => $request->user()->user_id,
                'track_id' => $responseObject->trackId,
                'card_number' => $request->card,
                'name' => $responseObject->result->name,
                'do_time' => $responseObject->result->doTime
            ]);
            return response()->json([
                "data"=>['name'=>$responseObject->result->name]
            ]);
        }
        return response()->error('استعلام نا موفق بود');
    }

    public function curlResponseCardInfo($sendUrl, $token)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $sendUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Bearer " . $token
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}
