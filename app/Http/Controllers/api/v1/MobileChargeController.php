<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\MobileCharge;
use App\Models\Transaction;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use stdClass;

class MobileChargeController extends Controller
{
    public $url = 'https://inax.ir/webservice.php';
    public $userName = '60f7d278567dd94df07c1021aae20b6e';
    public $password = '123456';
    public $categoryProduct = 1;

    /**
     * Get Credit My Account
     */
    public function showCredit()
    {
        $array = ['method' => 'credit', 'username' => $this->userName, 'password' => $this->password];
        return $this->getCurlResponse($array);
    }

    /**
     * Check Credit My Account For Buy Charge And Package
     * @param Request $request
     * @return stdClass
     */
    public function checkCredit(Request $request)
    {
        $showCredit = $this->showCredit();
        $showCreditObject = json_decode($showCredit);
        $checkCredit = new stdClass();
        $checkCredit->status = true;
        if ($showCreditObject->code != 1) {
            $checkCredit->status = false;
            $checkCredit->msg = $showCreditObject->msg;
            return $checkCredit;
        }
        if ($showCreditObject->credit < $request->amount) {
            $checkCredit->status = false;
            $checkCredit->msg = 'سیستم به این میزان شارژ ندارد';
            return $checkCredit;
        }
        return $checkCredit;
    }

    /**
     * Buy Credit
     * @param Request $request
     * @return Object
     */
    public function buyCredit(Request $request)
    {
        $checkCredit = $this->checkCredit($request);
        if (!$checkCredit->status) {
            return response()->error($checkCredit->msg);
        }
        $validator = $this->getValidateDirectCharge($request);
        if ($validator->fails()) {
            return response()->error('اطلاعات ورودی اشتباه است');
        }
        $user = $request->user();
        $orderId = $this->getOrderIdForBuyCredit($user, $request);
        $request->amountWP = $request->amount / $this->getPercentProfit($this->categoryProduct);
        $array = ['method' => 'topup',
            'username' => $this->userName,
            'password' => $this->password,
            'operator' => $request->operator,
            'amount' => $request->amountWP,
            'mobile' => '0' . $request->mobile,
            'charge_type' => 'normal',
            'order_id' => $orderId,
            'company' => 'صدرا سیستم'
        ];
        $response = $this->getCurlResponse($array);
        $checkResponse = $this->checkResponse($response, $orderId);
        if (!$checkResponse->status) {
            return response()->error($checkResponse->msg);
        };
        return $this->acceptMobileChargeTransaction($orderId, $request);
    }

    public function getProducts()
    {
        $array = ['method' => 'get_products', 'username' => '60f7d278567dd94df07c1021aae20b6e', 'password' => '123456'];
        $response = $this->getCurlResponse($array);
        $responseObject = json_decode($response);
        $selectedMonthlyPackage = array();
        $listOfOperators = ['MTN', 'MCI', 'RTL'];
        foreach ($listOfOperators as $operator) {
            $monthlyPackages[$operator] = $responseObject->products->internet->$operator->credit->monthly;
            $numberSelectedPackage = 0;
            foreach ($monthlyPackages[$operator] as $monthlyPackage) {
                if (20000 < $monthlyPackage->amount && $monthlyPackage->amount < 50000) {
                    if (!strpos($monthlyPackage->title, '60') && !strpos($monthlyPackage->title, '90')) {
                        $selectedMonthlyPackage[$operator][$numberSelectedPackage] = $monthlyPackage;
                        $selectedMonthlyPackage[$operator][$numberSelectedPackage]->amount = round($monthlyPackage->amount * $this->getPercentProfit($this->categoryProduct), -3, PHP_ROUND_HALF_DOWN);
                        $numberSelectedPackage += 1;
                    }
                }
            }
        }
        return response()->json([
            'data' => [
                'products' => $selectedMonthlyPackage]]);
    }

    public function getValidateDirectCharge(Request $request)
    {
        return Validator::make($request->all(), [
            'operator' => 'required',
            'amount' => 'required|min:3|max:5',
            'mobile' => 'required|size:10',
        ]);
    }

    public function getValidateBuyProducts(Request $request)
    {
        return Validator::make($request->all(), [
            'product_id' => 'required',
            'mobile' => 'required|size:10',
        ]);
    }

    public function getOrderId($user, $product, Request $request)
    {
        $order = MobileCharge::create([
            'user_id' => $user->user_id,
            'operator' => $product->operator,
            'product_id' => $product->id,
            'product_name' => $product->name,
            'product_title' => $product->title,
            'amount' => $product->amount,
            'mobile' => $request->mobile
        ]);
        return $order->order_id;
    }

    public function getOrderIdForBuyCredit($user, Request $request)
    {
        $order = MobileCharge::create([
            'user_id' => $user->user_id,
            'operator' => $request->operator,
            'amount' => $request->amount,
            'mobile' => $request->mobile
        ]);
        return $order->order_id;
    }

    public function buyProduct(Request $request)
    {
        $product = $this->getSelectedProduct($request);
        $user = $request->user();
        $amount = TransactionController::amount($request);
        if ($product->amount > $amount) {
            return response()->error('Money Not Enough');
        }
        $validator = $this->getValidateBuyProducts($request);
        if ($validator->fails()) {
            return response()->error('اطلاعات ورودی اشتباه است');
        }
        $orderId = $this->getOrderId($user, $product, $request);
        $array = ['method' => 'buy_internet', 'username' => $this->userName, 'password' => $this->password,
            'operator' => $product->operator, 'mobile' => '0' . $request->mobile, 'product_id' => $request->product_id,
            'internet_type' => 'monthly', 'sim_type' => 'credit', 'order_id' => "$orderId"];
        $response = $this->getCurlResponse($array);
        $checkResponse = $this->checkResponse($response, $orderId);
        if ($checkResponse->status) {
            return response()->error($checkResponse->msg);
        };
        return $this->acceptMobileBuyProduct($orderId, $product, $request);
    }

    public function acceptMobileChargeTransaction($orderId, Request $request)
    {
        $user = $request->user();
        MobileCharge::find($orderId)->update([
            'status' => 1
        ]);
        $transaction = Transaction::create([
            'user_id' => $user->user_id,
            'category_id' => 1,
            'amount' => $request->amount,
            'amount_wp' => $request->amountWP,
            'type_of_transaction' => 0,
            'status' => 1,
        ]);
        $request->transaction = $transaction->transaction_id;
        return response()->json([
            'data' => ['msg' => 'خرید با موفقیت انجام شد']
        ]);
    }

    public function acceptMobileBuyProduct($orderId, $product, Request $request)
    {
        $user = $request->user();
        MobileCharge::find($orderId)->update([
            'status' => 1
        ]);
        $transaction = Transaction::create([
            'user_id' => $user->user_id,
            'category_id' => $this->categoryProduct,
            'amount' => $product->amount,
            'amount_wp' => $product->amountWP,
            'type_of_transaction' => 0,
            'status' => 1,
        ]);
        $request->transaction = $transaction->transaction_id;
        return response()->json([
            'data' => ['msg' => 'خرید با موفقیت انجام شد']
        ]);
    }

    public function test(Request $request)
    {
        $user = $request->user();
        Transaction::create([
            'user_id' => $user->user_id,
            'category_id' => 1,
            'amount' => $request->amount,
            'type_of_transaction' => 0,
            'status' => 1,
        ]);
    }

    public function checkResponse($response, $orderId)
    {
        $showResponseObject = json_decode($response);
        $checkResponse = new stdClass();
        $checkResponse->status = true;
        if ($showResponseObject->code != 1) {
            MobileCharge::find($orderId)->update([
                'status' => 2
            ]);
            $checkResponse->status = false;
            $checkResponse->msg = $showResponseObject->msg;
            return $checkResponse;
        }
        return $checkResponse;
    }

    public function getCurlResponse(array $array)
    {
        $jsonArray = json_encode($array);
        return CurlResponse($jsonArray, $this->url);
    }

    /**
     * @param int $category_id
     * @return mixed
     */
    public function getPercentProfit($category_id = 1)
    {
        $category =Category::find($category_id);
        return (1 + $category->percent / 100);
    }

    public function getSelectedProduct(Request $request)
    {
        $array = ['method' => 'get_products', 'username' => '60f7d278567dd94df07c1021aae20b6e', 'password' => '123456'];
        $response = $this->getCurlResponse($array);
        $responseObject = json_decode($response);
        $operators = ["MTN", "MCI", "RTL"];
        foreach ($operators as $operator) {
            $products = $responseObject->products->internet->$operator->credit->monthly;
            foreach ($products as $product) {
                if ($product->id == $request->product_id) {
                    $product->amountWP = $product->amount;
                    $product->amount = round($product->amount * $this->getPercentProfit($this->categoryProduct), -3, PHP_ROUND_HALF_DOWN);
                    return $product;
                }
            }
        }
        return false;
    }

}
