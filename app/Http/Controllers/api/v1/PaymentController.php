<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Transaction;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public $statusCode = 200;
    public $apiKey = 'b44fbd8273bd9988900a64c7b969593b5d004046';
    public $callBackUrl = 'http://new-cash.ir/api/verify/';
    public $sendUrl = 'https://vandar.io/api/ipg/send';
    public $verifyUrl = 'https://ipg.vandar.io/api/v3/verify';

    public function getLink(Request $request, $transaction_id)
    {
        $response = $this->postDataToGetaway($request);
        $responseArray = json_decode($response);
        if ($responseArray->status == 1) {
            Payment::create([
                'transaction_id' => $transaction_id,
                'token' => $responseArray->token,
                'amount' => $request->amount,
                'status' => config('global.status_pre')
            ]);
            return $responseArray->token;
        }
        return $responseArray;
    }


    public function verify(Request $request)
    {
        $response = $this->verifyPaymentGetaway($request);
        $payment = Payment::whereToken($request->token)->first();
        if (!$payment) {
            return response()->json([
                'data' => ['message' => 'تراکنشی با این شماره پیگیری موجود نمی باشد']
            ], $this->statusCode);
        }
        if ($response['status'] == 1) {
            Payment::whereToken($request->token)->update([
                'status' => $response->status,
                'amount' => $response->amount,
                'real_amount' => $response->realAmount,
                'wage' => $response->wage,
                'card_number' => $response->cardNumber,
                'payment_date' => $response->paymentDate,
                'cid' => $response->cid,
                'message' => $response->message
            ]);
            Transaction::find($payment->transaction_id)->update(['status' => 1]);
            return response()->json([
                'data' => ['msg' => 'پرداخت مورد تایید قرار گرفت']
            ]);
        }
        $statusTransaction = Transaction::find($payment->transaction_id);
        if ($statusTransaction->status == 0) {
            Transaction::find($payment->transaction_id)->update(['status' => config('global.status_cancel')]);
            return response()->json(['data' => ['msg' => 'پرداخت نا موفق بود']], $this->statusCode);
            $payment->update(['status' => config('global.status_cancel')]);
        }
        return response()->json([
            'data' => ['msg' => 'این تراکنش قبلا به صورت موفقیت آمیز پرداخت شده است']]);
    }

    public function postDataToGetaway(Request $request)
    {
        $array = ['api_key' => $this->apiKey, 'amount' => $request->amount,
            'callback_url' => $this->callBackUrl];
        $jsonArray = json_encode($array);
        return CurlResponse($jsonArray,$this->sendUrl);
    }

    public function verifyPaymentGetaway(Request $request)
    {
        $array = ['api_key' => $this->apiKey, 'token' => $request->token];
        $jsonArray = json_encode($array);
        return CurlResponse($jsonArray,$this->verifyUrl);
    }

}
