<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Panel\TaxiController as panelTaxiController;
use App\Models\Taxi;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class TaxiController extends Controller
{
    public function location(Request $request)
    {
       // $request->user();

        $user = User::where('user_id' ,$request->user()->user_id)->first();

/*dd
        if (!$user)
        {
            return response()->error('کاربری با مشخصات وارد شده موجود نمی باشد');
        }*/
        $user = User::select('users.country_code','users.mobile','user_profiles.full_name')
            ->where('users.user_id' ,$user->user_id)
            ->Join('user_profiles', function($join ) {
                $join->on('users.user_id', '=', 'user_profiles.user_id') ;
            })->get();

        $taxi=Taxi::create([
                'ride_id'=>1,
                'mobile'=> $user[0]['mobile'],
                'contact_mobile'=>$user[0]['mobile'],
                'destination_lat'=> 0,
                'destination_lng'=> 0,
               /* 'extra_destination_lat'=> 0,
                'extra_destination_lng'=>0,*/
                'origin_lat'=>$request->origin_lat ,
                'origin_lng'=>$request->origin_lng ,
                'contact_name'=>$user[0]['full_name'],
                'round_trip'=> "false",
                'service_type'=> 0,
                'waiting'=> 0,
                'status'=>0,
            ]
        );
        if($taxi) {
            return response()->json([
                'data' => $request->origin_lat
            ], 200);
        }
        else{
            return response()->error('error in request');
        }
    }
    public function calculatePrice(Request $request)
    {
        $pnlTaxi=new panelTaxiController;
        $result=$pnlTaxi->calculatePrice($request);
       if($result) {
           return response()->json([
               'data' => ['price'=>$result]
           ], 200);
       }
       else{
           return response()->error('Not calculated Price');
       }
    }
    public function requestTaxi(Request $request)
    {
        $pnlTaxi=new panelTaxiController;
        $result=$pnlTaxi->requestService($request);
        if($result) {
            $resultR=$pnlTaxi->requestRefresh($result);
        }
        if($resultR) {
            return response()->json([
                'data' => ['result_request'=>$resultR]
            ], 200);
        }
        else{
            return response()->error('Not Service');
        }
    }
}
