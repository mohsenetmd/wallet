<?php

namespace App\Http\Controllers\api\v1;
use App\Http\Controllers\Controller;
use App\Models\selectedPlace;
use Illuminate\Http\Request;


class TourismController extends Controller
{
    public function selectedPlace(Request $request)
    {
        $cityId=$request->city_id;

        $selectedPlace = selectedPlace::select('pl_selected_place.name','pl_selected_place.city_id','pl_selected_place.icon','pl_selected_place.address','pl_selected_place.id')
            ->Join('pl_category_place', function($join ) {
                $join->on('pl_category_place.id', '=', 'pl_selected_place.category_id')

                     ->where('pl_category_place.typecategory_id', '=' ,'0');

            })->where('pl_selected_place.city_id','=', $cityId)
           ->join('pl_type_category', function($join_2) {
                $join_2->on('pl_category_place.parent_id', '=' ,'pl_type_category.id')
                    ->where('pl_type_category.id', '=' ,'1');
            })->get();

      /*  ->where('pl_selected_place.city_id','=',$request->city_id);*/
        return response()->json([
            'data' => [
                'selected_place'=>$selectedPlace,
            ],
        ]);


    }
}
