<?php

namespace App\Http\Controllers\Api\v1;

use App\Category;
use App\Http\Resources\TransactionCollection;
use App\Http\Resources\TransactionResource;
use App\Marketer;
use App\Transaction;
use App\Http\Controllers\Controller;
use App\User;
use App\wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Collection;

class TransactionController extends Controller
{
    /*
     | Define Fixed Variable
     */
    public $typeOfTransactionMinus = '0';
    public $typeOfTransactionPlus = '1';
    public $statusPre = '0';
    public $statusAccept = '1';
    public $statusCancel = '2';
    public $categoryBuy = '1';
    public $categoryOnline = '2';
    public $categoryTransferSend = '3';
    public $categoryTransferGive = '4';
    public $categoryMarketer = '5';
    public $percentMarketer = 0.01;
    public $statusCode = 200;
    public $lengthPage = 20;


    /*
    |--------------------------------------------------------------------------
    | Store Transaction
    |--------------------------------------------------------------------------
    */
    public function store(Request $request)
    {
        //Validation Request
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return response()->json($this->Message($validator->errors()), $this->statusCode);
        }
        $data = $this->getDataForCreateTransaction($request);
        //Check Input Data For UserExist And Amount
        switch ($data) {
            case "NotFoundUser":
                return response()->json($this->Message('Not Found Any User'), $this->statusCode);
                break;
            case 'NotEnoughMoney':
                return response()->json($this->Message('Not Enough Money'), $this->statusCode);
                break;
        }
        //Create Transaction And Return Response For ANy Category
        $transaction = Transaction::create($data);
        switch ($transaction->category_id) {
            case $this->categoryBuy:
                return response()->json($this->Message('تراکنش با موفقیت ثبت شد'));
                break;
            case $this->categoryOnline:
                return $this->transactionOnline($request, $transaction);
                break;
            case $this->categoryTransferSend:
                return $this->transactionTransfer($transaction, $data);
                break;
        }
    }

    public function getValidator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'amount' => 'required|min:3|max:8',
        ], [
            'category_id.required' => 'انتخاب دسته بندی الزامی است',
            'amount.required' => 'وارد کردن مبلغ الزامی است.',
            'amount.min' => 'مقدار وارد شده کمتر از حد مجاز است',
            'amount.max' => 'مقدار وارد شده بیشتر از حد مجاز است',
        ]);
        return $validator;
    }

    public function getDataForCreateTransaction(Request $request)
    {
        $data = $request->all();
        $user = $request->user();
        if ($request->mobile) {
            $getUser = User::where('mobile', $request->mobile)->first();
            //Check User Exist For Transfer And Online
            if (!$getUser) {
                return "NotFoundUser";
            }
            //Create Data For Online
            if ($request->category_id == $this->categoryOnline) {
                $payer = $request->user();
                $data['payer'] = $payer->user_id;
                $user = $getUser;
            }
            //Create Data For Transfer
            if ($request->category_id == $this->categoryTransferSend) {
                $data['payer'] = $getUser->user_id;
            }
            unset($data['mobile']);
        }
        $category = Category::find($data['category_id']);
        $data['type_of_transaction'] = $category->type_of_transaction;
        //Check Money For Buy And Transfer
        if ($category->type_of_transaction == $this->typeOfTransactionMinus) {
            $amount = wallet::where('user_id', $user->user_id)->sum('amount_enter','-','amount_exit');
            if ($data['amount'] > $amount) {
                return 'NotEnoughMoney';
            }
        }
        $data['user_id'] = $user->user_id;
        unset($data['api_token']);
        return $data;
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $transaction = Transaction::where(['transaction_id' => $request->transaction_id, 'user_id' => $user->user_id])->first();
        $flag = isset($transaction);
        if ($flag == 1) {
            $checkStatus = $transaction->status;
            if ($checkStatus != 0) {
                return response()->json($this->Message('این تراکنش قبلا ثبت شده است'), $this->statusCode);
            }
        }
        if ($flag != 1) {
            return response()->json($this->Message('اطلاعات وارد شده صحیح نمی باشد'), $this->statusCode);
        }
        $transactionId=$this->createTransactionForMarketer($request, $transaction);
        if($transactionId) {
            $transaction->update([
                'status' => $request->status,
                'parent_id' => $transactionId
            ]);
        }
        $this->updateWallet($user->user_id, $transaction);
        return response()->json($this->Message('تراکنش با موفقیت ثبت شد'));
    }

    public function createTransactionForMarketer(Request $request, $transaction)
    {
        $userMarketer = User::find($request->user()->marketer_id);
        if ($request->status == $this->statusAccept && $userMarketer) {
            $categoryId = $transaction->category_id;
            if ($categoryId == $this->categoryBuy) {
                $percent=Category::find($categoryId);
                $transaction=Transaction::create([
                    'user_id' => $userMarketer->user_id,
                    'category_id' => $this->categoryMarketer,
                    'amount' => ($transaction->amount * $percent->percent) / 100,
                    'type_of_transaction' => $this->typeOfTransactionPlus,
                    'status' => $this->statusAccept,
                ]);
                $this->updateWallet($userMarketer->user_id,$transaction);
                return $transaction->transaction_id;
            }
        }
        return false;
    }



    public function updateWallet($userId, $transaction): void
    {
        $wallet = wallet::where('user_id', $userId)->first();
        if ($transaction->type_of_transaction == $this->typeOfTransactionPlus) {
            $wallet->update([
                'amount_enter' => $wallet->amount_enter + $transaction->amount,
                'last_transaction_id' => $transaction->transaction_id,
            ]);
        } else {
            $wallet->update([
                'amount_exit' => $wallet->amount_exit + $transaction->amount,
                'last_transaction_id' => $transaction->transaction_id,
            ]);
        }
    }


    public function createDataForTransferGive($data)
    {
        $payer = $data['user_id'];
        $data['user_id'] = $data['payer'];
        $data['payer'] = $payer;
        $data['category_id'] = $this->categoryTransferGive;
        $data['type_of_transaction'] = $this->typeOfTransactionPlus;
        $data['status'] = 1;
        return $data;
    }

    /**
     * @param Request $request
     * @param $transaction
     * @return \Illuminate\Http\JsonResponse
     */
    public function transactionOnline(Request $request, $transaction): \Illuminate\Http\JsonResponse
    {
        $payment = new PaymentController();
        $link = $payment->getLink($request, $transaction->transaction_id);
        if ($link == false) {
            $transaction->update([
                'status' => $this->statusCancel
            ]);
            return response()->json($this->Message('اتصال با درگاه با خطا مواجه شد.'), $this->statusCode);
        }
        return response()->json([
            'data' => ['msg' => 'تراکنش با موفقیت ثبت شد', 'link' => 'https://ipg.vandar.io/v3/' . $link]
        ]);
    }

    /**
     * @param $transaction
     * @param $data
     */
    public function transactionTransfer($transaction, $data)
    {
        $this->updateWallet($transaction->user_id, $transaction);
        $transaction->update([
            'status' => $this->statusAccept
        ]);
        $dataGive = $this->createDataForTransferGive($data);
        $transactionGive = Transaction::create($dataGive);
        $this->updateWallet($transactionGive->user_id, $transactionGive);
        return response()->json($this->Message('انتقال با موفقیت انجام شد'));
    }

    public function Message($msg): array
    {
        return ['data' => ['msg' => $msg]];
    }

    public function show(Request $request)
    {
        $from = $request->from ? $request->from : now()->format('U') - (30 * 24 * 3600);
        $to = $request->to ? $request->to : 9999999999999;
        $transaction = Transaction::where('updated_at', '>', $from)->where('updated_at', '<', $to)->where('status',1)->orderBy('updated_at', 'desc')->take($this->lengthPage)->get();
        $output=TransactionResource::collection($transaction);
        return response()->json([
            'data'=>["transaction"=>$output]
        ]);
    }
}
