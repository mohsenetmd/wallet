<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Category;
use App\Http\Resources\TransactionResource;
use App\Models\Transaction;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * Class TransactionController
 * @package App\Http\Controllers\api\v1
 */
class TransactionController extends Controller
{
    /*
     | Define Fixed Variable
     */
    public $statusCode = 200;
    public $lengthPage = 20;

    /*
    |--------------------------------------------------------------------------
    | Store Transaction
    |--------------------------------------------------------------------------
    */
    public function store(Request $request)
    {

        /**
         * Validation Request
         */
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return response()->json($this->Message($validator->errors()), $this->statusCode);
        }

        /**
         * Check Input Data For UserExist And Amount
         */
        $data = $this->getDataForCreateTransaction($request);
        switch ($data) {
            case "NotFoundUser":
                return response()->error('Not Found Any User');
                break;
            case 'MobileSameWithUser':
                return response()->error('Mobile Same User');
        }

        /**
         * Create Transaction And Return Response For Any Category
         */
        $transaction = Transaction::create($data);
        switch ($transaction->category_id) {
            case config('global.category_buy'):
                return response()->json($this->Message('تراکنش با موفقیت ثبت شد'));
                break;
            case config('global.category_online'):
                return $this->transactionOnline($request, $transaction);
                break;
            case config('global.category_transfer_send'):
                return $this->transactionTransfer($transaction, $data);
                break;
        }

        /**
         * NO ANY OF TOP
         */
        return false;
    }

    public function getValidator(Request $request)
    {
        return Validator::make($request->all(), [
            'category_id' => 'required',
            'amount' => 'required|min:3|max:8',
        ], [
            'category_id.required' => 'انتخاب دسته بندی الزامی است',
            'amount.required' => 'وارد کردن مبلغ الزامی است.',
            'amount.min' => 'مقدار وارد شده کمتر از حد مجاز است',
            'amount.max' => 'مقدار وارد شده بیشتر از حد مجاز است',
        ]);
    }

    public function getDataForCreateTransaction(Request $request)
    {
        $data = $request->all();
        $user = $request->user();
        if ($request->mobile) {
            $getUser = User::whereMobile($request->mobile)->first();
            //Check User Exist For Transfer And Online
            if (!$getUser) {
                return "NotFoundUser";
            }
            //Create Data For Online
            if ($request->category_id == config('global.category_online')) {
                $payer = $request->user();
                $data['payer'] = $payer->user_id;
                $user = $getUser;
            }
            //Create Data For Transfer
            if ($request->category_id == config('global.category_transfer_send')) {
                if ($user->mobile == $request->mobile) {
                    return "MobileSameWithUser";
                }
                $data['payer'] = $getUser->user_id;
            }
            unset($data['mobile']);
        }
        $category = Category::find($data['category_id']);
        $data['type_of_transaction'] = $category->type_of_transaction;
        unset($data['api_token']);
        return $data;
    }

    public function update(Request $request)
    {
        $user = $request->user();
        return $user->user_id;
        $transaction = Transaction::where(['transaction_id' => $request->transaction_id, 'user_id' => $user->user_id,
            'amount' => $request->amount , 'category_id' => $request->category_id])->first();
        if (!$transaction) {
            return response()->error('اطلاعات وارد شده صحیح نمی باشد');
        }
        $checkStatus = $transaction->status;
        if ($checkStatus != 0) {
            return response()->error('این تراکنش قبلا ثبت شده است');
        }
        $request->transaction=$transaction->transaction_id;
        $transaction->update([
            'status' => $request->status,
        ]);
        return response()->json($this->Message('تراکنش با موفقیت ثبت شد'),200);
    }



    public function createDataForTransferGive($data)
    {
        $payer = $data['user_id'];
        $data['user_id'] = $data['payer'];
        $data['payer'] = $payer;
        $data['category_id'] = config('global.category_transfer_give');
        $data['type_of_transaction'] = config('global.type_of_transaction_plus');
        $data['status'] = 1;
        return $data;
    }

    /**
     * @param Request $request
     * @param $transaction
     * @method array update(array $rules, ...$params)
     * @return object
     */
    public function transactionOnline(Request $request,Transaction $transaction)
    {
        $payment = new PaymentController();
        $link = $payment->getLink($request, $transaction->transaction_id);
        if ($link == false) {
            $transaction->update([
                'status' => config('global.status_cancel')
            ]);
            return response()->error('اتصال با درگاه با خطا مواجه شد.');
        }
        return response()->json([
            'data' => ['msg' => 'تراکنش با موفقیت ثبت شد', 'link' => 'https://ipg.vandar.io/v3/' . $link]
        ]);
    }


    public function transactionTransfer(Transaction $transaction, $data)
    {
        $transaction->update([
            'status' => config('global.status_accept')
        ]);
        $dataGive = $this->createDataForTransferGive($data);
        $transactionGive = Transaction::create($dataGive);
        $from = User::find($data['user_id']);
        $to = User::find($data['payer']);
        return response()->json(['data' => ['from' => $from->mobile,
            'to' => $to->mobile,
            'name' => '',
            'amount'=>$data['amount']
        ]
        ]);
    }

    public function Message($msg): array
    {
        return ['data' => ['msg' => $msg]];
    }

    public function show(Request $request)
    {
        $from = $request->from ? $request->from : now()->format('U') - (30 * 24 * 3600);
        $to = $request->to ? $request->to : 9999999999999;
        $transaction = Transaction::where('updated_at', '>', $from)->where('updated_at', '<', $to)->whereStatus(1)->orderBy('updated_at', 'desc')->take($this->lengthPage)->get();
        $output = TransactionResource::collection($transaction);
        return response()->json([
            'data' => ["transaction" => $output]
        ]);
    }

    public static function amount(Request $request)
    {
        $user = $request->user();
        $amount = DB::select("select round(sum(case
                                when transaction.type_of_transaction = 0 && transaction.status = 1 then -1*amount
                                when transaction.type_of_transaction = 1 && transaction.status = 1 then amount
                                else 0 end),0) as amount
                                from transactions as transaction
                                where transaction.user_id ='$user->user_id' ");
        return $amount[0]->amount;
    }

    public function getAmount(Request $request)
    {
        $amount = TransactionController::amount($request);
        return response()->json([
            'data' => ['wallet' => $amount]
        ]);
    }
}
