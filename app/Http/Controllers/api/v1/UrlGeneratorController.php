<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UrlRequest;
use App\UrlGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UrlGeneratorController extends Controller
{
    /*
    | Define Fixed Variable
    */
    public $category_id='2';
    public $type='1';
    public $lengthUrl = 5;
    public $invoiceStatus='0';
    public $statusPre = '0';

    public function create($transaction){
        $url=$this->uniqUrl();
        UrlGenerator::create([
           'user_id'=>$transaction->user_id,
           'transaction_id'=>$transaction->transaction_id,
           'amount'=>$transaction->amount,
           'status'=>$this->statusPre,
           'url'=>$url
        ]);
        return $url;
    }

    /*
    | Create Random Uniq Link
    */
    public function uniqUrl(){
        $url=Str::random($this->lengthUrl);
        $urlExist=UrlGenerator::where('url',$url)->first();
        if(empty($urlExist)){
            return $url;
        }
        $this->uniqUrl();
    }

    public function show($url){
        $urlGenrated=UrlGenerator::where('url',$url)->first();
        $payment=new PaymentController();
        $link=$payment->getLink($urlGenrated->amount);
        dd($link);
    }
}
