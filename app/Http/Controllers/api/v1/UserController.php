<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CharityListResource;
use App\Http\Resources\UserProfileResource;
use App\Models\categoryPlace;
use App\Models\Charity;
use App\Models\Marketer;
use App\Models\MobileDevice;
use App\Models\User;
use App\Models\UserProfile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{

    /*
     | Define Fixed Variable
     */
    public $sendSmsTOUser = 1;
    public $statusCode = 200;

    /*
    |--------------------------------------------------------------------------
    | Create Or Update User With Random Token
    |--------------------------------------------------------------------------
    */
    public function addMobile(Request $request)
    {
        //check user mobile
        if (!$this->getValidateMobileUser($request)) {
            return response()->error('Mobile Format Is Wrong');
        }
        $user = $this->checkUserExist($request->mobile);
        //check input mobile marketer or not
        if (!$request->filled('marketer_mobile')) {
            return $this->CreatOrUpdateUser($user, $request);
        }
        //check validation mobile marketer
        if ($this->getValidateMobileMarketer($request)) {
            return response()->error('Mobile Format Marketer Is Wrong');
        }
        //check marketer exist
        $userMarketer = $this->checkUserWithCDExist($request->marketer_mobile);
        if (!$userMarketer) {
            return response()->error('Marketer Is Not Found');
        }
        //
        if ($user) {
            if ($this->checkMarketerSet($user)) {
                $this->updateUser($user);
                return response()->staticCode();
            }
            $this->updateUser($user);
            $user->update([
                'marketer_id' => $userMarketer->user_id
            ]);
            return response()->staticCode();
        }
        $this->createUser($request->country_code, $request->mobile, $userMarketer->user_id);
        return response()->staticCode();
    }

    public
    function sync(Request $request)
    {
        $user = $request->user();
        $amount = $this->getAmount($user);
        $checkUserProfile = UserProfile::whereUserId($user->user_id)->first();
        $categoryPlace = categoryPlace::where('parent_id', '=', '0')->where('typecategory_id','=',1)->get(['id','name','icon']);

        if ($checkUserProfile) {
            $userProfile = new UserProfileResource($checkUserProfile);
        } else {
            $userProfile = ['full_name' => null,
                'country_name' => null,
                'email' => null,
                "gender" => null,
                "image" => null,
                "birthday" => null];
        }
        $charity = CharityListResource::collection(Charity::all());
        return response()->json([
            'data' => ['user' => [
                'user_id' => $user->url,
                'wallet' => $amount
            ],
                'user_profile' => $userProfile,
                'charity' => $charity,
                'category_place'=>$categoryPlace,
            ],
        ]);
    }

    public function checkUserExist($mobile)
    {
        $user = User::whereMobile($mobile)->first();
        if ($user) {
            return $user;
        }
        return false;

    }

    public
    function checkUserWithCDExist($mobile)
    {
        $user = User::whereMobile($mobile)->first();
        if ($user) {
            return $user;
        }
        return false;

    }

    public
    function checkMarketerExist($userMarketer)
    {
        $marketer = Marketer::whereUserId($userMarketer->user_id)->count();
        if ($marketer == 0) {
            return false;
        }
        return Marketer::whereUserId($userMarketer->user_id)->first();
    }

    public
    function checkMarketerSet($user)
    {
        if (!$user->marketer_id) {
            return false;
        }
        return true;
    }

    public
    function resendPin(Request $request)
    {
        $user = $this->checkUserExist($request->mobile);
        if (!$user) {
            return response()->error('User Not Exist');
        }
        if ($this->checkTime($user->user_id) > config('global.token_time')) {
            $token = rand(config('global.min_token_rand'), config('global.max_token_rand'));
            $this->sendSms($token, $user->mobile);
            $user->update([
                'token_code' => $token,
            ]);
            return response()->json([
                'data' => []
            ]);
        }
        return response()->error('Time Not Elapsed');
    }

    /*
    |--------------------------------------------------------------------------
    | Check Token
    | If True Update Status User And Go To Marketer Insert
    | If False Update User With New Random Token And Refresh Page
    |--------------------------------------------------------------------------
    */
    public
    function verifyMobile(Request $request)
    {
        $user = User::where(['country_code' => $request->country_code, 'mobile' => $request->mobile])->first();
        $userId = $user->user_id;
        if ($this->checkTime($user->user_id) < config('global.token_time')) {
            if ($user->token_code == $request->pin_code) {
                if ($user->status == config('global.status_pre')) {
                    $user->update([
                        'status' => config('global.status_accept'),
                    ]);
                }
                MobileDevice::updateOrCreate(['user_id' => $user->user_id],
                    [
                        'user_id' => $user->user_id,
                        'brand' => $request->brand,
                        'model' => $request->model,
                        'manufacture' => $request->manufacture,
                        'os' => $request->os,
                        'display' => $request->display,
                        'sdk' => $request->sdk
                    ]);
                $amount = $this->getAmount($user);
                $userProfile = UserProfile::whereUserId($userId)->first();
                if ($userProfile) {
                    return response()->json($this->getResponseUserVerifyWithUserProfile($user, $amount, $userProfile));
                }
                return response()->json($this->getResponseUserVerify($user, $amount));
            }
            return response()->error('Pin Code Not Valid');
        }
        return response()->error('Time Pin Code Is Expired');
    }


    /*
    |--------------------------------------------------------------------------
    | Other Function
    |--------------------------------------------------------------------------
    */

//Check Time For Mobile Token
    public
    function checkTime($user_id)
    {
        $user = User::find($user_id);
        $from = Carbon::now()->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $user->updated_at);
        return $to->diffInSeconds($from);
    }

    public
    function createUser($country_code, $mobile, $marketer = NULL, $sendSms = 1)
    {
        $token = rand(config('global.min_token_rand'), config('global.max_token_rand'));
        if ($sendSms == $this->sendSmsTOUser) {
            $this->sendSms($token, $mobile);
        }
        $user = User::create([
            'marketer_id' => $marketer,
            'country_code' => $country_code,
            'mobile' => $mobile,
            'token_code' => $token,
            'status' => config('global.status_pre'),
            'password' => Hash::make($token),
            'url' => Str::random(6),
            'api_token' => Str::random(40)
        ]);
        return $user;
    }

    public
    function getValidateMobileUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'size:10'
        ]);
        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    public
    function getValidateMobileMarketer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketer_mobile' => 'size:10'
        ]);
        if ($validator->fails()) {
            return false;
        }
        return $validator;
    }

    public
    function updateUser(User $user)
    {
        $token = rand(config('global.min_token_rand'), config('global.max_token_rand'));
        $this->sendSms($token, $user->mobile);
        $user->update([
            'token_code' => $token,
            'api_token' => Str::random(40),
        ]);
    }

    public
    function sendSms($token, $mobile)
    {
//        $username = "09380880634";
//        $password = '05137606605';
//        $from = "+983000505";
//        $pattern_code = "c80sece28s";
//        // $to = array("9158438332,9150286772");
//        $to = array($mobile);
//        $input_data = array("verification-code" => $token);
//        $url = "https://ippanel.com/patterns/pattern?username=" . $username . "&password=" . urlencode($password) . "&from=$from&to=" . json_encode($to) . "&input_data=" . urlencode(json_encode($input_data)) . "&pattern_code=$pattern_code";
//        $handler = curl_init($url);
//        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
//        curl_setopt($handler, CURLOPT_POSTFIELDS, $input_data);
//        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
//        $response = curl_exec($handler);
    }

    /**
     * @param $user
     * @return mixed
     */
    public function getAmount($user)
    {
        $amount = DB::select("select round(sum(case
                                when transaction.type_of_transaction = 0 && transaction.status = 1 then -1*amount
                                when transaction.type_of_transaction = 1 && transaction.status = 1 then amount
                                else 0 end),0) as amount
                                from transactions as transaction
                                where transaction.user_id ='$user->user_id' ");
        return (int)$amount[0]->amount;
    }

    public function checkMobile(Request $request)
    {
        $user = User::where(['mobile' => $request->mobile, 'status' => 1])->first();
        if (!$user) {
            return response()->error('کاربری با مشخصات وارد شده موجود نمی باشد');
        }
        return response()->json([
            'data' => ['mobile' => $request->mobile]]);

    }

    public function getResponseUserVerify($user, int $amount): array
    {
        return [
            'data' => [
                'user' =>
                    ['user_id' => $user->url,
                        'api_token' => $user->api_token,
                        'country_code' => $user->country_code,
                        'mobile' => $user->mobile,
                        'wallet' => $amount
                    ],
                'user_profile' => [
                    'full_name' => null,
                    'country_name' => null,
                    'email' => null,
                    "gender" => null,
                    "image" => null,
                    "birthday" => null
                ],
            ],
        ];
    }

    public function getResponseUserVerifyWithUserProfile($user, int $amount, $userProfile): array
    {
        return [
            'data' => [
                'user' =>
                    ['user_id' => $user->url,
                        'api_token' => $user->api_token,
                        'country_code' => $user->country_code,
                        'mobile' => $user->mobile,
                        'wallet' => $amount
                    ],
                'user_profile' => [
                    'full_name' => $userProfile->full_name,
                    'country_name' => $userProfile->country_name,
                    'email' => $userProfile->email,
                    "gender" => $userProfile->gender,
                    "image" => $userProfile->image,
                    "birthday" => $userProfile->birthday
                ],
            ],
        ];
    }


    public function CreatOrUpdateUser($user, Request $request)
    {
        if ($user) {
            $this->updateUser($user);
            return response()->staticCode();
        }
        $this->createUser($request->country_code, $request->mobile);
        return response()->staticCode();
    }
}
