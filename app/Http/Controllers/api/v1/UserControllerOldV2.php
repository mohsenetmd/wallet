<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Marketer;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Psy\Util\Str;

class UserController extends Controller
{
    public function show(Request $request)
    {
        return $request->user();
    }

    /*
     | Define Fixed Variable
     */
    public $statusPreVerify = '0';
    public $statusAcceptVerify = '1';
    public $statusAcceptMarketer = '2';
    public $minTokenRand = '1234';
    public $maxTokenRand = '9876';
    public $tokenTime = 1200000;
    public $defaultMarketer = '1';

    /*
    |--------------------------------------------------------------------------
    | Create Or Update User With Random Token
    |--------------------------------------------------------------------------
    */
    public function addMobile(Request $request)
    {
        $checkMobileValidation = $this->getValidateMobile($request);
        $user=$this->checkUserExist();
        if (!$checkMobileValidation->fails()) {
            if (!$user) {
                $this->createUser("mobile");
            } else {
                $this->updateUser("mobile");
                if($user->marketer_id == $this->defaultMarketer){

                }
            }
            return response()->json([
                'data' => []
            ]);
        }else{
            return response()->json([
                'data' => ['msg' => 'فرمت موبایل صحیح شده وارد نمی باشد.',
                    'error_code'=>1]
            ],400);
        }
    }



    public function resendPin(Request $request)
    {
        $user = $this->getUserWithMobile();
        if(empty($user)){
            return response()->json([
                'data' => ['msg' => 'با این شماره موبایل کاربری یافت نشد',
                    'error_code'=>4]
            ],400);
        }
        $token = rand($this->minTokenRand, $this->maxTokenRand);
        $this->sendSms($token);
        $user->update([
            'token_code' => $token,
        ]);
        return response()->json([
            'data' => []
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Check Token
    | If True Update Status User And Go To Marketer Insert
    | If False Update User With New Random Token And Refresh Page
    |--------------------------------------------------------------------------
    */
    public function verifyMobile(Request $request)
    {
        $user = User::where('mobile', $request->mobile)->first();
        $userId = $user->user_id;
        if ($this->checkTime($userId)) {
            if ($user->token_code == $request->token_code) {
                if ($user->status == $this->statusPreVerify) {
                    $user->update([
                        'status' => $this->statusAcceptVerify,
                    ]);
                }
                return response()->json([
                    'data' => ['msg' => 'کد وارد شده صحیح است.',
                        'user_token' => $user->api_token],
                ]);
            }
        }
        return response()->json([
            'data' => ['msg' => 'کد وارد شده صحیح نیست',
                'error_code'=>2],
        ],400);
    }

    /*
    |--------------------------------------------------------------------------
    | Store Marketer
    | If User->Marketer Dosnt Exist Create New User
    | If Marketer Dosnt Exist Create New Marketer
    | Update User MarketerId
    |--------------------------------------------------------------------------
    */
    public function marketerStore(Request $request)
    {
        $user = $request->user();
        if ($user->status != $this->statusAcceptMarketer) {
            $userId = $user->user_id;
            if (!$userId) {
                $userId = $this->createUser('user_id');
            }
            $marketerId = $this->checkMarketerExist($userId);
            if (!$marketerId) {
                $marketerId = $this->createMarketer($userId);
            }
            User::find($userId)->update([
                'marketer_id' => $marketerId,
                'status' => $this->statusAcceptMarketer
            ]);
            return response()->json([
                'data' => ["message" => "ثبت معرف با موفقیت انجام شد."],
                'stats' => 'success',
            ]);
        };
        return response()->json([
            'data' => ["message" => "برای این کاربر معرف دیگری تعریف شده است",
                'error_code'=>3],
            'stats' => 'error',
        ]);
    }



    /*
    |--------------------------------------------------------------------------
    | Other Function
    |--------------------------------------------------------------------------
    */

    //Check Time For Mobile Token
    public function checkTime($user_id)
    {
        $user = User::find($user_id);
        $from = Carbon::now()->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $user->updated_at);
        $diff_in_seconds = $to->diffInSeconds($from);
        if ($diff_in_seconds < $this->tokenTime) {
            return $diff_in_seconds;
        }
        return false;
    }

    public function checkUserExist()
    {
        $user = $this->getUserWithMobile();
        if (empty($user)) {
            return false;
        }
        return $user;
    }

    public function checkMarketerExist($userId)
    {
        $marketer = Marketer::where('user_id', $userId)->first();
        if (empty($marketer)) {
            return false;
        }
        return $marketer->marketer_id;
    }

    public function createUser($returnedValue)
    {
        $token = rand($this->minTokenRand, $this->maxTokenRand);
        $this->sendSms($token);
        $user = User::create([
            'marketer_id' => 1,
            'country_code' => \request()->country_code,
            'mobile' => \request()->mobile,
            'token_code' => $token,
            'status' => $this->statusPreVerify,
            'password' => Hash::make($token),
            'api_token' => \Illuminate\Support\Str::random(40)
        ]);
        Wallet::create(['user_id' => $user->user_id]);
        return $user->$returnedValue;
    }

    public function getValidateMobile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'size:10'
        ]);
        return $validator;
    }

    public function updateUser($returnedValue)
    {
        $token = rand($this->minTokenRand, $this->maxTokenRand);
        $this->sendSms($token);
        $user = $this->getUserWithMobile();
        $user->update([
            'token_code' => $token,
            'api_token' => \Illuminate\Support\Str::random(40),
        ]);
        return $user->$returnedValue;
    }

    public function sendToken($user_id)
    {
        $user = User::find($user_id);
        SendSms($user->token_code, '9158438332');
    }

    public function getUserWithMobile()
    {
        return User::where(['mobile' => \request()->mobile, 'country_code' => \request()->country_code])->first();
    }

    public function createMarketer($userId)
    {
        $marketer = Marketer::create([
            'user_id' => $userId
        ]);
        return $marketer->marketer_id;
    }

    public function sendSms($token)
    {
        $url = 'http://ippanel.com:8080/?apikey=St2NLVzW1_eyC88xvULwIyPJEN4BqEZJk-TulpXyiqM=&pid=c80sece28s&fnum=3000505&tnum=09158438332&p1=verification-code&v1=' . $token;
        $ch = curl_init($url);
        curl_exec($ch);
    }

}
