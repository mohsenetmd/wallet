<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Marketer;
use App\MobileDevice;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use http\Cookie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Psy\Util\Str;

class UserController extends Controller
{
    public function show(Request $request)
    {
        return $request->user();
    }

    /*
     | Define Fixed Variable
     */
    public $statusPreVerify = '0';
    public $statusAcceptVerify = '1';
    public $statusAcceptMarketer = '2';
    public $minTokenRand = '1234';
    public $maxTokenRand = '9876';
    public $tokenTime = 120;
    public $defaultMarketer = '1';
    public $sendSmsTOUser = 1;
    public $sendSmsTOMarketer = 0;
    public $statusCode = 200;

    /*
    |--------------------------------------------------------------------------
    | Create Or Update User With Random Token
    |--------------------------------------------------------------------------
    */
    public function addMobile(Request $request)
    {
        $checkMobileUser = $this->getValidateMobileUser($request);
        $checkMobileMarketer = $this->getValidateMobileMarketer($request);
        $user = $this->checkUserExist($request->country_code, $request->mobile);
        $userMarketer = $this->checkUserWithCDExist($request->marketer_mobile);
        //check user mobile
        if ($checkMobileUser->fails()) {
            return response()->error('Mobile Format Is Wrong');
        }
        //check input mobile marketer or not
        if (strlen($request->marketer_mobile) == 0) {
            if ($user) {
                $this->updateUser($user);
                return response()->staticCode();
            }
            $this->createUser($request->country_code, $request->mobile);
            return response()->staticCode();
        }
        //check validation mobile marketer
        if ($checkMobileMarketer->fails()) {
            return response()->error('Mobile Format Marketer Is Wrong');
        }
        if (!$userMarketer) {
            return response()->error('Marketer Is Not Found');
        }
        if ($user) {
            if ($this->checkMarketerSet($user)) {
                $this->updateUser($user);
                return response()->staticCode();
            }
            $this->updateUser($user);
            $user->update([
                'marketer_id' => $userMarketer->user_id
            ]);
            return response()->staticCode();
        }
        $this->createUser($request->country_code, $request->mobile, $userMarketer->user_id);
        return response()->staticCode();
    }

    public
    function sync(Request $request)
    {
        $user = $request->user();
        $wallet = Wallet::where('user_id', $user->user_id)->first();
        $amount = $wallet->amount_enter - $wallet->amount_exit;
        return response()->json([
            'data' => ['user' => [
                'user_id' => $user->url,
                'wallet' => $amount
            ]]
        ]);
    }

    public function checkUserExist($countryCode, $mobile)
    {
        $user = User::where(['country_code' => $countryCode, 'mobile' => $mobile])->count();
        if ($user == 0) {
            return false;
        }
        return User::where(['country_code' => $countryCode, 'mobile' => $mobile])->first();

    }

    public
    function checkUserWithCDExist($mobile)
    {
        $user = User::where('mobile', $mobile)->count();
        if ($user == 0) {
            return false;
        }
        return User::where('mobile', $mobile)->first();

    }

    public
    function checkMarketerExist($userMarketer)
    {
        $marketer = Marketer::where('user_id', $userMarketer->user_id)->count();
        if ($marketer == 0) {
            return false;
        }
        return Marketer::where('user_id', $userMarketer->user_id)->first();
    }

    public
    function checkMarketerSet($user)
    {
        if (!$user->marketer_id) {
            return false;
        }
        return true;
    }

    public
    function resendPin(Request $request)
    {
        $user = $this->checkUserExist($request->country_code, $request->mobile);
        if (!$user) {
            return response()->error('User Not Exist');
        }
        if ($this->checkTime($user->user_id) > $this->tokenTime) {
            $token = rand($this->minTokenRand, $this->maxTokenRand);
            $this->sendSms($token, $user->mobile);
            $user->update([
                'token_code' => $token,
            ]);
            return response()->json([
                'data' => []
            ]);
        }
        return response()->error('Time Not Elapsed');
    }

    /*
    |--------------------------------------------------------------------------
    | Check Token
    | If True Update Status User And Go To Marketer Insert
    | If False Update User With New Random Token And Refresh Page
    |--------------------------------------------------------------------------
    */
    public
    function verifyMobile(Request $request)
    {
        $user = User::where(['country_code' => $request->country_code, 'mobile' => $request->mobile])->first();
        $userId = $user->user_id;
        if ($this->checkTime($userId)) {
            if ($user->token_code == $request->pin_code) {
                if ($user->status == $this->statusPreVerify) {
                    $user->update([
                        'status' => $this->statusAcceptVerify,
                    ]);
                }
                MobileDevice::updateOrCreate(['user_id' => $user->user_id],
                    [
                        'user_id' => $user->user_id,
                        'brand' => $request->brand,
                        'model' => $request->model,
                        'manufacture' => $request->manufactor,
                        'os' => $request->os,
                        'display' => $request->display,
                        'sdk' => $request->sdk
                    ]);
                $amount = Wallet::where('user_id', $user->user_id)->sum('amount_enter','-','amount_exit');
                return response()->json([
                    'data' => ['user' => ['user_id' => $user->url,
                        'api_token' => $user->api_token,
                        'country_code' => $user->country_code,
                        'mobile' => $user->mobile,
                        'wallet' => $amount
                    ]],
                ]);
            }
            return response()->error('Pin Code Not Valid');
        }
        return response()->error('Time Pin Code Is Expired');
    }


    /*
    |--------------------------------------------------------------------------
    | Other Function
    |--------------------------------------------------------------------------
    */

//Check Time For Mobile Token
    public
    function checkTime($user_id)
    {
        $user = User::find($user_id);
        $from = Carbon::now()->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $user->updated_at);
        $diff_in_seconds = $to->diffInSeconds($from);
        if ($diff_in_seconds < $this->tokenTime) {
            if ($diff_in_seconds == 0) {
                return true;
            }
            return $diff_in_seconds;
        }
        return false;
    }

    public
    function createUser($country_code, $mobile, $marketer = NULL, $sendSms = 1)
    {
        $token = rand($this->minTokenRand, $this->maxTokenRand);
        if ($sendSms == $this->sendSmsTOUser) {
            $this->sendSms($token, $mobile);
        }
        $user = User::create([
            'marketer_id' => $marketer,
            'country_code' => $country_code,
            'mobile' => $mobile,
            'token_code' => $token,
            'status' => $this->statusPreVerify,
            'password' => Hash::make($token),
            'url' => \Illuminate\Support\Str::random(6),
            'api_token' => \Illuminate\Support\Str::random(40)
        ]);
        Wallet::create(['user_id' => $user->user_id]);
        return $user;
    }

    public
    function getValidateMobileUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'size:10'
        ]);
        return $validator;
    }

    public
    function getValidateMobileMarketer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketer_mobile' => 'size:10'
        ]);
        return $validator;
    }

    public
    function updateUser($user)
    {
        $token = rand($this->minTokenRand, $this->maxTokenRand);
        $this->sendSms($token, $user->mobile);
        $user->update([
            'token_code' => $token,
            'api_token' => \Illuminate\Support\Str::random(40),
        ]);
    }

    public
    function sendSms($token, $mobile)
    {
//        $username = "09380880634";
//        $password = '05137606605';
//        $from = "+983000505";
//        $pattern_code = "c80sece28s";
//        // $to = array("9158438332,9150286772");
//        $to = array($mobile);
//        $input_data = array("verification-code" => $token);
//        $url = "https://ippanel.com/patterns/pattern?username=" . $username . "&password=" . urlencode($password) . "&from=$from&to=" . json_encode($to) . "&input_data=" . urlencode(json_encode($input_data)) . "&pattern_code=$pattern_code";
//        $handler = curl_init($url);
//        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
//        curl_setopt($handler, CURLOPT_POSTFIELDS, $input_data);
//        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
//        $response = curl_exec($handler);
    }

}
