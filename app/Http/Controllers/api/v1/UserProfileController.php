<?php

namespace App\Http\Controllers\api\v1;

use App\Models\UserProfile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserProfileController extends Controller
{
    public function userProfile(Request $request)
    {
        $validator = $this->getValidationUserProfile($request);
        if ($validator->fails()) {
            return response()->error($validator->errors());
        }
        $file = $this->saveFile($request);
        $userProfile = UserProfile::where('user_id', $request->user()->user_id)->first();
        if (!$userProfile) {
            UserProfile::create([
                'user_id' => $request->user()->user_id,
                'full_name' => $request->full_name,
                'country_name' => $request->country_name,
                'email' => $request->email,
                "gender" => $request->gender,
                "image" => $file ? $file : "",
                "birthday" => $request->birthday
            ]);
            return response()->json([
                'data' => ['user_profile' =>
                    ['full_name' => $request->full_name,
                        'country_name' => $request->country_name,
                        'email' => $request->email,
                        'mobile' => $request->mobile,
                        'gender' => $request->gender,
                        'image' => $file ? $file : "",
                        "birthday" => $request->birthday
                    ]]
            ]);
        }
        UserProfile::where('user_id', $request->user()->user_id)->update([
            'full_name' => $request->has('full_name') ? $request->full_name : $userProfile->full_name,
            'country_name' => $request->has('country_name') ? $request->country_name : $userProfile->country_name,
            'email' => $request->has('email') ? $request->email : $userProfile->email,
            "gender" => $request->has('gender') ? $request->gender : $userProfile->gender,
            "image" => $file ? $file : $userProfile->image,
            "birthday" => $request->has('birthday') ? $request->birthday : $userProfile->birthday
        ]);
        return response()->json([
            'data' => ['user_profile' =>
                [
                    'full_name' => $request->has('full_name') ? $request->full_name : $userProfile->full_name,
                    'country_name' => $request->has('country_name') ? $request->country_name : $userProfile->country_name,
                    'email' => $request->has('email') ? $request->email : $userProfile->email,
                    "gender" => $request->has('gender') ? $request->gender : $userProfile->gender,
                    "image" => $file ? $file : $userProfile->image,
                    "birthday" => $request->has('birthday') ? $request->birthday : $userProfile->birthday
                ]]
        ]);
    }

    public function getValidationUserProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpg,jpeg,png,bmp,JPG,JPEG,PNG.BMP|nullable'
        ], [
            'image.mimes' => 'input file format wrong'
        ]);
        return $validator;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function saveFile(Request $request)
    {
        if ($request->file('image')) {
            $name = $request->file('image')->getClientOriginalName();
            $year = Carbon::now()->year;
            $month = Carbon::now()->month;
            $day = Carbon::now()->day;
            $pathFile = 'image' . '/' . 'avatar' . '/' . $year . '/' . $month . '/' . $day;
            $file = $request->file('image')->store($pathFile, 'app');
            return  $file;
        }
        return null;
    }
}
