<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\WalletResource;
use App\Transaction;
use App\wallet;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function show(Request $request){
        $user=$request->user();
        $wallet=wallet::where('user_id',$user->user_id)->sum('amount_enter', '-', 'amount_exit');
        return response()->json([
            'data'=>['wallet'=>$wallet],
            'status'=>'success'
        ]);
    }
}
