<?php

namespace App\Http\Livewire\Charity;

use App\Models\Charity;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;

    public $charityId;


    public function setCharityId($charityId)
    {
        $this->charityId = $charityId;
    }

    public function refresh()
    {

    }

    public function render()
    {
        $charities = Charity::where('created_at', '!=', null)->paginate(5);
        return view('livewire.charity.table', ['charities' => $charities]);
    }
}
