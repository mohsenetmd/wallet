<?php

namespace App\Http\Livewire\Finotech;

use App\Models\CodeFinotech;
use Livewire\Component;
use Livewire\WithPagination;

class CodeAc extends Component
{
    use WithPagination;

    public $sortBy='updated_at';

    public function sort(){
    $this->sortBy="created_at";
    }

    public function render()
    {
        $lists=CodeFinotech::orderBy($this->sortBy)->paginate(20);
        return view('livewire.finotech.code-ac',['lists'=>$lists]);
    }
}
