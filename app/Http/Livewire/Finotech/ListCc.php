<?php

namespace App\Http\Livewire\Finotech;

use App\Http\Controllers\FinoTechController;
use App\Models\TokenFinotech;
use Carbon\Carbon;
use Livewire\Component;

class ListCc extends Component
{
    public $clientCredentials=array();
    public $message;

    public function checkTokenList()
    {
        $token = TokenFinotech::where("scopes", "like", "boomrang:tokens:get")->first();
        if (!$token) {
            $this->message="توکن برای این درخواست موجود نمی باشد";
            return false;
        }
        $now = Carbon::now()->format('U');
        if ($now > $token->life_time) {
            $this->message="تاریخ اعتبار توکن گذشته است";
            return false;
        }
        return true;
    }

    public function showList()
    {
        $finotech = new FinoTechController();
        $this->clientCredentials = $finotech->getListTokenCc();
    }


    public function render()
    {
        $showButton=$this->checkTokenList();
        return view('livewire.finotech.list-cc', ['clientCredentials' => $this->clientCredentials,'showButton'=>$showButton]);
    }
}
