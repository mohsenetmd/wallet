<?php

namespace App\Http\Livewire\Finotech;

use Livewire\Component;

class RightNavbarAc extends Component
{
    public $tabs;

    public function __construct($id = null)
    {
        $this->tabs=['1'=>1,'2'=>0,'3'=>0];
    }

    public function showTab($numberTab){
        $this->tabs=array_fill(1, count($this->tabs), 0);
        $this->tabs[$numberTab]=1;
    }


    public function render()
    {
        return view('livewire.finotech.right-navbar-ac');
    }
}
