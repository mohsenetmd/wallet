<?php

namespace App\Http\Livewire\Finotech;

use Livewire\Component;

class RightNavbarCc extends Component
{
    public $tabs;

    public function __construct($id = null)
    {
        $this->tabs=['1'=>1,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0];
    }

    public function showTab($numberTab){
        $this->tabs=array_fill(1, count($this->tabs), 0);
        $this->tabs[$numberTab]=1;
    }

    public function render()
    {
        return view('livewire.finotech.right-navbar-cc');
    }
}
