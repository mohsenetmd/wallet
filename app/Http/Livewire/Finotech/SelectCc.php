<?php

namespace App\Http\Livewire\Finotech;

use App\Http\Controllers\FinoTechController;
use Illuminate\Support\Facades\Redis;
use Livewire\Component;

class SelectCc extends Component
{
    public $indexScopes;
    public $valueScopes;
    public $indexSelectedScopes;
    public $valueSelectedScopes;

    public function __construct()
    {
        if (Redis::get('index_scopes')) {
            $this->indexScopes = json_decode(Redis::get('index_scopes'), true);
            $this->valueScopes = json_decode(Redis::get('value_scopes'), true);
            $this->indexSelectedScopes = json_decode(Redis::get('index_selected_scopes'), true);
            $this->valueSelectedScopes = json_decode(Redis::get('value_selected_scopes'), true);
        } else {
            $this->indexScopes = ['ارسال پیامک', 'تایید کد یکبار رمز پیامکی',
                'حذف توکن', 'مشاهده توکن های برنامه',
                'مشاهده کارمزد ها', 'استعلام کارت',
                'اطلاعات بانک ها', 'دریافت اطلاعات شبا',
                'پیگیری انتقال از حساب به کارت شتابی'];
            $this->valueScopes = ['boomrang:sms-send:execute', 'boomrang:sms-verify:execute',
                'boomrang:token:delete', 'boomrang:tokens:get',
                'boomrang:wages:get', 'card:information:get',
                'facility:cc-bank-info:get', 'oak:iban-inquiry:get',
                'refund:track-refund-with-card:get'];
            $this->indexSelectedScopes = array();
            $this->valueSelectedScopes = array();
        };
    }

    public function saveClienCredentical()
    {
        $scopes = implode(',', $this->valueSelectedScopes);
        $finotech = new FinoTechController();
        $finotech->getClientCredential($scopes);
        $this->delRedis();
        return $this->redirect(route('panelFinotech'));
    }

    public function selectScope($input)
    {
        array_push($this->valueSelectedScopes, $this->valueScopes[$input]);
        unset($this->valueScopes[$input]);
        array_push($this->indexSelectedScopes, $this->indexScopes[$input]);
        unset($this->indexScopes[$input]);
        $this->setRedis();
    }

    public function removeScope($input)
    {
        array_push($this->valueScopes, $this->valueSelectedScopes[$input]);
        unset($this->valueSelectedScopes[$input]);
        array_push($this->indexScopes, $this->indexSelectedScopes[$input]);
        unset($this->indexSelectedScopes[$input]);
        $this->setRedis();
    }

    public function setRedis()
    {
        Redis::setex('index_scopes', 3600, json_encode($this->indexScopes));
        Redis::setex('value_scopes', 3600, json_encode($this->valueScopes));
        Redis::setex('index_selected_scopes', 3600, json_encode($this->indexSelectedScopes));
        Redis::setex('value_selected_scopes', 3600, json_encode($this->valueSelectedScopes));
    }

    public function delRedis()
    {
        Redis::del('index_scopes','value_scopes','index_selected_scopes', 'value_selected_scopes');
    }


    public function render()
    {
        return view('livewire.finotech.select-cc', ['index_scopes' => $this->indexScopes, 'selected_scopes' => $this->indexSelectedScopes]);
    }
}
