<?php

namespace App\Http\Livewire\Finotech;

use App\Http\Controllers\FinoTechController;
use App\Models\CardInformation;
use App\Models\TokenFinotech;
use Livewire\Component;
use Livewire\WithPagination;

class TableCard extends Component
{
    public $cards;

    use WithPagination;

    public $perPage = 10;
    public $sortField = 'created_at';
    public $sortAsc = true;
    public $search = '';
    public $count = '1';
    public $response;

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
        $cardInfoes = CardInformation::where('card_number', '!=' , null)
            ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
            ->paginate($this->perPage);
        return view('livewire.finotech.table-card',['cardInfoes'=>$cardInfoes]);
    }
}
