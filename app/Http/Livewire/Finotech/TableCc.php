<?php

namespace App\Http\Livewire\Finotech;

use App\Http\Controllers\FinoTechController;
use App\Models\TokenFinotech;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class TableCc extends Component
{
    use WithPagination;

    public $perPage = 10;
    public $sortField = 'monthly_call_limitation';
    public $sortAsc = true;
    public $search = '';
    public $count = '1';
    public $response;

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }


    public function delete($id)
    {
        $finotech = new FinoTechController();
        $delete = $finotech->deleteTokenCc($id);
        if ($delete) {
            TokenFinotech::where(['id' => $id])->delete();
            $this->response = 'توکن با موفقیت ابطال شد';
        } else {
            $this->response = "در اتصال با فینوتک به مشکل خوردیم";
        }
    }

    public function refreshToken($id)
    {
        $finotech = new FinoTechController();
        $token = TokenFinotech::find($id);
        $finotech->refreshTokenCc($token->refresh_token);
        TokenFinotech::where(['id' => $id])->delete();
    }

    public function render()
    {
        $tokens = TokenFinotech::search($this->search)->where('monthly_call_limitation', null)
            ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
            ->paginate($this->perPage);
        $finotech=new FinoTechController();
        $checkDeleteToken = TokenFinotech::where('scopes', "like", "%boomrang:token:delete%")->first();
        foreach ($tokens as $key => $token) {
            $objectDate = jdate($token->life_time);
            $tokens[$key]->life_time = $objectDate->getYear() . '/' . $objectDate->getMonth() . '/' . $objectDate->getDay() . "   " .
                $objectDate->getHour() . ':' . $objectDate->getMinute() . ':' . $objectDate->getSecond();
            $tokens[$key]->creation_date = styleCreationDateFinoTech($token->creation_date);
            $tokens[$key]->scopes = $finotech->persianScope($token->scopes);
        }
        return view('livewire.finotech.table-cc', [
            'tokens' => $tokens, 'checkDeleteToken' => $checkDeleteToken
        ]);
    }
}
