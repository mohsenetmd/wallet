<?php

namespace App\Http\Livewire\Finotech;

use Livewire\Component;

class UrlAc extends Component
{
    public $indexScopes;
    public $valueScopes;

    public function __construct()
    {
        $this->indexScopes = ['مانده حساب', 'استعلام واریز وجه',
            'مانده و 100 گردش آخر حساب', 'واریز وجه به',
            'انتقال از حساب به کارت شتابی'];
        $this->valueScopes = ['oak:balance:get', 'oak:inquiry-transfer:get',
            'oak:statement:get', 'oak:transfer-to:execute',
            'refund:deposit-card:post'];
    }

    public function render()
    {
        return view('livewire.finotech.url-ac');
    }
}
