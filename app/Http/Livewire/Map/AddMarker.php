<?php

namespace App\Http\Livewire\Map;

use Livewire\Component;

class AddMarker extends Component
{
    public $messages=[[36.289632, 59.616130]];
    public function render()
    {
        return view('livewire.add-marker');
    }
}
