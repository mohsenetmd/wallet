<?php

namespace App\Http\Livewire\Map;

use App\Http\Controllers\SnapController;
use Livewire\Component;

class Map extends Component
{
    public $search;
    public $latitude;
    public $longitude;
    public $searchResult = array();

//    protected $listeners = ['postAdded'];

    public function searchLocation($flag = true)
    {
        if ($flag) {
            $snap = new SnapController();
            $this->searchResult = $snap->search($this->search);
        }else{
            $this->searchResult = array();
        }
    }

    public function addToMap($latitude, $longitude, $name)
    {
        $this->search = $name;
        $this->latitude=$latitude;
        $this->longitude=$longitude;
        $this->searchLocation(false);
        $this->emit('landmarkAdd', [$latitude, $longitude,$name]);
    }

    public function render()
    {
        return view('livewire.map');
    }
}
