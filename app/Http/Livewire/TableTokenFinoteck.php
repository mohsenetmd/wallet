<?php

namespace App\Http\Livewire;

use App\Models\TokenFinotech;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class TableTokenFinoteck extends Component
{
    use WithPagination;

    public $perPage = 10;
    public $sortField = 'monthly_call_limitation';
    public $sortAsc = true;
    public $search = '';
    public $count = '1';

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
        $tokens=TokenFinotech::search($this->search)
            ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
            ->paginate($this->perPage);
        foreach($tokens as $key=>$token){
            $objectDate=jdate($token->life_time);
            $tokens[$key]->life_time=$objectDate->getYear() . '/' . $objectDate->getMonth() . '/' . $objectDate->getDay() . "   ".
            $objectDate->getHour() . ':' . $objectDate->getMinute() . ':' . $objectDate->getSecond();
            $tokens[$key]->creation_date=styleCreationDateFinoTech($token->creation_date);
        }
        return view('livewire.table-token-finoteck', [
            'tokens' => $tokens,
        ]);
    }
}
