<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Http\Controllers\api\v1\TransactionController;
use Closure;

class AmountChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $amount=TransactionController::amount($request);
        $category=Category::find($request->category_id);
        $typeOfTransaction = $category->type_of_transaction;
        if($request->amount > $amount && $typeOfTransaction == 0){
            return response()->error('Not Enough Money');
        }
        return $next($request);
    }
}
