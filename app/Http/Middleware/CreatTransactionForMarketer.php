<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\Profit;
use App\Models\Transaction;
use App\Models\User;
use Closure;

class CreatTransactionForMarketer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if($request->transaction){
            $transaction=Transaction::find($request->transaction);
            $user=User::find($transaction->user_id);
            if($user->marketer_id){
                $category=Category::find($transaction->category_id);
                if($category->type_of_transaction == 0 && $category->percent > 0 && $transaction->status == 1){
                    Transaction::create([
                        'user_id' => $user->marketer_id,
                        'category_id' => 5,
                        'amount' => ($transaction->amount * $category->percent) / 100,
                        'type_of_transaction' => 1,
                        'status' => 1,
                    ]);
                }
            }
        }
        return $response;
    }
}
