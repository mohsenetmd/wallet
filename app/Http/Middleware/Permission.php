<?php

namespace App\Http\Middleware;

use App\Method;
use App\PermissionGroup;
use App\UserGroup;
use App\UserPanel;
use Closure;
use Illuminate\Support\Facades\Auth;
use mysql_xdevapi\RowResult;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $method)
    {
        $array = explode('@', $method);
        $USER = Auth::guard("user_panel")->user();
        if ($USER->isAdmin=='1') {
            return $next($request);
        } else {

            $userGroup = UserGroup::where('user_id', $USER->user_id)->first();
            $userGroup->PermissionGroup();
            $method_id = Method::where('name', $method)->first();

            if ($userGroup->PermissionGroup()->first() and $method_id) {
                $permission = PermissionGroup::where(['group_id' => $userGroup->PermissionGroup()->first()->group_id, 'method_id' => $method_id->method_id])->first();
                if ($permission) {
                    return $next($request);
                }
            }

            return redirect()->route('panel.user.permission');
            // return $next($request);//  return view('welcome')->withErrors('no permission');
        }
    }
}
