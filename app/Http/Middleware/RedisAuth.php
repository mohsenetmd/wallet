<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Redis;

class RedisAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redis = Redis::get($request->api_token);
        if ($redis) {
            $redisObject = json_decode($redis);
            $request->user = $redisObject;
            return $next($request);
        }
        $user = User::where('api_token', $request->api_token)->first();
        Redis::setex($request->api_token, 3600, $user);
        return $next($request);
    }
}
