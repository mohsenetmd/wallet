<?php

namespace App\Http\Middleware;

use App\Models\TokenFinotech;
use Carbon\Carbon;
use Closure;

class TokenChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $scope)
    {
        $scope=str_replace('|',":",$scope);
        $token=TokenFinotech::where("scopes","like",$scope)->first();
        if(!$token){
            return response()->error('No Token For Action');
        }
        $now=Carbon::now()->format('U');
        if($now > $token->life_time){
            return response()->error('Token Time Expired');
        }
        $request->tokenMiddleware=$token->token;
        return $next($request);
    }
}
