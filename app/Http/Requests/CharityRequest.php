<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CharityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'account_number'=>'required',
            'icon'=>'required|image|max:100'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'وارد کردن نام الزامی است',
            'account_number.required'=>'وارد کردن شماره حساب الزامی است',
            'icon.required'=>'وارد کردن عکس الزامی است',
            'icon.image'=>'فرمت عکس ورودی اشتباه است',
            'icon.max'=>'حجم عکس وروردی زیاد است'
        ];
    }
}
