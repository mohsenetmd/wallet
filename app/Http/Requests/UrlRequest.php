<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
//        $this->merge([
//            'wp_post_id'=>$IDWpPost->ID
//        ]);
    }

    public function rules()
    {
        return [
            'user_id'=>'required',
            'payer'=>'required|size:10',
            'amount'=>'required|min:4|max:10'
        ];
    }

}
