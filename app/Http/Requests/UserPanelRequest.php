<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserPanelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'=>'required|size:11|unique:pl_users',
            'username'=>'required|min:6|unique:pl_users',
            'password'=>'required|min:8',
        ];
    }

}
