<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AirFareCityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name_fa'=>$this->name_fa,
            'name_en'=>$this->name_en,
            'city_name'=>$this->city_name,
            'country_name'=>$this->country_name
        ];
    }
}
