<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ListDonateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'amount'=>$this->amount,
            'name'=>$this->name,
            'icon'=>$this->icon,
            'color'=>$this->color,
        ];
    }
}
