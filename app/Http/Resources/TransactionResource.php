<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'category_id' => $this->category_id,
                'amount' => $this->amount,
                'type_of_transaction' => $this->type_of_transaction,
                'payer' => $this->payer,
                'updated_at' => $this->updated_at
        ];
    }
}
