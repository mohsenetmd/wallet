<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'full_name' => $this->full_name,
            'country_name' => $this->country_name,
            'email' => $this->email,
            "gender" => $this->gender,
            "image" => $this->image,
            "birthday" => $this->birthday
        ];
    }
}
