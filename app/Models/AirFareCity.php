<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AirFareCity
 *
 * @property int $id
 * @property string|null $name_en
 * @property string|null $name_fa
 * @property string|null $full_name
 * @property string|null $abbreviation
 * @property string|null $city_name
 * @property string|null $state_name
 * @property string|null $country_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereCityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereCountryName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereNameFa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereStateName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirFareCity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AirFareCity extends Model
{
    use HasFactory;
    protected $guarded=[];
}
