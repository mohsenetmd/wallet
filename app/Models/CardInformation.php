<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CardInformation
 *
 * @property int $id
 * @property int $user_id
 * @property string $track_id
 * @property string $card_number
 * @property string $name
 * @property string $do_time
 * @property int|null $deleted_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation query()
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereDoTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereTrackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CardInformation whereUserId($value)
 * @method static Transaction where($value)
 * @method static Transaction create($value)
 * @method static Transaction find($value)
 * @method static Transaction update($value)
 * @method static Transaction orderBy($value)
 * @method static Transaction first()
 * @mixin \Eloquent
 */
class CardInformation extends Model
{
    use HasFactory;
    protected $table="card_information";
    protected $guarded=[];
    protected $dateFormat="U";

}
