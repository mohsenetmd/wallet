<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property int $category_id
 * @property int|null $parent_id
 * @property int|null $percent
 * @property int|null $profit
 * @property string $name
 * @property string $type_of_transaction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereProfit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereTypeOfTransaction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @method static Transaction where($value)
 * @method static Transaction create($value)
 * @method static Transaction find($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    protected $guarded=[];
    protected $primaryKey="category_id";
}
