<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class categoryPlace extends Model
{
    protected $primaryKey='category_place_id';
    protected $guarded=[];
    protected $table='pl_category_place';
    public function typeCategory(){
        return $this->belongsTo(typeCategory::class, 'type_category_id', 'type_category_id');
    }
}
