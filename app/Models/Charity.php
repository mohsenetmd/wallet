<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Charity
 *
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property string $account_number
 * @property string $color
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Charity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Charity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Charity query()
 * @method static \Illuminate\Database\Eloquent\Builder|Charity whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charity whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charity whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charity whereUpdatedAt($value)
 * @method static Transaction where($value)
 * @method static Transaction create($value)
 * @method static Transaction find($value)
 * @method static Transaction update($value)
 * @method static Transaction orderBy($value)
 * @method static Transaction first()
 * @mixin \Eloquent
 */
class Charity extends Model
{
    use HasFactory;
    protected $guarded=[];
}
