<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CodeFinotech
 *
 * @property int $id
 * @property string|null $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CodeFinotech newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CodeFinotech newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CodeFinotech query()
 * @method static \Illuminate\Database\Eloquent\Builder|CodeFinotech whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeFinotech whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeFinotech whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CodeFinotech whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CodeFinotech extends Model
{
    use HasFactory;
    protected $table="code_finotech";
    protected $fillable=['code'];
}
