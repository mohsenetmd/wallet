<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CountryCode
 *
 * @property int $country_id
 * @property int $country_code
 * @property string $flag
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode whereFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountryCode whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CountryCode extends Model
{
    protected $guarded=[];
    protected $primaryKey="country_id";
}
