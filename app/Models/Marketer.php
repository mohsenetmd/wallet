<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Marketer
 *
 * @property int $marketer_id
 * @property string $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $User
 * @property-read int|null $user_count
 * @method static \Illuminate\Database\Eloquent\Builder|Marketer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Marketer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Marketer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Marketer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Marketer whereMarketerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Marketer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Marketer whereUserId($value)
 * @mixin \Eloquent
 */
class Marketer extends Model
{
    protected $guarded=[];
    protected $primaryKey='marketer_id';

    public function User(){
        return $this->hasMany(User::class, 'marketer_id', 'marketer_id');
    }
}
