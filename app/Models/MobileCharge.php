<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MobileCharge
 *
 * @property int $order_id
 * @property int $user_id
 * @property string $operator
 * @property string|null $product_id
 * @property string|null $product_name
 * @property string|null $product_title
 * @property int $amount
 * @property int $mobile
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge query()
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereProductTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileCharge whereUserId($value)
 * @method static Transaction where($value)
 * @method static Transaction create($value)
 * @method static Transaction find($value)
 * @method static Transaction update($value)
 * @method static Transaction orderBy($value)
 * @method static Transaction first()
 * @mixin \Eloquent
 */
class MobileCharge extends Model
{
    protected $primaryKey="order_id";
    protected $guarded=[];

    public function User(){
        $this->belongsTo(User::class,'user_id','user_id');
    }
}
