<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MobileDevice
 *
 * @property int $mobile_id
 * @property int $user_id
 * @property string|null $brand
 * @property string|null $model
 * @property string|null $manufacture
 * @property string|null $os
 * @property string|null $display
 * @property string|null $sdk
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice query()
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereDisplay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereManufacture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereMobileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereOs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereSdk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MobileDevice whereUserId($value)
 * @mixin \Eloquent
 */
class MobileDevice extends Model
{
    protected $guarded=[];
    protected $primaryKey='mobile_id';
    protected $dateFormat='U';
}
