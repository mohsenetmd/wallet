<?php

namespace App\Models;

use App\Http\Requests\PermissinGroupPanelRequest;
use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    protected $primaryKey="permission_group_id";
    protected $table='pl_permission_groups';
    protected $guarded = ['id'];
}
