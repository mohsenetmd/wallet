<?php

namespace App\Models;

use App\Http\Requests\PermissionUserPanelRequest;
use Illuminate\Database\Eloquent\Model;

class PermissionUser extends Model
{
    protected $primaryKey="permission_user_id";
    protected $table='pl_permission_users';
    protected $guarded = ['id'];
}
