<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Taxi extends Model
{
    use HasFactory;
    protected  $primaryKey ="id";
    protected  $table="pl_request_taxi";
    protected  $dateFormat='U';
    protected  $fillable=['ride_id','mobile','contact_name','contact_mobile','destination_lat','destination_lng','origin_lat','origin_lng','extra_destination_lat','extra_destination_lng','round_trip','service_type','waiting','status'];

}
