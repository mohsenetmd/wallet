<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TokenFinotech
 *
 * @property int $id
 * @property int|null $monthly_call_limitation
 * @property int|null $max_amount_per_transaction
 * @property int|null $user_id
 * @property string|null $deposits
 * @property string|null $scopes
 * @property string $token
 * @property string $refresh_token
 * @property int $life_time
 * @property int $creation_date
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech query()
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereCreationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereDeposits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereLifeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereMaxAmountPerTransaction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereMonthlyCallLimitation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereRefreshToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereScopes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TokenFinotech whereUserId($value)
 * @mixin \Eloquent
 */
class TokenFinotech extends Model
{
    protected $table="token_finotech";
    public $dateFormat="U";
    protected $guarded=[];

    public function getCreatedAtAttributes($value)
    {
        return $value;
    }


    public function getUpdatedAtAttributes($value)
    {
        return $value;
    }

    public static function search($query)
    {
        return empty($query) ? static::query() : static::where('scopes', 'like', '%'.$query.'%');
    }
}
