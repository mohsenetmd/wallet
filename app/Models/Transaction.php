<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded=[];
    protected $primaryKey="transaction_id";
    protected $dateFormat='U';


    public function User(){
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function Category(){
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function TransactionDetail(){
        return $this->hasOne(TransactionDetail::class, 'transaction_id', 'transaction_id');
    }

    public function getUpdatedAtAttribute($value){
        return $value;
    }

}
