<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TransactionDetail
 *
 * @property int $transaction_detail_id
 * @property int $transaction_id
 * @property int $category_id
 * @property string $title
 * @property string $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Category $Category
 * @property-read \App\Models\Transaction $Transaction
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereTransactionDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransactionDetail extends Model
{
    protected $guarded=[];
    protected $primaryKey="transaction_detail_id";

    public function Transaction(){
        return $this->belongsTo(Transaction::class, 'transaction_id', 'transaction_id');
    }

    public function Category(){
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }
}
