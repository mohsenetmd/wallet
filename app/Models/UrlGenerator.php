<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UrlGenerator
 *
 * @property int $url_generator_id
 * @property int $user_id
 * @property int $transaction_id
 * @property string $url
 * @property string $status
 * @property string|null $payer
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator query()
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator wherePayer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereUrlGeneratorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UrlGenerator whereUserId($value)
 * @mixin \Eloquent
 */
class UrlGenerator extends Model
{
    protected $guarded=[];
    protected $primaryKey="url_generator_id";
}
