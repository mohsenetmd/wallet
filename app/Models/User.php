<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use  Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\User
 *
 * @property int $user_id
 * @property int|null $marketer_id
 * @property string $mobile
 * @property int $country_code
 * @property string $token_code
 * @property string $status
 * @property string $password
 * @property string $api_token
 * @property string $url
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Models\Marketer|null $Marketer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $Transaction
 * @property-read int|null $transaction_count
 * @property-read \App\Models\UserProfile|null $UserProfile
 * @property-read \App\Models\wallet|null $Wallet
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMarketerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTokenCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @method static Transaction where($value)
 * @method static Transaction create($value)
 * @method static Transaction find($value)
 * @method static Transaction update($value)
 * @method static Transaction orderBy($value)
 * @method static Transaction first()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['password', 'api_token', 'remember_token'];
    protected $primaryKey = 'user_id';
    protected $dateFormat="U";

    public function Marketer()
    {
        return $this->belongsTo(Marketer::class, 'marketer_id', 'marketer_id');
    }

    public function UserProfile()
    {
        return $this->hasOne(UserProfile::class, 'user_id', 'user_id');
    }

    public function Wallet()
    {
        return $this->hasOne(wallet::class, 'user_id', 'user_id');
    }

    public function Transaction()
    {
        return $this->hasMany(Transaction::class, 'transaction_id', 'transaction_id');
    }

}
