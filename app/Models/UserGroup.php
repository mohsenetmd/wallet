<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserGroup
 *
 * @property int $user_group_id
 * @property int $user_id
 * @property int $group_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PermissionGroup $PermissionGroup
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup whereUserGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserGroup whereUserId($value)
 * @mixin \Eloquent
 */
class UserGroup extends Model
{
    protected $primaryKey="user_group_id";
    protected $table='pl_user_groups';
    protected $guarded = ['id'];
    public function PermissionGroup(){
        return $this->belongsTo(PermissionGroup::class, 'group_id', 'group_id');
    }
}
