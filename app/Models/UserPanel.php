<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Model;

/**
 * App\Models\UserPanel
 *
 * @property int $user_id
 * @property string $mobile
 * @property string $status
 * @property string $username
 * @property string $password
 * @property int $isAdmin
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserPanel onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPanel whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|UserPanel withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserPanel withoutTrashed()
 * @mixin \Eloquent
 */
class UserPanel extends Model
{
    use SoftDeletes;
    protected $primaryKey="user_id";
    protected $guarded = ['id'];
    protected $guard = 'user_panel';
    protected $table='pl_users';
    public function getAuthPassword()
    {
        return $this->password;
    }
}
