<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\wallet
 *
 * @property int $wallet_id
 * @property int $user_id
 * @property int|null $amount_enter
 * @property int|null $amount_exit
 * @property int|null $last_transaction_id
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Models\User $User
 * @method static \Illuminate\Database\Eloquent\Builder|wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|wallet query()
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereAmountEnter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereAmountExit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereLastTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|wallet whereWalletId($value)
 * @mixin \Eloquent
 */
class wallet extends Model
{
    protected $guarded=[];
    protected $primaryKey="wallet_id";
    protected $dateFormat='U';

    public function User(){
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
