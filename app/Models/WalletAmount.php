<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WalletAmount
 *
 * @method static \Illuminate\Database\Eloquent\Builder|WalletAmount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WalletAmount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WalletAmount query()
 * @mixin \Eloquent
 */
class WalletAmount extends Model
{
    //
}
