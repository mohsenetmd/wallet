<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class selectedPlace extends Model
{
    protected $primaryKey='id';
    protected $guarded=[];
    protected $table='pl_selected_place';
}
