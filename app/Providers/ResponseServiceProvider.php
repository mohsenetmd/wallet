<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        $header = request()->header('LANG');
        Response::macro('error', function ($value) {
            return response()->json([
                'data' => ['msg' => $value]
            ], 400);
        });

        Response::macro('staticCode', function () {
            return response()->json([
                'data' => ['static_code' => 'AnkjlD']
            ]);
        });
    }
}
