<?php

use Illuminate\Http\Request;

function SendSms($Message, $Mobile)
{
    $url = "https://ippanel.com/services.jspd";
    $rcpt_nm = array($Mobile);
    $param = array
    (
        'uname' => '09380880634',
        'pass' => '05137606605',
        'from' => '+983000505',
        'message' => $Message,
        'to' => json_encode($rcpt_nm),
        'op' => 'send'
    );

    $handler = curl_init($url);
    curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($handler, CURLOPT_POSTFIELDS, $param);
    curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
    curl_exec($handler);
}

function send($api, $amount, $redirect, $mobile = null, $factorNumber = null, $description = null)
{
    return curl_post('https://pay.ir/pg/send', [
        'api' => $api,
        'amount' => $amount,
        'redirect' => $redirect,
        'mobile' => $mobile,
        'factorNumber' => $factorNumber,
        'description' => $description,
    ]);
}

function curl_post($url, $params)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json',
    ]);
    $res = curl_exec($ch);
    curl_close($ch);

    return $res;
}

function payment()
{
    $api = 'test';
    $amount = "50000";
    $mobile = "شماره موبایل";
    $factorNumber = "شماره فاکتور";
    $description = "توضیحات";
    $redirect = 'http://YOUR-CALLBACK-URL';
    $result = send($api, $amount, $redirect, $mobile, $factorNumber, $description);
    $result = json_decode($result);
    if ($result->status) {
        $go = "https://pay.ir/pg/$result->token";
        dd($go);
        header("Location: $go");
    } else {
        echo $result->errorMessage;
    }
}

function getUserIdPanel()
{
    return $_SESSION['user']->user_id;
}

function CurlResponse(string $jsonArray, $sendUrl)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $sendUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $jsonArray,
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Content-Type: application/json"
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function CurlResponseWithHeader(string $jsonArray, $sendUrl, $header)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $sendUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $jsonArray,
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic bmV3Y2FzaDE6ZGFkYmE3NzQwYmQ1MzhiYmFkMWI="
        ),
    ));
    $response = curl_exec($curl);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    return $response;
}

function div($a, $b)
{
    return (int)($a / $b);
}

function JalaliToTimeStamp($date)
{
    $j_y = substr($date, 0, 4);
    $j_m = substr($date, 4, 2);
    $j_d = substr($date, 6, 2);
    $h = substr($date, 8, 2);
    $m = substr($date, 10, 2);
    $s = substr($date, 12, 2);
    date_default_timezone_set("Asia/Tehran");
    $g_days_in_month = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    $j_days_in_month = array(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29);

    $jy = $j_y - 979;
    $jm = $j_m - 1;
    $jd = $j_d - 1;
    $j_day_no = 365 * $jy + div($jy, 33) * 8 + div($jy % 33 + 3, 4);
    for ($i = 0; $i < $jm; ++$i)
        $j_day_no += $j_days_in_month[$i];

    $j_day_no += $jd;

    $g_day_no = $j_day_no + 79;

    $gy = 1600 + 400 * div($g_day_no, 146097);
    $g_day_no = $g_day_no % 146097;

    $leap = true;
    if ($g_day_no >= 36525) {
        $g_day_no--;
        $gy += 100 * div($g_day_no, 36524);
        $g_day_no = $g_day_no % 36524;

        if ($g_day_no >= 365)
            $g_day_no++;
        else
            $leap = false;
    }

    $gy += 4 * div($g_day_no, 1461);
    $g_day_no %= 1461;

    if ($g_day_no >= 366) {
        $leap = false;

        $g_day_no--;
        $gy += div($g_day_no, 365);
        $g_day_no = $g_day_no % 365;
    }

    for ($i = 0; $g_day_no >= $g_days_in_month[$i] + ($i == 1 && $leap); $i++)
        $g_day_no -= $g_days_in_month[$i] + ($i == 1 && $leap);
    $gm = $i + 1;
    $gd = $g_day_no + 1;

    return mktime($h, $m, $s, $gm, $gd, $gy);

}

function styleCreationDateFinoTech($date){
    $j_y = substr($date, 0, 4);
    $j_m = substr($date, 4, 2);
    $j_d = substr($date, 6, 2);
    $h = substr($date, 8, 2);
    $m = substr($date, 10, 2);
    $s = substr($date, 12, 2);
    return $j_y . '/' . $j_m . '/' . $j_d . "   ".
        $h . ':' . $m . ':' . $s;
}

