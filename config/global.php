<?php

return [
    'category_buy'=> '1',
    'category_online'=>'2',
    'category_transfer_give'=>'4',
    'category_transfer_send'=>'3',
    'status_accept'=> '1',
    'status_cancel'=> '2',
    'status_pre'=>'0',
    'type_of_transaction_minus'=>'0',
    'type_of_transaction_plus' => '1',
    'min_token_rand'=>'1234',
    'max_token_rand'=>'1234',
    'token_time'=>'120',
]

?>
