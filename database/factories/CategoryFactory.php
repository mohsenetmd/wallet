<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name'=>$faker->name(),
        'type_of_transaction'=>rand(0,1),
        'status'=>rand(0,2),
    ];
});
