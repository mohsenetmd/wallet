<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CountryCode;
use Faker\Generator as Faker;

$factory->define(CountryCode::class, function (Faker $faker) {
    return [
        'country_code' => $faker->unique()->randomNumber(3),
        'flag' => $faker->unique()->url,
    ];
});

