<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\UserPanel;

$factory->define(UserPanel::class, function (Faker $faker) {
    return [
        'mobile' => rand(1111111111,9999999999),
        'username' => rand(1111111111,9999999999),
        'status'=>rand(0,1),
        'password'=>Str::random(10),
        'remember_token'=>Str::random(40),
        'isAdmin'=>0,
    ];
});
