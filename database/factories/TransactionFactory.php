<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Transaction;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    $randCategory=rand(1,3);
    $category=\App\Category::find($randCategory);
    return [
        'user_id'=>rand(1,50),
        'category_id'=>$randCategory,
        'amount'=>1000*rand(1,100),
        'type_of_transaction'=>$category->type_of_transaction,
        'status'=>rand(0,1),
        'payer'=>rand(1234567890,9876543210),
        'parent_id'=>0,
    ];
});
