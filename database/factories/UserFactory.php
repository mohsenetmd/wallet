<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $countryCode=App\CountryCode::find(rand(1,50));
    return [
        'mobile' => rand(1111111111,9999999999),
        'country_code' => $countryCode->country_code,
        'marketer_id'=>rand(1,50),
        'token_code'=> rand(1234,9876),
        'status'=>rand(0,1),
        'password'=>Str::random(10),
        'api_token'=>Str::random(40),
        'url'=>Str::random(6),
    ];
});
