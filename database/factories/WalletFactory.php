<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Wallet;
use Faker\Generator as Faker;

$factory->define(Wallet::class, function (Faker $faker) {
    $randUser=$faker->unique()->biasedNumberBetween(1,50);
    $lastTransactionId=App\Transaction::where('user_id',$randUser)->latest()->first()->transaction_id;
    $amount=App\Transaction::where('user_id',$randUser)->get();
    $totalAmountEnter=0;
    $totalAmountExit=0;
    foreach ($amount as $item){
        if($item->type_of_transaction == 1 && $item->status == 1){
            $totalAmountEnter=$totalAmountEnter + $item->amount;
        }
        if($item->type_of_transaction == 0 && $item->status == 1){
            $totalAmountExit=$totalAmountExit + $item->amount;
        }
    }
    return [
        'user_id'=>$randUser,
        'amount_enter'=>$totalAmountEnter,
        'amount_exit'=>$totalAmountExit,
        'last_transaction_id'=>$lastTransactionId,
    ];
});
