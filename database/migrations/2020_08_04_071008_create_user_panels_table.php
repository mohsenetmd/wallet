<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('pl_users')) return;
        Schema::create('pl_users', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('mobile')->unique();
            $table->string('status')->default(0);
            $table->string('username')->unique();
            $table->string('password');
            $table->tinyInteger('isAdmin')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pl_users');
    }
}
