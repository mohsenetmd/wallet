<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('pl_user_groups')) return;
        Schema::create('pl_user_groups', function (Blueprint $table) {
            $table->id('user_group_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('group_id');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('pl_users')->onDelete('cascade');
            $table->foreign('group_id')->references('group_id')->on('pl_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pl_user_groups');
    }
}
