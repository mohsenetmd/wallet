<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('pl_permission_users')) return;

        Schema::create('pl_permission_users', function (Blueprint $table) {
            $table->id('permission_user_id');
            $table->unsignedBigInteger('method_id');
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->foreign('method_id')->references('method_id')->on('pl_methods')->onDelete('cascade');
            $table->foreign('user_id')->references('user_id')->on('pl_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pl_permission_users');
    }
}
