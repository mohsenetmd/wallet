<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(Schema::hasTable('pl_permission_groups')) return;

        Schema::create('pl_permission_groups', function (Blueprint $table) {
            $table->id('permission_group_id');
            $table->unsignedBigInteger('method_id');
            $table->index(['method_id', 'group_id']);
            $table->unsignedBigInteger('group_id');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->foreign('method_id')->references('method_id')->on('pl_methods')->onDelete('cascade');
            $table->foreign('group_id')->references('group_id')->on('pl_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pl_permission_groups');
    }
}
