<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('transaction_id');
            $table->integer('status');
            $table->string('amount')->nullable();
            $table->string('real_amount')->nullable();
            $table->string('wage')->nullable();
            $table->string('card_number')->nullable();
            $table->string('payment_date')->nullable();
            $table->string('payment_id')->nullable();
            $table->string('cid')->nullable();
            $table->string('message')->nullable();
            $table->string('token');
            $table->timestamps();
            $table->foreign('transaction_id')->references('transaction_id')->on('transactions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
