<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_devices', function (Blueprint $table) {
            $table->id('mobile_id');
            $table->unsignedBigInteger('user_id');
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('manufacture')->nullable();
            $table->string('os')->nullable();
            $table->string('display')->nullable();
            $table->string('sdk')->nullable();
            $table->softDeletes();
            $table->integer('created_at');
            $table->integer('updated_at');
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_devices');
    }
}
