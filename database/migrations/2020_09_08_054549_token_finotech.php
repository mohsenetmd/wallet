<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TokenFinotech extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token_finotech', function (Blueprint $table) {
            $table->id();
            $table->integer('monthly_call_limitation')->nullable();
            $table->integer('max_amount_per_transaction')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('deposits')->nullable();
            $table->string('scopes')->nullable();
            $table->text('token');
            $table->text('refresh_token');
            $table->integer('life_time');
            $table->bigInteger('creation_date');
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token_finotech');
    }
}
