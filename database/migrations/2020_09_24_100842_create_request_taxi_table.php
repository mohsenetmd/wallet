<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestTaxiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(Schema::hasTable('pl_request_taxi')) return;

        Schema::create('pl_request_taxi', function (Blueprint $table) {
            $table->id();
            $table->string('mobile')->unique();
            $table->string('status')->default(0);
            $table->string('origin_lat');
            $table->string('origin_lng');
            $table->string('destination_lat');
            $table->string('destination_lng');
            $table->string('round_trip');
            $table->string('waiting');
            $table->string('contact_name');
            $table->string('contact_mobile');
            $table->string('service_type');
            $table->integer('created_at');
            $table->integer('updated_at');
            $table->string('ride_id')->unique();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pl_request_taxi');
    }
}
