<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSelectedPlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pl_selected_place', function (Blueprint $table) {
            $table->id();
            $table->string('place_lat');
            $table->string('place_lng');
            $table->string('name');
            $table->integer('category_id');
            $table->integer('city_id');
            $table->string( 'address');
            $table->string(  'tel',11);
            $table->string(  'icon');
            $table->string(  'fa_description');
            $table->string(  'en_description');
            $table->string(  'ohter_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pl_selected_place');
    }
}
