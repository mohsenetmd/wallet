<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CategoryPlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('pl_category_place')) return;
        Schema::create('pl_category_place', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id');
            $table->integer('typecategory_id');
            $table->string ('name');
            $table->string('icon');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('pl_category_place');
    }
}
