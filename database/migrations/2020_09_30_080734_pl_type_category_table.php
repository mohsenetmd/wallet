<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlTypeCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('pl_type_category')) return;
        Schema::create('pl_type_category', function (Blueprint $table) {
            $table->id();
            $table->string ('title');
            $table->boolean('status')->default(1);
            $table->string('icon')->default('icons/6.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('pl_type_category');
    }
}
