<?php

use Illuminate\Database\Seeder;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name=['خرید','شارژ آنلاین','انتقال وجه','دریافت وجه','سود معرف','خرید شارژ'];
        $type_of_transaction=['0','1','0','1','1','0'];
        $percent=['2',NULL,NULL,NULL,NULL,NULL,'2'];
        $profit=['5',NULL,NULL,NULL,NULL,NULL,'5'];
        for($i=0;$i<count($name);$i++){
            \App\Category::create([
                'name'=>$name[$i],
                'type_of_transaction'=>$type_of_transaction[$i],
                'percent'=>$percent[$i],
                'profit'=>$profit[$i]
            ]);
        }
    }
}
