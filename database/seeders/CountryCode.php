<?php

use Illuminate\Database\Seeder;

class CountryCode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CountryCode::class, 50)->create();
    }
}
