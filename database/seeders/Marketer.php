<?php

use Illuminate\Database\Seeder;

class Marketer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<50;$i++) {
            \App\Marketer::create([
            'user_id' => $i
            ]);
        }
    }
}
