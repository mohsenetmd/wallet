<?php

use Illuminate\Database\Seeder;

class PlUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\UserPanel::class, 50)->create();
    }
}
