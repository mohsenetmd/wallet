<?php

use Illuminate\Database\Seeder;

class Transaction extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Transaction::class, 80000)->create();
    }
}
