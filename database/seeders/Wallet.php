<?php

use Illuminate\Database\Seeder;

class Wallet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Wallet::class, 50)->create();
    }
}
