
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `percent` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_of_transaction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,2,'خرید','0','2020-08-26 05:14:24','2020-08-26 05:14:24'),(2,NULL,NULL,'شارژ آنلاین','1','2020-08-26 05:14:24','2020-08-26 05:14:24'),(3,NULL,NULL,'انتقال وجه','0','2020-08-26 05:14:24','2020-08-26 05:14:24'),(4,NULL,NULL,'دریافت وجه','1','2020-08-26 05:14:24','2020-08-26 05:14:24'),(5,NULL,NULL,'سود معرف','1','2020-08-26 05:14:24','2020-08-26 05:14:24');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `country_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_codes` (
  `country_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` int(11) NOT NULL,
  `flag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `country_codes` WRITE;
/*!40000 ALTER TABLE `country_codes` DISABLE KEYS */;
INSERT INTO `country_codes` VALUES (1,734,'http://tromp.com/earum-consequatur-quo-optio-in-et-sed-est','2020-08-26 05:14:24','2020-08-26 05:14:24'),(2,851,'https://www.heidenreich.com/in-vel-vero-possimus-et-ipsa-provident','2020-08-26 05:14:24','2020-08-26 05:14:24'),(3,552,'http://stamm.com/dignissimos-omnis-ab-rerum-et-voluptatem-dolor-molestiae','2020-08-26 05:14:24','2020-08-26 05:14:24'),(4,511,'https://www.harvey.com/ab-soluta-ut-rerum-est-officia-asperiores-amet','2020-08-26 05:14:24','2020-08-26 05:14:24'),(5,545,'http://www.lesch.com/nemo-aut-iste-sunt-doloribus-soluta-veniam','2020-08-26 05:14:24','2020-08-26 05:14:24'),(6,670,'http://langosh.net/magni-animi-a-itaque','2020-08-26 05:14:24','2020-08-26 05:14:24'),(7,154,'http://glover.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(8,236,'http://ritchie.com/excepturi-optio-sed-totam-est-quibusdam','2020-08-26 05:14:24','2020-08-26 05:14:24'),(9,895,'https://flatley.net/quia-reiciendis-et-iusto-cupiditate.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(10,836,'http://cruickshank.com/debitis-laudantium-facere-consectetur','2020-08-26 05:14:24','2020-08-26 05:14:24'),(11,695,'http://www.dibbert.info/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(12,427,'http://www.larson.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(13,682,'http://www.rogahn.com/occaecati-vel-sequi-ipsam-consequatur.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(14,835,'http://www.erdman.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(15,183,'http://brakus.com/nulla-necessitatibus-doloremque-est-perferendis-nihil-architecto-voluptatem-omnis','2020-08-26 05:14:24','2020-08-26 05:14:24'),(16,827,'http://www.fahey.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(17,717,'http://www.fritsch.com/velit-quis-iusto-reiciendis','2020-08-26 05:14:24','2020-08-26 05:14:24'),(18,246,'http://heaney.com/et-ad-cumque-eos-id-eveniet-voluptatibus-inventore-consequatur','2020-08-26 05:14:24','2020-08-26 05:14:24'),(19,296,'https://sauer.com/sunt-sed-aut-inventore-est-id-omnis.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(20,244,'http://www.huel.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(21,355,'https://www.kohler.info/et-est-occaecati-maxime-occaecati-libero-et-blanditiis','2020-08-26 05:14:24','2020-08-26 05:14:24'),(22,398,'https://www.franecki.com/velit-veniam-ut-libero','2020-08-26 05:14:24','2020-08-26 05:14:24'),(23,252,'http://www.streich.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(24,422,'http://yost.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(25,531,'http://www.walker.biz/nisi-iste-assumenda-sed','2020-08-26 05:14:24','2020-08-26 05:14:24'),(26,316,'http://tromp.com/inventore-suscipit-illum-aut-sed','2020-08-26 05:14:24','2020-08-26 05:14:24'),(27,25,'http://www.mcdermott.org/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(28,907,'http://www.rodriguez.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(29,271,'http://wisoky.com/sit-aut-qui-aliquid','2020-08-26 05:14:24','2020-08-26 05:14:24'),(30,817,'https://www.crona.com/error-deserunt-nihil-et-aut','2020-08-26 05:14:24','2020-08-26 05:14:24'),(31,461,'https://www.monahan.biz/non-nihil-est-sit-enim-dolores','2020-08-26 05:14:24','2020-08-26 05:14:24'),(32,74,'http://www.pagac.com/quia-aut-tempore-perspiciatis-qui-pariatur-sapiente-repellat','2020-08-26 05:14:24','2020-08-26 05:14:24'),(33,880,'http://dooley.com/facilis-voluptatem-maiores-quibusdam-nisi','2020-08-26 05:14:24','2020-08-26 05:14:24'),(34,146,'http://www.rice.com/vitae-voluptatem-dolor-in-earum-soluta-necessitatibus','2020-08-26 05:14:24','2020-08-26 05:14:24'),(35,579,'http://www.homenick.info/doloremque-quas-velit-iusto','2020-08-26 05:14:24','2020-08-26 05:14:24'),(36,911,'https://reinger.info/et-dolorem-iste-et-voluptatem.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(37,987,'http://www.turner.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(38,801,'https://mann.net/voluptas-consequatur-cupiditate-natus-rem-non-enim.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(39,637,'http://mckenzie.com/recusandae-sapiente-nostrum-non.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(40,962,'http://www.lebsack.info/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(41,105,'http://lind.com/impedit-odit-accusantium-quo-est','2020-08-26 05:14:24','2020-08-26 05:14:24'),(42,948,'http://www.hirthe.com/reprehenderit-assumenda-laudantium-laudantium-provident-rerum','2020-08-26 05:14:24','2020-08-26 05:14:24'),(43,285,'http://mann.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(44,477,'http://beier.com/ullam-ullam-eveniet-beatae-vitae.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(45,242,'http://www.harber.com/','2020-08-26 05:14:24','2020-08-26 05:14:24'),(46,375,'http://cummerata.com/non-libero-id-illum-doloribus-temporibus-maxime.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(47,991,'https://schroeder.com/adipisci-architecto-nesciunt-voluptas-quia-distinctio-qui-aperiam.html','2020-08-26 05:14:24','2020-08-26 05:14:24'),(48,359,'http://www.armstrong.com/officia-aperiam-ut-expedita-pariatur-et-iste','2020-08-26 05:14:24','2020-08-26 05:14:24'),(49,63,'https://www.kilback.com/totam-distinctio-enim-aut-quas','2020-08-26 05:14:24','2020-08-26 05:14:24'),(50,157,'http://dickinson.org/qui-eos-adipisci-corporis-excepturi-necessitatibus','2020-08-26 05:14:24','2020-08-26 05:14:24');
/*!40000 ALTER TABLE `country_codes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `marketers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketers` (
  `marketer_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`marketer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `marketers` WRITE;
/*!40000 ALTER TABLE `marketers` DISABLE KEYS */;
/*!40000 ALTER TABLE `marketers` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_02_000000_create_marketers_table',1),(2,'2014_10_12_000000_create_users_table',1),(3,'2014_10_12_100000_create_password_resets_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2020_07_21_061904_create_user_profiles_table',1),(6,'2020_07_21_062444_create_wallets_table',1),(7,'2020_07_21_063240_create_categories_table',1),(8,'2020_07_21_063507_create_transactions_table',1),(9,'2020_07_21_064326_create_transaction_details_table',1),(10,'2020_07_22_061935_create_url_generators_table',1),(11,'2020_07_28_043316_create_country_codes_table',1),(12,'2020_08_04_041754_create_sadras_table',1),(13,'2020_08_04_071008_create_user_panels_table',1),(14,'2020_08_04_071746_create_groups_table',1),(15,'2020_08_04_072037_create_user_groups_table',1),(16,'2020_08_04_074838_create_methods_table',1),(17,'2020_08_04_075001_create_permission_users_table',1),(18,'2020_08_04_075915_create_permission_groups_table',1),(19,'2020_08_12_055112_add_name_to_pl_methods_table',1),(20,'2020_08_12_060045_add_name_to_pl_methods',1),(21,'2020_08_19_080157_create_sessions_table',1),(22,'2020_08_22_043410_create_payments_table',1),(23,'2020_08_24_092441_create_mobile_devices_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mobile_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_devices` (
  `mobile_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manufacture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sdk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`mobile_id`),
  KEY `mobile_devices_user_id_foreign` (`user_id`),
  CONSTRAINT `mobile_devices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mobile_devices` WRITE;
/*!40000 ALTER TABLE `mobile_devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `mobile_devices` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_transaction_id_foreign` (`transaction_id`),
  CONSTRAINT `payments_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pl_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_groups` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pl_groups` WRITE;
/*!40000 ALTER TABLE `pl_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `pl_groups` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pl_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_methods` (
  `method_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pl_methods` WRITE;
/*!40000 ALTER TABLE `pl_methods` DISABLE KEYS */;
/*!40000 ALTER TABLE `pl_methods` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pl_permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_permission_groups` (
  `permission_group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `method_id` bigint(20) unsigned NOT NULL,
  `group_id` bigint(20) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`permission_group_id`),
  KEY `pl_permission_groups_method_id_group_id_index` (`method_id`,`group_id`),
  KEY `pl_permission_groups_group_id_foreign` (`group_id`),
  CONSTRAINT `pl_permission_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `pl_groups` (`group_id`) ON DELETE CASCADE,
  CONSTRAINT `pl_permission_groups_method_id_foreign` FOREIGN KEY (`method_id`) REFERENCES `pl_methods` (`method_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pl_permission_groups` WRITE;
/*!40000 ALTER TABLE `pl_permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `pl_permission_groups` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pl_permission_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_permission_users` (
  `permission_user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `method_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`permission_user_id`),
  KEY `pl_permission_users_method_id_foreign` (`method_id`),
  KEY `pl_permission_users_user_id_foreign` (`user_id`),
  CONSTRAINT `pl_permission_users_method_id_foreign` FOREIGN KEY (`method_id`) REFERENCES `pl_methods` (`method_id`) ON DELETE CASCADE,
  CONSTRAINT `pl_permission_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `pl_users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pl_permission_users` WRITE;
/*!40000 ALTER TABLE `pl_permission_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `pl_permission_users` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pl_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_user_groups` (
  `user_group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `group_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_group_id`),
  KEY `pl_user_groups_user_id_foreign` (`user_id`),
  KEY `pl_user_groups_group_id_foreign` (`group_id`),
  CONSTRAINT `pl_user_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `pl_groups` (`group_id`) ON DELETE CASCADE,
  CONSTRAINT `pl_user_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `pl_users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pl_user_groups` WRITE;
/*!40000 ALTER TABLE `pl_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `pl_user_groups` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` tinyint(4) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `pl_users_mobile_unique` (`mobile`),
  UNIQUE KEY `pl_users_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pl_users` WRITE;
/*!40000 ALTER TABLE `pl_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `pl_users` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `sadras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sadras` (
  `sadra_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `hi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sadra_id`),
  KEY `sadras_user_id_foreign` (`user_id`),
  CONSTRAINT `sadras_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `sadras` WRITE;
/*!40000 ALTER TABLE `sadras` DISABLE KEYS */;
/*!40000 ALTER TABLE `sadras` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_details` (
  `transaction_detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`transaction_detail_id`),
  KEY `transaction_details_transaction_id_foreign` (`transaction_id`),
  KEY `transaction_details_category_id_foreign` (`category_id`),
  CONSTRAINT `transaction_details_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE,
  CONSTRAINT `transaction_details_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `transaction_details` WRITE;
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_of_transaction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `payer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `transactions_user_id_foreign` (`user_id`),
  KEY `transactions_category_id_foreign` (`category_id`),
  CONSTRAINT `transactions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE,
  CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=801 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,13,3,NULL,'98000','0','0','4373673901',NULL,1598435065,1598435065),(2,10,1,NULL,'13000','0','1','3870117997',NULL,1598435065,1598435065),(3,7,3,NULL,'66000','0','0','6290339501',NULL,1598435065,1598435065),(4,30,1,NULL,'76000','0','1','5198034282',NULL,1598435065,1598435065),(5,3,1,NULL,'97000','0','0','6458551406',NULL,1598435065,1598435065),(6,11,3,NULL,'98000','0','1','8129658323',NULL,1598435065,1598435065),(7,37,1,NULL,'12000','0','0','1446505122',NULL,1598435065,1598435065),(8,35,3,NULL,'27000','0','0','4963297434',NULL,1598435065,1598435065),(9,47,1,NULL,'80000','0','0','6339973862',NULL,1598435065,1598435065),(10,8,2,NULL,'3000','1','1','1867261185',NULL,1598435065,1598435065),(11,19,3,NULL,'5000','0','1','1932231894',NULL,1598435065,1598435065),(12,12,2,NULL,'43000','1','0','8237141402',NULL,1598435065,1598435065),(13,15,1,NULL,'5000','0','0','1548405128',NULL,1598435065,1598435065),(14,36,3,NULL,'17000','0','1','9368416369',NULL,1598435065,1598435065),(15,10,2,NULL,'47000','1','1','7872201202',NULL,1598435065,1598435065),(16,16,2,NULL,'7000','1','0','2306126849',NULL,1598435065,1598435065),(17,4,3,NULL,'77000','0','0','3539130283',NULL,1598435065,1598435065),(18,43,3,NULL,'64000','0','0','1306057969',NULL,1598435065,1598435065),(19,41,1,NULL,'33000','0','0','7166596898',NULL,1598435065,1598435065),(20,10,3,NULL,'40000','0','1','7862597867',NULL,1598435065,1598435065),(21,11,2,NULL,'81000','1','0','8259517572',NULL,1598435065,1598435065),(22,7,3,NULL,'51000','0','0','8809580946',NULL,1598435065,1598435065),(23,49,1,NULL,'8000','0','0','5594291531',NULL,1598435065,1598435065),(24,15,3,NULL,'68000','0','1','4434485640',NULL,1598435065,1598435065),(25,19,1,NULL,'9000','0','1','4610331168',NULL,1598435065,1598435065),(26,1,1,NULL,'75000','0','1','3551114640',NULL,1598435065,1598435065),(27,22,1,NULL,'20000','0','0','4575175238',NULL,1598435065,1598435065),(28,48,1,NULL,'100000','0','0','7344198788',NULL,1598435065,1598435065),(29,34,1,NULL,'67000','0','0','4970626303',NULL,1598435065,1598435065),(30,26,1,NULL,'24000','0','1','8348610740',NULL,1598435065,1598435065),(31,2,2,NULL,'21000','1','1','2797815783',NULL,1598435065,1598435065),(32,24,2,NULL,'3000','1','0','3007604274',NULL,1598435065,1598435065),(33,20,2,NULL,'20000','1','0','8869373584',NULL,1598435065,1598435065),(34,1,3,NULL,'45000','0','0','9807055584',NULL,1598435065,1598435065),(35,2,3,NULL,'77000','0','0','8659406767',NULL,1598435065,1598435065),(36,22,3,NULL,'58000','0','1','6004821291',NULL,1598435065,1598435065),(37,16,3,NULL,'76000','0','0','3179051179',NULL,1598435065,1598435065),(38,36,2,NULL,'61000','1','1','6849705332',NULL,1598435065,1598435065),(39,40,1,NULL,'93000','0','1','9712448659',NULL,1598435065,1598435065),(40,33,3,NULL,'48000','0','1','3631909736',NULL,1598435065,1598435065),(41,43,3,NULL,'37000','0','1','2181724908',NULL,1598435065,1598435065),(42,50,2,NULL,'9000','1','0','1319003939',NULL,1598435065,1598435065),(43,20,2,NULL,'12000','1','1','7585222279',NULL,1598435065,1598435065),(44,21,1,NULL,'61000','0','0','2690797475',NULL,1598435065,1598435065),(45,14,1,NULL,'28000','0','0','9255237312',NULL,1598435065,1598435065),(46,37,1,NULL,'26000','0','0','8977320880',NULL,1598435065,1598435065),(47,28,2,NULL,'28000','1','1','9034533197',NULL,1598435065,1598435065),(48,26,2,NULL,'87000','1','0','6352842969',NULL,1598435065,1598435065),(49,38,3,NULL,'94000','0','0','8389146311',NULL,1598435065,1598435065),(50,46,3,NULL,'84000','0','1','9245866667',NULL,1598435065,1598435065),(51,11,2,NULL,'29000','1','0','8030993405',NULL,1598435065,1598435065),(52,42,1,NULL,'15000','0','1','6013108895',NULL,1598435065,1598435065),(53,3,1,NULL,'96000','0','1','7942121477',NULL,1598435065,1598435065),(54,18,1,NULL,'91000','0','0','2338771976',NULL,1598435065,1598435065),(55,5,2,NULL,'49000','1','0','6165723018',NULL,1598435065,1598435065),(56,32,1,NULL,'26000','0','0','1829990264',NULL,1598435065,1598435065),(57,36,3,NULL,'87000','0','0','3690507256',NULL,1598435065,1598435065),(58,34,2,NULL,'56000','1','0','4139893758',NULL,1598435065,1598435065),(59,47,2,NULL,'25000','1','0','8807747724',NULL,1598435065,1598435065),(60,3,1,NULL,'42000','0','0','4432683146',NULL,1598435065,1598435065),(61,8,1,NULL,'82000','0','0','5086797032',NULL,1598435065,1598435065),(62,47,2,NULL,'26000','1','1','4920023111',NULL,1598435065,1598435065),(63,23,3,NULL,'38000','0','1','5619697176',NULL,1598435065,1598435065),(64,39,1,NULL,'57000','0','0','3454583969',NULL,1598435065,1598435065),(65,37,1,NULL,'34000','0','0','3095754887',NULL,1598435065,1598435065),(66,50,1,NULL,'85000','0','1','4247573510',NULL,1598435065,1598435065),(67,10,1,NULL,'75000','0','1','8339516303',NULL,1598435065,1598435065),(68,31,3,NULL,'33000','0','1','5601040902',NULL,1598435065,1598435065),(69,32,1,NULL,'34000','0','0','9512061025',NULL,1598435065,1598435065),(70,18,1,NULL,'9000','0','0','8298364115',NULL,1598435065,1598435065),(71,12,2,NULL,'93000','1','1','7030690308',NULL,1598435065,1598435065),(72,35,1,NULL,'54000','0','0','4890821098',NULL,1598435065,1598435065),(73,19,1,NULL,'22000','0','1','9489485918',NULL,1598435065,1598435065),(74,6,3,NULL,'2000','0','1','4011896397',NULL,1598435065,1598435065),(75,37,3,NULL,'49000','0','0','2607148844',NULL,1598435065,1598435065),(76,39,3,NULL,'63000','0','0','3762601289',NULL,1598435065,1598435065),(77,14,3,NULL,'38000','0','1','4238816050',NULL,1598435065,1598435065),(78,13,1,NULL,'45000','0','0','3061339053',NULL,1598435065,1598435065),(79,45,1,NULL,'2000','0','1','4444764937',NULL,1598435065,1598435065),(80,20,1,NULL,'37000','0','0','7262419183',NULL,1598435065,1598435065),(81,22,3,NULL,'81000','0','1','1316750611',NULL,1598435065,1598435065),(82,49,2,NULL,'12000','1','0','7201190879',NULL,1598435065,1598435065),(83,38,3,NULL,'43000','0','0','4614642377',NULL,1598435065,1598435065),(84,28,2,NULL,'75000','1','0','4461518449',NULL,1598435065,1598435065),(85,10,1,NULL,'67000','0','1','3285723084',NULL,1598435065,1598435065),(86,14,3,NULL,'30000','0','0','8958322068',NULL,1598435065,1598435065),(87,12,1,NULL,'55000','0','1','5118388943',NULL,1598435065,1598435065),(88,27,2,NULL,'31000','1','0','9294569365',NULL,1598435065,1598435065),(89,1,2,NULL,'31000','1','1','2695379755',NULL,1598435065,1598435065),(90,11,3,NULL,'86000','0','0','8323691049',NULL,1598435065,1598435065),(91,42,2,NULL,'24000','1','0','8242518305',NULL,1598435065,1598435065),(92,21,3,NULL,'52000','0','0','7716530623',NULL,1598435065,1598435065),(93,18,1,NULL,'80000','0','1','3819241244',NULL,1598435065,1598435065),(94,44,1,NULL,'6000','0','1','2613481391',NULL,1598435065,1598435065),(95,23,3,NULL,'51000','0','0','3980268637',NULL,1598435065,1598435065),(96,38,2,NULL,'69000','1','0','7230849979',NULL,1598435065,1598435065),(97,14,2,NULL,'7000','1','1','4005831685',NULL,1598435065,1598435065),(98,10,2,NULL,'55000','1','1','9033229981',NULL,1598435065,1598435065),(99,36,3,NULL,'79000','0','1','6988365148',NULL,1598435065,1598435065),(100,10,3,NULL,'3000','0','1','8081473664',NULL,1598435065,1598435065),(101,6,3,NULL,'75000','0','1','1603044925',NULL,1598435065,1598435065),(102,23,3,NULL,'10000','0','1','6763950917',NULL,1598435065,1598435065),(103,3,3,NULL,'81000','0','0','3007887293',NULL,1598435065,1598435065),(104,40,3,NULL,'88000','0','0','9695357388',NULL,1598435065,1598435065),(105,2,1,NULL,'37000','0','0','2499472774',NULL,1598435065,1598435065),(106,6,3,NULL,'50000','0','1','3509558027',NULL,1598435065,1598435065),(107,27,2,NULL,'39000','1','0','6693570462',NULL,1598435065,1598435065),(108,16,3,NULL,'99000','0','1','9187031682',NULL,1598435065,1598435065),(109,38,3,NULL,'78000','0','1','8022906038',NULL,1598435065,1598435065),(110,8,2,NULL,'31000','1','1','8173321809',NULL,1598435065,1598435065),(111,23,3,NULL,'30000','0','0','8924149829',NULL,1598435065,1598435065),(112,12,3,NULL,'31000','0','1','5379909906',NULL,1598435065,1598435065),(113,29,2,NULL,'32000','1','1','7328625627',NULL,1598435065,1598435065),(114,41,1,NULL,'86000','0','0','9424344016',NULL,1598435065,1598435065),(115,15,2,NULL,'17000','1','0','2213633878',NULL,1598435065,1598435065),(116,13,3,NULL,'45000','0','0','5189504158',NULL,1598435065,1598435065),(117,16,2,NULL,'83000','1','0','8799731889',NULL,1598435065,1598435065),(118,24,1,NULL,'4000','0','0','2800957375',NULL,1598435065,1598435065),(119,40,2,NULL,'56000','1','1','2525024164',NULL,1598435065,1598435065),(120,47,2,NULL,'15000','1','1','9746770681',NULL,1598435065,1598435065),(121,45,2,NULL,'90000','1','1','7788696731',NULL,1598435065,1598435065),(122,8,3,NULL,'56000','0','0','8238889791',NULL,1598435065,1598435065),(123,19,1,NULL,'80000','0','0','9399694654',NULL,1598435065,1598435065),(124,1,1,NULL,'81000','0','1','5486195343',NULL,1598435065,1598435065),(125,48,2,NULL,'31000','1','0','9819171055',NULL,1598435065,1598435065),(126,18,3,NULL,'3000','0','1','9664151449',NULL,1598435065,1598435065),(127,13,3,NULL,'52000','0','0','1891224169',NULL,1598435065,1598435065),(128,40,2,NULL,'17000','1','1','7956167195',NULL,1598435065,1598435065),(129,37,3,NULL,'62000','0','0','1556172891',NULL,1598435065,1598435065),(130,8,1,NULL,'27000','0','0','9693149066',NULL,1598435065,1598435065),(131,42,1,NULL,'95000','0','0','3121252926',NULL,1598435065,1598435065),(132,10,3,NULL,'55000','0','1','7813097188',NULL,1598435065,1598435065),(133,19,1,NULL,'22000','0','1','3676039068',NULL,1598435065,1598435065),(134,28,1,NULL,'69000','0','0','4975658898',NULL,1598435065,1598435065),(135,47,1,NULL,'41000','0','1','9872062477',NULL,1598435065,1598435065),(136,14,2,NULL,'98000','1','1','1326866254',NULL,1598435065,1598435065),(137,22,2,NULL,'28000','1','1','1579325952',NULL,1598435065,1598435065),(138,34,2,NULL,'75000','1','0','2000175731',NULL,1598435065,1598435065),(139,25,1,NULL,'55000','0','0','3767287896',NULL,1598435065,1598435065),(140,43,3,NULL,'63000','0','0','8959762255',NULL,1598435065,1598435065),(141,39,3,NULL,'37000','0','0','8488317544',NULL,1598435065,1598435065),(142,38,3,NULL,'28000','0','1','4835855929',NULL,1598435065,1598435065),(143,18,1,NULL,'38000','0','1','6629088182',NULL,1598435065,1598435065),(144,8,1,NULL,'40000','0','1','3676356072',NULL,1598435065,1598435065),(145,34,2,NULL,'81000','1','0','7280433712',NULL,1598435065,1598435065),(146,24,1,NULL,'9000','0','1','9224319334',NULL,1598435065,1598435065),(147,46,2,NULL,'34000','1','1','8641665534',NULL,1598435065,1598435065),(148,7,3,NULL,'44000','0','1','2504133410',NULL,1598435065,1598435065),(149,34,1,NULL,'5000','0','1','5374987839',NULL,1598435065,1598435065),(150,50,1,NULL,'76000','0','0','2497351341',NULL,1598435065,1598435065),(151,23,1,NULL,'18000','0','1','1896308007',NULL,1598435065,1598435065),(152,15,2,NULL,'29000','1','1','1278777848',NULL,1598435065,1598435065),(153,17,3,NULL,'53000','0','0','2950298621',NULL,1598435065,1598435065),(154,24,2,NULL,'7000','1','0','4791884496',NULL,1598435065,1598435065),(155,33,2,NULL,'13000','1','0','2212494991',NULL,1598435065,1598435065),(156,44,1,NULL,'36000','0','1','7431932895',NULL,1598435065,1598435065),(157,35,3,NULL,'51000','0','0','1719899704',NULL,1598435065,1598435065),(158,11,1,NULL,'41000','0','1','4659650827',NULL,1598435065,1598435065),(159,33,2,NULL,'32000','1','1','9838475701',NULL,1598435065,1598435065),(160,41,3,NULL,'88000','0','1','5481859031',NULL,1598435065,1598435065),(161,5,1,NULL,'55000','0','0','2120348120',NULL,1598435065,1598435065),(162,35,1,NULL,'8000','0','1','5831667075',NULL,1598435065,1598435065),(163,37,3,NULL,'79000','0','0','7641858015',NULL,1598435065,1598435065),(164,33,1,NULL,'72000','0','1','3165483459',NULL,1598435065,1598435065),(165,18,3,NULL,'38000','0','1','7513696662',NULL,1598435065,1598435065),(166,15,3,NULL,'6000','0','0','8158480613',NULL,1598435065,1598435065),(167,35,1,NULL,'70000','0','1','6891071616',NULL,1598435065,1598435065),(168,24,1,NULL,'30000','0','0','3824598012',NULL,1598435065,1598435065),(169,39,2,NULL,'64000','1','0','7120988991',NULL,1598435065,1598435065),(170,6,1,NULL,'95000','0','0','7326682575',NULL,1598435065,1598435065),(171,44,3,NULL,'6000','0','0','8881208684',NULL,1598435065,1598435065),(172,34,3,NULL,'2000','0','0','2254145801',NULL,1598435065,1598435065),(173,45,2,NULL,'75000','1','1','5163638611',NULL,1598435065,1598435065),(174,31,2,NULL,'4000','1','0','4227945946',NULL,1598435065,1598435065),(175,45,1,NULL,'81000','0','0','5351303772',NULL,1598435065,1598435065),(176,7,1,NULL,'18000','0','0','7759951493',NULL,1598435065,1598435065),(177,44,3,NULL,'5000','0','0','1786566695',NULL,1598435065,1598435065),(178,18,2,NULL,'17000','1','1','7578568319',NULL,1598435065,1598435065),(179,29,3,NULL,'24000','0','0','2172533088',NULL,1598435065,1598435065),(180,2,1,NULL,'12000','0','0','6992092507',NULL,1598435065,1598435065),(181,42,1,NULL,'73000','0','0','1718335913',NULL,1598435065,1598435065),(182,13,3,NULL,'58000','0','1','5224619810',NULL,1598435065,1598435065),(183,32,2,NULL,'96000','1','0','5264688329',NULL,1598435065,1598435065),(184,2,3,NULL,'11000','0','1','1581625072',NULL,1598435065,1598435065),(185,21,2,NULL,'93000','1','0','4075346674',NULL,1598435065,1598435065),(186,18,1,NULL,'17000','0','1','3272754304',NULL,1598435065,1598435065),(187,27,2,NULL,'81000','1','1','4210293602',NULL,1598435065,1598435065),(188,49,2,NULL,'44000','1','1','8673522267',NULL,1598435065,1598435065),(189,28,1,NULL,'37000','0','0','7089017115',NULL,1598435065,1598435065),(190,44,1,NULL,'64000','0','0','2051045002',NULL,1598435065,1598435065),(191,43,2,NULL,'85000','1','1','8099652707',NULL,1598435065,1598435065),(192,40,3,NULL,'11000','0','0','3226896248',NULL,1598435065,1598435065),(193,13,1,NULL,'98000','0','1','1617827838',NULL,1598435065,1598435065),(194,1,3,NULL,'99000','0','1','2169556600',NULL,1598435065,1598435065),(195,8,1,NULL,'61000','0','0','1767503227',NULL,1598435065,1598435065),(196,45,2,NULL,'77000','1','0','7376042781',NULL,1598435065,1598435065),(197,26,2,NULL,'83000','1','1','7973030671',NULL,1598435065,1598435065),(198,8,3,NULL,'19000','0','1','3657071388',NULL,1598435065,1598435065),(199,25,2,NULL,'1000','1','1','4162315551',NULL,1598435065,1598435065),(200,14,3,NULL,'15000','0','1','4497888160',NULL,1598435065,1598435065),(201,9,3,NULL,'98000','0','1','7670286896',NULL,1598435065,1598435065),(202,7,2,NULL,'96000','1','0','4935742072',NULL,1598435065,1598435065),(203,35,2,NULL,'26000','1','0','5969267800',NULL,1598435065,1598435065),(204,50,1,NULL,'94000','0','1','3486309368',NULL,1598435065,1598435065),(205,33,2,NULL,'94000','1','1','5207387546',NULL,1598435065,1598435065),(206,27,2,NULL,'24000','1','1','5690345815',NULL,1598435065,1598435065),(207,28,2,NULL,'68000','1','1','1662201034',NULL,1598435065,1598435065),(208,43,3,NULL,'13000','0','0','6654127230',NULL,1598435065,1598435065),(209,24,3,NULL,'36000','0','1','5682826356',NULL,1598435065,1598435065),(210,36,2,NULL,'94000','1','1','4087296751',NULL,1598435065,1598435065),(211,20,3,NULL,'44000','0','1','6200981213',NULL,1598435065,1598435065),(212,23,3,NULL,'6000','0','1','7360360831',NULL,1598435065,1598435065),(213,34,3,NULL,'88000','0','0','2183808352',NULL,1598435065,1598435065),(214,15,3,NULL,'46000','0','0','1619019200',NULL,1598435065,1598435065),(215,39,2,NULL,'12000','1','1','2421390759',NULL,1598435065,1598435065),(216,25,2,NULL,'39000','1','0','8040145929',NULL,1598435065,1598435065),(217,15,2,NULL,'92000','1','0','8967923619',NULL,1598435065,1598435065),(218,7,3,NULL,'20000','0','1','1852990238',NULL,1598435065,1598435065),(219,48,1,NULL,'65000','0','0','6609719889',NULL,1598435065,1598435065),(220,50,2,NULL,'18000','1','1','3475351257',NULL,1598435065,1598435065),(221,30,2,NULL,'38000','1','1','2792763499',NULL,1598435065,1598435065),(222,48,1,NULL,'90000','0','0','3978239218',NULL,1598435065,1598435065),(223,47,1,NULL,'74000','0','1','1647182219',NULL,1598435065,1598435065),(224,49,1,NULL,'85000','0','0','6391199504',NULL,1598435065,1598435065),(225,31,2,NULL,'90000','1','0','9687457131',NULL,1598435065,1598435065),(226,48,1,NULL,'26000','0','1','8294200522',NULL,1598435065,1598435065),(227,16,3,NULL,'58000','0','1','4593842294',NULL,1598435065,1598435065),(228,8,2,NULL,'97000','1','0','5768499846',NULL,1598435065,1598435065),(229,23,1,NULL,'6000','0','0','4480063612',NULL,1598435065,1598435065),(230,24,1,NULL,'84000','0','1','6351130562',NULL,1598435065,1598435065),(231,48,1,NULL,'17000','0','1','1918695803',NULL,1598435065,1598435065),(232,9,2,NULL,'3000','1','0','9413725646',NULL,1598435065,1598435065),(233,13,3,NULL,'27000','0','1','6054578746',NULL,1598435065,1598435065),(234,12,2,NULL,'94000','1','0','7482717646',NULL,1598435065,1598435065),(235,49,2,NULL,'52000','1','1','2909929014',NULL,1598435065,1598435065),(236,38,3,NULL,'40000','0','1','4889503424',NULL,1598435065,1598435065),(237,28,2,NULL,'57000','1','1','2713635664',NULL,1598435065,1598435065),(238,17,3,NULL,'58000','0','0','1755167247',NULL,1598435065,1598435065),(239,34,3,NULL,'25000','0','0','6224555983',NULL,1598435065,1598435065),(240,33,2,NULL,'12000','1','1','7863107514',NULL,1598435065,1598435065),(241,24,3,NULL,'24000','0','1','5206618193',NULL,1598435065,1598435065),(242,5,3,NULL,'28000','0','1','6719340363',NULL,1598435065,1598435065),(243,18,3,NULL,'57000','0','1','5671819901',NULL,1598435065,1598435065),(244,30,3,NULL,'34000','0','0','1683379222',NULL,1598435065,1598435065),(245,10,2,NULL,'40000','1','1','7264387667',NULL,1598435065,1598435065),(246,11,1,NULL,'72000','0','0','2321737374',NULL,1598435065,1598435065),(247,43,2,NULL,'82000','1','1','7881788063',NULL,1598435065,1598435065),(248,4,3,NULL,'79000','0','0','2052687324',NULL,1598435065,1598435065),(249,36,3,NULL,'91000','0','0','7427859981',NULL,1598435065,1598435065),(250,32,3,NULL,'99000','0','0','7519572901',NULL,1598435065,1598435065),(251,23,3,NULL,'41000','0','0','2043346734',NULL,1598435065,1598435065),(252,23,1,NULL,'48000','0','1','8825478080',NULL,1598435065,1598435065),(253,40,3,NULL,'25000','0','0','7569122794',NULL,1598435065,1598435065),(254,49,1,NULL,'15000','0','1','5075264484',NULL,1598435065,1598435065),(255,32,2,NULL,'34000','1','1','9725561474',NULL,1598435065,1598435065),(256,3,2,NULL,'59000','1','1','3265292432',NULL,1598435065,1598435065),(257,6,3,NULL,'67000','0','1','6818051035',NULL,1598435065,1598435065),(258,10,1,NULL,'76000','0','0','8930456415',NULL,1598435065,1598435065),(259,32,2,NULL,'26000','1','0','5097185486',NULL,1598435065,1598435065),(260,18,1,NULL,'27000','0','0','1447508676',NULL,1598435065,1598435065),(261,46,1,NULL,'58000','0','1','1883299404',NULL,1598435065,1598435065),(262,14,3,NULL,'30000','0','1','6317131816',NULL,1598435065,1598435065),(263,19,2,NULL,'96000','1','1','3431436274',NULL,1598435065,1598435065),(264,21,1,NULL,'4000','0','1','3391466022',NULL,1598435065,1598435065),(265,24,1,NULL,'66000','0','0','8546707241',NULL,1598435065,1598435065),(266,31,1,NULL,'43000','0','1','7465540536',NULL,1598435065,1598435065),(267,18,2,NULL,'35000','1','1','1933380244',NULL,1598435065,1598435065),(268,50,3,NULL,'74000','0','1','4307659894',NULL,1598435065,1598435065),(269,8,2,NULL,'84000','1','0','9172933767',NULL,1598435065,1598435065),(270,5,1,NULL,'46000','0','0','4638898987',NULL,1598435065,1598435065),(271,33,3,NULL,'61000','0','0','8905425422',NULL,1598435065,1598435065),(272,36,1,NULL,'100000','0','0','8817900760',NULL,1598435065,1598435065),(273,47,3,NULL,'92000','0','0','6654099585',NULL,1598435065,1598435065),(274,48,3,NULL,'31000','0','1','1378190709',NULL,1598435065,1598435065),(275,3,2,NULL,'74000','1','0','6546507136',NULL,1598435065,1598435065),(276,32,1,NULL,'86000','0','0','3613382013',NULL,1598435065,1598435065),(277,46,2,NULL,'21000','1','0','1619877383',NULL,1598435065,1598435065),(278,26,1,NULL,'53000','0','0','4230957914',NULL,1598435065,1598435065),(279,8,3,NULL,'1000','0','1','4789299203',NULL,1598435065,1598435065),(280,21,1,NULL,'57000','0','1','8718232467',NULL,1598435065,1598435065),(281,27,2,NULL,'33000','1','0','5693453213',NULL,1598435065,1598435065),(282,27,2,NULL,'76000','1','0','4316125074',NULL,1598435065,1598435065),(283,39,1,NULL,'87000','0','1','4868313873',NULL,1598435065,1598435065),(284,41,2,NULL,'36000','1','0','7574453333',NULL,1598435065,1598435065),(285,2,2,NULL,'31000','1','0','5177208899',NULL,1598435065,1598435065),(286,21,2,NULL,'8000','1','0','6862184571',NULL,1598435065,1598435065),(287,12,3,NULL,'71000','0','1','7406070452',NULL,1598435065,1598435065),(288,43,3,NULL,'49000','0','1','8840617925',NULL,1598435065,1598435065),(289,42,1,NULL,'48000','0','0','2456052561',NULL,1598435065,1598435065),(290,8,2,NULL,'55000','1','1','6262335208',NULL,1598435065,1598435065),(291,44,3,NULL,'64000','0','0','9617215459',NULL,1598435065,1598435065),(292,6,3,NULL,'60000','0','1','2443657699',NULL,1598435065,1598435065),(293,2,1,NULL,'56000','0','1','6526110259',NULL,1598435065,1598435065),(294,6,2,NULL,'72000','1','1','6466704596',NULL,1598435065,1598435065),(295,8,1,NULL,'97000','0','1','6816421867',NULL,1598435065,1598435065),(296,42,1,NULL,'31000','0','0','3451238740',NULL,1598435065,1598435065),(297,17,1,NULL,'35000','0','0','3535242411',NULL,1598435065,1598435065),(298,44,3,NULL,'26000','0','1','6626924806',NULL,1598435065,1598435065),(299,41,3,NULL,'54000','0','0','2313389975',NULL,1598435065,1598435065),(300,10,1,NULL,'25000','0','0','2685444208',NULL,1598435065,1598435065),(301,34,3,NULL,'70000','0','0','3266977994',NULL,1598435065,1598435065),(302,48,1,NULL,'53000','0','1','4393275618',NULL,1598435065,1598435065),(303,27,2,NULL,'4000','1','1','6201451695',NULL,1598435065,1598435065),(304,33,3,NULL,'94000','0','1','3772540893',NULL,1598435065,1598435065),(305,35,3,NULL,'95000','0','1','4058545405',NULL,1598435065,1598435065),(306,4,1,NULL,'39000','0','1','2571400685',NULL,1598435065,1598435065),(307,4,1,NULL,'87000','0','1','1340838291',NULL,1598435065,1598435065),(308,14,3,NULL,'39000','0','0','6681570709',NULL,1598435065,1598435065),(309,18,3,NULL,'93000','0','1','2469194830',NULL,1598435065,1598435065),(310,6,3,NULL,'36000','0','1','3052455537',NULL,1598435065,1598435065),(311,1,3,NULL,'56000','0','0','5847762438',NULL,1598435065,1598435065),(312,19,1,NULL,'47000','0','0','2438794958',NULL,1598435065,1598435065),(313,17,2,NULL,'12000','1','0','8032630806',NULL,1598435065,1598435065),(314,21,1,NULL,'49000','0','1','5391696085',NULL,1598435065,1598435065),(315,14,3,NULL,'60000','0','1','6355504190',NULL,1598435066,1598435066),(316,27,1,NULL,'50000','0','0','7037696910',NULL,1598435066,1598435066),(317,35,1,NULL,'5000','0','0','4132754714',NULL,1598435066,1598435066),(318,10,3,NULL,'17000','0','0','2088981186',NULL,1598435066,1598435066),(319,31,2,NULL,'78000','1','1','8078943629',NULL,1598435066,1598435066),(320,24,3,NULL,'16000','0','0','3531450324',NULL,1598435066,1598435066),(321,19,2,NULL,'67000','1','0','9483689446',NULL,1598435066,1598435066),(322,1,2,NULL,'88000','1','1','6182249239',NULL,1598435066,1598435066),(323,38,3,NULL,'26000','0','0','1696581011',NULL,1598435066,1598435066),(324,11,1,NULL,'2000','0','0','2071448408',NULL,1598435066,1598435066),(325,49,2,NULL,'95000','1','0','5413400651',NULL,1598435066,1598435066),(326,4,1,NULL,'5000','0','0','4224057761',NULL,1598435066,1598435066),(327,6,1,NULL,'11000','0','0','1305538816',NULL,1598435066,1598435066),(328,10,1,NULL,'78000','0','1','3281696286',NULL,1598435066,1598435066),(329,9,2,NULL,'90000','1','1','2652942273',NULL,1598435066,1598435066),(330,21,3,NULL,'13000','0','1','8813501945',NULL,1598435066,1598435066),(331,26,3,NULL,'51000','0','0','8118571177',NULL,1598435066,1598435066),(332,43,1,NULL,'90000','0','1','1786907745',NULL,1598435066,1598435066),(333,7,3,NULL,'60000','0','0','7249082702',NULL,1598435066,1598435066),(334,43,3,NULL,'96000','0','1','9419063551',NULL,1598435066,1598435066),(335,15,3,NULL,'67000','0','1','2400014593',NULL,1598435066,1598435066),(336,8,2,NULL,'36000','1','0','3139229935',NULL,1598435066,1598435066),(337,24,2,NULL,'17000','1','1','1665439486',NULL,1598435066,1598435066),(338,46,1,NULL,'96000','0','1','4561047317',NULL,1598435066,1598435066),(339,34,2,NULL,'32000','1','0','4734867712',NULL,1598435066,1598435066),(340,14,1,NULL,'3000','0','1','8308217627',NULL,1598435066,1598435066),(341,44,1,NULL,'55000','0','0','1535417652',NULL,1598435066,1598435066),(342,18,3,NULL,'89000','0','0','2812580977',NULL,1598435066,1598435066),(343,6,2,NULL,'58000','1','1','4903728110',NULL,1598435066,1598435066),(344,44,3,NULL,'23000','0','0','2084345899',NULL,1598435066,1598435066),(345,19,2,NULL,'94000','1','0','3560467323',NULL,1598435066,1598435066),(346,11,1,NULL,'78000','0','0','6071930645',NULL,1598435066,1598435066),(347,18,1,NULL,'2000','0','0','9814775490',NULL,1598435066,1598435066),(348,4,3,NULL,'15000','0','0','9498244760',NULL,1598435066,1598435066),(349,2,1,NULL,'59000','0','0','5589122562',NULL,1598435066,1598435066),(350,45,3,NULL,'67000','0','1','8900693267',NULL,1598435066,1598435066),(351,37,2,NULL,'11000','1','1','9519358044',NULL,1598435066,1598435066),(352,31,3,NULL,'92000','0','0','5438194138',NULL,1598435066,1598435066),(353,26,3,NULL,'95000','0','1','2263708401',NULL,1598435066,1598435066),(354,36,1,NULL,'32000','0','1','2974218270',NULL,1598435066,1598435066),(355,8,1,NULL,'99000','0','1','6088650224',NULL,1598435066,1598435066),(356,33,3,NULL,'45000','0','1','2926793568',NULL,1598435066,1598435066),(357,43,1,NULL,'51000','0','1','6689464702',NULL,1598435066,1598435066),(358,32,3,NULL,'37000','0','1','3179199307',NULL,1598435066,1598435066),(359,47,1,NULL,'32000','0','0','8713815300',NULL,1598435066,1598435066),(360,3,3,NULL,'70000','0','0','5615135822',NULL,1598435066,1598435066),(361,27,3,NULL,'90000','0','1','2842863980',NULL,1598435066,1598435066),(362,39,3,NULL,'43000','0','1','6753359975',NULL,1598435066,1598435066),(363,48,3,NULL,'40000','0','1','1236805254',NULL,1598435066,1598435066),(364,39,3,NULL,'93000','0','1','5326831164',NULL,1598435066,1598435066),(365,13,2,NULL,'77000','1','1','3064059613',NULL,1598435066,1598435066),(366,5,3,NULL,'57000','0','1','6385779642',NULL,1598435066,1598435066),(367,18,2,NULL,'20000','1','0','5432661000',NULL,1598435066,1598435066),(368,45,2,NULL,'21000','1','0','6217173545',NULL,1598435066,1598435066),(369,41,2,NULL,'78000','1','0','5506710044',NULL,1598435066,1598435066),(370,46,1,NULL,'25000','0','0','5193992112',NULL,1598435066,1598435066),(371,26,3,NULL,'35000','0','1','9800969202',NULL,1598435066,1598435066),(372,42,1,NULL,'65000','0','0','5003267651',NULL,1598435066,1598435066),(373,35,3,NULL,'6000','0','1','6752051340',NULL,1598435066,1598435066),(374,22,1,NULL,'17000','0','0','4908008777',NULL,1598435066,1598435066),(375,49,2,NULL,'55000','1','1','3154054239',NULL,1598435066,1598435066),(376,1,3,NULL,'70000','0','1','8717582952',NULL,1598435066,1598435066),(377,22,1,NULL,'76000','0','1','6720039531',NULL,1598435066,1598435066),(378,23,1,NULL,'72000','0','0','3541293801',NULL,1598435066,1598435066),(379,15,2,NULL,'77000','1','1','6097226995',NULL,1598435066,1598435066),(380,10,2,NULL,'37000','1','1','2620585546',NULL,1598435066,1598435066),(381,46,2,NULL,'22000','1','0','1535651856',NULL,1598435066,1598435066),(382,30,1,NULL,'3000','0','0','3665854787',NULL,1598435066,1598435066),(383,27,1,NULL,'37000','0','0','8387832382',NULL,1598435066,1598435066),(384,41,1,NULL,'42000','0','0','7312924212',NULL,1598435066,1598435066),(385,42,2,NULL,'27000','1','0','2905314648',NULL,1598435066,1598435066),(386,46,2,NULL,'32000','1','0','4268772588',NULL,1598435066,1598435066),(387,5,2,NULL,'89000','1','0','5403496484',NULL,1598435066,1598435066),(388,38,2,NULL,'20000','1','0','8624697441',NULL,1598435066,1598435066),(389,2,1,NULL,'65000','0','0','8216988291',NULL,1598435066,1598435066),(390,45,1,NULL,'70000','0','0','5791588533',NULL,1598435066,1598435066),(391,5,2,NULL,'49000','1','1','6067682780',NULL,1598435066,1598435066),(392,31,2,NULL,'17000','1','1','3835852231',NULL,1598435066,1598435066),(393,13,2,NULL,'16000','1','1','1297847674',NULL,1598435066,1598435066),(394,42,1,NULL,'92000','0','0','4759910826',NULL,1598435066,1598435066),(395,4,3,NULL,'21000','0','1','8740542256',NULL,1598435066,1598435066),(396,11,2,NULL,'74000','1','0','3693827805',NULL,1598435066,1598435066),(397,24,2,NULL,'89000','1','0','4490589463',NULL,1598435066,1598435066),(398,37,3,NULL,'10000','0','0','3948678988',NULL,1598435066,1598435066),(399,29,1,NULL,'14000','0','1','8639305334',NULL,1598435066,1598435066),(400,40,3,NULL,'96000','0','1','4075069361',NULL,1598435066,1598435066),(401,14,3,NULL,'80000','0','1','5369208399',NULL,1598435066,1598435066),(402,16,3,NULL,'80000','0','1','6607144171',NULL,1598435066,1598435066),(403,30,3,NULL,'81000','0','0','6657184037',NULL,1598435066,1598435066),(404,29,1,NULL,'18000','0','0','3954283751',NULL,1598435066,1598435066),(405,36,3,NULL,'34000','0','1','6626479467',NULL,1598435066,1598435066),(406,23,1,NULL,'26000','0','0','1740999727',NULL,1598435066,1598435066),(407,34,3,NULL,'43000','0','1','9669754521',NULL,1598435066,1598435066),(408,25,1,NULL,'75000','0','1','8256378816',NULL,1598435066,1598435066),(409,14,1,NULL,'84000','0','0','1348556529',NULL,1598435066,1598435066),(410,23,1,NULL,'14000','0','0','6534973030',NULL,1598435066,1598435066),(411,23,3,NULL,'62000','0','1','5968231014',NULL,1598435066,1598435066),(412,16,1,NULL,'56000','0','0','2669365860',NULL,1598435066,1598435066),(413,46,2,NULL,'6000','1','0','3664249548',NULL,1598435066,1598435066),(414,6,1,NULL,'84000','0','1','8724300429',NULL,1598435066,1598435066),(415,13,2,NULL,'33000','1','1','3920187621',NULL,1598435066,1598435066),(416,2,2,NULL,'81000','1','1','6313464460',NULL,1598435066,1598435066),(417,31,1,NULL,'88000','0','1','5385796057',NULL,1598435066,1598435066),(418,49,3,NULL,'84000','0','0','4392502874',NULL,1598435066,1598435066),(419,28,1,NULL,'36000','0','1','4952663356',NULL,1598435066,1598435066),(420,37,1,NULL,'43000','0','1','9176222688',NULL,1598435066,1598435066),(421,45,1,NULL,'7000','0','0','5723744988',NULL,1598435066,1598435066),(422,37,1,NULL,'87000','0','0','6061992077',NULL,1598435066,1598435066),(423,31,1,NULL,'17000','0','0','4583278062',NULL,1598435066,1598435066),(424,41,3,NULL,'36000','0','1','9288154178',NULL,1598435066,1598435066),(425,43,2,NULL,'36000','1','0','7046286656',NULL,1598435066,1598435066),(426,41,1,NULL,'77000','0','1','4560887125',NULL,1598435066,1598435066),(427,41,1,NULL,'18000','0','1','2463714931',NULL,1598435066,1598435066),(428,43,1,NULL,'78000','0','1','1963720950',NULL,1598435066,1598435066),(429,45,1,NULL,'88000','0','1','7576472538',NULL,1598435066,1598435066),(430,27,2,NULL,'33000','1','0','6329660909',NULL,1598435066,1598435066),(431,5,2,NULL,'55000','1','0','5273904616',NULL,1598435066,1598435066),(432,3,2,NULL,'48000','1','0','7525951934',NULL,1598435066,1598435066),(433,18,1,NULL,'82000','0','1','2523318038',NULL,1598435066,1598435066),(434,32,1,NULL,'42000','0','1','4839822265',NULL,1598435066,1598435066),(435,2,3,NULL,'82000','0','1','7933958029',NULL,1598435066,1598435066),(436,3,2,NULL,'71000','1','0','8854676654',NULL,1598435066,1598435066),(437,16,3,NULL,'69000','0','1','5313822043',NULL,1598435066,1598435066),(438,33,2,NULL,'93000','1','0','8705182527',NULL,1598435066,1598435066),(439,11,2,NULL,'4000','1','1','1691120623',NULL,1598435066,1598435066),(440,21,1,NULL,'65000','0','0','6212820413',NULL,1598435066,1598435066),(441,36,2,NULL,'78000','1','0','1891059105',NULL,1598435066,1598435066),(442,5,1,NULL,'97000','0','1','6301429525',NULL,1598435066,1598435066),(443,10,1,NULL,'31000','0','0','1848167510',NULL,1598435066,1598435066),(444,29,1,NULL,'80000','0','1','1438261656',NULL,1598435066,1598435066),(445,12,3,NULL,'47000','0','1','7076657492',NULL,1598435066,1598435066),(446,47,1,NULL,'22000','0','1','9805260780',NULL,1598435066,1598435066),(447,34,1,NULL,'40000','0','1','5819424088',NULL,1598435066,1598435066),(448,18,2,NULL,'74000','1','0','9011650023',NULL,1598435066,1598435066),(449,20,2,NULL,'31000','1','0','7624383581',NULL,1598435066,1598435066),(450,2,3,NULL,'100000','0','1','6660756774',NULL,1598435066,1598435066),(451,9,3,NULL,'82000','0','0','7695209046',NULL,1598435066,1598435066),(452,30,1,NULL,'50000','0','1','6507871576',NULL,1598435066,1598435066),(453,41,1,NULL,'43000','0','1','8739642641',NULL,1598435066,1598435066),(454,19,3,NULL,'38000','0','0','3223715385',NULL,1598435066,1598435066),(455,6,2,NULL,'92000','1','0','5010430321',NULL,1598435066,1598435066),(456,44,2,NULL,'98000','1','0','7968362035',NULL,1598435066,1598435066),(457,19,3,NULL,'42000','0','0','3964716199',NULL,1598435066,1598435066),(458,1,2,NULL,'92000','1','1','2586786743',NULL,1598435066,1598435066),(459,43,1,NULL,'33000','0','1','1265374127',NULL,1598435066,1598435066),(460,42,1,NULL,'93000','0','1','7859183710',NULL,1598435066,1598435066),(461,17,3,NULL,'70000','0','1','9353608359',NULL,1598435066,1598435066),(462,36,1,NULL,'2000','0','1','7451191007',NULL,1598435066,1598435066),(463,48,2,NULL,'29000','1','1','2410616262',NULL,1598435066,1598435066),(464,39,3,NULL,'56000','0','1','1828349822',NULL,1598435066,1598435066),(465,37,1,NULL,'42000','0','0','8796425828',NULL,1598435066,1598435066),(466,37,1,NULL,'83000','0','1','5000614005',NULL,1598435066,1598435066),(467,13,2,NULL,'30000','1','0','5831833817',NULL,1598435066,1598435066),(468,18,3,NULL,'16000','0','1','5287695613',NULL,1598435066,1598435066),(469,2,1,NULL,'30000','0','1','6240012839',NULL,1598435066,1598435066),(470,31,3,NULL,'35000','0','1','4097458512',NULL,1598435066,1598435066),(471,37,2,NULL,'91000','1','0','2712568353',NULL,1598435066,1598435066),(472,27,3,NULL,'14000','0','0','7793779677',NULL,1598435066,1598435066),(473,11,2,NULL,'49000','1','1','6953705889',NULL,1598435066,1598435066),(474,33,1,NULL,'64000','0','1','6248533206',NULL,1598435066,1598435066),(475,27,3,NULL,'21000','0','0','3433201334',NULL,1598435066,1598435066),(476,36,1,NULL,'2000','0','0','2596289627',NULL,1598435066,1598435066),(477,45,2,NULL,'33000','1','1','1580964534',NULL,1598435066,1598435066),(478,31,1,NULL,'6000','0','1','3626606074',NULL,1598435066,1598435066),(479,4,1,NULL,'61000','0','0','6207351334',NULL,1598435066,1598435066),(480,37,2,NULL,'39000','1','0','3988533842',NULL,1598435066,1598435066),(481,45,1,NULL,'17000','0','0','1677656671',NULL,1598435066,1598435066),(482,21,3,NULL,'60000','0','0','8189586094',NULL,1598435066,1598435066),(483,1,1,NULL,'72000','0','1','2483279161',NULL,1598435066,1598435066),(484,20,1,NULL,'26000','0','0','7960638721',NULL,1598435066,1598435066),(485,27,1,NULL,'21000','0','1','3176525452',NULL,1598435066,1598435066),(486,36,3,NULL,'31000','0','1','2478900111',NULL,1598435066,1598435066),(487,40,3,NULL,'30000','0','1','9749451463',NULL,1598435066,1598435066),(488,44,2,NULL,'82000','1','1','8541089905',NULL,1598435066,1598435066),(489,27,1,NULL,'20000','0','1','8764166238',NULL,1598435066,1598435066),(490,45,2,NULL,'41000','1','0','5832475624',NULL,1598435066,1598435066),(491,38,2,NULL,'94000','1','1','4457803287',NULL,1598435066,1598435066),(492,21,1,NULL,'58000','0','0','4398534158',NULL,1598435066,1598435066),(493,16,3,NULL,'94000','0','1','8555051014',NULL,1598435066,1598435066),(494,34,2,NULL,'78000','1','0','7620134820',NULL,1598435066,1598435066),(495,5,3,NULL,'94000','0','0','4953427774',NULL,1598435066,1598435066),(496,42,1,NULL,'97000','0','0','2376293332',NULL,1598435066,1598435066),(497,30,3,NULL,'23000','0','1','3525178919',NULL,1598435066,1598435066),(498,33,3,NULL,'79000','0','0','3065975190',NULL,1598435066,1598435066),(499,9,1,NULL,'28000','0','1','8446949567',NULL,1598435066,1598435066),(500,9,1,NULL,'10000','0','0','7137891866',NULL,1598435066,1598435066),(501,30,3,NULL,'97000','0','1','6491521643',NULL,1598435066,1598435066),(502,33,2,NULL,'67000','1','1','1991888025',NULL,1598435066,1598435066),(503,48,1,NULL,'6000','0','1','1520921289',NULL,1598435066,1598435066),(504,46,1,NULL,'61000','0','0','8517752293',NULL,1598435066,1598435066),(505,23,3,NULL,'32000','0','1','3955306800',NULL,1598435066,1598435066),(506,19,2,NULL,'12000','1','0','8484326118',NULL,1598435066,1598435066),(507,30,1,NULL,'42000','0','1','4262383361',NULL,1598435066,1598435066),(508,19,1,NULL,'95000','0','0','6822570060',NULL,1598435066,1598435066),(509,11,3,NULL,'30000','0','0','6180208775',NULL,1598435066,1598435066),(510,4,3,NULL,'83000','0','0','8106736309',NULL,1598435066,1598435066),(511,16,1,NULL,'88000','0','1','5419235890',NULL,1598435066,1598435066),(512,36,1,NULL,'58000','0','0','9612119703',NULL,1598435066,1598435066),(513,4,3,NULL,'61000','0','0','7931599082',NULL,1598435066,1598435066),(514,35,2,NULL,'24000','1','0','7756376922',NULL,1598435066,1598435066),(515,38,1,NULL,'55000','0','0','1517130819',NULL,1598435066,1598435066),(516,24,3,NULL,'13000','0','1','5348170675',NULL,1598435066,1598435066),(517,23,1,NULL,'4000','0','1','3471797231',NULL,1598435066,1598435066),(518,22,3,NULL,'39000','0','0','7907615565',NULL,1598435066,1598435066),(519,10,1,NULL,'88000','0','1','9574418244',NULL,1598435066,1598435066),(520,20,2,NULL,'71000','1','0','4543625667',NULL,1598435066,1598435066),(521,47,3,NULL,'19000','0','1','7654554157',NULL,1598435066,1598435066),(522,1,1,NULL,'100000','0','1','3443562829',NULL,1598435066,1598435066),(523,1,1,NULL,'70000','0','1','4423586709',NULL,1598435066,1598435066),(524,18,1,NULL,'69000','0','1','1475920117',NULL,1598435066,1598435066),(525,28,3,NULL,'3000','0','1','2300297689',NULL,1598435066,1598435066),(526,18,3,NULL,'75000','0','1','8178648410',NULL,1598435066,1598435066),(527,12,3,NULL,'74000','0','1','6344103154',NULL,1598435066,1598435066),(528,4,2,NULL,'68000','1','1','4663914466',NULL,1598435066,1598435066),(529,32,1,NULL,'3000','0','1','8817251183',NULL,1598435066,1598435066),(530,37,2,NULL,'60000','1','1','7227392314',NULL,1598435066,1598435066),(531,39,2,NULL,'27000','1','0','9205001876',NULL,1598435066,1598435066),(532,47,3,NULL,'27000','0','1','1979053921',NULL,1598435066,1598435066),(533,29,2,NULL,'52000','1','1','9726674658',NULL,1598435066,1598435066),(534,25,3,NULL,'51000','0','1','6097307046',NULL,1598435066,1598435066),(535,2,3,NULL,'70000','0','1','6264254384',NULL,1598435066,1598435066),(536,28,1,NULL,'43000','0','1','6287297158',NULL,1598435066,1598435066),(537,43,2,NULL,'24000','1','1','6911232379',NULL,1598435066,1598435066),(538,25,2,NULL,'96000','1','0','5578512236',NULL,1598435066,1598435066),(539,37,2,NULL,'37000','1','1','4100194406',NULL,1598435066,1598435066),(540,19,3,NULL,'19000','0','0','5156982700',NULL,1598435066,1598435066),(541,9,2,NULL,'88000','1','0','9848851164',NULL,1598435066,1598435066),(542,23,2,NULL,'6000','1','1','3840232663',NULL,1598435066,1598435066),(543,34,2,NULL,'50000','1','1','5601166017',NULL,1598435066,1598435066),(544,17,3,NULL,'9000','0','1','5059069654',NULL,1598435066,1598435066),(545,31,2,NULL,'75000','1','1','2698051967',NULL,1598435066,1598435066),(546,42,3,NULL,'16000','0','0','7188088550',NULL,1598435066,1598435066),(547,9,3,NULL,'34000','0','1','5447848771',NULL,1598435066,1598435066),(548,34,1,NULL,'1000','0','0','3965330251',NULL,1598435066,1598435066),(549,44,2,NULL,'75000','1','0','5377953921',NULL,1598435066,1598435066),(550,39,1,NULL,'18000','0','0','4837409221',NULL,1598435066,1598435066),(551,3,2,NULL,'72000','1','0','4498277410',NULL,1598435066,1598435066),(552,5,3,NULL,'69000','0','1','9283084791',NULL,1598435066,1598435066),(553,5,3,NULL,'26000','0','0','2659146604',NULL,1598435066,1598435066),(554,22,3,NULL,'86000','0','1','2552049805',NULL,1598435066,1598435066),(555,40,1,NULL,'85000','0','1','6071176478',NULL,1598435066,1598435066),(556,23,3,NULL,'69000','0','1','6990870069',NULL,1598435066,1598435066),(557,11,2,NULL,'96000','1','1','6479213372',NULL,1598435066,1598435066),(558,23,2,NULL,'41000','1','1','1504517585',NULL,1598435066,1598435066),(559,31,1,NULL,'44000','0','1','8719468638',NULL,1598435066,1598435066),(560,10,1,NULL,'100000','0','0','6734049947',NULL,1598435066,1598435066),(561,13,3,NULL,'48000','0','1','1821413096',NULL,1598435066,1598435066),(562,12,1,NULL,'76000','0','0','8513341843',NULL,1598435066,1598435066),(563,20,3,NULL,'95000','0','1','4268735845',NULL,1598435066,1598435066),(564,27,2,NULL,'48000','1','0','8387659934',NULL,1598435066,1598435066),(565,19,1,NULL,'88000','0','0','5058251068',NULL,1598435066,1598435066),(566,38,3,NULL,'84000','0','1','5629764356',NULL,1598435066,1598435066),(567,5,1,NULL,'96000','0','0','4502950113',NULL,1598435066,1598435066),(568,9,3,NULL,'61000','0','0','1564850540',NULL,1598435066,1598435066),(569,41,1,NULL,'76000','0','0','2645153167',NULL,1598435066,1598435066),(570,26,2,NULL,'67000','1','1','8567666563',NULL,1598435066,1598435066),(571,19,2,NULL,'64000','1','0','5599959247',NULL,1598435066,1598435066),(572,32,3,NULL,'99000','0','1','9737850173',NULL,1598435066,1598435066),(573,6,2,NULL,'87000','1','0','5873074267',NULL,1598435066,1598435066),(574,50,2,NULL,'68000','1','0','6089732168',NULL,1598435066,1598435066),(575,44,2,NULL,'67000','1','0','2647884396',NULL,1598435066,1598435066),(576,1,2,NULL,'43000','1','1','2796459997',NULL,1598435066,1598435066),(577,28,3,NULL,'87000','0','0','7612974154',NULL,1598435066,1598435066),(578,43,2,NULL,'10000','1','0','5541679771',NULL,1598435066,1598435066),(579,30,3,NULL,'73000','0','1','9556960954',NULL,1598435066,1598435066),(580,16,3,NULL,'79000','0','1','5599310346',NULL,1598435066,1598435066),(581,33,3,NULL,'64000','0','1','4898276766',NULL,1598435066,1598435066),(582,41,3,NULL,'70000','0','1','9053079861',NULL,1598435066,1598435066),(583,42,3,NULL,'70000','0','0','6031447472',NULL,1598435066,1598435066),(584,35,2,NULL,'12000','1','0','9344687666',NULL,1598435066,1598435066),(585,47,1,NULL,'29000','0','0','2306148305',NULL,1598435066,1598435066),(586,12,1,NULL,'19000','0','0','7195936934',NULL,1598435066,1598435066),(587,44,2,NULL,'68000','1','0','3840159740',NULL,1598435066,1598435066),(588,21,2,NULL,'8000','1','0','9028884892',NULL,1598435066,1598435066),(589,8,3,NULL,'30000','0','1','5956886684',NULL,1598435066,1598435066),(590,43,1,NULL,'27000','0','1','8172254258',NULL,1598435066,1598435066),(591,47,2,NULL,'86000','1','1','3018591897',NULL,1598435066,1598435066),(592,10,2,NULL,'72000','1','1','7807202292',NULL,1598435066,1598435066),(593,14,3,NULL,'7000','0','1','2697336753',NULL,1598435066,1598435066),(594,35,1,NULL,'44000','0','0','9496183925',NULL,1598435066,1598435066),(595,18,1,NULL,'88000','0','0','6469252782',NULL,1598435066,1598435066),(596,4,1,NULL,'36000','0','0','3937860669',NULL,1598435066,1598435066),(597,22,2,NULL,'23000','1','0','3242852706',NULL,1598435066,1598435066),(598,30,1,NULL,'4000','0','0','2288607304',NULL,1598435066,1598435066),(599,35,3,NULL,'84000','0','1','9645392351',NULL,1598435066,1598435066),(600,3,3,NULL,'28000','0','0','2511967115',NULL,1598435066,1598435066),(601,14,2,NULL,'43000','1','0','9258894282',NULL,1598435066,1598435066),(602,14,3,NULL,'56000','0','0','4234688464',NULL,1598435066,1598435066),(603,43,1,NULL,'62000','0','0','6007721194',NULL,1598435066,1598435066),(604,38,1,NULL,'14000','0','0','6359601644',NULL,1598435066,1598435066),(605,49,1,NULL,'49000','0','1','6787674799',NULL,1598435066,1598435066),(606,24,3,NULL,'41000','0','0','4655907743',NULL,1598435066,1598435066),(607,35,1,NULL,'59000','0','0','2213953964',NULL,1598435066,1598435066),(608,21,1,NULL,'90000','0','0','9657427694',NULL,1598435066,1598435066),(609,1,3,NULL,'48000','0','0','9628699461',NULL,1598435066,1598435066),(610,17,3,NULL,'28000','0','0','2158247096',NULL,1598435066,1598435066),(611,32,3,NULL,'64000','0','0','7323806583',NULL,1598435066,1598435066),(612,2,2,NULL,'25000','1','1','2211379001',NULL,1598435066,1598435066),(613,9,1,NULL,'12000','0','0','4784004309',NULL,1598435066,1598435066),(614,17,2,NULL,'57000','1','0','2676913887',NULL,1598435066,1598435066),(615,45,3,NULL,'2000','0','1','3316060280',NULL,1598435066,1598435066),(616,37,2,NULL,'71000','1','1','8329950031',NULL,1598435066,1598435066),(617,11,2,NULL,'17000','1','0','6968592793',NULL,1598435066,1598435066),(618,2,2,NULL,'39000','1','0','9871917549',NULL,1598435066,1598435066),(619,18,1,NULL,'59000','0','0','6079552227',NULL,1598435066,1598435066),(620,50,2,NULL,'99000','1','1','7893449992',NULL,1598435066,1598435066),(621,48,3,NULL,'86000','0','1','9643409878',NULL,1598435066,1598435066),(622,35,1,NULL,'85000','0','0','2690868940',NULL,1598435066,1598435066),(623,47,1,NULL,'90000','0','0','8966686400',NULL,1598435066,1598435066),(624,13,3,NULL,'51000','0','1','1493066025',NULL,1598435066,1598435066),(625,22,2,NULL,'37000','1','1','4582732110',NULL,1598435066,1598435066),(626,12,3,NULL,'35000','0','0','8101699577',NULL,1598435066,1598435066),(627,24,1,NULL,'43000','0','1','1666243703',NULL,1598435066,1598435066),(628,21,2,NULL,'88000','1','0','5524120621',NULL,1598435066,1598435066),(629,27,1,NULL,'74000','0','1','3240106122',NULL,1598435066,1598435066),(630,42,2,NULL,'85000','1','0','3309062316',NULL,1598435066,1598435066),(631,36,1,NULL,'58000','0','0','7270648298',NULL,1598435066,1598435066),(632,23,3,NULL,'72000','0','1','3831292570',NULL,1598435066,1598435066),(633,29,3,NULL,'67000','0','0','5631069571',NULL,1598435066,1598435066),(634,6,2,NULL,'11000','1','0','8637866889',NULL,1598435066,1598435066),(635,3,1,NULL,'60000','0','1','4624671124',NULL,1598435066,1598435066),(636,46,1,NULL,'92000','0','1','6393918233',NULL,1598435066,1598435066),(637,44,1,NULL,'26000','0','1','3799701468',NULL,1598435066,1598435066),(638,29,2,NULL,'100000','1','1','3548411792',NULL,1598435066,1598435066),(639,4,1,NULL,'64000','0','0','4469376446',NULL,1598435066,1598435066),(640,31,2,NULL,'45000','1','0','2853909632',NULL,1598435066,1598435066),(641,2,3,NULL,'25000','0','0','5082653440',NULL,1598435066,1598435066),(642,32,3,NULL,'18000','0','1','1964970501',NULL,1598435066,1598435066),(643,12,1,NULL,'42000','0','0','2225939794',NULL,1598435066,1598435066),(644,21,2,NULL,'73000','1','1','6691017566',NULL,1598435066,1598435066),(645,13,1,NULL,'46000','0','1','2326876499',NULL,1598435066,1598435066),(646,33,2,NULL,'66000','1','1','6533463579',NULL,1598435066,1598435066),(647,12,2,NULL,'95000','1','0','5312240805',NULL,1598435066,1598435066),(648,28,3,NULL,'64000','0','0','6253902089',NULL,1598435066,1598435066),(649,16,1,NULL,'43000','0','0','6768154527',NULL,1598435066,1598435066),(650,41,1,NULL,'38000','0','0','8728477602',NULL,1598435066,1598435066),(651,30,2,NULL,'86000','1','0','6876887908',NULL,1598435066,1598435066),(652,38,1,NULL,'74000','0','0','4793183632',NULL,1598435067,1598435067),(653,6,1,NULL,'70000','0','1','5719891407',NULL,1598435067,1598435067),(654,4,1,NULL,'20000','0','1','8326501442',NULL,1598435067,1598435067),(655,48,2,NULL,'43000','1','0','4832139162',NULL,1598435067,1598435067),(656,17,3,NULL,'49000','0','1','8177361718',NULL,1598435067,1598435067),(657,50,1,NULL,'40000','0','1','4917633070',NULL,1598435067,1598435067),(658,37,3,NULL,'63000','0','0','1791331549',NULL,1598435067,1598435067),(659,17,3,NULL,'89000','0','1','3305001612',NULL,1598435067,1598435067),(660,43,2,NULL,'38000','1','0','5601162712',NULL,1598435067,1598435067),(661,1,2,NULL,'91000','1','1','7905461628',NULL,1598435067,1598435067),(662,10,3,NULL,'22000','0','1','6127927104',NULL,1598435067,1598435067),(663,12,2,NULL,'4000','1','0','3736367066',NULL,1598435067,1598435067),(664,16,1,NULL,'84000','0','1','6029535422',NULL,1598435067,1598435067),(665,1,1,NULL,'91000','0','1','2868416722',NULL,1598435067,1598435067),(666,4,1,NULL,'14000','0','0','5060254005',NULL,1598435067,1598435067),(667,2,3,NULL,'44000','0','0','8822691485',NULL,1598435067,1598435067),(668,4,3,NULL,'33000','0','0','3190407131',NULL,1598435067,1598435067),(669,22,1,NULL,'96000','0','1','3082697947',NULL,1598435067,1598435067),(670,43,2,NULL,'32000','1','0','7609177321',NULL,1598435067,1598435067),(671,16,3,NULL,'51000','0','1','5388127337',NULL,1598435067,1598435067),(672,45,1,NULL,'36000','0','1','6564794561',NULL,1598435067,1598435067),(673,28,2,NULL,'70000','1','1','6603172719',NULL,1598435067,1598435067),(674,35,1,NULL,'64000','0','1','5246096470',NULL,1598435067,1598435067),(675,8,2,NULL,'3000','1','1','3266705739',NULL,1598435067,1598435067),(676,42,3,NULL,'16000','0','1','3181952844',NULL,1598435067,1598435067),(677,13,2,NULL,'36000','1','0','2390473001',NULL,1598435067,1598435067),(678,17,2,NULL,'9000','1','0','2787686710',NULL,1598435067,1598435067),(679,28,3,NULL,'33000','0','0','5431253629',NULL,1598435067,1598435067),(680,50,1,NULL,'40000','0','1','8641230083',NULL,1598435067,1598435067),(681,42,2,NULL,'77000','1','0','1640567700',NULL,1598435067,1598435067),(682,32,2,NULL,'33000','1','0','4638847634',NULL,1598435067,1598435067),(683,32,2,NULL,'87000','1','0','3094095980',NULL,1598435067,1598435067),(684,49,3,NULL,'57000','0','1','3260239462',NULL,1598435067,1598435067),(685,12,3,NULL,'21000','0','0','3264570908',NULL,1598435067,1598435067),(686,2,3,NULL,'42000','0','0','7620170804',NULL,1598435067,1598435067),(687,35,1,NULL,'100000','0','0','1914752980',NULL,1598435067,1598435067),(688,39,1,NULL,'31000','0','1','3016534144',NULL,1598435067,1598435067),(689,22,3,NULL,'75000','0','0','7815422592',NULL,1598435067,1598435067),(690,24,1,NULL,'93000','0','0','5189373981',NULL,1598435067,1598435067),(691,25,1,NULL,'97000','0','0','1768942461',NULL,1598435067,1598435067),(692,43,3,NULL,'76000','0','0','5842675938',NULL,1598435067,1598435067),(693,29,3,NULL,'4000','0','0','4838598854',NULL,1598435067,1598435067),(694,17,1,NULL,'98000','0','1','5254332492',NULL,1598435067,1598435067),(695,37,2,NULL,'7000','1','1','6089811843',NULL,1598435067,1598435067),(696,3,1,NULL,'20000','0','0','5591630644',NULL,1598435067,1598435067),(697,25,3,NULL,'23000','0','0','9416524199',NULL,1598435067,1598435067),(698,9,1,NULL,'37000','0','0','5767420580',NULL,1598435067,1598435067),(699,27,2,NULL,'48000','1','1','2204658122',NULL,1598435067,1598435067),(700,25,1,NULL,'80000','0','0','3871587096',NULL,1598435067,1598435067),(701,25,2,NULL,'79000','1','0','2921936065',NULL,1598435067,1598435067),(702,16,3,NULL,'63000','0','1','3140640109',NULL,1598435067,1598435067),(703,1,3,NULL,'25000','0','1','4154102997',NULL,1598435067,1598435067),(704,13,3,NULL,'77000','0','0','7884004807',NULL,1598435067,1598435067),(705,31,1,NULL,'13000','0','1','6884068636',NULL,1598435067,1598435067),(706,25,1,NULL,'5000','0','0','4934016342',NULL,1598435067,1598435067),(707,26,3,NULL,'45000','0','0','7045976176',NULL,1598435067,1598435067),(708,10,3,NULL,'82000','0','0','8196423795',NULL,1598435067,1598435067),(709,50,2,NULL,'61000','1','0','7430734139',NULL,1598435067,1598435067),(710,24,3,NULL,'32000','0','1','8928205830',NULL,1598435067,1598435067),(711,9,1,NULL,'8000','0','0','4690943656',NULL,1598435067,1598435067),(712,34,1,NULL,'63000','0','1','8084626008',NULL,1598435067,1598435067),(713,11,3,NULL,'57000','0','1','3975464895',NULL,1598435067,1598435067),(714,8,1,NULL,'100000','0','1','1270830043',NULL,1598435067,1598435067),(715,33,3,NULL,'66000','0','1','2276123037',NULL,1598435067,1598435067),(716,1,3,NULL,'18000','0','0','6315246515',NULL,1598435067,1598435067),(717,45,1,NULL,'94000','0','0','4473523331',NULL,1598435067,1598435067),(718,9,3,NULL,'91000','0','0','6773651897',NULL,1598435067,1598435067),(719,43,3,NULL,'92000','0','1','7941334699',NULL,1598435067,1598435067),(720,45,3,NULL,'36000','0','1','1528889716',NULL,1598435067,1598435067),(721,5,3,NULL,'62000','0','1','8870830061',NULL,1598435067,1598435067),(722,39,1,NULL,'10000','0','1','9678118331',NULL,1598435067,1598435067),(723,7,2,NULL,'85000','1','0','4549634118',NULL,1598435067,1598435067),(724,13,1,NULL,'99000','0','1','5438482807',NULL,1598435067,1598435067),(725,25,3,NULL,'10000','0','1','4926146168',NULL,1598435067,1598435067),(726,15,1,NULL,'4000','0','1','5330002667',NULL,1598435067,1598435067),(727,20,2,NULL,'91000','1','0','2671772793',NULL,1598435067,1598435067),(728,30,2,NULL,'48000','1','1','3955419406',NULL,1598435067,1598435067),(729,3,1,NULL,'30000','0','1','6260599018',NULL,1598435067,1598435067),(730,27,3,NULL,'97000','0','0','7043353794',NULL,1598435067,1598435067),(731,10,3,NULL,'42000','0','0','5820050238',NULL,1598435067,1598435067),(732,26,2,NULL,'4000','1','1','5950564564',NULL,1598435067,1598435067),(733,11,3,NULL,'97000','0','1','3028018305',NULL,1598435067,1598435067),(734,41,2,NULL,'59000','1','0','5633955524',NULL,1598435067,1598435067),(735,9,3,NULL,'37000','0','0','6833894407',NULL,1598435067,1598435067),(736,26,2,NULL,'16000','1','0','2206836250',NULL,1598435067,1598435067),(737,4,1,NULL,'59000','0','0','5809348833',NULL,1598435067,1598435067),(738,19,1,NULL,'71000','0','0','5100791674',NULL,1598435067,1598435067),(739,1,3,NULL,'20000','0','0','5583299875',NULL,1598435067,1598435067),(740,19,2,NULL,'81000','1','1','6692249229',NULL,1598435067,1598435067),(741,14,3,NULL,'98000','0','0','4795802738',NULL,1598435067,1598435067),(742,20,1,NULL,'73000','0','0','7054597543',NULL,1598435067,1598435067),(743,31,1,NULL,'30000','0','0','1786395307',NULL,1598435067,1598435067),(744,42,2,NULL,'72000','1','1','1874761021',NULL,1598435067,1598435067),(745,36,3,NULL,'6000','0','0','7010665108',NULL,1598435067,1598435067),(746,15,3,NULL,'45000','0','1','4428850680',NULL,1598435067,1598435067),(747,36,2,NULL,'71000','1','0','1935501843',NULL,1598435067,1598435067),(748,37,2,NULL,'26000','1','1','9639608992',NULL,1598435067,1598435067),(749,40,1,NULL,'24000','0','1','1927134787',NULL,1598435067,1598435067),(750,17,2,NULL,'87000','1','1','6564731187',NULL,1598435067,1598435067),(751,39,2,NULL,'52000','1','1','4102757704',NULL,1598435067,1598435067),(752,4,2,NULL,'27000','1','0','9308765536',NULL,1598435067,1598435067),(753,10,3,NULL,'100000','0','0','6094378095',NULL,1598435067,1598435067),(754,7,1,NULL,'59000','0','1','8048317087',NULL,1598435067,1598435067),(755,5,2,NULL,'57000','1','1','2786817099',NULL,1598435067,1598435067),(756,24,1,NULL,'76000','0','1','3051823787',NULL,1598435067,1598435067),(757,43,1,NULL,'20000','0','0','5951331364',NULL,1598435067,1598435067),(758,31,2,NULL,'98000','1','0','1779420421',NULL,1598435067,1598435067),(759,23,1,NULL,'14000','0','1','9247356899',NULL,1598435067,1598435067),(760,47,2,NULL,'51000','1','1','4540793811',NULL,1598435067,1598435067),(761,45,1,NULL,'86000','0','0','2998013677',NULL,1598435067,1598435067),(762,12,3,NULL,'94000','0','1','4441775912',NULL,1598435067,1598435067),(763,36,3,NULL,'23000','0','1','5891756029',NULL,1598435067,1598435067),(764,42,1,NULL,'77000','0','0','8850076249',NULL,1598435067,1598435067),(765,29,1,NULL,'60000','0','1','7393143493',NULL,1598435067,1598435067),(766,10,1,NULL,'12000','0','0','4673039563',NULL,1598435067,1598435067),(767,50,2,NULL,'2000','1','0','9705821782',NULL,1598435067,1598435067),(768,1,1,NULL,'71000','0','0','9070169996',NULL,1598435067,1598435067),(769,39,3,NULL,'38000','0','1','9623146561',NULL,1598435067,1598435067),(770,49,1,NULL,'56000','0','1','4437186457',NULL,1598435067,1598435067),(771,19,3,NULL,'21000','0','1','3064306102',NULL,1598435067,1598435067),(772,4,1,NULL,'28000','0','0','8296300195',NULL,1598435067,1598435067),(773,5,2,NULL,'55000','1','1','8335934488',NULL,1598435067,1598435067),(774,12,2,NULL,'87000','1','1','9678978468',NULL,1598435067,1598435067),(775,43,1,NULL,'67000','0','1','9108908367',NULL,1598435067,1598435067),(776,13,3,NULL,'94000','0','0','8531926424',NULL,1598435067,1598435067),(777,48,3,NULL,'35000','0','0','1404017025',NULL,1598435067,1598435067),(778,50,2,NULL,'4000','1','0','2237011381',NULL,1598435067,1598435067),(779,23,1,NULL,'11000','0','1','7858143792',NULL,1598435067,1598435067),(780,44,3,NULL,'73000','0','1','9396888838',NULL,1598435067,1598435067),(781,34,2,NULL,'48000','1','0','5553805299',NULL,1598435067,1598435067),(782,33,3,NULL,'24000','0','1','3004336440',NULL,1598435067,1598435067),(783,7,1,NULL,'67000','0','0','6759758901',NULL,1598435067,1598435067),(784,43,3,NULL,'5000','0','0','7628737599',NULL,1598435067,1598435067),(785,34,2,NULL,'49000','1','0','9398147142',NULL,1598435067,1598435067),(786,22,3,NULL,'18000','0','0','2317061928',NULL,1598435067,1598435067),(787,21,1,NULL,'100000','0','0','6945176727',NULL,1598435067,1598435067),(788,24,1,NULL,'68000','0','1','1974895837',NULL,1598435067,1598435067),(789,50,1,NULL,'2000','0','1','5631939703',NULL,1598435067,1598435067),(790,50,2,NULL,'27000','1','1','4515883839',NULL,1598435067,1598435067),(791,45,2,NULL,'1000','1','1','5716873108',NULL,1598435067,1598435067),(792,37,2,NULL,'80000','1','1','7750642967',NULL,1598435067,1598435067),(793,40,3,NULL,'70000','0','1','5692870900',NULL,1598435067,1598435067),(794,11,3,NULL,'77000','0','0','5182624612',NULL,1598435067,1598435067),(795,46,2,NULL,'16000','1','1','5972189976',NULL,1598435067,1598435067),(796,31,1,NULL,'15000','0','0','2647260308',NULL,1598435067,1598435067),(797,48,3,NULL,'54000','0','0','4230224543',NULL,1598435067,1598435067),(798,36,1,NULL,'43000','0','0','1667277320',NULL,1598435067,1598435067),(799,13,1,NULL,'41000','0','1','9356892191',NULL,1598435067,1598435067),(800,17,3,NULL,'57000','0','0','5774316747',NULL,1598435067,1598435067);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `url_generators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_generators` (
  `url_generator_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `transaction_id` bigint(20) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`url_generator_id`),
  KEY `url_generators_user_id_foreign` (`user_id`),
  KEY `url_generators_transaction_id_foreign` (`transaction_id`),
  CONSTRAINT `url_generators_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`) ON DELETE CASCADE,
  CONSTRAINT `url_generators_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `url_generators` WRITE;
/*!40000 ALTER TABLE `url_generators` DISABLE KEYS */;
/*!40000 ALTER TABLE `url_generators` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profiles` (
  `user_profile_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_profile_id`),
  KEY `user_profiles_user_id_foreign` (`user_id`),
  CONSTRAINT `user_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `marketer_id` bigint(20) unsigned DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` bigint(20) unsigned NOT NULL,
  `token_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_mobile_unique` (`mobile`),
  UNIQUE KEY `users_api_token_unique` (`api_token`),
  UNIQUE KEY `users_url_unique` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,42,'7035254814',552,'2058','0','cRr7OGyf2m','tH7Wfa4L3eijWcKFwr4L5UYwLGG7DKzp5erjZAn4','JgVSTk',NULL,1598435064,1598435064),(2,47,'1254165207',717,'6998','0','CU56GABhgT','5vbO9l54xshkkisk1KVAGfi2IZ6QrvWMswO7q0U8','9C4P7z',NULL,1598435064,1598435064),(3,47,'1295086788',375,'9313','0','lAtDVSErBw','qrNHDpEtIDsviGR1UY8r4ENo8FENv75vxXPZYgzm','M4e3Xn',NULL,1598435064,1598435064),(4,7,'4472338123',531,'4795','0','E7vBKWZNmi','1T0CdeIR4eV2VCx8Q6gLo9ofHF6laVmjeyV4BgqU','rEqb7l',NULL,1598435064,1598435064),(5,27,'1887365710',296,'6972','0','T6w77IVQ6L','A6EsmHkVzasvz2W3EfaxDFjquTNE5t1trhEQdIsl','lAKT04',NULL,1598435064,1598435064),(6,50,'1360314522',948,'1456','1','QCH4CLdsCn','C8DP0iLmk70x2uJrlGGuYipxYxH8UPgyKQSu3wZW','jx3NJP',NULL,1598435064,1598435064),(7,24,'2143219573',74,'7332','0','uWwPPvTxoS','vkWYzwdGdjAhAdIfV0WNiyrrtE2WuGSYZjvnlQOv','DIaVae',NULL,1598435064,1598435064),(8,5,'9473864283',246,'6025','0','WEFkxGBEOk','CVkrI6SoZhLobAcieO6KZwlRke2dzofwcYwzDfq6','BLzZMH',NULL,1598435064,1598435064),(9,28,'1270619096',422,'6795','0','VTSOsfvy44','WUWo9WV9s4UYwcUHCZn3hdA2Buq0Ebq93f7TiG3L','Myx1tM',NULL,1598435064,1598435064),(10,39,'6579472867',734,'1820','0','PYnyGw1rLq','GNeaWihuz6MME4XZRXGWuLn1cSVn1vZbEVijGc54','qGL0oj',NULL,1598435064,1598435064),(11,28,'9704766505',271,'6971','0','QfpA0eB4df','dPT5l6JPZaM8RSb4NUhsHzuCgtHNxcyvFW5imDVT','zuOIeN',NULL,1598435064,1598435064),(12,22,'9777217866',25,'9178','0','3cGYv8vmLx','lzwo4aUQaNaupfPOTKjyEfSLSHBssGAnmlGKDH5e','Kx6Kmv',NULL,1598435064,1598435064),(13,42,'3408965371',962,'5254','1','hsZtHCkaQA','uaxVBpawcfbFcIKVjmRnU6wyCKsVFzKulibPZlyM','27fKul',NULL,1598435064,1598435064),(14,9,'7726926527',895,'7207','1','ed90i1p0z4','k10naAOc1FxKDv1cOcJT4BvR1efHeKYS4zqr7hzI','tJvcYQ',NULL,1598435064,1598435064),(15,1,'7289808115',962,'9557','1','FuvYXGlQVA','k8wC4Ez5jOfeZ2g2GhDzPAMXS0GJSLJGzEdvf14a','r6Li5c',NULL,1598435064,1598435064),(16,13,'6629283152',246,'2772','0','lLY07lM4Cz','gc8u0qYyWGpSEDimeOoLmc41eGLXwdu5ts6WQaXb','ekd6Kw',NULL,1598435064,1598435064),(17,42,'2982144104',552,'2305','1','HXMYNLnIyk','jinJsyXqp09NBBGeIBvAjzIn8VFbXeaUIwrhy256','B50WLT',NULL,1598435064,1598435064),(18,34,'8968300610',836,'9757','0','FM6CHaXcMw','UDcxR5oyp3wSVSqTj7dMheSwmabcpEaXGv1MOUgb','YNMSq5',NULL,1598435064,1598435064),(19,14,'7597398258',801,'2883','1','kg4wOrUart','Mc2AwRSmEB9o9tF26GBSXZv04rI3ekTF8o90RIMW','6nKNJ3',NULL,1598435064,1598435064),(20,27,'1661937971',637,'8946','1','BOb0GLEcse','rNAtGSA9JAVhj6pfwwhCCEf19w2y914RDtZKp8qs','7OuFdC',NULL,1598435064,1598435064),(21,29,'4571065614',105,'2357','0','JNVoZmhFBu','3heCztBASEIPvdkn2l1wvgV9xwrAgai2wOhXcrLq','MjQbVW',NULL,1598435064,1598435064),(22,3,'8193690937',375,'5191','1','ZEH9X6cy7E','tbzLPQDPMG0aDPKvWQBzfzXdk4NLNDqu6OdNpt0F','Gg8HEq',NULL,1598435064,1598435064),(23,32,'4459840398',427,'3301','0','lk4Zkx0R9q','xfL3py08BBNHEfSPFkWaxwP4wZARYLj1sMSFKM6Y','Nn9vwe',NULL,1598435064,1598435064),(24,40,'2431955351',682,'5950','1','0Gh8AkGukf','f7ywqRCNTdo3zI8TAXUlQRwc2RqsvDKMFOo2NkvH','C7czr8',NULL,1598435064,1598435064),(25,9,'5062414819',962,'5805','1','JmVv0ASdI0','kWFE1sLpFoIV5ICC58OK2GonzxWPtSjzV5Q4jrPO','PpnYW7',NULL,1598435064,1598435064),(26,8,'9503573519',962,'9572','1','Lm6aJScWhc','3IaYcrpH5TIbRXRKyPx1GJNobr2SzUCu9QPg8NPB','moCbyN',NULL,1598435064,1598435064),(27,48,'7225924068',801,'1540','1','s8sxYsGvwi','kPSW0Yf2Zf23Mw1RPgIeleenrYhZp6vnerHF4EUe','USpMeZ',NULL,1598435064,1598435064),(28,43,'6024213763',717,'4912','0','yVUf9oNOG8','g9ALxQa5xTYRmcWeOyfO6KZM7cE0wrdBCwiFpqI4','M75z0y',NULL,1598435064,1598435064),(29,7,'1741529087',427,'6449','1','zfqz9e8Z9a','ZsJZI1yXslVFPC6MAkynMWcr5vAx31GnKh7uLsnf','9a25hz',NULL,1598435064,1598435064),(30,37,'4029750013',477,'3740','0','byUXTMfAqG','QgsNhMqOp0n5W56MaDNtl9gxDGUTzEavtRLkFWer','kS5D4K',NULL,1598435064,1598435064),(31,34,'4929490909',531,'1478','1','rZXVjiUUzQ','gGUjboCAxlSt2Ha8YQ8TfkyPfbQ5VbBVheK5Ulpn','YFzBiM',NULL,1598435064,1598435064),(32,15,'2323068959',285,'7684','0','KHiLm9Di0R','x3q687IdZWO8wY4MZgwz8TtnQRAvch8lQ3RwUuxA','a2Cr17',NULL,1598435064,1598435064),(33,31,'9309976179',25,'9570','0','0srXsHR9jG','GJAWzvAdS8LIXWbxfihJLxXhLgal8s1dtY7eTnhM','S2IWER',NULL,1598435064,1598435064),(34,42,'6962191635',154,'5445','1','oJW3VNVPLp','xLtjuTmVt9t0Ymd5WSjCGA0wGb7nw9A0tyRwWdP7','hcW9We',NULL,1598435064,1598435064),(35,15,'3976342890',670,'9675','0','GdiJz9OirH','sYD1JE68dlYZxOofejzHr43nhaf5NYXDbc8U7Vhv','hdZy4Z',NULL,1598435064,1598435064),(36,48,'8879600885',105,'8776','0','mbjjsYG7po','Lcq1VCuHN9Ab8daoBZhxItMgrs9GkbYe0mlsjAgH','KOeXDT',NULL,1598435064,1598435064),(37,3,'1112232686',316,'8314','0','7favGjFAXl','6cx7ejn2smQ4d7YnnnSyM8RL4v0kAjnu5vCXA2fL','vEUCB6',NULL,1598435064,1598435064),(38,50,'7164599925',545,'6973','1','28UCHRwQzT','GikJS9jQqr6RKagoUhJUgpT7JSxu4HzgTZcPUlUs','KYaClU',NULL,1598435064,1598435064),(39,42,'8979494513',398,'3600','0','diJCtI8dRM','7Zt4oX5KdmcNKfP1PIxdhFccmYydPMR1M5c7vimX','KIomCr',NULL,1598435064,1598435064),(40,30,'3991262695',962,'8476','0','d9QXbJquE8','L34At202ihjcZNF11W7DvNbb2H7CZcqg7dlHGfk8','5evpBh',NULL,1598435064,1598435064),(41,35,'9073670053',579,'2763','1','HuiMJ82unY','lTtpNmKN8L8rKhZII8aDEVeF2ujWFIu8OZLrJMWN','kq1BaL',NULL,1598435064,1598435064),(42,41,'8403296881',948,'5560','1','5Yh2qvn7Qm','AZSyGyWStHQtIfpZwL6CDBOzYLyYt1d8uWB1AZOt','ltsR69',NULL,1598435064,1598435064),(43,44,'3665199740',74,'8459','1','CB1o012C9V','o1O848qjRkz6qFHVtRlWzjq2SUiOGjZ5a9g006F1','yYTbK1',NULL,1598435064,1598435064),(44,31,'8814007690',579,'7127','0','aH7XLeJ2I7','hPLJup2QHmPtADYUw9b7qNLlmzIIul23gwIsGmCt','AD2Pez',NULL,1598435064,1598435064),(45,45,'7465858822',244,'1517','0','BITobYayPU','Hw70FGZ5wTvAdECeY92xPzyd83747cQNKvbslLWe','j9x9bN',NULL,1598435064,1598435064),(46,14,'1720493772',801,'4136','0','lc71gyN4Ep','xKtL4VQb5x7E1FEjjLO2dEOZSI4nBvLPafPXNgB0','uEkHLS',NULL,1598435064,1598435064),(47,5,'2106294847',296,'8929','0','Nuu3z6ibVk','BLI81wQcjIAcl9szAeQEwalk1SU6QPC2dNg6lCOV','mrz4yk',NULL,1598435064,1598435064),(48,3,'4830005182',63,'4875','1','q8o3715k2G','5kttOx0Mv4gNLyu91XIr2SacHdcePxn8hxZuGUtx','Jjorwu',NULL,1598435064,1598435064),(49,20,'9058630126',375,'6090','1','Aj1tIa045c','1Zu4nP7NrnCjmVTNrQeZQ8ZXkpNqdu1A7AkEg6q2','ijvgt5',NULL,1598435064,1598435064),(50,31,'2422901155',422,'3812','1','e6MJLhiuTO','sUyFC6XlXF4Exh6WpNCfxZkzAieWJZgQJkXzhQnu','iQjk0b',NULL,1598435064,1598435064);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `wallets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallets` (
  `wallet_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `amount_enter` bigint(20) unsigned DEFAULT NULL,
  `amount_exit` bigint(20) unsigned DEFAULT NULL,
  `last_transaction_id` bigint(20) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`wallet_id`),
  KEY `wallets_user_id_foreign` (`user_id`),
  CONSTRAINT `wallets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wallets` WRITE;
/*!40000 ALTER TABLE `wallets` DISABLE KEYS */;
INSERT INTO `wallets` VALUES (1,30,86000,361000,728,NULL,1598435067,1598435067),(2,36,155000,218000,798,NULL,1598435067,1598435067),(3,33,271000,477000,782,NULL,1598435067,1598435067),(4,31,170000,262000,796,NULL,1598435067,1598435067),(5,42,72000,124000,676,NULL,1598435067,1598435067),(6,47,178000,183000,760,NULL,1598435067,1598435067),(7,32,34000,199000,682,NULL,1598435067,1598435067),(8,40,73000,398000,793,NULL,1598435067,1598435067),(9,18,52000,568000,524,NULL,1598435067,1598435067),(10,27,157000,205000,699,NULL,1598435067,1598435067),(11,50,144000,335000,778,NULL,1598435067,1598435067),(12,45,199000,231000,791,NULL,1598435067,1598435067),(13,28,223000,82000,673,NULL,1598435067,1598435067),(14,35,0,327000,674,NULL,1598435067,1598435067),(15,29,184000,154000,693,NULL,1598435067,1598435067),(16,48,29000,259000,777,NULL,1598435067,1598435067),(17,39,64000,358000,769,NULL,1598435067,1598435067),(18,44,82000,167000,780,NULL,1598435067,1598435067),(19,21,73000,123000,787,NULL,1598435067,1598435067),(20,16,0,765000,664,NULL,1598435067,1598435067),(21,49,151000,177000,770,NULL,1598435067,1598435067),(22,11,149000,293000,794,NULL,1598435067,1598435067),(23,26,154000,154000,707,NULL,1598435067,1598435067),(24,15,106000,184000,726,NULL,1598435067,1598435067),(25,41,0,332000,734,NULL,1598435067,1598435067),(26,8,92000,386000,675,NULL,1598435067,1598435067),(27,24,17000,385000,788,NULL,1598435067,1598435067),(28,34,50000,151000,781,NULL,1598435067,1598435067),(29,43,191000,620000,775,NULL,1598435067,1598435067),(30,5,161000,313000,773,NULL,1598435067,1598435067),(31,20,12000,139000,727,NULL,1598435067,1598435067),(32,13,126000,468000,776,NULL,1598435067,1598435067),(33,23,47000,384000,779,NULL,1598435067,1598435067),(34,38,94000,230000,652,NULL,1598435067,1598435067),(35,14,105000,233000,741,NULL,1598435067,1598435067),(36,22,65000,397000,786,NULL,1598435067,1598435067),(37,9,90000,160000,698,NULL,1598435067,1598435067),(38,25,1000,136000,691,NULL,1598435067,1598435067),(39,37,292000,126000,792,NULL,1598435067,1598435067),(40,12,180000,372000,774,NULL,1598435067,1598435067),(41,17,87000,315000,800,NULL,1598435067,1598435067),(42,46,50000,330000,795,NULL,1598435067,1598435067),(43,19,177000,79000,771,NULL,1598435067,1598435067),(44,10,251000,441000,662,NULL,1598435067,1598435067),(45,6,130000,444000,653,NULL,1598435067,1598435067),(46,4,68000,167000,772,NULL,1598435067,1598435067),(47,3,59000,186000,696,NULL,1598435067,1598435067),(48,7,0,123000,783,NULL,1598435067,1598435067),(49,2,127000,349000,667,NULL,1598435067,1598435067),(50,1,345000,683000,768,NULL,1598435067,1598435067);
/*!40000 ALTER TABLE `wallets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

