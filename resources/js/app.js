require('./bootstrap');
require('ziggy-js');
window.Vue = require('vue');
import Vue from 'vue'

Vue.component('plz-show', require('./components/plzShow.vue').default);

window.onload = function () {
    let app = new Vue({
        el: '#app',
    });
};
