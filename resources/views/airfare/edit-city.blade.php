@extends('layouts.app')
@section('content')
    <div class="container">

        <div class="row" id="city">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-6" v-for="city in cities">
                        <div class="btn-danger btn-light" style="padding: 10px;font-size:14px;margin: 10px;">
                            @{{city.full_name}}
                            <div class="btn btn-success" @click="createForm(city.id)">ویرایش</div>
                            <div class="btn btn-danger" @click="deleteCity(city.id)">حذف</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3" v-if="formUpdate" v-show="formUpdate" style="display: none;font-size:14px;">
                <input class="form-control" type="hidden" name="id" v-model="formId">
                <label>نام انگلیسی</label>
                <input class="form-control" name=name_en" v-model="name_en" style="font-size:16px;direction: ltr;text-align: left;">
                <label>نام فارسی</label>
                <input class="form-control" name="name_fa" v-model="name_fa" style="font-size: 16px;">
                <label>نام کامل</label>
                <input class="form-control" name="full_name" v-model="full_name" style="font-size:16px;direction: ltr;text-align: left;">
                <button @click="saveUpdate" class="btn btn-success">بروز رسانی</button>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        let city = new Vue({
            el: "#city",
            data: {
                cities:{!! json_encode($listAirFareCityJson) !!},
                formUpdate: false,
                formId: "",
                name_en: "",
                name_fa: "",
                full_name: ""
            },
            methods: {
                createForm: function (cityId) {
                    let index = _.findIndex(this.cities, ['id', cityId]);
                    this.formUpdate = this.cities[index];
                    this.formId = this.formUpdate.id;
                    this.name_en = this.formUpdate.name_en;
                    this.name_fa = this.formUpdate.name_fa;
                    this.full_name = this.formUpdate.full_name;
                },
                saveUpdate: function () {
                    let post = {
                        form_id: this.formId,
                        name_en: this.name_en,
                        name_fa: this.name_fa,
                        full_name: this.full_name,
                    };
                    let index = _.findIndex(this.cities, ['id', this.formUpdate.id]);
                    this.cities[index].name_en = this.name_en;
                    this.cities[index].name_fa = this.name_fa;
                    this.cities[index].full_name = this.full_name;
                    axios.post('http://sadra:8000/airfare/update', post).then(res => {
                        this.formUpdate = false;
                    })
                },
                deleteCity:function (formId) {
                    let post = {
                        form_id: formId,
                    };
                    console.log(formId);
                    let index = _.findIndex(this.cities, ['id', formId]);
                    axios.post('http://sadra:8000/airfare/delete', post).then(res => {
                        this.cities.splice(index, 1);
                    })
                }
            }
        })
    </script>
@endsection
