@extends('layouts.app')
@section('content')
    <div class="container">

        <div class="row" id="city">
            <span class="col-lg-3 btn btn-primary" style="font-size:14px;color:white;padding: 10px;font-size:14px;margin: 10px;" @click="orderCity('NameEN')">مرتب سازی بر اساس نام انگلیسی شهر</span>
            <span class="col-lg-3 btn btn-primary" style="font-size:14px;color:white;padding: 10px;font-size:14px;margin: 10px;" @click="orderCity('NameFA')">مرتب سازی بر اساس نام فارسی شهر</span>
            <span class="col-lg-3 btn btn-primary" style="font-size:14px;color:white;padding: 10px;font-size:14px;margin: 10px;" @click="orderCity('FullName')">مرتب سازی بر اساس نام کامل</span>
                <div class="col-lg-3" v-for="city in cities">
                <div
                    :class="_.findIndex(notShow, ['abbreviation', city.Abbreviation]) != -1 ? 'btn-danger':'btn-light'"
                    @click="addCity(city.Abbreviation)" class="btn" :ref="city.Abbreviation"
                    style="padding: 10px;font-size:14px;margin: 10px;">@{{city.FullName}}</div>
                </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        let city = new Vue({
            el: "#city",
            data: {
                notShow:{!! $selectedCity !!},
                cities:{!! $cityVue !!},
                order:Array
            },
            methods: {
                addCity: function (Abbreviation) {
                    axios.post('http://sadra:8000/airfare/save/' + Abbreviation).then(res => {
                        this.$refs[Abbreviation][0].classList.toggle("btn-danger");
                        this.$refs[Abbreviation][0].classList.toggle("btn-light");
                    })
                },
                orderCity:function(orderby){
                    if (this.order[orderby] === undefined) {
                        this.order[orderby] = true;
                    }
                    this.cities = this.order[orderby] ?  _.orderBy(this.cities, orderby, 'asc'): _.orderBy(this.cities, orderby, 'desc');
                    this.order[orderby] = !this.order[orderby];
                }
            }
        })
    </script>
@endsection
