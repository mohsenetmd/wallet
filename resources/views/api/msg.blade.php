@extends('layouts.app')
@section('content')
    <div class="container" style="margin-top: 20px">
        <div class="alert
            @if($type == 'error')
            alert-danger
            @elseif($type == 'success')
            alert-success
            @endif
            ">{{$msg}}
        </div>
        <a href="{{route('panelFinotechAc')}}" class="btn-success">بازگشت به پنل</a>
    </div>
@endsection
