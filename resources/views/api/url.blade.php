@extends('layouts.app')
@section('content')
    <div style="display: block;text-align: center;margin-top: 100px;">
        <a href="{{$url}}" class="btn btn-success" style="font-size: 14px;">اجازه
            دسترسی به حساب بانکی</a>
    </div>
    <livewire:table-token-finoteck></livewire:table-token-finoteck>
@endsection
@section('script')
    <script>
        let app=new Vue({
            el:"#app",
            data:{
                counter:0
            }
        });
    </script>
@endsection
