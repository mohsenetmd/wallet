@extends('layouts.app')
@section('content')
    <style>
        .font-size {
            font-size: 16px;
        }
    </style>
    <div class="container">
        <form class="row" style="font-size:16px;" method="post" action="{{route('charity.update',$charity->id)}}"
              enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="col-lg-6">
                <label>نام خیریه</label>
                <input class="form-control font-size" type="text" name="name" value="{{$charity->name}}">
            </div>
            <div class="col-lg-6">
                <label>شماره حساب</label>
                <input class="form-control font-size" type="text" name="account_number"
                       value="{{$charity->account_number}}">
            </div>
            <div class="col-lg-6" style="margin-top: 10px">
                <label>ارسال عکس</label>
                <input type="file" name="icon">
            </div>
            <div class="col-lg-3">
                <button type="submit" class="btn btn-danger font-size" style="margin-top: 10px">ذخیره</button>
            </div>
        </form>
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="alert alert-danger">{{$error}}</div>
            @endforeach
        @endif
    </div>
@endsection
