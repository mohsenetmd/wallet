@extends('layouts.app')
@section('content')
    <style>
        .table-style:nth-child(even) {
            background-color: rgba(0, 0, 0, 0.1) !important;
        }
        .table-style{
            padding-top: 10px;
            padding-bottom: 10px;
            font-size: 14px;
        }
    </style>
    <div id="order" style="margin-top: 20px;">
        <div class="container">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <a class="btn btn-primary" href="{{route('charity.create')}}" style="color: white;float: left;font-size: 14px;">افزودن خیریه</a>
                </div>
            </div>
            <div class="row" style="font-size:14px;">
                <div class="col-lg-3 btn btn-light" style="font-size:14px" @click="orderCharity('name_charity')">
                    نام خیریه
                </div>
                <div class="col-lg-3 btn btn-light" style="font-size:14px" @click="orderCharity('account_number')">
                    شماره حساب
                </div>
                <div class="col-lg-3">
                    آیکون
                </div>
            </div>
            <div v-for="list in lists" class="row table-style" v-show="lists" style="display: none;">
                <div class="col-lg-3">@{{ list.name_charity }}</div>
                <div class="col-lg-3">@{{ list.account_number }}</div>
                <div class="col-lg-3"><img width="100px" height="100px" :src=" 'http://sadra:8000/' + list.icon "></div>
                <div class="col-lg-3">
                    <button class="btn btn-danger" @click="deleteCharity(list.id)">حذف</button>
                    <a class="btn btn-success" :href="'http://sadra:8000/charity/' + list.id + '/edit'" >ویرایش</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // function testu() {
        //     var formData = new FormData();
        //     var imagefile = document.querySelector('#file');
        //     formData.append("image", imagefile.files[0]);
        //     axios.post('upload_file', formData, {
        //         headers: {
        //             'Content-Type': 'multipart/form-data'
        //         }
        //     }).then(res=>{
        //         console.log(res);
        //     })
        // }
        let sort = new Vue({
            el: "#order",
            name:"",
            data: {
                lists: Object,
                order: Array,
            },
            mounted() {
                this.lists = this.getCharity();
            },
            methods: {
                getCharity: function () {
                    let post = {orderBy: 'created_at'};
                    axios.post('{{route('charity.index')}}', post).then(response => {
                        this.lists = response.data;
                    });
                },
                orderCharity: function (orderby) {
                    this.lists = _.orderBy(this.lists, orderby, 'desc');
                    if (this.order[orderby] === undefined) {
                        this.order[orderby] = true;
                    }
                    if (this.order[orderby]) {
                        this.lists = _.orderBy(this.lists, orderby, 'desc');
                    } else {
                        this.lists = _.orderBy(this.lists, orderby, 'asc');
                    }
                    this.order[orderby] = !this.order[orderby];
                },
                deleteCharity: function (charityId) {
                    axios.delete('http://sadra:8000/charity/'+charityId).then(response => {
                        let index=_.findIndex(this.lists, {'id': charityId });
                        this.lists.splice(index, 1);
                    });
                },
            },
        })
    </script>
@endsection
