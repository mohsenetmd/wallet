@extends('layouts.app')
@section('content')
    <div class="container" style="font-size:14px !important;">
        <div class="row" id="select">
            <div class="col-lg-3">
                <ul>
                    <li class="btn btn-light" v-on:click="showTab(0)" style="margin: 5px;font-size:14px;width:100%;">
                        نمایش فرم
                    </li>
                    <li class="btn btn-light" v-on:click="showTab(1)" style="margin: 5px;font-size:14px;width:100%;">
                        نمایش جدول
                    </li>
                    <li class="btn btn-light" v-on:click="showTab(2)" style="margin: 5px;font-size:14px;width:100%;">
                        نمایش لیست توکن ها با استفاده از API
                    </li>
                    <li class="btn btn-light" v-on:click="showTab(3)" style="margin: 5px;font-size:14px;width:100%;">
                        نمایش کارمزدها ها با استفاده از API
                    </li>
                    <li class="btn btn-light" v-on:click="showTab(4)" style="margin: 5px;font-size:14px;width:100%;">
                        نمایش کارت ها
                    </li>
                    <li class="btn btn-light" v-on:click="showTab(5)" style="margin: 5px;font-size:14px;width:100%;">
                          نمایش بانک ها با استفاده از API
                    </li>
                </ul>
            </div>
            <div class="col-lg-9">
                <div v-show="tabs[0]">
                    <livewire:finotech.select-cc></livewire:finotech.select-cc>
                </div>
                <div v-show="tabs[1]" style="display: none">
                    <livewire:finotech.table-cc></livewire:finotech.table-cc>
                </div>
                <div v-show="tabs[2]" style="display: none">
                    <livewire:finotech.list-cc></livewire:finotech.list-cc>
                </div>
                <div v-show="tabs[3]" style="display: none">
                    <livewire:finotech.list-wage></livewire:finotech.list-wage>
                </div>
                <div v-show="tabs[4]" style="display: none">
                    <livewire:finotech.table-card></livewire:finotech.table-card>
                </div>
                <div v-show="tabs[5]" style="display: none">
                    <livewire:finotech.bank-info></livewire:finotech.bank-info>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        let app = new Vue({
            el: "#select",
            data: {
                tabs: [1, 0, 0, 0, 0,0],
            },
            methods: {
                showTab(index) {
                    this.tabs.fill(0);
                    Vue.set(this.tabs, index, 1);
                }
            }
        })
    </script>
@endsection
