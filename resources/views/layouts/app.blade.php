<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.jpg')}}">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="NineKit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <style>
        #map {position: absolute; top: 0;bottom: 0;left:0;right:0}
    </style>
    <title>{{ config('app.name', 'sadra') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
   {{-- //----------------------------------}}
   <meta charset="utf-8" />
    <title>Searching map services</title>
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />

    <!-- Load Leaflet from CDN -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
            integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
            crossorigin=""></script>

    <!-- Load Esri Leaflet from CDN -->
    <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
            integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
            crossorigin=""></script>

    <!-- Load Esri Leaflet Geocoder from CDN -->
    <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
          integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
          crossorigin="">
    <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
            integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
            crossorigin=""></script>

    <style>
        body { margin:0; padding:0; }
        #map { position: absolute; top:0; bottom:0; right:0; left:0; }
    </style>
   {{-- //-------------------------------}}
</head>

<div  class="header">
    <div class="header-content">

        <div class="header-left">

            <ul>
                <li class="icons position-relative"><a href="javascript:void(0)"><i
                            class="icon-magnifier f-s-16"></i></a>
                    <div class="drop-down animated bounceInDown">
                        <div class="dropdown-content-body">
                            <div class="header-search" id="header-search">
                                <form action="#">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="جستجو ...">
                                        <div class="input-group-append"><span class="input-group-text"><i
                                                    class="icon-magnifier"></i></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <div  class="header-right">
            <ul>
                <li class="icons">
                    <a href="javascript:void(0)">
                        <i class="mdi mdi-comment"></i>
                        <div class="pulse-css"></div>
                    </a>
                    <div class="drop-down animated bounceInDown">
                        <div class="dropdown-content-heading">
                            <span class="pull-right">پیام ها</span>
                            <a href="javascript:void()" class="pull-left text-white">مشاهده همه</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="dropdown-content-body">
                            <ul>
                                <li class="notification-unread">
                                    <a href="javascript:void()">
                                        <img class="pull-left mr-3 avatar-img"
                                             src="../../assets/images/avatar/1.jpg" alt="">
                                        <div class="notification-content">
                                            <div class="notification-heading">رضا افشار</div>
                                            <div class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                ایپسوم</div><small class="notification-timestamp">08 ساعت
                                                پیش</small>
                                        </div>
                                    </a><span class="notify-close"><i class="ti-close"></i></span>
                                </li>
                                <li class="notification-unread">
                                    <a href="javascript:void()">
                                        <img class="pull-left mr-3 avatar-img"
                                             src="../../assets/images/avatar/2.jpg" alt="">
                                        <div class="notification-content">
                                            <div class="notification-heading">امیر صبوری</div>
                                            <div class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                ایپسوم</div><small class="notification-timestamp">08 ساعت
                                                پیش</small>
                                        </div>
                                    </a><span class="notify-close"><i class="ti-close"></i></span>
                                </li>
                                <li>
                                    <a href="javascript:void()">
                                        <img class="pull-left mr-3 avatar-img"
                                             src="../../assets/images/avatar/3.jpg" alt="">
                                        <div class="notification-content">
                                            <div class="notification-heading">پدرام شریفی</div>
                                            <div class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                ایپسوم</div><small class="notification-timestamp">08 ساعت
                                                پیش</small>
                                        </div>
                                    </a><span class="notify-close"><i class="ti-close"></i></span>
                                </li>
                                <li>
                                    <a href="javascript:void()">
                                        <img class="pull-left mr-3 avatar-img"
                                             src="../../assets/images/avatar/4.jpg" alt="">
                                        <div class="notification-content">
                                            <div class="notification-heading">نسترن سبحانی</div>
                                            <div class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                ایپسوم</div><small class="notification-timestamp">08 ساعت
                                                پیش</small>
                                        </div>
                                    </a><span class="notify-close"><i class="ti-close"></i></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class="icons">
                    <a href="javascript:void(0)">
                        <i class="mdi mdi-bell"></i>
                        <div class="pulse-css"></div>
                    </a>
                    <div  class="drop-down animated bounceInDown dropdown-notfication">
                        <div class="dropdown-content-body">
                            <ul>
                                <li>
                                    <a href="javascript:void()">
                                                <span class="mr-3 avatar-icon bg-success-lighten-2"><i
                                                        class="fa fa-check"></i></span>
                                        <div class="notification-content">
                                            <div class="notification-heading">یلدا افشار</div>
                                            <span class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                        ایپسوم</span>
                                            <small class="notification-timestamp">20 تیر 1399، 15:30</small>
                                        </div>
                                    </a>
                                    <span class="notify-close"><i class="ti-close"></i>
                                            </span>
                                </li>
                                <li><a href="javascript:void()"><span
                                            class="mr-3 avatar-icon bg-danger-lighten-2"><i
                                                class="fa fa-close"></i></span>
                                        <div class="notification-content">
                                            <div class="notification-heading">کیمیا صبوری</div><span
                                                class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                        ایپسوم</span> <small class="notification-timestamp">20 تیر 1399،
                                                15:30</small>
                                        </div>
                                    </a>
                                    <span class="notify-close"><i class="ti-close"></i>
                                            </span>
                                </li>
                                <li><a href="javascript:void()"><span
                                            class="mr-3 avatar-icon bg-success-lighten-2"><i
                                                class="fa fa-check"></i></span>
                                        <div class="notification-content">
                                            <div class="notification-heading">پدرام شریفی</div><span
                                                class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                        ایپسوم</span> <small class="notification-timestamp">20 تیر 1399،
                                                15:30</small>
                                        </div>
                                    </a>
                                    <span class="notify-close"><i class="ti-close"></i>
                                            </span>
                                </li>
                                <li><a href="javascript:void()"><span
                                            class="mr-3 avatar-icon bg-danger-lighten-2"><i
                                                class="fa fa-close"></i></span>
                                        <div class="notification-content">
                                            <div class="notification-heading">نسترن سبحانی</div><span
                                                class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                        ایپسوم</span> <small class="notification-timestamp">20 تیر 1399،
                                                15:30</small>
                                        </div>
                                    </a>
                                    <span class="notify-close"><i class="ti-close"></i>
                                            </span>
                                </li>
                                <li class="text-left"><a href="javascript:void()" class="more-link">نمایش همه
                                        اعلان ها</a> <span class="pull-left"><i
                                            class="fa fa-angle-left"></i></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="icons">
                    <a href="javascript:void(0)" class="log-user">
                        <img src="../../assets/images/avatar/1.jpg" alt=""> <span> {{session('username') }} </span> <i
                            class="fa fa-caret-down f-s-14" aria-hidden="true"></i>
                    </a>
                    <div class="drop-down dropdown-profile animated bounceInDown">
                        <div class="dropdown-content-body">
                            <ul>
                                <li><a href="javascript:void()"><i class="icon-user"></i> <span>پروفایل
                                                    من</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-wallet"></i> <span>کیف پول
                                                    من</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-envelope"></i>
                                        <span>صندوق دریافت</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="fa fa-cog"></i>
                                        <span>تنظیمات</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-lock"></i> <span>قفل
                                                    صفحه</span></a>
                                </li>
                                <li><a  href="{{route('panel.user.logout')}}  " ><i class="icon-power"></i>
                                        <span>خروج</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div>

<body dir="rtl">

<main class="py-4">
    <div  >
        <div class="card-header pb-0">
            <h5  class="card-title mt-2">
                <a  id="pageTitle"  href="{{route('panel.user.home')}}"  >خانه</a>

            </h5>
        </div>

    </div>
    @yield('content')
</main>
<script src="{{ asset('assets/plugins/common/common.min.js')}}"></script>
<script src="{{ asset('js/custom.min.js')}}"></script>
<script src="{{ asset('js/settings.js')}}"></script>
<script src="{{ asset('js/gleek.js')}}"></script>
<script src="{{ asset('js/styleSwitcher.js')}}"></script>

<!-- Chartjs chart -->
<script src="{{ asset('assets/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{ asset('assets/plugins/d3v3/index.js')}}"></script>
<script src="{{ asset('assets/plugins/topojson/topojson.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datamaps/datamaps.world.min.js')}}')}}"></script>

<script src="{{ asset('js/plugins-init/datamap-world-init.js')}}"></script>

<script src="{{ asset('assets/plugins/datamaps/datamaps.usa.min.js')}}"></script>

<script src="{{ asset('js/dashboard/dashboard-1.js')}}"></script>

<script src="{{ asset('js/plugins-init/datamap-usa-init.js')}}"></script>

@livewireScripts
<script src="{{ asset('js/livewire-vue.js')}}"></script>
<script src="{{ asset('js/vue.min.js')}}"></script>
</body>
@yield('script')
@yield('errors')
</html>
