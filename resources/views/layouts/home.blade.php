@extends('layouts.app')
@section('content')
    <div id="main-wrapper">
        <div class="nav-header">
            <div class="brand-logo"><a href="index.html"><b><img src="../../assets/images/f-logo.jpg" alt=""> </b><span
                        class="brand-title"> </span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span> <span class="line"></span> <span class="line"></span>
                </div>
            </div>
        </div>
        <div class="nk-sidebar">
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">داشبورد</li>
                    <li class="mega-menu mega-menu-lg">
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-view-dashboard"></i><span class="nav-text">داشبورد</span><span
                                class="badge bg-dpink text-white nav-badge">21</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a  href="{{route('panel.user.list')}}  ">لیست کاربران</a></li>
                            <li><a  href="{{route('user.list')}}  ">لیست مشتریان</a></li>
                            <li><a  href="{{route('user.listMarketer')}}  ">لیست بازاریاب ها</a></li>
                            <li><a href="{{route('panel.group.insert')}}">لیست گروه ها</a>
                            </li>
                            <li><a href="{{route('panel.usergroup.insert')}}">لیست گروه های کاربری</a>
                            </li>
                            <li><a href="{{route('panel.permissiongroup.insert')}}">دسترسی گروه ها</a>
                            </li>
                            <li><a href="{{route('panel.permissionuser.insert')}}">دسترسی کاربران</a>
                            </li>
                            <li><a href="{{route('panel.tourism.typeCategory')}}">انواع دسته بندی </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-label">جداول</li>
                    <li class="nav-label">بیشتر</li>
                    <li>
                 </li>
                </ul>
            </div>
        </div>
        <div class="content-body">
            <div  id="container-menu"  style="visibility: hidden">
                <table class="table table-bordered data-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>نام کاربر</th>
                        <th>تاریخ و زمان ثبت</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="container-fluid"     id="container-page" >
                <div class="row" id="dragdrop">
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-two">
                                    <div class="media">
                                        <div class="media-body">
                                            <h2 class="mt-0 mb-1 text-info"> </h2><span class="">
                                               </span>
                                        </div>
                                        <a href="{{route('panel.taxi.request')}}" ><img class="ml-3" src="../../assets/images/icons/1.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-two">
                                    <div class="media">
                                        <a href="{{route('panel.taxi.request')}}">
                                            {{--<div class="media-body">
                                                <h2 class="mt-0 mb-1 text-warning"></h2><span class=""></span>
                                            </div>--}}
                                            <img class="ml-3" src="../../assets/images/icons/taxi1.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8 col-xxl-7 col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title m-t-10">گردشگری</h4>
                                <div class="table-action float-right">
                                    <form action="#">
                                        <div class="form-row">
                                            <div class="form-group m-b-0" style="width: 300px">
                                                <select id="placeInfo" class="form-control" onchange="listTypeCategory()">

                                                </select>
                                            </div>
                                        </div>
                                    </form>


                                    <div class="col-md-4 col-sm-12">


                                       {{-- <div id='typeCategory' class="form-group col-lg-4">

                                            --}}{{-- <input type="hidden" id="typeCategory">
                                            <a  href="{{route('panel.tourism.homeTourism')}}">dddddddd</a>--}}{{--

                                        </div>--}}
                                    </div>


                                </div>
                            </div>



                                    <div id='typeCategory' class="card-body">


                                    </div>




                        </div>
                    </div>
                    <div class="col-xl-4 col-xxl-5 col-lg-4">
                        <div class="card">
                            <div class="card-header">
                                <!-- <div class="card-action"><a href="javascript:void(0)" data-action="collapse"><i class="ti-plus"></i></a> <a href="javascript:void(0)" data-action="expand"><i class="icon-size-fullscreen"></i></a>
                                    <a href="javascript:void(0)" data-action="close"><i class="ti-close"></i>
                                    </a><a href="javascript:void(0)" data-action="reload"><i class="icon-reload"></i></a>
                                </div> -->
                                <h4 class="card-title">پرفروش ترین محصولات</h4>
                            </div>
                            <div class="card-body">
                                <canvas id="most-selling-items"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">مشتریان در سرتاسر جهان</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5 col-xl-6">
                                        <div id="world-map-restaurant"></div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="map-coruntry-list">
                                            <h4 class="mb-5">لیست کشورها</h4>
                                            <ul>
                                                <li><a href="javascript:void()"><i
                                                            class="fa fa-circle-o text-success"></i> ایران
                                                        <span>55%</span></a>
                                                </li>
                                                <li><a href="javascript:void()"><i
                                                            class="fa fa-circle-o text-warning"></i> کانادا
                                                        <span>60%</span></a>
                                                </li>
                                                <li><a href="javascript:void()"><i class="fa fa-circle-o text-info"></i>
                                                        روسیه <span>18%</span></a>
                                                </li>
                                                <li><a href="javascript:void()"><i
                                                            class="fa fa-circle-o text-secondary"></i> چین
                                                        <span>20%</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-3">
                                        بدون استفاده
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <!-- <div class="col-lg-12 d-flex"> -->
                    <div class="col-lg-9">
                        <div class="card">
                            <div class="card-body text-center">
                                <div class="row">
                                    <div class="col-sm-3 mb-sm-0">
                                        <div class="stat-widget-three py-2">
                                            <div class="media">
                                                <img class="mr-4 mt-3" src="../../assets/images/icons/4.png" alt="">
                                                <div class="media-body">
                                                    <h2 class="mt-0 mb-1 text-info">62,150</h2>
                                                    <span class="text-pale-sky ">کل سفارشات</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 mb-sm-0">
                                        <div class="stat-widget-three py-2">
                                            <div class="media">
                                                <img class="mr-4 mt-3" src="../../assets/images/icons/5.png" alt="">
                                                <div class="media-body">
                                                    <h2 class="mt-0 mb-1 text-success">9,750</h2>
                                                    <span class="text-pale-sky ">کل تحویل ها</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="stat-widget-three py-2">
                                            <div class="media">
                                                <img class="mr-4 mt-3" src="../../assets/images/icons/6.png" alt="">
                                                <div class="media-body">
                                                    <h2 class="mt-0 mb-1 text-danger">4,250</h2>
                                                    <span class="text-pale-sky ">سفارشات در انتظار</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="stat-widget-three py-2">
                                            <div class="media">
                                                <img class="mr-4 mt-3" src="../../assets/images/icons/7.png" alt="">
                                                <div class="media-body">
                                                    <h2 class="mt-0 mb-1 text-warning">4,250</h2>
                                                    <span class="text-pale-sky ">سفارشات ارسال شده</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body text-center">
                                <div class="stat-widget-three py-2">
                                    <div class="media">
                                        <img class="mr-4" src="../../assets/images/icons/8.png" alt="">
                                        <div class="media-body">
                                            <div class="rating d-flex align-items-center">
                                                <span class="m-0">
                                                    <span class="text-warning"><i class="fa fa-star"></i></span>
                                                    <span class="text-warning"><i class="fa fa-star"></i></span>
                                                    <span class="text-warning"><i class="fa fa-star"></i></span>
                                                    <span class="text-warning"><i class="fa fa-star"></i></span>
                                                    <span class=""><i class="fa fa-star"></i></span>
                                                </span>
                                                <h2 class="mt-0 mb-0 ml-3 text-warning">4.0</h2>
                                            </div>
                                            <span class="text-pale-sky ">رضایت مشتری</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- </div> -->
                </div>
                <div class="row">
                    <div class="col-lg-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var typeTest=1;

            listCity();
            listTypeCategory();
        });
        function listTypeCategory(){
            var group=document.getElementById("placeInfo");
            var seleted=group.selectedIndex;
           if(seleted==-1) {
                seleted = 1;
            }
            $.ajax({
                url: '{{route('panel.tourism.listTypeCategory')}}',
                type: 'POST',
                data:
                    {
                        'cityId': seleted,
                    },
                success: function(data ) {
                    $('#typeCategory').html(data);
                    //$('#placeInfo').html(data);
                    /*for (i = 0; i < data.length; i++) {
                       var car = new Option(data[i]['title']);
                       group.options.add(car);
                   } */
                }
            });
        }
        function listCity(){
             var group=document.getElementById("placeInfo");
            $.ajax({
                url: '{{route('panel.tourism.listCity')}}',
                type: 'POST',
                success: function(data ) {
                   //('#typeCategory').html(data);
                    //$('#placeInfo').html(data);
                      for (i = 0; i < data.length; i++) {
                        var car = new Option(data[i]);
                        group.options.add(car);
                    }
                }
            });
        }
        var table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{{route('panel.user.list')}}",
                method: 'POST'
            },
            columns: [
                {data: 'username', name: 'username'},
                {data: 'mobile', name: 'mobile'},
                {data: 'created_at', name: 'created_at'},
                {data: 'link_city', name: 'link_city'},
                // {data: 'number_user', name: 'number_user'},
                /* {data: 'mobile', name: 'mobile'},
                 {data: 'created_at', name: 'username'},
                 {data: 'action', name: 'action'},*/
            ]
        });
        console.log(table);


        function  showDiv(name) {
            if(name=='page') {
                alert(name);
                document.getElementById("container-page").style.visibility = 'visible'
                document.getElementById("container-menu").style.visibility = 'hidden'
            }
            else{
                alert(name);
                document.getElementById("container-menu").style.visibility = 'visible'
                document.getElementById("container-page").style.visibility = 'hidden'
            }
        }
        function selectTypeGroup(){
             var group=document.getElementById("placeInfo");
             var seleted=group.selectedIndex;

            $.ajax({
                url: '{{route('panel.tourism.listTypeCategory')}}',
                type: 'POST',
                data:
                    {
                        cityId:seleted
                    },
                success: function(data ) {

                }
            });

        }
        function  showhome(){
            showDiv('page');
            $('#container-menu').innerHTML="dsddd";
        }
        function listUser() {
            showDiv('menu');
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('panel.user.list')}}",
                    method: 'POST'
                },
                columns: [

                    {data: 'username', name: 'username'}
                ]
            });
            console.log(table);
        }
        function listGroup(){
            $.ajax({
                url: '{{route('panel.user.list')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    group_id: 5
                },
                dataType: 'json',
                success: function(data ) {

                    $('#list-content').html(data);
                    $('#pageTitle').html($('.pageTitle').attr('id')) ;
                }
            });
        }
    </script>

@endsection
