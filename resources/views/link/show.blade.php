<form method="post" action="{{route('pay')}}">
    <input type="number" name="amount">
    <input type="hidden" name="url" value="{{$user->url}}">
    <button type="submit">ارسال</button>
</form>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
