<div class="container">
    <table class="table" style="font-size:14px;">
        <thead>
        <tr>
            <th><span style="cursor: pointer;" role="button" href="#">
                            نام خیریه
                        </span></th>
            <th><span role="button" href="#">
                            شماره حساب
                        </span></th>
            <th><span role="button" href="#">
                            ایکن
                        </span></th>
            <th><span role="button" href="#">
                            ویرایش
                        </span></th>
        </tr>
        </thead>
        <tbody style="">
        @foreach ($charities as $charity)
            <tr>
                <td>{{ $charity->name }}</td>
                <td style="text-align: left;direction: ltr">{{ $charity->account_number}}</td>
                <td style="text-align: left;direction: ltr"><img
                        src="{{\Illuminate\Support\Facades\URL::to($charity->icon)}}"
                        style="max-width: 100px;max-height: 100px;"></td>
                <td>
                    <a class="btn btn-success" href="{{route('charity.update', $charity->id)}}">برزو رسانی</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $charities->links("pagination::bootstrap-4") }}
</div>

