<div>

    @if($showButton)
        <button wire:click="showList" class="btn btn-primary"
                style="color:white;font-size: 14px;margin: auto;margin-top: 10px;margin-bottom: 10px;display: block;">
            فراخوانی لیست بانک ها از API
        </button>
    @endif
    @if($message)
        <div class="alert alert-danger">{{$message}}</div>
    @endif
    <table class="table" style="font-size:14px;">
        <thead>
        <tr>
            <th><span style="cursor: pointer;" role="button" href="#">
                            فرمت شماره کارت
                        </span></th>
            <th><span role="button" href="#">
                            نام بانک
                        </span></th>
        </tr>
        </thead>
        <tbody style="">
        @foreach ($bankInfos as $bankInfo)
            <tr>
                <td>{{ $bankInfo->cardPrefix }}</td>
                <td>{{ $bankInfo->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
