<div>
    <table class="table" style="font-size:14px;">
        <thead>
        <tr>
            <th><span role="button" href="#">
                            scopes
                        </span></th>
            <th><span style="cursor: pointer;" wire:click.prevent="sort" role="button" href="#">
                            تاریخ ایجاد
                        </span></th>
        </tr>
        </thead>
        <tbody style="">
        @foreach ($lists as $list)
            <tr>
                <td>{{$list->code}}</td>
                <td>{{$list->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $lists->links("pagination::bootstrap-4") }}
</div>
