<div>
    @if($showButton)
        <button wire:click="showList" class="btn btn-primary" style="color:white;font-size: 14px;margin: auto;margin-top: 10px;margin-bottom: 10px;display: block;">فراخوانی توکن از API
        </button>
    @endif
    @if($message)
        <div class="alert alert-danger">{{$message}}</div>
    @endif
    <table class="table" style="font-size:14px;">
        <thead>
        <tr>
            <th><span style="cursor: pointer;" role="button" href="#">
                            تاریخ ایجاد
                        </span></th>
            <th><span role="button" href="#">
                            scopes
                        </span></th>
        </tr>
        </thead>
        <tbody style="">
        @foreach ($clientCredentials as $clientCredential)
            <tr>
                <td>{{ $clientCredential->creationDate }}</td>
                <td>{{ $clientCredential->scopes }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
