<div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <li class="btn btn-light" wire:click="showTab(1)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش url
                </li>
                <li class="btn btn-light" wire:click="showTab(2)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش لیست کدها
                </li>
                <li class="btn btn-light" wire:click="showTab(3)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش لیست توکن ها
                </li>
            </div>
            <div class="col-lg-9">
                @if($tabs[1])
                    <div>
                        @livewire('finotech.url-ac')
                    </div>
                @endif
                @if($tabs[2])
                    <div>
                        @livewire('finotech.code-ac')
                    </div>
                @endif
                @if($tabs[3])
                    <div>
                        @livewire('finotech.table-ac')
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
