<div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <li class="btn btn-light" wire:click="showTab(1)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش فرم
                </li>
                <li class="btn btn-light" wire:click="showTab(2)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش جدول توکن ها
                </li>
                <li class="btn btn-light" wire:click="showTab(3)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش لیست توکن ها با استفاده از API
                </li>
                <li class="btn btn-light" wire:click="showTab(4)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش کارمزد ها با استفاده از API
                </li>
                <li class="btn btn-light" wire:click="showTab(5)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش کارت ها
                </li>
                <li class="btn btn-light" wire:click="showTab(6)" style="margin: 5px;font-size:14px;width:100%;">
                    نمایش بانک ها با استفاده از API
                </li>
            </div>
            <div class="col-lg-9">
                @if($tabs[1])
                    <div>
                        @livewire('finotech.select-cc')
                    </div>
                @endif
                @if($tabs[2])
                    <div>
                        @livewire('finotech.table-cc')
                    </div>
                @endif
                @if($tabs[3])
                    <div>
                        @livewire('finotech.list-cc')
                    </div>
                @endif
                @if($tabs[4])
                    <div>
                        @livewire('finotech.list-wage')
                    </div>
                @endif
                @if($tabs[5])
                    <div>
                        @livewire('finotech.table-card')
                    </div>
                @endif
                @if($tabs[6])
                    <div>
                        @livewire('finotech.bank-info')
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

