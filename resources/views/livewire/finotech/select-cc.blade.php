<div>
    <div>
        @foreach($index_scopes as $index=>$value)
            <button class="btn btn-light" style="font-size:14px;margin: 5px;"
                    wire:click="selectScope({{$index}})">{{$value}}</button>
        @endforeach
    </div>
    <div>
        @foreach($selected_scopes as $index=>$selected_scope)
            <button class="btn btn-success" style="font-size:14px;color:white;margin: 5px"
                    wire:click="removeScope({{$index}})">{{$selected_scope}}</button>
        @endforeach
    </div>
    <div>
        <button class="btn btn-danger" wire:click.pervent="saveClienCredentical()" style="font-size:18px">ذخیره</button>
    </div>
</div>
