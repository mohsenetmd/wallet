<div>
    <div class="container">
        <div class="row">
            <table class="table" style="font-size:14px;">
                <thead>
                <tr>
                    <th><span role="button" href="#">
                            scopes
                        </span></th>
                    <th><span style="cursor: pointer;" wire:click.prevent="sortBy('life_time')" role="button" href="#">
                            تاریخ اعتبار
                        </span></th>
                    <th><span role="button" href="#">
                            تاریخ ایجاد
                        </span></th>
                    <th><span role="button" href="#">
                            ویرایش
                        </span></th>
                </tr>
                </thead>
                <tbody style="">
                @foreach ($tokens as $token)
                    <tr>
                        <td>{{ $token->scopes }}</td>
                        <td style="text-align: left;direction: ltr">{{ $token->life_time}}</td>
                        <td style="text-align: left;direction: ltr">{{ $token->creation_date }}</td>
                        <td>
                            @if($checkDeleteToken)
                                <span class="btn btn-danger" wire:click="delete({{$token->id}})">حذف</span>
                            @endif
                            <span class="btn btn-success" wire:click="refreshToken({{$token->id}})">تمدید</span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $tokens->links("pagination::bootstrap-4") }}
        <div class="col text-right text-muted">
            Showing {{ $tokens->firstItem() }} to {{ $tokens->lastItem() }} out of {{ $tokens->total() }} results
        </div>
        <div class="alert alert-light">{{$response}}</div>
    </div>
</div>
