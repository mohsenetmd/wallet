<div>
    <div class="container">
        <div class="row">
            <table class="table" style="font-size:14px;direction: ">
                <thead>
                <tr>
                    <th><span style="cursor: pointer;direction: ltr" wire:click.prevent="sortBy('created_at')" role="button" href="#">
                            تاریخ ایجاد
                        </span></th>
                    <th><span role="button" href="#">
                            شماره کارت
                        </span></th>
                    <th><span role="button" href="#">
                            نام
                        </span></th>
                    <th><span role="button" href="#">
                            شماره موبایل فرد درخواست دهنده
                        </span></th>
                </tr>
                </thead>
                <tbody style="">
                @foreach ($cardInfoes as $cardInfo)
                    <tr>
                        <td style="direction: ltr">{{ $cardInfo->do_time }}</td>
                        <td style="text-align: center;">{{ $cardInfo->card_number}}</td>
                        <td style="text-align: center;">{{ $cardInfo->name }}</td>
                        <td style="text-align: center;">{{ $cardInfo->name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $cardInfoes->links("pagination::bootstrap-4") }}
        <div class="col text-right text-muted">
            Showing {{ $cardInfoes->firstItem() }} to {{ $cardInfoes->lastItem() }} out of {{ $cardInfoes->total() }} results
        </div>
    </div>
</div>
