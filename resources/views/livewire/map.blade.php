<div>
    <label>جستجو</label>
    <input class="form-control" wire:keydown="searchLocation" wire:model="search">
    <div>
        @if(count($searchResult)>0)
            @foreach($searchResult as $searchResults)
                <div class="btn btn-danger"
                     wire:click="addToMap({{$searchResults['location']['latitude']}},{{$searchResults['location']['longitude']}},{{'"'.$searchResults['name'].'"'}})">{{$searchResults['name']}}</div>
            @endforeach
        @endif
    </div>
    <input class="form-control"  wire:model="latitude">
    <input class="form-control"  wire:model="longitude">
</div>
