<div>
    <div class="container" id="app">
        <div class="row">
            <div class="col-lg-4" style="text-align: left;direction: ltr;">
                Per Page: &nbsp;<div @click="counter++" class="btn btn-danger">{{ $count }}</div>
                <select wire:model="perPage" class="form-control">
                    <option>10</option>
                    <option>15</option>
                    <option>25</option>
                </select>
            </div>

            <div class="col-lg-4">
                Search:
                <input wire:model="search" class="form-control" type="text" placeholder="Search Contacts...">
            </div>
        </div>
        <div class="row">
            <table class="table" style="font-size:14px;">
                <thead>
                <tr>
                    <th><span style="cursor: pointer;" wire:click.prevent="sortBy('monthly_call_limitation')" role="button" href="#">
                            تعداد تراکنش ماهانه
                        </span></th>
                    <th><span style="cursor: pointer;" wire:click.prevent="sortBy('max_amount_per_transaction')" role="button" href="#">
                            حداکثر مبلغ انتقال برای هر تراکنش
                        </span></th>
                    <th><span role="button" href="#">
                            scopes
                        </span></th>
                    <th><span style="cursor: pointer;" wire:click.prevent="sortBy('life_time')" role="button" href="#">
                            تاریخ اعتبار
                        </span></th>
                    <th><span role="button" href="#">
                            تاریخ ایجاد
                        </span></th>
                </tr>
                </thead>
                <tbody style="">
                @foreach ($tokens as $token)
                    <tr>
                        <td>{{ $token->monthly_call_limitation }}</td>
                        <td>{{ $token->max_amount_per_transaction }}</td>
                        <td>{{ $token->scopes }}</td>
                        <td style="text-align: left;direction: ltr">{{ $token->life_time}}</td>
                        <td style="text-align: left;direction: ltr">{{ $token->creation_date }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $tokens->links("pagination::bootstrap-4") }}
        <div class="col text-right text-muted">
            Showing {{ $tokens->firstItem() }} to {{ $tokens->lastItem() }} out of {{ $tokens->total() }} results
        </div>
    </div>
</div>
