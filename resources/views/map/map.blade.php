@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <livewire:map.map></livewire:map.map>
            </div>
            <div class="col-lg-9">
                <div id="content">
                    <div class="mapSnapp">
                        <div id="map">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
    <script>
        var map = L.map('map').setView([36.289632, 59.616130], 35);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        var popup = L.popup();
        function onMapClick(e) {
            popup
                .setLatLng(e.latlng)
                .setContent("مکان انتخابی:" + e.latlng.toString())
                .openOn(map);
        }

        map.on('click', onMapClick);
        var marker=false;
        Livewire.on('landmarkAdd', location => {
            if(marker){
                map.removeLayer(marker);
            }
            marker = new L.marker([location[0], location[1]]);
            map.addLayer(marker);
            marker.bindPopup('"'+location[2]+'"')
                .openPopup();
            var popup = L.popup();
            let app=new Vue({
                el:"app",
                data:{

                },
                methods:{
                    changeColor(event){
                        console.log(event);
                    }
                }
            })
        })
    </script>
@endsection
