@extends('layouts.app')
@section('content')

    <div class="container form-otah">
        <form enctype="multipart/form-data" method="post" action="{{route('panel.group.store')}}">
            @csrf
            <h1 class="titr"> گروه کاربری </h1>
            <hr>
            <div class="row">
                <div class="form-group col-lg-4">
                    <label>عنوان گروه</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <button class="btn btn-success" type="submit" style="height: 40px;">ارسال</button>
            </div>
        </form>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
