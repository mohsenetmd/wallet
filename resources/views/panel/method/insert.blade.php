@extends('layouts.app')
@section('content')
    <div class="container form-otah">
        <form enctype="multipart/form-data" method="post" action="{{route('method.store')}}">
            @csrf
            <h1 class="titr">عملیات</h1>
            <hr>
            <div class="row">
                <div class="form-group col-lg-4">
                    <label>عنوان:</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <button class="btn btn-success" type="submit" style="height: 40px;">ارسال</button>
            </div>
        </form>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
