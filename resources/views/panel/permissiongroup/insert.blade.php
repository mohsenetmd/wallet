@extends('layouts.app')
@section('content')
    <div class="container form-otah">
        <form enctype="multipart/form-data" name="myform" method="post" action="{{route('panel.permissiongroup.store')}}" >
            @csrf
            <h1 class="titr">دسترسی گروه ها</h1>
            <hr>
            <div >
                <div class="form-group col-lg-4">
                    <select name="group_id" class="form-control" onchange="selectGroup()">
                        @foreach ($groups as $group)
                            <option value="{{$group->group_id}}">{{$group->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div id="g_permission">
                </div>
              {{-- <button class="btn btn-success" type="submit" style="height: 40px;">ارسال</button>--}}
               <button class="btn btn-success" type="button"  onclick="save()" style="height: 40px;">ارسال</button>

            </div>
        </form>
    </div>
    <div id="g_result">
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
@section('script')
    <script  type="text/javascript" src="jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
       var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
       function selectGroup() {
           var group_id =  $('select[name="group_id"]').val();
           $.ajax({
               url: '{{route('panel.permissiongroup.ajax')}}',
               type: 'POST',
               data: {
                   _token: CSRF_TOKEN,
                   group_id: group_id
               },
               dataType: 'json',
               success: function(data ) {
                   $('#g_permission').html(data);
               }
           });
       }
      /* function save() {
           var group_id =  $('select[name="group_id"]').val();
           var status =  $('checkbox[name="status[1]"]').val();
           alert(status);
           $.ajax({
               //url:
               type: 'POST',
               data: {
                   _token: CSRF_TOKEN,
                   group_id: group_id,
                   status:status,

               },
               dataType: 'json',
               success: function(data ) {
                   $('#g_result').html(data);
               }
           });
       }*/
       function save() {
           var group_id =  $('select[name="group_id"]').val();
           var arr = $('input[name="myCheckboxes[]"]').map(function(){
               var dataString = $(this).serialize();
               return dataString;
           }).get();
           $.ajax({
               type: "POST",
               url:  '{{route('panel.permissiongroup.ajaxStore')}}',
               dataType: 'html',
               data: {myCheckboxes:arr ,group_id:group_id},
               dataType: 'json',
               success: function(data ) {
                   $('#g_result').html(data);
               }
           });
           return false;
       }


    </script>
@endsection

