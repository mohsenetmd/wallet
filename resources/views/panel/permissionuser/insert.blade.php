@extends('layouts.app')
@section('content')
    <div class="container form-otah">
        <form enctype="multipart/form-data" method="post" action="{{route('panel.permissionuser.store')}}">
            @csrf
            <h1 class="titr">دسترسی کاربران</h1>
            <hr>
            <div>
                <div class="form-group col-lg-4">
                    <select name="user_id" class="form-control" onchange="selectUser()">
                        @foreach ($users as $user)
                            <option value="{{$user->user_id}}">{{$user->username}}</option>
                        @endforeach
                    </select>
                </div><br>
                <div id="u_permission">
                </div><br>
                <button class="btn btn-success" type="submit" style="height: 40px;">ارسال</button>
            </div>
        </form>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
@section('script')
    <script  type="text/javascript" src="jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function selectUser() {
            var user_id =  $('select[name="user_id"]').val();
            $.ajax({
                url: '{{route('panel.permissionuser.ajax')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    user_id: user_id
                },
                dataType: 'json',
                success: function(data ) {
                    $('#u_permission').html(data);
                }
            });
        }
        function save() {
            $.ajax({
                url: '{{route('panel.permissiongroup.store')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    group_id: group_id
                },
                dataType: 'json',
                success: function(data ) {
                    alert('ok');
                }
            });
        }



    </script>
@endsection
