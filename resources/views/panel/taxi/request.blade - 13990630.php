@extends('layouts.app')
@section('content')
    <hr>
    <div id="content">
        <div class="mapSnapp">
            <div id="map">

            </div>
            
            <div class="col-lg-6" style="z-index: 10000;">
                <div  class="card">
                    <div class="card-body">
                        <div id="accordion-two" class="accordion">
                            <div class="card">
                                <div class="card-header" onclick="showDiv('collapseOne1')">
                                    <h5 class="mb-0" data-toggle="collapse" data-target="#collapseOne1"
                                        aria-expanded="true" aria-controls="collapseOne1"><i class="fa"
                                                                                             aria-hidden="true"></i>درخواست تاکسی</h5>
                                </div>
                                <div id="collapseOne1" class="collapse show" data-parent="#accordion-two">
                                    <div class="card-body pt-0">
                                        <div id="v-pills-home2" class="tab-pane fade active show">
                                            <h3> </h3>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label" >مبدا:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text"  onchange="requestService()"  class="form-control"  placeholder="انتخاب مبدا"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="origin"
                                                               aria-describedby="origin">
                                                        <input id="originLatLng"  type="hidden">
                                                        <button class="btn" onclick="deleteOrigin()"><i class="fa fa-trash"></i> </button>
                                                        <button type="button" onclick="chooseOrigin()" class="btn btn-outline-primary btn-sm">انتخاب مبدا از نقشه</button>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">مقصد:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text"     style="font-family: Iransans_Web; font-size: 14px"class="form-control"
                                                               id="destination"
                                                               placeholder="انتخاب مقصد"
                                                               aria-describedby="destination">
                                                        <input id="destinationLatLng"  type="hidden">
                                                        <button class="btn" onclick="deleteDestination();"><i class="fa fa-trash"></i> </button>

                                                        <button type="button"  onclick="chooseDestination()" class="btn btn-outline-primary btn-sm">انتخاب مقصدازنقشه</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">سرویس: </label>
                                                <div class="col-lg-9">
                                                    <select class="form-control" style="font-family: Iransans_Web; font-size: 10px">
                                                        <option id="service_type_name" class="text-muted" disabled selected   style="display: none">
                                                            انتخاب یک مورد</option>
                                                        <option  id="service_E" class="obtion_DDL">اسنپ اکو</option>
                                                        <option   id="service_P" class="obtion_DDL">اسنپ پلاس</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">هزینه:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="price" placeholder="لطفا مبدا و مقصد خود را انتخاب کنید"
                                                               disabled readonly aria-describedby="price">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">تلفن همراه مسافر:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="cellphone"
                                                               placeholder="" maxlength="11"
                                                               aria-describedby="inputGroupPrepend2">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">نام مسافر:</label>
                                                <div class="col-sm-9">
                                                    <p id="name" class="text-muted">سمانه عرب نظرگاه</p>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">زمان انتظار: </label>
                                                <div class="col-lg-9">
                                                    <select class="form-control" id="waiting" style="font-family: Iransans_Web; font-size: 10px">
                                                        <option class="text-muted" disabled selected style="display: none">
                                                            بدون زمان انتظار</option>
                                                        <option value="0" class="obtion_DDL">0 تا 5 دقیقه</option>
                                                        <option  value="5" class="obtion_DDL">5 تا 10 دقیقه</option>
                                                        <option  value="1" class="obtion_DDL">10 تا 15 دقیقه</option>
                                                        <option  value="15" class="obtion_DDL">15 تا 20 دقیقه</option>
                                                        <option   value="25" class="obtion_DDL">25 تا 30 دقیقه</option>
                                                        <option   value="30" class="obtion_DDL">30 تا 45 دقیقه</option>
                                                        <obtion  value="45" class="obtion_DDL">45 دقیقه تا 1 ساعت</option>
                                                            <option  value="1"class="obtion_DDL">1 تا 1.5 ساعت </option>
                                                            <option  class="obtion_DDL" value="1.5">1.5 تا 2 ساعت</option>
                                                            <option  value="2" class="obtion_DDL">2 تا 2.5 ساعت</option>
                                                            <option  value="2.5" class="obtion_DDL">2.5 تا 3 ساعت</option>
                                                            <option  value="3" class="obtion_DDL">3 تا 3.5 ساعت</option>
                                                            <option  value="3.5" class="obtion_DDL">3.5 تا 4 ساعت</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">رفت و برگشت </label>
                                                <div class="col-lg-9">
                                                    <div class="form-check p-l-0">
                                                        <input type="checkbox"
                                                               id="round_trip">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">  مقصد دوم:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="extra_destination"
                                                               placeholder="مقصد دوم"
                                                               aria-describedby="extra_destination">
                                                        <input id="extra_destinationLatLng"  type="hidden">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">

                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="hidden" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="result_ride_id" placeholder="..."
                                                               disabled readonly aria-describedby="result_ride_id">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-label"></label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <button   onclick="requestService()" class="btn-sl-lg">درخواست</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" onclick="showDiv('collapseTwo2')">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo2" aria-expanded="false"
                                        aria-controls="collapseTwo2"><i class="fa" aria-hidden="true"></i>
                                        مشخصات راننده</h5>
                                </div>
                                <div id="collapseTwo2" class="collapse" data-parent="#accordion-two">
                                    <div class="card-body pt-0">
                                        <div  id="driverInfo" >

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script type="text/javascript">
        var driverStatus=0;
        var originStatus=0;
        var destinationStatus=0;
        var markerDestination;
        var markerOrigin;
        var delOriginStatus=0;
        var delDestinationStatus=0;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {

            var table = $('.table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('panel.user.list')}}",
                    method: 'POST'
                },
                columns: [

                    {data: 'username', name: 'username'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'link_city', name: 'link_city'},
                    // {data: 'number_user', name: 'number_user'},
                    /* {data: 'mobile', name: 'mobile'},
                     {data: 'created_at', name: 'username'},
                     {data: 'action', name: 'action'},*/
                ]
            });
            console.log(table);
        });
        function calculatePrice()
        {

            var originLatLng=document.getElementById('originLatLng').value;
            var destinationLatLng=document.getElementById('destinationLatLng').value;
            var extra_destinationLatLng=document.getElementById('extra_destinationLatLng').value;
            var origin_lat =  originLatLng.substring(7,16);
            var origin_lng =  originLatLng.substring(17,27);
            var destination_lat = destinationLatLng.substring(7,16);
            var destination_lng = destinationLatLng.substring(17,27);


            $.ajax({
                url: '{{  route("panel.taxi.calculatePrice") }}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    origin_lat:origin_lat ,
                    origin_lng:origin_lng,
                    destination_lat:destination_lat,
                    destination_lng:destination_lng,
                    //extra_destination:$extra_destinationLatLng,
                    round_trip:false,
                    waiting:-1
                },
                dataType: 'json',
                success: function(data ) {

                    document.getElementById('price').value=data;
                }
            });
        }
        function showDiv(divId)
        {

            if( document.getElementById(divId).style.display=='block') {
                document.getElementById(divId).style.display = 'none';

            }
            else {
                document.getElementById(divId).style.display = 'block';
            }
        }
        function requestService(){
            document.getElementById('collapseTwo2').style.display='block';
            document.getElementById('collapseOne1').style.display='none';
            $.ajax({
                url: '{{route("panel.taxi.requestService")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN
                },
                dataType: 'html',
                success: function(data ) {

                    document.getElementById('result_ride_id').value = data;
                    /* if (data) {
                     driverStatus = 1
                     }*/


                    myVar = setInterval(refresh( document.getElementById('result_ride_id').value), 30);

                }
            });
        }
        function refresh(ride_id) {
            //if (driverStatus == 1) {
            driverStatus=1;
            $.ajax({

                url: '{{route("panel.taxi.requestRefresh")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    ride_id: ride_id
                },
                dataType: 'html',
                success: function (data) {
                    if(!data) {
                        refresh( document.getElementById('result_ride_id').value);
                    }
                    ('#collapseTwo2').class="collapse show";
                    document.getElementById('driverInfo').innerHTML=data;
                }
            });
            //}
        }
        function chooseOrigin(){
            originStatus=true;
            destinationStatus=false;
            delOriginStatus=false;
        }
    </script>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function chooseDestination(){
            originStatus=false;
            destinationStatus=true;
            delDestinationStatus=false;
        }
        function deleteDestination(){
            delDestinationStatus=true;
            document.getElementById('destination').value = "";
            document.getElementById('destinationLatLng').value = "";
        }

        function deleteOrigin(){
            delOriginStatus=true;
            document.getElementById('origin').value = "";
            document.getElementById('originLatLng').value = "";
        }
    </script>
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
    <script>
        var map = L.map('map').setView([36.289632, 59.616130], 35);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        var popup = L.popup();
        function onMapClick(e) {

            popup
                .setLatLng(e.latlng)
                .setContent("مکان انتخابی:" + e.latlng.toString())
                .openOn(map);

            if (originStatus && !delOriginStatus ) {

                document.getElementById('origin').value = 'مبدا انتخاب شد';
                document.getElementById('originLatLng').value = e.latlng;
                var originLatLng=document.getElementById('originLatLng').value;
                var origin_lat =e.latlng.lat;  //originLatLng.substring(7,16);
                var origin_lng = e.latlng.lng; //originLatLng.substring(17,27);

                if (markerOrigin) { // check
                    map.removeLayer(markerOrigin); // remove
                }
                const issIcon=L.icon({
                    iconSize:[90,62]
                });
                  markerOrigin = new L.marker([origin_lat,origin_lng],  {draggable:true},{icon:issIcon}).addTo(map)
                    .bindPopup('مبدا')
                    .openPopup();

            }
            else
            {
            if(!delDestinationStatus) {

                document.getElementById('destination').value = 'مقصد انتخاب شد';
                document.getElementById('destinationLatLng').value = e.latlng;
                 var destinationLatLng=document.getElementById('destinationLatLng').value;
                 var destination_lat = e.latlng.lat//destinationLatLng.substring(7,16);
                 var destination_lng = e.latlng.lng//destinationLatLng.substring(17,27);
                if (markerDestination) { // check
                    map.removeLayer(markerDestination); // remove
                }
                markerDestination=L.marker([destination_lat, destination_lng], {draggable:true}).addTo(map)
                    .bindPopup('مقصد')
                    .openPopup();

                }
            }
            if(document.getElementById('destinationLatLng').value && document.getElementById('originLatLng').value) {
                calculatePrice();
            }
        }
        map.on('click', onMapClick);
    </script>
@endsection
