@extends('layouts.app')
@section('content')
    <hr>
    <div id="content">
        <div class="mapSnapp">

            <div id="map">

            </div>

            <div class="col-lg-6" style="z-index: 10000;">
                <div  class="card">
                    <div class="card-body">
                        <div id="accordion-two" class="accordion">
                            <div class="card">
                                <div class="card-header" onclick="showDiv('collapseTwo4')">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo4" aria-expanded="false"
                                        aria-controls="collapseTwo4"><i class="fa" aria-hidden="true"></i>
                                        <div id="countRequest">  درخواست های جاری</div></h5>
                                </div>

                                <div id="collapseTwo4" class="collapse" data-parent="#accordion-two4">
                                    <div class="card-body pt-0">
                                        <div  id="currentRequest">
                                           {{-- <div class="animated bounceInDown">
                                                <div class="dropdown-content-body">
                                                    <ul>
                                                        <li class="notification-unread">
                                                            <a href="javascript:void()">
                                                                --}}{{-- <img class="pull-left mr-3 avatar-img"
                                                                      src="../../assets/images/avatar/1.jpg" alt="">--}}{{--
                                                                <div onclick="setToMap();"  class="notification-content">
                                                                    <div class="notification-heading">رضا افشار</div>
                                                                    <div class="notification-text">
                                                                        مبدا : هتل رضا
                                                                        <br>
                                                                        مقصد: حرم امام رضا
                                                                    </div><small class="notification-timestamp">08 دقیقه
                                                                        پیش</small>
                                                                </div>
                                                            </a><span class="notify-close"><i class="ti-close"></i></span>
                                                        </li>
                                                        <li class="notification-unread">
                                                            <a href="javascript:void()">

                                                                <div class="notification-content">
                                                                    <div class="notification-heading">امیر صبوری</div>
                                                                    <div class="notification-text">این یک پیام شگفت انگیز است، لورم
                                                                        ایپسوم</div><small class="notification-timestamp">08 ساعت
                                                                        پیش</small>
                                                                </div>
                                                            </a><span class="notify-close"><i class="ti-close"></i></span>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" onclick="showDiv('collapseOne1')">
                                    <h5 class="mb-0" data-toggle="collapse" data-target="#collapseOne1"
                                        aria-expanded="true" aria-controls="collapseOne1"><i class="fa"
                                                                                             aria-hidden="true"></i>درخواست تاکسی</h5>
                                </div>
                                <div id="collapseOne1" class="collapse show" data-parent="#accordion-two">
                                    <div class="card-body pt-0">
                                        <div id="v-pills-home2" class="tab-pane fade active show">
                                           {{-- <h3> </h3>
                                           --}}{{-- <div class="autocomplete" style="width:300px;">
                                                <input id="myInput"  type="text" name="myCountry" placeholder="Country">
                                            </div>--}}{{--
                                            <input type="button" onclick="searchnew()">--}}

                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label"  >مبدا:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text"  onchange="requestService()"  class="form-control"  placeholder="انتخاب مبدا"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="origin"
                                                               aria-describedby="origin">
                                                        <input id="originLatLng"  type="hidden">
                                                        <button class="btn" onclick="deleteOrigin()"><i class="fa fa-trash"></i> </button>
                                                        <button type="button" onclick="chooseOrigin()" class="btn btn-outline-primary btn-sm">انتخاب مبدا از نقشه</button>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label"> </label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <div class="dropdown-content-body">
                                                            <div class="header-search" id="header-search">
                                                                <form action="#">
                                                                    <div class="input-group">
                                                                        <div class="autocomplete" style="width:300px;">
                                                                            <input id="myInputOrigin" onclick="search('myInputOrigin')" type="text" name="myCountry" placeholder="جستجوی مبدا در نقشه...">                                                                        </div>

                                                                        <div class="input-group-append"><span onclick="search(myInputOrigin)" class="input-group-text"><i class="icon-magnifier"></i> </span>
                                                                        </div>


                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label"  >مقصد:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text"     style="font-family: Iransans_Web; font-size: 14px"class="form-control"
                                                               id="destination"
                                                               placeholder="انتخاب مقصد"
                                                               aria-describedby="destination">
                                                        <input id="destinationLatLng"  type="hidden">

                                                        <button class="btn" onclick="deleteDestination();"><i class="fa fa-trash"></i> </button>

                                                        <button type="button"  onclick="chooseDestination()" class="btn btn-outline-primary btn-sm">انتخاب مقصدازنقشه</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label"> </label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                <div class="dropdown-content-body">
                                                    <div class="header-search" id="header-search">
                                                        <form action="#">
                                                            <div class="input-group">
                                                                <div class="autocomplete" style="width:300px;">
                                                                    <input id="myInput" onchange="search('myInput')" type="text" name="myCountry" placeholder="جستجوی مقصد در نقشه ...">
                                                                </div>

                                                                <div class="input-group-append"><span onclick="search('myinput')" class="input-group-text"><i class="icon-magnifier"></i></span>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">سرویس: </label>
                                                <div class="col-lg-9">
                                                    <select  id="service_type"  class="form-control" style="font-family: Iransans_Web; font-size: 10px">
                                                        <option id="service_type_name" class="text-muted" disabled selected   style="display: none">
                                                            انتخاب یک مورد</option>
                                                        <option  id="1" class="obtion_DDL">اسنپ اکو</option>
                                                        <option  id="2" class="obtion_DDL">اسنپ پلاس</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">هزینه:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="price" placeholder="لطفا مبدا و مقصد خود را انتخاب کنید"
                                                               disabled readonly aria-describedby="price">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">تلفن همراه مسافر:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="contact_mobile"
                                                               placeholder="" maxlength="11"
                                                               aria-describedby="inputGroupPrepend2">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">نام مسافر:</label>
                                                <div class="col-sm-9">

                                                    <input type="text" class="form-control"
                                                           style="font-family: Iransans_Web; font-size: 14px"
                                                           id="contact_name"
                                                           placeholder=""
                                                          >
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">زمان انتظار: </label>
                                                <div class="col-lg-9">
                                                    <select class="form-control" id="waiting" style="font-family: Iransans_Web; font-size: 10px">
                                                        <option class="text-muted" disabled selected style="display: none">
                                                            بدون زمان انتظار</option>
                                                        <option value="0" class="obtion_DDL">0 تا 5 دقیقه</option>
                                                        <option  value="5" class="obtion_DDL">5 تا 10 دقیقه</option>
                                                        <option  value="1" class="obtion_DDL">10 تا 15 دقیقه</option>
                                                        <option  value="15" class="obtion_DDL">15 تا 20 دقیقه</option>
                                                        <option   value="25" class="obtion_DDL">25 تا 30 دقیقه</option>
                                                        <option   value="30" class="obtion_DDL">30 تا 45 دقیقه</option>
                                                        <obtion  value="45" class="obtion_DDL">45 دقیقه تا 1 ساعت</option>
                                                            <option  value="1"class="obtion_DDL">1 تا 1.5 ساعت </option>
                                                            <option  class="obtion_DDL" value="1.5">1.5 تا 2 ساعت</option>
                                                            <option  value="2" class="obtion_DDL">2 تا 2.5 ساعت</option>
                                                            <option  value="2.5" class="obtion_DDL">2.5 تا 3 ساعت</option>
                                                            <option  value="3" class="obtion_DDL">3 تا 3.5 ساعت</option>
                                                            <option  value="3.5" class="obtion_DDL">3.5 تا 4 ساعت</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">رفت و برگشت </label>
                                                <div class="col-lg-9">
                                                    <div class="form-check p-l-0">
                                                        <input type="checkbox"
                                                               id="round_trip">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label">  مقصد دوم:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="extra_destination"
                                                               placeholder="مقصد دوم"
                                                               aria-describedby="extra_destination">
                                                        <input id="extra_destinationLatLng"  type="hidden">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="hidden" class="form-control"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="result_ride_id" placeholder="..."
                                                               disabled readonly aria-describedby="result_ride_id">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-label"></label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <button   onclick="requestService()" class="btn-sl-lg">درخواست</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" onclick="showDiv('collapseTwo2')">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo2" aria-expanded="false"
                                        aria-controls="collapseTwo2"><i class="fa" aria-hidden="true"></i>
                                        مشخصات راننده</h5>
                                </div>
                                <div id="collapseTwo2" class="collapse" data-parent="#accordion-two">
                                    <div class="card-body pt-0">
                                        <div  id="driverInfo" >

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" onclick="showDiv('collapseTwo3')">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo3" aria-expanded="false"
                                        aria-controls="collapseTwo3"><i class="fa" aria-hidden="true"></i>
                                        مکان های منتخب</h5>
                                </div>
                             <div id="collapseTwo3" class="collapse" data-parent="#accordion-two3">
                                    <div class="card-body pt-0">
                                        <div  id="placeInfo" >
                                            <div class="form-group row align-items-center">
                                                <label class="col-sm-3 col-form-label text-label"  >نام مکان:</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text"   class="form-control"  placeholder="انتخاب مکان"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="selectedPlace"
                                                              >
                                                        <input type="text"   class="form-control"  placeholder="نام مکان"
                                                               style="font-family: Iransans_Web; font-size: 14px"
                                                               id="namePlace"
                                                                >
                                                        <input id="placeLatLng"  type="hidden">
                                                        <button class="btn" onclick="deleteSelectedPlace()"><i class="fa fa-trash"></i> </button>
                                                        <button type="button" onclick="chooseSelectedPlace()" class="btn btn-outline-primary btn-sm">انتخاب مکان از نقشه</button>
                                                        <button class="button" onclick="saveSelectedPlace()">  ثبت</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda"];
        var driverStatus=0;
        var originStatus=0;
        var destinationStatus=0;
        var markerDestination;
        var markerOrigin;
        var markerPlace;
        var markerPlaceOrigin;
        var delOriginStatus=0;
        var delDestinationStatus=0;
        var selectedPlaceStatus=0;
        const selectedLat=0;
        const selectedLng=0;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
           f();

        });

        function autocomplete(inp, arr1) {
           var res = arr1.split(",");
           arr=res;
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value;
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                if (!val) { return false;}
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(arr[i].indexOf(val), val.length).toUpperCase() == val.toUpperCase()) {

                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");

                        /*make the matching letters bold:*/
                        var v2=arr[i].substr(arr[i].indexOf(val), val.length);
                        var v1=arr[i].substr(0,arr[i].indexOf(val));
                        var v3=arr[i].substr(arr[i].indexOf(v2)+val.length);
                        //alert(v1+"<strong>" + v2+"</strong>" +v3 );
                        b.innerHTML = v1+"<strong>" + v2+"</strong>" +v3; //v1+"<strong>" + v2  + "</strong>"+v3 ;
                      //  b.innerHTML = "<strong>" + arr[i]  + "</strong>";
                        //b.innerHTML = "<strong>" + arr[i].includes(val) + "</strong>";
                       // b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function(e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x){
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }
        function autocomplete1(inp, arr1) {
            var res = arr1.split("#");
            arr=res;
            var currentFocus;
            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value;
                closeAllLists();
                if (!val) { return false;}
                currentFocus = -1;
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                this.parentNode.appendChild(a);
                /*for each item in the array...*/

                for (i = 0; i < arr.length; i++) {

                    var place = arr[i].split(",");
            if (place[0].substr(place[0].indexOf(val), val.length).toUpperCase() == val.toUpperCase()) {

                        b = document.createElement("DIV");
                        var v2=place[0].substr(place[0].indexOf(val), val.length);
                        var v1=place[0].substr(0,place[0].indexOf(val));
                        var v3=place[0].substr(place[0].indexOf(v2)+val.length);
                        //alert(v1+"<strong>" + v2+"</strong>" +v3 );
                        b.innerHTML = v1+"<strong>" + v2+"</strong>" +v3; //v1+"<strong>" + v2  + "</strong>"+v3 ;
                        b.innerHTML += "<input type='hidden' value='" + place[0] + "'>";
                        b.innerHTML += "<input type='hidden'  value='" + place[1] + "'>";
                        b.innerHTML += "<input type='hidden' value='" + place[2] + "'>";
                        b.addEventListener("click", function(e) {
                            inp.value = this.getElementsByTagName("input")[0].value;
                            onMapClick1(inp,this.getElementsByTagName("input")[1].value,this.getElementsByTagName("input")[2].value);
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });

            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    currentFocus++;
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x){
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }
        function searchnew() {
            autocomplete(document.getElementById("myInput"), countries);
        }

        function getCurrentRequest()
        {

            $.ajax({
                url: '{{ route("panel.taxi.currentRequest") }}',
                type: 'GET',
                dataType: 'html',
                success: function(data ) {


                    document.getElementById('currentRequest').innerHTML=data;

                }
            });
        }
        function getCountRequest()
        {

            $.ajax({
                url: '{{ route("panel.taxi.countRequest") }}',
                type: 'GET',
                dataType: 'html',
                success: function(data ) {

                    document.getElementById('countRequest').innerHTML=data;
                }
            });
        }
        $(function () {

            var table = $('.table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('panel.user.list')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'username', name: 'username'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'link_city', name: 'link_city'},
                    // {data: 'number_user', name: 'number_user'},
                    /* {data: 'mobile', name: 'mobile'},
                     {data: 'created_at', name: 'username'},
                     {data: 'action', name: 'action'},*/
                ]
            });
            console.log(table);
        })
        function f(){
            this.updateNowInterval = setInterval(getCurrentRequest, 20e3);
            this.updateNowInterval = setInterval(getCountRequest, 19e3)

        }
        function calculatePrice()
        {

            var originLatLng=document.getElementById('originLatLng').value;
            var destinationLatLng=document.getElementById('destinationLatLng').value;
            var extra_destinationLatLng=document.getElementById('extra_destinationLatLng').value;
            var origin_lat =  originLatLng.substring(7,16);
            var origin_lng =  originLatLng.substring(17,27);
            var destination_lat = destinationLatLng.substring(7,16);
            var destination_lng = destinationLatLng.substring(17,27);
            $.ajax({
                url: '{{  route("panel.taxi.calculatePrice") }}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    origin_lat:origin_lat ,
                    origin_lng:origin_lng,
                    destination_lat:destination_lat,
                    destination_lng:destination_lng,
                    //extra_destination:$extra_destinationLatLng,
                    round_trip:false,
                    waiting:-1
                },
                dataType: 'json',
                success: function(data ) {


                    document.getElementById('price').value=data;
                //}*/
               // success: function () {
                   // var t = this;
                   // this.updateNowInterval = setInterval(getCurrentRequest, 20e3)
                }
            });
        }
        function showDiv(divId)
        {
            if( document.getElementById(divId).style.display=='block') {
                document.getElementById(divId).style.display = 'none';
            }
            else {
                document.getElementById(divId).style.display = 'block';
            }
        }
        function saveSelectedPlace(){

            var place_lat = document.getElementById('placeLatLng').value.substring(7,16);
            var place_lng = document.getElementById('placeLatLng').value.substring(17,27);
            $.ajax({
                url: '{{route("panel.taxi.storeSelectedPlace")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    place_lat:place_lat ,
                    place_lng:place_lng,
                    name:document.getElementById('namePlace').value,
                },
                dataType: 'html',
                success: function(data ) {
               alert('مکان ثبت شد');
                }
            });
        }
        function requestService(){
            document.getElementById('collapseTwo2').style.display='block';
            document.getElementById('collapseOne1').style.display='none';
            var service_type=document.getElementById('service_type').selectedIndex ;
            var waiting=document.getElementById('waiting').selectedIndex ;
            var originLatLng=document.getElementById('originLatLng').value;
            var destinationLatLng=document.getElementById('destinationLatLng').value;
            var extra_destinationLatLng=document.getElementById('extra_destinationLatLng').value;
            var origin_lat =  originLatLng.substring(7,16);
            var origin_lng =  originLatLng.substring(17,27);
            var destination_lat = destinationLatLng.substring(7,16);
            var destination_lng = destinationLatLng.substring(17,27);
            var extra_destination_lat = extra_destinationLatLng.substring(7,16);
            var extra_destination_lng = extra_destinationLatLng.substring(17,27);
            var contact_name=document.getElementById('contact_name').value;
            var contact_mobile=document.getElementById('contact_mobile').value;
            var round_trip= document.getElementById('round_trip').checked;
            //contact_name": "کاملی",\r\n    "contact_mobile": "09154146825",\r\n    "contact_id": "",\r\n    "by_credit": false,\r\n    "service_type
            $.ajax({
                url: '{{route("panel.taxi.requestService")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    origin_lat:origin_lat ,
                    origin_lng:origin_lng,
                    destination_lat:destination_lat,
                    destination_lng:destination_lng,
                    extra_destination_lat:extra_destination_lat,
                    extra_destination_lng:extra_destination_lng,
                    contact_name:contact_name,
                    contact_mobile:contact_mobile,
                    round_trip:round_trip,
                    service_type:service_type,
                    waiting:waiting
                },
                dataType: 'html',
                success: function(data ) {
                    document.getElementById('result_ride_id').value = data;
                    myVar = setInterval(refresh( document.getElementById('result_ride_id').value), 30);
                }
            });
        }
        function refresh(ride_id) {
            //if (driverStatus == 1) {
            driverStatus=1;
            $.ajax({
                url: '{{route("panel.taxi.requestRefresh")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    ride_id: ride_id
                },
                dataType: 'html',
                success: function (data) {
                    if(!data) {
                        refresh( document.getElementById('result_ride_id').value);
                    }
                    ('#collapseTwo2').class="collapse show";
                    document.getElementById('driverInfo').innerHTML=data;
                }
            });
            $.ajax({
                url: '{{route("panel.taxi.storeTrip")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    ride_id: ride_id
                },
                dataType: 'html',
                success: function (data) {
                    alert(data);
                }
            });
            //}
        }
        function search(inputPlace) {
            if (document.getElementById(inputPlace)) {
                $.ajax({
                    url: '{{route("panel.taxi.search")}}',
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        _token: CSRF_TOKEN,
                        myInput: document.getElementById(inputPlace).value,
                    },
                    success: function (data) {
                        autocomplete1(document.getElementById(inputPlace), data);
                    }
                });
                //}
            }
        }
        function chooseOrigin()
        {
            originStatus = true;
            destinationStatus = false;
            delOriginStatus = false;
            selectedPlaceStatus=false;
        }
        function chooseSelectedPlace()
         {
               originStatus = false;
               destinationStatus = false;
               selectedPlaceStatus=true;
         }
    </script>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function chooseDestination(){
            originStatus=false;
            destinationStatus=true;
            delDestinationStatus=false;
            selectedPlaceStatus=false;
        }
        function deleteDestination(){
            document.getElementById('price').value="";
            if (markerDestination) { // check
                map.removeLayer(markerDestination); // remove
            }
            delDestinationStatus=true;
            document.getElementById('destination').value = "";
            document.getElementById('destinationLatLng').value = "";
        }
        function deleteOrigin(){
            if (markerOrigin) { // check
                map.removeLayer(markerOrigin); // remove
            }
            delOriginStatus=true;
            document.getElementById('price').value="";
            document.getElementById('origin').value = "";
            document.getElementById('originLatLng').value = "";
        }
        function setToMap(origin_lat,origin_lng,request){
            chooseOrigin();
            if (markerPlaceOrigin) { // check
                map.removeLayer(markerPlaceOrigin); // remove
            }
            alert(JSON.parse(request));
            document.getElementById('contact_mobile').value=JSON.parse(request).mobile;
            document.getElementById('contact_name').value=JSON.parse(request).full_name;
            onMapClick1('requestOrigin',origin_lat,origin_lng)
        }
    </script>
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
    <script src="../js/styleSwitcher.js"></script>
    <script>
        var map = L.map('map').setView([36.289632, 59.616130], 35);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        var popup = L.popup();
       // var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();
       /* L.esri.Geocoding.geosearch({
            providers: [
                arcgisOnline,
                L.esri.Geocoding.arcgisOnlineProvider({
                  countries: ['USA', 'GUM', 'VIR', 'PRI'],
                  categories: ['Address', 'Postal', 'Populated Place', ],

                }),
                L.esri.Geocoding.mapServiceProvider({
                    countries: [ 'GUM' ],
                    label: 'جستجوی شهر و کشور',
                    url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer',
                    layers: [2, 3],
                    searchFields: ['NAME', 'STATE_NAME']
                })
            ]
        }).addTo(map);*/
        //----------------------------------
        function onMapClick(e) {
            popup
                .setLatLng(e.latlng)
                .setContent("مکان انتخابی:" + e.latlng.toString())
                .openOn(map);
            if (originStatus && !delOriginStatus ) {
                document.getElementById('origin').value = 'مبدا انتخاب شد';
                document.getElementById('originLatLng').value = e.latlng;
                var originLatLng=document.getElementById('originLatLng').value;
                var origin_lat =e.latlng.lat;  //originLatLng.substring(7,16);
                var origin_lng = e.latlng.lng; //originLatLng.substring(17,27);
                if (markerOrigin) { // check
                    map.removeLayer(markerOrigin); // remove
                }
                if (markerPlaceOrigin) { // check
                    map.removeLayer(markerPlaceOrigin); // remove
                }
                const issIcon=L.icon({
                    iconSize:[90,62]
                });
                  markerOrigin = new L.marker([origin_lat,origin_lng], {draggable:true}).addTo(map)
                    .bindPopup('مبدا')
                    .openPopup();
            }
            else if(destinationStatus)
            {
            if(!delDestinationStatus) {
                document.getElementById('destination').value = 'مقصد انتخاب شد';
                document.getElementById('destinationLatLng').value = e.latlng;
                 var destinationLatLng=document.getElementById('destinationLatLng').value;
                 var destination_lat = e.latlng.lat//destinationLatLng.substring(7,16);
                 var destination_lng = e.latlng.lng//destinationLatLng.substring(17,27);
                if (markerDestination) { // check
                    map.removeLayer(markerDestination); // remove
                }
                if (markerPlace) { // check
                    map.removeLayer(markerPlace); // remove
                }
                markerDestination=L.marker([destination_lat, destination_lng], {draggable:true}).addTo(map)
                    .bindPopup('مقصد')
                    .openPopup();
                }
            }
            else if (selectedPlaceStatus)
            {
                document.getElementById('selectedPlace').value = 'مکان انتخاب شد';
                document.getElementById('placeLatLng').value = e.latlng;
            }
            if(document.getElementById('destinationLatLng').value && document.getElementById('originLatLng').value)
            {
                calculatePrice();
            }
        }
        map.on('click', onMapClick);
        function onMapClick1(inp,lat,lng) {
                popup
                var origin_lat =lat;  //originLatLng.substring(7,16);
                var origin_lng =lng; //originLatLng.substring(17,27);
                const issIcon=L.icon({
                    iconSize:[90,62]
                });

            if(inp.id=='myInputOrigin' || inp=='requestOrigin'){

                chooseOrigin();
                markerPlaceOrigin = new L.marker([origin_lat,origin_lng], {draggable:true},{icon:issIcon}).addTo(map)
                    .bindPopup('مکان جستجوشده مبدا')
                    .openPopup();
            }
            else
            {
                chooseDestination();
                markerPlace = new L.marker([origin_lat,origin_lng],  {draggable:true},{icon:issIcon}).addTo(map)
                    .bindPopup('مکان جستجوشده مقصد')
                    .openPopup();
            }
        }

    </script>
@endsection
