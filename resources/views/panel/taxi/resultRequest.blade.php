
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-md-6 col-xl-9">
                        <div class="invoice-info-left">
                            <div class="distributors">
                                <div class="mb-4">
                                    <h4 class="text-primary mb-2">{{$driver_name}}</h4>

                                </div>

                               <div class="mb-4">
                                   <h5>مبدا : <span class="text-muted"  style="font-family: Iransans_Web; font-size: 10px">{{$origin}}</span>
                                   </h5>
                                   <h5>مقصد : <span class="text-muted"  style="font-family: Iransans_Web; font-size: 10px">{{$destination}}</span>
                                   </h5>
                                   <hr>
                               </div>

                                <div class="mb-4">
                                    <h5>شماره تماس : <span class="text-muted"  style="font-family: Iransans_Web; font-size: 14px">{{($cellphone)}}</span>
                                    </h5>
                                   <h5> مدل ماشین : <span class="text-muted"  style="font-family: Iransans_Web; font-size: 14px">{{$vehicle_model}}</span></h5>
                                    <h5> پلاک ماشین : <span class="text-muted"  style="font-family: Iransans_Web; font-size: 14px">{{$plate}}</span></h5>

                                </div>

                            </div>
                            <div class="invoice-to">
                              <img src="http://statics.snapp.ir/driver/Bo7LQMvKXGeV52nWjq9b-1.jpg">
                                {{--<img src="https://snapp.taxi/trip/share/GX92meBQv892pYE">--}}
                            </div>
                        </div>
                    </div>

                </div>

            </div>

