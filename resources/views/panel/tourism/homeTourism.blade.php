@extends('layouts.app')
@section('content')
    <hr>
    <div id="content">
        <div class="mapSnapp">
            <input type="hidden" id="cityId" value={{$cityId}}>
            <div id="map">
            </div>

          {{--  <div class="col-xl-6 col-xxl-12" style="z-index: 10000;" >
                <div >
                    <div class="card-body campaign-statistics p-0">
                        <h4 class="card-title">آمار کمپین</h4>
                        <p>آمار کمپین</p>
                        <div class="current-campaign-list">


                            <div class="card mb-3">
                                <div class="card-body d-flex">
                                    <div class="item campaign-status">
                                        <p class="text-success m-0">فعال</p>
                                    </div>
                                    <div class="item campaign-details">
                                        <p class="text-muted m-0">ارسال رایگان برای خرید بالای 25 هزار تومان</p>
                                    </div>
                                    <div class="item status-icon ml-auto">

                                                <span class="text-pale-sky c-pointer"><i class="fa fa-pause-circle-o"
                                                                                         aria-hidden="true"></i></span>
                                        <span class="pl-sm-5 pl-3"></span>
                                        <span class="text-pale-sky c-pointer"><i class="fa fa-stop-circle-o"
                                                                                 aria-hidden="true"></i></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <div class="col-md-4 col-sm-12" style="z-index: 10000;">

                <div  class="card">
                    <div class="card-body">
                        <div class="stat-widget-two">
                            <div class="media">
                                <a href="{{route('panel.taxi.request')}}">
                                    <img   src="../../../../assets/images/icons/taxi.jpg" alt="">
                                </a>
                            </div></div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        {{--<div id='typeCategory' class="form-group col-lg-4">
                            <select id="gInfo" class="form-control" onchange="selectTypeGroup()">
                                @foreach ($typeCategory as $type)
                                <option value="{{$type->id}}">{{$type->title}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" id="typeCategory">

                        </div>--}}
                    </div>
                    <div class="card-body">

                        <div id="accordion-two" class="accordion">
                             <div class="card">
                                 <div class="card-header" onclick="showDiv('collapseTwo3')">
                                     <h5 class="mb-0 collapsed" data-toggle="collapse"
                                         data-target="#collapseTwo3" aria-expanded="false"
                                         aria-controls="collapseTwo3"><i class="fa" aria-hidden="true"></i>
                                         {{$kind." ".$cityName}}</h5>
                                 </div>
                                <div id="collapseTwo3" class="collapse" data-parent="#accordion-two3">

                                    <div class="card-body pt-0" >

                                        <a class="fa fa-plus" onclick="" style="font-size: 20px;">   </a>
                                        <br><br>
                                        <div style="overflow: auto; height:400px;">
                                        <ul class="metismenu" id="menu">
                                             @foreach($categoryList as $catParent )
                                                <li  >
                                                    <a  href="javascript:void()" aria-expanded="false">
                                                        <p style="color: #0c0c0c"><i class="mdi mdi-view-dashboard"></i><span   style="font-family: Iransans_Web; font-size: 16px;  " >{{$catParent['name']}}</span><span
                                                            class="badge bg-dpink text-white nav-badge"></span>
                                                        </p>
                                                        <ul aria-expanded="false">
                                                            <li   class="mega-menu mega-menu-lg">
                                                                <i    style="font-size: 15px; background-color: #d22d72"   > <a onclick=" addCategory({{$catParent['id']}});" >افزودن دسته  </a></i>

                                                            </li>
                                                            <div id="cat{{$catParent['id']}}" style="display: none;">
                                                                <label >نام دسته:</label>

                                                                <input type="text" id="catTitle{{$catParent['id']}}" >

                                                                <button type="button"  onclick="storeCategoryPlace({{$catParent['id']}})" class="btn btn-outline-primary btn-sm"> ثبت</button>
                                                            </div>
                                                        </ul>

                                                        <br>
                                                    </a>
                                                        @foreach($childs as $child )
                                                            @if($child['parent_id']==$catParent['id'])
                                                            <ul aria-expanded="false">
                                                                <li  style="background-color: #eeeeee; " class="mega-menu mega-menu-lg">
                                                                    <a   href="javascript:void()" aria-expanded="false">
                                                                        <i  ></i><span class="nav-text"  style="  font-family: Iransans_Web; font-size: 13px"> {{$child['name']}} </span>
                                                                        <span
                                                                            class="badge bg-dpink text-white nav-badge"></span>
                                                                    </a>
                                                                      <i class="fa fa-edit"   style="font-size:14px"  aria-hidden="true"  onclick="editCategoryPlace('{{$child['name']}}','{{$child['id']}}','{{$catParent['id']}}')" >  ویرایش</i>
                                                                      <i class="fa fa-plus"   style="font-size:14px"  aria-hidden="true" onclick="addToCategory({{$child['id']}});">  افزودن</i>
                                                                    <hr>
                                                                    @foreach($places as $place )
                                                                        @if($place['category_id']==$child['id'])

                                                                    <ul aria-expanded="false">
                                                                        <div class="card mb-3">
                                                                            <div class="card-body d-flex">
                                                                                {{--<div class="item campaign-status">
                                                                                    <p class="text-success m-0">فعال</p>
                                                                                </div>--}}
                                                                                <div   aria-hidden="true" >
                                                                                    <a href="javascript:addToMap({{$place['place_lat']}}+','+{{$place['place_lng']}})" > <p style="font-size:12px;max-width: 200px; line-break: auto;" >{{$place['name']}}</p></a>
                                                                                </div>


                                                                            </div>
                                                                            <div >

                                                                               <span class="text-pale-sky c-pointer">
                                                                                    <img  src="../../../../assets/images/avatar/doctor-4.jpg" >
                                                                                <a  href="{{route('panel.tourism.placeInfo', ['id'=> $place['id'] ])}}">  <i class="fa fa-info-circle"    style="font-size:20px" aria-hidden="true"></i></a>
                                                                               </span>
                                                                               <span class="pl-sm-5 pl-3"></span>
                                                                               <span class="text-pale-sky c-pointer"><i class="fa fa-trash" style="font-size:20px"  onclick="deleteSelectedPlace()"></i></span>

                                                                            </div>
                                                                        </div>
                                                                        {{--<li ><a href="javascript:addToMap({{$place['place_lat']}}+','+{{$place['place_lng']}})" ><p style="font-family: Iransans_Web; font-size: 11px; ">{{$place['name']}}</p> </a><i class="fa fa-info-circle"  style="font-size:20px" aria-hidden="true"></i><i class="fa fa-trash" style="font-size:20px"  onclick="deleteSelectedPlace()"></i></li>--}}

                                                                    </ul>

                                                                        @endif
                                                                    @endforeach

                                                                    </li>


                                                            </ul>
                                                            @endif

                                                    @endforeach

                                                </li>

                                            @endforeach

                                        </ul>
                                    </div>
                                        <div  id="placeInfo" >
                                            <div class="input-group row mb-5">
                                            </div>
                                            <div class="form-group row align-items-center">
                                                <div  >
                                                    <div class="input-group">
                                                        <input type="hidden"
                                                               style="font-family: Iransans_Web; font-size: 14px; width: 50px;"
                                                               id="selectedPlace"      >
                                                        <input type="text"   placeholder="نام مکان"
                                                               style="font-family: Iransans_Web; font-size: 14px;width: 200px;"
                                                               id="namePlace"                >
                                                        <input type="text"   placeholder="آدرس"
                                                               style="font-family: Iransans_Web; font-size: 14px;width: 200px;"
                                                               id="address"                >
                                                        <input type="text"   placeholder="تلفن"
                                                               style="font-family: Iransans_Web; font-size: 14px;width: 200px;"
                                                               id="tel"                >
                                                        <div class="form-group">
                                                            <label for="published_at">انتخاب تصویر</label>
                                                            <input type="file" name="file" id="file">
                                                        </div>


                                                        <input id="placeLatLng"  type="input" readonly>

                                                        <button class="btn btn-success"  onclick="saveSelectedPlace()">  ثبت</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script type="text/javascript">
        var driverStatus=0;
        var originStatus=0;
        var destinationStatus=0;
        var markerDestination;
        var markerOrigin;
        var markerPlace;
        var markerPlaceOrigin;
        var delOriginStatus=0;
        var delDestinationStatus=0;
        var selectedPlaceStatus=0;
        const selectedLat=0;
        const selectedLng=0;
        var categoryId=0;
        var idEdit=0;
        var idEditParent=0
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function autocomplete(inp, arr1) {

           var res = arr1.split(",");
           arr=res;
            var currentFocus;
            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value;
                closeAllLists();
                if (!val) { return false;}
                currentFocus = -1;

                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/

                for (i = 0; i < arr.length; i++) {

                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(arr[i].indexOf(val), val.length).toUpperCase() == val.toUpperCase()) {

                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");

                        /*make the matching letters bold:*/
                        var v2=arr[i].substr(arr[i].indexOf(val), val.length);
                        var v1=arr[i].substr(0,arr[i].indexOf(val));
                        var v3=arr[i].substr(arr[i].indexOf(v2)+val.length);
                        //alert(v1+"<strong>" + v2+"</strong>" +v3 );
                        b.innerHTML = v1+"<strong>" + v2+"</strong>" +v3; //v1+"<strong>" + v2  + "</strong>"+v3 ;
                      //  b.innerHTML = "<strong>" + arr[i]  + "</strong>";
                        //b.innerHTML = "<strong>" + arr[i].includes(val) + "</strong>";
                       // b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/

                        b.addEventListener("click", function(e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;

                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }
        function autocomplete1(inp, arr1) {

            var res = arr1.split("#");
            arr=res;

            var currentFocus;

            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value;

                closeAllLists();

                if (!val) { return false;}
                currentFocus = -1;

                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");

                this.parentNode.appendChild(a);
                /*for each item in the array...*/

                for (i = 0; i < arr.length; i++) {

                    var place = arr[i].split(",");
            if (place[0].substr(place[0].indexOf(val), val.length).toUpperCase() == val.toUpperCase()) {


                        b = document.createElement("DIV");


                        var v2=place[0].substr(place[0].indexOf(val), val.length);
                        var v1=place[0].substr(0,place[0].indexOf(val));
                        var v3=place[0].substr(place[0].indexOf(v2)+val.length);
                        //alert(v1+"<strong>" + v2+"</strong>" +v3 );
                        b.innerHTML = v1+"<strong>" + v2+"</strong>" +v3; //v1+"<strong>" + v2  + "</strong>"+v3 ;

                        b.innerHTML += "<input type='hidden' value='" + place[0] + "'>";
                        b.innerHTML += "<input type='hidden'  value='" + place[1] + "'>";
                        b.innerHTML += "<input type='hidden' value='" + place[2] + "'>";

                        b.addEventListener("click", function(e) {

                            inp.value = this.getElementsByTagName("input")[0].value;

                            onMapClick1(inp,this.getElementsByTagName("input")[1].value,this.getElementsByTagName("input")[2].value);


                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });

            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {

                    currentFocus++;

                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }


        function searchnew() {
            autocomplete(document.getElementById("myInput"), countries);
        }

        function calculatePrice()
        {

            var originLatLng=document.getElementById('originLatLng').value;
            var destinationLatLng=document.getElementById('destinationLatLng').value;
            var extra_destinationLatLng=document.getElementById('extra_destinationLatLng').value;
            var origin_lat =  originLatLng.substring(7,16);
            var origin_lng =  originLatLng.substring(17,27);
            var destination_lat = destinationLatLng.substring(7,16);
            var destination_lng = destinationLatLng.substring(17,27);


            $.ajax({
                url: '{{  route("panel.taxi.calculatePrice") }}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    origin_lat:origin_lat ,
                    origin_lng:origin_lng,
                    destination_lat:destination_lat,
                    destination_lng:destination_lng,
                    //extra_destination:$extra_destinationLatLng,
                    round_trip:false,
                    waiting:-1
                },
                dataType: 'json',
                success: function(data ) {

                    document.getElementById('price').value=data;
                }
            });
        }
        function showDiv(divId)
        {
            if( document.getElementById(divId).style.display=='block') {
                document.getElementById(divId).style.display = 'none';
            }
            else {
                document.getElementById(divId).style.display = 'block';
            }

            if('{{$cityName}}'=='مشهد')

            {
                addToMap('35.697335,51.391208')
            }
             if('{{$cityName}}'=='تهران')
            {
                addToMap('35.697335,51.391208')
            }
            if('{{$cityName}}'=='شیراز')
            {
                addToMap('29.551478,52.708808')
            }

            if('{{$cityName}}'=='اصفهان')
            {
                addToMap('32.651998,51.663564')
            }

        }
        function addToCategory(category_id  )
        {

            categoryId=category_id;

        }
        function addCategory(parent_id  )
        {
            if( document.getElementById('cat'+parent_id).style.display=='block')
               document.getElementById('cat'+parent_id).style.display='none';
             else
               document.getElementById('cat'+parent_id).style.display='block';
         }
            function storeCategoryPlace(parent_id) {
                if (idEdit == 0) {
                    addCategoryPlace(parent_id);
                } else {

                    //alert(idEdit);
                    updateCategoryPlace();
                }
            }
            function addCategoryPlace(parent_id)
            {
            $.ajax({
                url:  '{{route("panel.tourism.storeCategoryPlace")}}' ,
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    parent_id: parent_id,
                    typecategory_id : 0,
                    icon:'noImg.jpg',
                    name: document.getElementById('catTitle'+parent_id).value,
                },
                dataType: 'html',
                success: function (data) {

                }
            });
        }
        function editCategoryPlace(name,id,parent_id)
        {
            document.getElementById('cat'+parent_id).style.display='block';
            document.getElementById('catTitle'+parent_id).value=name;
            idEditParent=parent_id;
            idEdit=id;


        }
        function updateCategoryPlace()
        {

            $.ajax({
                url: '{{route("panel.tourism.editCategoryPlace")}}'  ,
                type: 'POST',
                data: {
                    id:idEdit,
                    parent_id: idEditParent,
                    typecategory_id : '0',
                    icon:'noImg.jpg',
                    name: document.getElementById('catTitle'+idEditParent).value,
                },
                dataType: 'html',
                success: function (data) {

                    alert('دسته با موفقیت ویرایش شد');
                }
            });
        }

        function addToMap(lat,lng){

            onMapClick1('myPlace',lat,lng);
        }
        function placeOrigin(){
            var place_lat = document.getElementById('placeLatLng').value.substring(7, 16);
            var place_lng = document.getElementById('placeLatLng').value.substring(17, 27);
            $.ajax({
                url: '{{route("panel.taxi.request")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    place_lat: place_lat,
                    place_lng: place_lng,
                    typePlace: 'origin',
                },
                dataType: 'html',
                success: function (data) {

                }
            });
        }


        function saveSelectedPlace() {
            if (categoryId == 0) {
                alert('لطفا دسته  را انتخاب نمایید');
                return;
            } else {
                var place_lat = document.getElementById('placeLatLng').value.substring(7, 16);
                var place_lng = document.getElementById('placeLatLng').value.substring(17, 26);
                var cityId=document.getElementById('cityId').value;
               /* var formData = new FormData($('#addPost')[0]);
                formData.append('file', $('input[type=file]')[0].files[0]);
                formData.append('file', $('#file')[0].files[0]);*/



                // other inputs
               // formData.append("file", $('#file')[0].files[0]);

                //append some non-form data also
             //   formData.append('name',$('#file')[0].files[0]).val(),


                $.ajax({
                    url: '{{route("panel.taxi.storeSelectedPlace")}}',
                    type: 'POST',
                    data: {
                        _token: CSRF_TOKEN,
                        place_lat: place_lat,
                        place_lng: place_lng,
                        cityId :cityId ,
                        address : document.getElementById('address').value,
                        category_Id: categoryId,
                        name: document.getElementById('namePlace').value,
                        //file: formData,
                        tel: document.getElementById('tel').value,
                    },
                    dataType: 'html',
                    success: function (data) {
                        alert('مکان ثبت شد');
                    }
                });
            }
        }
       /* function selectTypeGroup(){
            $.ajax({

                url:  ,
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    id: document.getElementById('gInfo').selectedIndex
                }
            });
        }*/
        function requestService(){


            document.getElementById('collapseTwo2').style.display='block';
            document.getElementById('collapseOne1').style.display='none';
            var service_type=document.getElementById('service_type').selectedIndex ;
            var waiting=document.getElementById('waiting').selectedIndex ;
            var originLatLng=document.getElementById('originLatLng').value;
            var destinationLatLng=document.getElementById('destinationLatLng').value;
            var extra_destinationLatLng=document.getElementById('extra_destinationLatLng').value;


            var origin_lat =  originLatLng.substring(7,16);
            var origin_lng =  originLatLng.substring(17,27);
            var destination_lat = destinationLatLng.substring(7,16);
            var destination_lng = destinationLatLng.substring(17,27);
            var extra_destination_lat = extra_destinationLatLng.substring(7,16);
            var extra_destination_lng = extra_destinationLatLng.substring(17,27);
            var contact_name=document.getElementById('contact_name').value;
            var contact_mobile=document.getElementById('contact_mobile').value;
            var round_trip= document.getElementById('round_trip').checked;

            $.ajax({
                url: '{{route("panel.taxi.requestService")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    origin_lat:origin_lat ,
                    origin_lng:origin_lng,
                    destination_lat:destination_lat,
                    destination_lng:destination_lng,
                    extra_destination_lat:extra_destination_lat,
                    extra_destination_lng:extra_destination_lng,
                    contact_name:contact_name,
                    contact_mobile:contact_mobile,
                    round_trip:round_trip,
                    service_type:service_type,
                    waiting:waiting
                },
                dataType: 'html',
                success: function(data ) {

                    document.getElementById('result_ride_id').value = data;

                    myVar = setInterval(refresh( document.getElementById('result_ride_id').value), 30);

                }
            });
        }
        function refresh(ride_id) {
            //if (driverStatus == 1) {
            driverStatus=1;
            $.ajax({

                url: '{{route("panel.taxi.requestRefresh")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    ride_id: ride_id
                },
                dataType: 'html',
                success: function (data) {
                    if(!data) {
                        refresh( document.getElementById('result_ride_id').value);
                    }
                    ('#collapseTwo2').class="collapse show";
                    document.getElementById('driverInfo').innerHTML=data;
                }
            });
            $.ajax({

                url: '{{route("panel.taxi.storeTrip")}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    ride_id: ride_id
                },
                dataType: 'html',
                success: function (data) {
                    alert(data);
                }
            });

            //}
        }
        function search(inputPlace) {

            if (document.getElementById(inputPlace)) {

                $.ajax({
                    url: '{{route("panel.taxi.search")}}',
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        _token: CSRF_TOKEN,
                        myInput: document.getElementById(inputPlace).value,
                    },
                    success: function (data) {

                        autocomplete1(document.getElementById(inputPlace), data);
                    }
                });
                //}
            }
        }
        function chooseOrigin() {
            originStatus = true;
            destinationStatus = false;
            delOriginStatus = false;
            selectedPlaceStatus=false;
        }
           function chooseSelectedPlace()
           {
               originStatus = false;
               destinationStatus = false;
               selectedPlaceStatus=true;
           }

    </script>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function chooseDestination(){
            originStatus=false;
            destinationStatus=true;
            delDestinationStatus=false;
            selectedPlaceStatus=false;
        }
        function deleteDestination(){
            document.getElementById('price').value="";
            if (markerDestination) { // check
                map.removeLayer(markerDestination); // remove
            }
            delDestinationStatus=true;
            document.getElementById('destination').value = "";
            document.getElementById('destinationLatLng').value = "";
        }

        function deleteOrigin(){
            if (markerOrigin) { // check
                map.removeLayer(markerOrigin); // remove
            }
            delOriginStatus=true;
            document.getElementById('price').value="";
            document.getElementById('origin').value = "";
            document.getElementById('originLatLng').value = "";
        }
    </script>
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
    <script>
        var map = L.map('map').setView([36.289632, 59.616130], 35);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        var popup = L.popup();
        function onMapClick(e) {
           // map = L.map('map').setView([36.289632, 59.616130], 35);

            popup
                .setLatLng(e.latlng)
                .setContent("مکان انتخابی:" + e.latlng.toString())
                .openOn(map);

            document.getElementById('placeLatLng').value = e.latlng;
        }
        map.on('click', onMapClick);

        function onMapClick1(inp,lat,lng) {

                popup

                var origin_lat =lat;  //originLatLng.substring(7,16);
                var origin_lng =lng; //originLatLng.substring(17,27);

                const issIcon=L.icon({
                    iconSize:[90,62]
                });
            if(inp=='myPlace'){
                var  palce=lat.split(',');
                markerPlace = new L.marker([palce[0],palce[1]], {draggable:true},{icon:issIcon}).addTo(map)
                    .bindPopup('مکان جستجوشده مبدا')
                    .openPopup();

            }
            if(inp.id=='myInputOrigin'){

                chooseOrigin();
                markerPlaceOrigin = new L.marker([origin_lat,origin_lng], {draggable:true},{icon:issIcon}).addTo(map)
                    .bindPopup('مکان جستجوشده مبدا')
                    .openPopup();
            }
            else
            {
                chooseDestination();
                markerPlace = new L.marker([origin_lat,origin_lng],  {draggable:true},{icon:issIcon}).addTo(map)
                    .bindPopup('مکان جستجوشده مقصد')
                    .openPopup();
            }
        }
    </script>
@endsection
