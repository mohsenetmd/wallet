@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">

                    <div class="user-info-settings">
                        <h4 class="text-primary section-heading card-intro-title">ویرایش اطلاعات</h4>
                        <form method="post" action="{{route('panel.tourism.homeTourism',['id'=>$typeCat,'cityId'=>$place['city_id']])}} " class="user-info-setting-form">
                            {{--<form method="post" action="{{route('panel.tourism.storeDetail')}}" class="user-info-setting-form">--}}
                                @csrf
                                <input name="id" type="hidden" class="form-control" value="{{$place['id']}}">
                            <div class="form-group">
                                <label class="text-label">انتخاب شهر</label>

                                <select name="inputCity" class="form-control">
                                    @foreach ($city as $item)
                                    <option value="1" >{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="text-label">تخصص</label>
                                <select name="inputCat" class="form-control" value="{{$groupName}}">
                                    @foreach ($group as $child)
                                        <option value={{$child['id']}} >{{$child['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="text-label">نام کامل</label>
                                <input name="name" type="text" class="form-control" value="{{$place['name']}} ">
                            </div>
                            <div class="form-group">
                                <label class="text-label">شماره تلفن</label>
                                <input name="tel" type="tel" class="form-control" value="{{$place['tel']}}">
                            </div>
                            <div class="form-group">
                                <label class="text-label">آدرس</label>
                                <input type="texteara"  name="address" class="form-control"  value={{$place['address']}} cols="10" rows="5"></input>
                            </div>
                            <div class="form-group">
                                <label class="text-label">توضیحات</label>
                                <textareac name="description" class="form-control" cols="10" rows="5"></textareac>
                            </div>

                            <div class="submit-buttons mb-5">
                                <button type="submit" class="btn btn-card btn-success">به روز رسانی
                                    </button>
                                <button type="submit" class="btn btn-card btn-primary">انصراف</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 my-5 my-lg-0">
            <div class="card card-full-width rounded-0">
                <div class="card-body user-details-contact text-center">
                    <div class="user-details-image mb-3">
                       <img class="rounded-circle"   src="../../../../assets/images/avatar/5.jpg" alt="">
                    </div>
                    <div class="user-intro">
                        <h4 class="text-primary card-intro-title mb-0 text-center">{{$place['name']}}</h4>
                        <p><small>{{$groupName}}</small>
                        </p>
                        <p>افهوم</p>
                    </div>
                    <div class="contact-addresses">
                        <ul class="contact-address-list">
                            <li class="address">
                                <h5><i class="fa fa-map text-primary" aria-hidden="true"></i> آدرس</h5>
                                <p>{{$place['address']}}</p>
                            </li>
                            <li class="email">
                                <h5><i class="fa fa-envelope text-primary"></i> آدرس ایمیل</h5>
                                <p>RezaAfshar@gmail.com</p>
                            </li>
                            <li class="phone">
                                <h5><i class="fa fa-phone text-primary"></i> تلفن</h5>
                                <p>{{$place['tel']}}</p>
                            </li>
                            <li class="address">
                                <h5><i class="fa fa-map text-primary" aria-hidden="true"></i> توضیحات </h5>
                                <p>{{$place['description']}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')


@endsection
