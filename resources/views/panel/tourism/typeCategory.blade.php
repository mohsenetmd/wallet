@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-6">
            <div class="card forms-card">
                <div class="card-body">

                    <div class="basic-form">
                        <form enctype="multipart/form-data" method="post" action="{{route('panel.tourism.storeTypeCategory')}}">
                            @csrf
                            <div class="form-group">
                                <label class="text-label">عنوان دسته بندی</label>
                                <input type="text" name="title"  class="form-control"  required   >
                            </div>
                            <button type="submit" class="btn btn-primary btn-form mr-2">تایید</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
