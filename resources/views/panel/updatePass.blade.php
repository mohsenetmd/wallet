{{-- @extends('layouts.app')--}}
{{--@section('content')--}}
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.jpg')}}">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="NineKit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'sadra') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
    <div class="login-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content login-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="logo text-center">
                                    <a href="index.html">
                                        <img src="../../assets/images/f-logo.jpg" alt="" class="ml-4">
                                    </a>
                                </div>
                                <h4 class="text-center mt-4">بازیابی رمز عبور</h4>
                                <form enctype="multipart/form-data" method="post" action="{{route('panel.user.RestorePass')}}">
                                    @csrf
                                    <div class="form-group">
                                        <label>شماره همراه خود را وارد نمایید</label>
                                        <input type="text" class="form-control" name="mobile" required>
                                    </div>

                                    <div class="text-center mb-4 mt-4">
                                        <button class="btn btn-success" type="submit" style="height: 40px;">بازیابی رمز عبور</button>
                                    </div>
                                </form>
                                <div class="text-center">


                                    <p class="mt-5">حساب کاربری ندارید؟ <a href="{{route('panel.user.insert')}}">حالا ثبت نام
                                            کنید</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="container form-otah">
        <form enctype="multipart/form-data" method="post" action="{{route('panel.user.login')}}">
            @csrf
            <h1 class="titr">وارد کردن اطلاعات کاربر</h1>
            <hr>
            <div class="row">
                <div class="form-group col-lg-4">
                    <label>نام کاربری</label>
                    <input type="text" class="form-control" name="username" required>
                </div>
                <div class="form-group col-lg-4">
                    <label>پسورد</label>
                    <input type="password" class="form-control" name="password" required>
                </div>
                <button class="btn btn-success" type="submit" style="height: 40px;">ارسال</button>
            </div>
        </form>
    </div>--}}

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

{{--@endsection--}}
@section('script')
    <script src="../../assets/plugins/common/common.min.js"></script>
    <!-- Custom script -->
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
@endsection
