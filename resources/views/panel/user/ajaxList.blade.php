<div id=" {{ $pageTitle}}" class="pageTitle"></div>

    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست کاربران</h1>
    {{--<a class="btn btn-primary" href="{{route('user.insert')}}">افزودن</a>--}}
    <hr>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>نام کاربر</th>
            <th>تاریخ و زمان ثبت</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

 @section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                _token: CSRF_TOKEN,
                ajax: {
                    url: "{{route('panel.user.list')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'name', name: 'username'},
                    {data: 'jalali_create', name: 'created_at'},
                    {data: 'jalali_update', name: 'user_id'}
                ]
            });
            console.log(table);
        });
    </script>
@endsection
{{--//-------------------------------------------------------}}
 {{--<div class="table-responsive">
    <table class="table table-padded recent-order-list-table table-responsive-fix-big">
        <thead>
        <tr>
            <th>#کد</th>
            <th>نام کاربر</th>
            <th>تاریخ و زمان ثبت</th>

        </tr>
        </thead>
        <tbody>
        @foreach ($userList  as $d)
        <tr>
            <td class="text-pale-sky">
            {{  $d['user_id']}}
            </td><td class="text-pale-sky" >
            {{  $d['username']}}
            </td><td  class="text-muted">
            {{$d['created_at']}}
            </td>
        </tr>

        @endforeach
        </tbody>
    </table>
</div> --}}






