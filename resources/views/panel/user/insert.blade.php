@extends('layouts.app')
@section('content')
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <div class="login-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content login-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="logo text-center">
                                    <a href="index.html">
                                        <img src="../../assets/images/f-logo.jpg" alt="" class="ml-5">
                                    </a>
                                </div>
                                <h4 class="text-center mt-4">ثبت نام</h4>
                                    <form class="mt-5 mb-5" enctype="multipart/form-data" method="post" action="{{route('panel.user.store')}}">
                                        @csrf
                                    <div class="form-group">
                                        <label>نام کاربری</label>
                                        <input  type ="text"    name="username" class="form-control" placeholder="نام کاربری">
                                    </div>
                                    <div class="form-group">
                                        <label>موبایل</label>
                                        <input type ="text"      name="mobile" class="form-control" placeholder="موبایل">
                                    </div>
                                    <div class="form-group">
                                        <label>رمز عبور</label>
                                        <input  type ="password"  name="password" class="form-control" placeholder="رمز عبور">
                                    </div>
                                    <div class="form-group">
                                        <label>تکرار رمز عبور</label>
                                        <input type ="password"    name="confirm-password" class="form-control"
                                               placeholder="تکرار رمز عبور">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12" style="margin-right: 2rem;">
                                            <div class="form-check p-l-0">
                                                <input class="form-check-input float-left" type="checkbox"
                                                       id="basic_checkbox_1">
                                                <label class="form-check-label ml-2 float-left"
                                                       for="basic_checkbox_1">با قوانین و مقررات موافقم</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center mb-4 mt-4">
                                        <button type="submit" class="btn btn-primary">ثبت نام</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('errors')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
@endsection
@section('script')
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>


@endsection
