
<div class="login-bg h-100">
    <div class="container h-100">
        <div class="row justify-content-center h-100">
            <div class="col-xl-6">
                <div class="form-input-content login-form">
                    <div class="card">
                        <div class="card-body">
                            <div class="logo text-center">
                                <a href="index.html">
                                    <img src="../../assets/images/f-logo.jpg" alt="" class="ml-5">
                                </a>
                            </div>
                            <h4 class="text-center mt-4">افزودن کاربرجدید</h4>
                            <form class="mt-5 mb-5" enctype="multipart/form-data" method="post" action="">
                                @csrf
                                <div class="form-group">
                                    <label>نام کاربری</label>
                                    <input  type ="text"    id="username" class="form-control" placeholder="نام کاربری">
                                </div>
                                <div class="form-group">
                                    <label>موبایل</label>
                                    <input type ="text"      id="mobile" class="form-control" placeholder="موبایل">
                                </div>
                                <div class="form-group">
                                    <label>رمز عبور</label>
                                    <input  type ="password"  id="password" class="form-control" placeholder="رمز عبور">
                                </div>
                                <div class="form-group">
                                    <label>تکرار رمز عبور</label>
                                    <input type ="password"    id="confirm-password" class="form-control"
                                           placeholder="تکرار رمز عبور">
                                </div>

                                <div class="text-center mb-4 mt-4">
                                    <button type="submit" onclick="addUser();" class="btn btn-primary">ثبت نام</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group col-lg-4" id="success">

</div>

<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    function addUser() {

        var mobile = document.getElementById('mobile').value;
        var username = document.getElementById('username').value;
        var  password = document.getElementById('password').value;
        var confirm_password = document.getElementById('confirm-password').value;
             $.ajax({
            url: '{{route('panel.user.storeUser')}}',
            type: 'post',
            data: {
                _token: CSRF_TOKEN,
                password :password,
                confirm_password :confirm_password,
                username: username,
                mobile: mobile,

            },
            dataType: 'json',
            success: function(data ) {

                $('#success').html(data);
            }
        });
    }
</script>

