@extends('layouts.app')
@section('content')
    <hr>
    <a class="btn btn-primary" onclick="add()">افزودن</a>
    <hr>
    <div id="content">
    <table class="table">
        <thead class="thead-page">
        <tr>
            <th scope="col">نام کاربر</th>
            <th scope="col">موبایل</th>
            <th scope="col">تاریخ و زمان ثبت</th>
            <th scope="col">عملیات</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var route="{{route('panel.user.list')}}";
            var title='-> لیست کاربران'
            $('#pageTitle').append("<a  href='"+route+"' >"+title+"</a>");
            var table = $('.table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('panel.user.list')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'username', name: 'username'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'link_city', name: 'link_city'},
                   // {data: 'number_user', name: 'number_user'},
                   /* {data: 'mobile', name: 'mobile'},
                    {data: 'created_at', name: 'username'},
                    {data: 'action', name: 'action'},*/
                ]
            });
            console.log(table);
        });
    </script>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function add(){
            $.ajax({
                url: '{{route('panel.user.insertajax')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    group_id:  '1'
                },
                dataType: 'json',
                success: function(data ) {
                    $('#content').html(data);
                }
            });
        }
        function editrow(userid){
            $.ajax({
                url: '{{route('panel.user.updateAjax')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    user_id:userid
                },
                dataType: 'json',
                success: function(data ) {
                    $('#content').html(data);
                }
            });
        }
    </script>
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
@endsection
