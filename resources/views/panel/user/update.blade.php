

<div class="login-bg h-100">
    <div class="container h-100">
        <div class="row justify-content-center h-100">
            <div class="col-xl-6">
                <div class="form-input-content login-form">
                    <div class="card">
                        <div class="card-body">
                            <div class="logo text-center">
                                <a href="index.html">
                                    <img src="../../assets/images/f-logo.jpg" alt="" class="ml-5">
                                </a>
                            </div>
                            <h4 class="text-center mt-4">ویرایش اطلاعات کاربر</h4>
                            <form class="mt-5 mb-5" enctype="multipart/form-data" method="post" >
                                @csrf
                                <div class="form-group">
                                    <label>نام کاربری</label>
                                    <input  type ="text"   value="{{$username}}" id="username" class="form-control" placeholder="نام کاربری">
                                    <input  type ="hidden"   value="{{$user_id}}" id="user_id" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>موبایل</label>
                                    <input type ="text"    value="{{$mobile}}"  id="mobile" class="form-control" placeholder="موبایل">
                                </div>
                                <div class="form-group">
                                    <label>رمز عبور</label>
                                    <input  type ="text" value= "{{$password}}" id="password" class="form-control" placeholder="رمز عبور">
                                </div>
                                <div class="text-center mb-4 mt-4">
                                    <button type="submit" class="btn btn-primary" onclick="edit()">تایید</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-lg-4" id="success">
    </div>
</div>
<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    function edit() {
        var mobile = document.getElementById('mobile').value;
        var password = document.getElementById('password').value;
        var username = document.getElementById('username').value;
        var user_id = document.getElementById('user_id').value;

        $.ajax({
            url: '{{route('panel.user.update')}}',
            type: 'put',
            data: {
                _token: CSRF_TOKEN,
                user_id :user_id,
                username: username,
                mobile: mobile,
                password:password,

            },
            dataType: 'json',
            success: function(data ) {
                $('#success').html(data);
            }
        });
    }
</script>
