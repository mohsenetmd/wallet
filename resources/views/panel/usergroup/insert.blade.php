@extends('layouts.app')
@section('content')
    <div class="container form-otah">
        <form enctype="multipart/form-data" method="post" action="{{route('panel.usergroup.store')}}">
            @csrf
            <h1 class="titr">گروه کاربران</h1>
            <hr>
            <div class="row">
                <div class="form-group col-lg-4">
                    <select name="group_id" class="form-control">
                        @foreach ($groups as $group)
                        <option value="{{$group->group_id}}">{{$group->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4">
                        <select name="user_id" class="form-control">
                            @foreach ($users as $user)
                                <option value="{{$user->user_id}}">{{$user->username}}</option>
                            @endforeach
                        </select>
                </div>
                <button class="btn btn-success" type="submit" style="height: 40px;">ارسال</button>
            </div>
        </form>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
