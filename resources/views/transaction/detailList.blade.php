
                <div class="table-responsive">

                    <table id="example-plugin-1" class="table" style="min-width: 845px">
                       <thead class="thead-page-detail">

                        <tr>
                            <th>شماره تراکنش</th>
                            <th>افزایش اعتبار</th>
                            <th>کاهش اعتیار</th>
                            <th>پرداخت کننده</th>
                            <th>زمان تغییراعتبار</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($allTransaction  as $d)

                            <tr>
                                <td>{{$d->transaction_id}}</td>
                                <td>{{$d->added}}</td>
                                <td>{{$d->reduced}} </td>
                                <td>{{$d->payer}}</td>
                                <td>{{$d->updated_at}} </td>

                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot    class="thead-page">

                        </tfoot>
                    </table>
                </div>


{{--<div class="row">
    --}}{{-- <div class="col-lg-6">
         <div class="card">
             <div class="card-body">
                 <div class="card-title">
                     <h4 class="mb-4">جدول پایه</h4>
                 </div>
                 <div class="table-responsive">
                     <table class="table" style="min-width: 450px;">
                         <thead>
                         <tr>
                             <th>#</th>
                             <th>نام</th>
                             <th>وضعیت</th>
                             <th>تاریخ</th>
                             <th>قیمت</th>
                         </tr>
                         </thead>
                         <tbody>
                         <tr>
                             <th>1</th>
                             <td>تی شرت مردانه</td>
                             <td><span class="badge badge-primary">فروخته شده</span>
                             </td>
                             <td>22 خرداد</td>
                             <td class="color-primary">21,000 تومان</td>
                         </tr>
                         <tr>
                             <th>2</th>
                             <td>تی شرت زنانه</td>
                             <td><span class="badge badge-success">در حال ارسال</span>
                             </td>
                             <td>10 خرداد</td>
                             <td class="color-success">55,000 تومان</td>
                         </tr>
                         <tr>
                             <th>3</th>
                             <td>لباس آبی برای کودک</td>
                             <td><span class="badge badge-danger">مرجوع شده</span>
                             </td>
                             <td>25 اردیبهشت</td>
                             <td class="color-danger">14,000 تومان</td>
                         </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
     </div>--}}{{--
    <div  class="card story-widget">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">لیست تراکنش ها</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    --}}{{--<div id="demo"></div>--}}{{--

                    <table id="example-plugin-1" class="table" style="min-width: 350px">
                        <thead class="thead-page">

                        <tr>
                            <th>شماره تراکنش</th>
                            <th>افزایش اعتبار</th>
                            <th>کاهش اعتیار</th>
                            <th>پرداخت کننده</th>
                            <th>زمان تغییراعتبار</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($allTransaction  as $d)

                            <td>{{$d->transaction_id}}</td>
                            <td>{{$d->added}}</td>
                            <td>{{$d->reduced}} </td>
                            <td>{{$d->payer}}</td>
                            <td>{{$d->updated_at}} </td>
                            --}}{{--<tr><td colspan="4"></td><tr>--}}{{--

                        @endforeach

                        </tbody>
                        <tfoot    class="thead-page">


                        </tfoot>
                    </table>
                    --}}{{-- <table><tr style="border: 0px"> <td  ><div    id="{{$d->category_id}}" name="detail"></div></td> </tr></table>--}}{{--
                </div>
            </div>
        </div>
    </div>
    <div    id="category" name="detail"></div>

</div>--}}

{{--

<table id="example-plugin-2" class="table" style="min-width: 350px">
    <thead class="thead-page">

    <tr>
        <th>شماره تراکنش</th>
        <th>افزایش اعتبار</th>
        <th>کاهش اعتیار</th>
        <th>پرداخت کننده</th>
        <th>زمان تغییراعتبار</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($allTransaction  as $d)

        <tr>
            <td>{{$d->transaction_id}}</td>
            <td>{{$d->added}}</td>
            <td>{{$d->reduced}} </td>
            <td>{{$d->payer}}</td>
            <td>{{$d->updated_at}} </td>
        </tr>

    @endforeach

    </tbody>
    <tfoot    class="thead-page">

    <tr>
        <th>شماره تراکنش</th>
        <th>افزایش اعتبار</th>
        <th>کاهش اعتیار</th>
        <th>پرداخت کننده</th>
        <th>زمان تغییراعتبار</th>
    </tr>
    </tfoot>
</table>
--}}

