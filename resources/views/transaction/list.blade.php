<div class="row">
    <div class="col-12">
        <div class="card">

            <div class="card-body">
                <div class="table-responsive">
                    {{--<div id="demo"></div>--}}
                    <table id="example-plugin-1" class="table" style="min-width: 845px">
                        <thead class="thead-page">

                        <tr>
                            <th>نام دسته بندی</th>
                            <th>افزایش اعتبار</th>
                            <th>کاهش اعتیار</th>
                            <th>نمایش جزئیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($allTransaction  as $d)

                            <tr>
                                <td>{{$d->name}}</td>
                                <td>{{$d->added}}</td>
                                <td>{{$d->reduced}} </td>
                                <td><input type ="hidden" value="{{$d->category_id}}}}"><button class="btn btn-primary"    onclick="showDetail({{$d->category_id}})">جزئیات</button></td>

                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
            </div>

            <div    id="category"  ></div>
        </div>
    </div>
</div>
<script src="../../assets/plugins/common/common.min.js"></script>
<script src="../js/custom.min.js"></script>
<script src="../js/settings.js"></script>
<script src="../js/gleek.js"></script>
<script src="../js/styleSwitcher.js"></script>
<script src="../../assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="../js/plugins-init/datatables.init.js"></script>
<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(function () {
        var route="{{route('user.list')}}";
        var title='-> لیست تراکنش ها'
        $('#pageTitle').append("<a   href='"+route+"' >"+title+"</a>");
    });
    function showDetail( category_id,user_id) {
        $.ajax({
            url: '{{route('Transaction.detailList')}}',
            type: 'POST',
            data: {
                _token: CSRF_TOKEN,
                category_id:category_id,
                user_id: {{$user_id}}
            },
            dataType: 'json',
            success: function(data ) {
                $('#category').html(data);
            }
        });

    }
</script>


