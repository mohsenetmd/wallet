<div class="card-header">
    <h4 class="card-title">لیست تراکنش ها</h4>
</div>
<div class="card-body">
    <div class="table-responsive">
        {{--<div id="demo"></div>--}}

        <table id="example-plugin-3" class="table" style="min-width: 350px">
            <thead class="thead-page">

            <tr>
                <th>نام دسته بندی</th>
                <th>افزایش اعتبار</th>
                <th>کاهش اعتیار</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($allTransaction  as $d)

                <tr>
                    <td>{{$d->name}}</td>
                    <td>{{$d->added}}</td>
                    <td>{{$d->reduced}} </td>

                </tr>
                {{--<tr><td colspan="4"></td><tr>--}}

            @endforeach

            </tbody>
            <tfoot    class="thead-page">

            <tr>
                <th>نام دسته بندی</th>
                <th>افزایش اعتبار</th>
                <th>کاهش اعتیار</th>

            </tr>
            </tfoot>
        </table>
        {{-- <table><tr style="border: 0px"> <td  ><div    id="{{$d->category_id}}" name="detail"></div></td> </tr></table>--}}
    </div>
</div>
