
<div class="login-bg h-100">
    <div class="container h-100">
        <div class="row justify-content-center h-100">
            <div class="col-xl-6">
                <div class="form-input-content login-form">
                    <div class="card">
                        <div class="card-body">


                            <form class="mt-5 mb-5" enctype="multipart/form-data" method="post" action="">
                                @csrf
                                <div class="form-group">
                                    <label>موبایل</label>
                                    <input type ="text"      id="mobile" class="form-control" placeholder="موبایل">
                                </div>
                                <div class="form-group">
                                    <label>کدکشور</label>
                                    <input type ="text"      id="country_code" class="form-control" placeholder="کدکشور">
                                </div>
                                <div class="form-group">
                                    <label>رمز عبور</label>
                                    <input  type ="password"  id="password" class="form-control" placeholder="رمز عبور">
                                </div>
                                <div class="form-group">
                                    <label>تکرار رمز عبور</label>
                                    <input type ="password"    id="confirm-password" class="form-control"
                                           placeholder="تکرار رمز عبور">
                                </div>

                                <div class="text-center mb-4 mt-4">
                                    <a class="btn btn-primary" onclick="addUser()">تایید</a>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    function addUser() {

        var mobile = document.getElementById('mobile').value;
        var country_code = document.getElementById('country_code').value;
        var  password = document.getElementById('password').value;
        var confirm_password = document.getElementById('confirm-password').value;
        $.ajax({
            url: '{{route('user.createUser')}}',
            type: 'put',
            data: {
                _token: CSRF_TOKEN,
                password :password,
                confirm_password :confirm_password,
                country_code: country_code,
                mobile: mobile,

            },
            dataType: 'json',
            success: function(data ) {
                $('#success').html(data);
            }
        });
    }
</script>

