@extends('layouts.app')
@section('content')

   {{-- <h6 style="font-size: 14px;display: inline-block;margin-left: 20px;">   <a    href="{{route('user.list')}}  ">لیست مشتریان</a></h6>
    <hr>--}}

    <div id="content">
    <table class="table">
        <thead class="thead-page">
        <tr>

            <th scope="col"> کاربر</th>
            <th scope="col">موبایل</th>
            <th scope="col">کدکشور</th>
            <th scope="col">تاریخ و زمان ثبت</th>
            <th scope="col">عملیات</th>
        </tr>
        </thead>
        <tbody>


        </tbody>
    </table>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            var route="{{route('user.list')}}";
            var title='-> لیست مشتریان'
            $('#pageTitle').append("<a  href='"+route+"' >"+title+"</a>");
            var table = $('.table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('user.list')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'user_id', name: 'user_id'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'country_img', name: 'country_img'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'link_edit', name: 'link_edit'},
                ]
            });
            console.log(table);
        });
       /* function addc() {
            return 'country_code';

        }
*/
    </script>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function add() {

            $.ajax({
                url: '{{route('user.insertajax')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN
                },
                dataType: 'json',
                success: function(data ) {
                    $('#content').html(data);
                }
            });
        }
        function editrow( userid){

     $.ajax({
                url: '{{route('user.update')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                     user_id:userid
                },
                dataType: 'json',
         success: function(data ) {

                    $('#content').html(data);
                }
            });
        }
        function showTransaction( userid){

            $.ajax({
                url: '{{route('Transaction.list')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    user_id:userid
                },
                dataType: 'json',
                success: function(data ) {

                    $('#content').html(data);
                }
            });
        }
    </script>
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
@endsection
