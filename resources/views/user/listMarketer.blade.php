@extends('layouts.app')
@section('content')
    <div id="content">
        <table class="table">
            <thead class="thead-page">
            <tr>
                <th scope="col"> بازاریاب</th>
                <th scope="col">موبایل</th>
                <th scope="col">مجموع سود</th>
                <th scope="col">تاریخ و زمان ثبت</th>
                <th scope="col">عملیات</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            var route="{{route('user.listMarketer')}}";
            var title='-> لیست بازاریاب ها'
            $('#pageTitle').append("<a  href='"+route+"' >"+title+"</a>");
            var table = $('.table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('user.listMarketer')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'user_id', name: 'user_id'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'Profit', name: 'Profit'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action'},
                ]
            });
            console.log(table);
        });
        /* function addc() {
             return 'country_code';

         }
 */
    </script>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        function editrow( userid){

            $.ajax({
                url: '{{route('user.update')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    user_id:userid
                },
                dataType: 'json',
                success: function(data ) {

                    $('#content').html(data);
                }
            });
        }
        function showTransaction( userid){
            $.ajax({
                url: '{{route('Transaction.listForMarketer')}}',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    user_id:userid,
                },
                dataType: 'json',
                success: function(data ) {

                    $('#content').html(data);
                }
            });
        }
    </script>
    <script src="../../assets/plugins/common/common.min.js"></script>
    <script src="../js/custom.min.js"></script>
    <script src="../js/settings.js"></script>
    <script src="../js/gleek.js"></script>
@endsection
