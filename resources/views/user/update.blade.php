{{--
<labe>کدکشور</labe>
<input type="text" id="country_code" value="{{$country_code}}">
<label>موبایل</label>
<input type="mobile" id="mobile" value="{{$mobile}}">
<input type="hidden" value="{{$user_id}}" id="user_id">
<a class="btn btn-primary" onclick="edit()">تایید</a>--}}
<div class="col-lg-12">
    <div class="card form-card">
        <div class="card-body">
            <h4 class="card-title mb-4"></h4>
            <form class="form-inline">
                <div class="form-group">
                    <label class="sr-only" for="inlineFormInputName2">کدکشور</label>
                    <input value="{{$country_code}}" type="text" size="50" class="form-control mb-2 mr-sm-5"
                           id="country_code" placeholder="کدکشو" required>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="inlineFormInputGroupUsername2">موبایل</label>
                    <input  value="{{$mobile}}" type="text" size="50" class="form-control mb-2 mr-sm-5"
                           id="mobile"  placeholder="موبایل">
                </div>
                <input type="hidden" value="{{$user_id}}" id="user_id">
                <div class="form-group">
                    <button    class="btn btn-primary btn-form" onclick="edit()" >تایید</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="form-group col-lg-4" id="success">

</div>


    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function edit() {

            var mobile = document.getElementById('mobile').value;
            var country_code = document.getElementById('country_code').value;
            var user_id = document.getElementById('user_id').value;
            $.ajax({
                url: '{{route('user.editUser')}}',
                type: 'put',
                data: {
                    _token: CSRF_TOKEN,
                    user_id :user_id,
                    country_code: country_code,
                    mobile: mobile,

                },
                dataType: 'json',
                success: function(data ) {
                    $('#success').html(data);
                }
            });
        }
        </script>
