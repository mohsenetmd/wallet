<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
|User Login
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->namespace('api\v1')->prefix('user/')->group(function () {
    Route::get('', 'UserController@show');
    Route::post('marketer-store','UserController@marketerStore');
});

Route::namespace('api\v1')->group(function () {
    Route::post('user/add-mobile', 'UserController@addMobile');
    Route::post('user/resend-pin', 'UserController@resendPin');
    Route::post('user/verify-mobile', 'UserController@verifyMobile');
    Route::post('user/check-mobile', 'UserController@checkMobile');
});

/*
|--------------------------------------------------------------------------
|Transaction
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->namespace('api\v1')->prefix('transaction/')->group(function () {
    Route::post('/store', 'TransactionController@store')->middleware('amount.checker');
    Route::post('/update', 'TransactionController@update')->middleware('Buy');
    Route::post('/amount', 'TransactionController@getAmount');
});

/*
|--------------------------------------------------------------------------
|Payment
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->namespace('api\v1')->prefix('payment/')->group(function () {
    Route::post('/get-link', 'PaymentController@getLink');
});

Route::namespace('api\v1')->prefix('payment/')->group(function () {
    Route::post('verify/', 'PaymentController@verify');
    Route::any('mobile-charge/get-products', 'MobileChargeController@getProducts');
});

Route::namespace('api\v1')->prefix('payment/')->group(function () {
    Route::post('verify/', 'PaymentController@verify');
});


/*
|--------------------------------------------------------------------------
|Sync
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->namespace('api\v1')->group(function () {
    Route::post('user/sync', 'UserController@sync');
});


/*
|--------------------------------------------------------------------------
|Mobile Charge
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->namespace('api\v1')->group(function () {
    Route::post('mobile/show-credit', 'MobileChargeController@showCredit');
    Route::post('mobile/buy-credit', 'MobileChargeController@buyCredit')->middleware(['amount.checker','transaction.marketer']);
    Route::post('mobile/buy-product', 'MobileChargeController@buyProduct')->middleware('transaction.marketer');
    Route::post('mobile-/test', 'MobileChargeController@test')->middleware('buy');
    Route::get('mobile/get-products', 'MobileChargeController@getProducts');
});
/*
|--------------------------------------------------------------------------
| FinoTech
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->namespace('api\v1')->group(function () {
    Route::post('/finotech/card-information', 'FinoTechController@cardInformation');
});
/*
|--------------------------------------------------------------------------
| Test Speed
|--------------------------------------------------------------------------
*/

Route::middleware('auth:api')->namespace('api\v1')->group(function () {
    Route::post('/user-profile', 'UserProfileController@userProfile');
});
//|--------------------------------------------------------------------------
//|Taxi
//|--------------------------------------------------------------------------
//*/
Route::middleware('auth:api')->namespace('api\v1')->prefix('taxi')->group(function () {
     Route::post('/location', 'TaxiController@location');
     Route::post('/calculate-price', 'TaxiController@calculatePrice');
     Route::post('/request-taxi', 'TaxiController@requestTaxi');
});
//tourism
Route::namespace('api\v1')->prefix('tourism')->group(function () {
     Route::post('/selectedPlace', 'TourismController@selectedPlace');
});
/*
|--------------------------------------------------------------------------
| Charity
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->namespace('api\v1')->prefix('charity')->group(function () {
    Route::get('/list','CharityController@list');
    Route::post('/donate','CharityController@donate');
    Route::get('/list-donate','CharityController@getListDonate');
});
/*
|--------------------------------------------------------------------------
| map places
|--------------------------------------------------------------------------
*/
Route::get('/list-places',function (){
   $place = \App\Models\CategoryPlace::all();
    $plaseRespobse=\App\Http\Resources\PlaceResource::collection($place);
    return response()->json([
        "data"=>[
            'category_places'=>$plaseRespobse
        ]
    ]);
});

/*
|--------------------------------------------------------------------------
| AirFare
|--------------------------------------------------------------------------
*/
Route::namespace('api\v1')->prefix('airfare')->group(function (){
    Route::get('/city','AirFareController@listCity')->name('airfare.list.city');
});
