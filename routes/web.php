<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| Login And Registration System
|--------------------------------------------------------------------------
*/

Route::get('/login', 'UserController@login')->name('user.login');
Route::get('/verify', 'UserController@createToken')->name('user.createToken');
Route::any('/accept', 'UserController@checkToken')->name('user.checkToken');
Route::any('/marketer/store', 'UserController@marketerStore')->name('user.marketerStore');
Route::any('list', 'UserController@list')->name('user.list');
Route::any('listMarketer', 'UserController@listMarketer')->name('user.listMarketer');
Route::any('/insertajax', 'UserController@insertajax')->name('user.insertajax');
Route::any('update', 'UserController@update')->name('user.update');
Route::any('updateUser', 'UserController@editUser')->name('user.editUser');
Route::any('insertUser', 'UserController@createUser')->name('user.createUser');
Route::delete('delete', 'UserController@delete')->name('user.delete');
/*
|--------------------------------------------------------------------------
| Url Create
|--------------------------------------------------------------------------
*/
Route::any('/url', 'UrlGeneratorController@create');
Route::any('/p{url?}', 'UrlGeneratorController@createPaymentLink');

/*
|--------------------------------------------------------------------------
| Transaction System
|--------------------------------------------------------------------------
*/

Route::any('/transaction/insert/', 'TransactionController@store');
Route::any('/transaction/list/', 'TransactionController@list')->name('Transaction.list');
Route::any('/transaction/listMarketer/', 'TransactionController@list2')->name('Transaction.listForMarketer');
Route::any('/transaction/detailList/', 'TransactionController@detailList')->name('Transaction.detailList');
/*
|--------------------------------------------------------------------------
| PanelUserController
|--------------------------------------------------------------------------
*/

Route::prefix('panel/user')->name('panel.user.')->namespace('panel')->group(function () {
    Route::get('insert', 'UserPanelController@insert')->name('insert');
    Route::post('store', 'UserPanelController@store')->name('store');
    Route::any('update', 'UserPanelController@update')->name('update');
    Route::any('updatePass', 'UserPanelController@updatePass')->name('updatePass');
    Route::any('RestorePass', 'UserPanelController@RestorePass')->name('RestorePass');
    Route::any('delete', 'UserPanelController@delete')->name('delete');
    Route::any('list', 'UserPanelController@list')->name('list');
    Route::any('/listhome', 'UserPanelController@listhome')->name('listhome');
    Route::any('show', 'UserPanelController@show')->name('show');
    Route::get('login', function () {
        return view('panel.login');
    });
    Route::any('/insert', 'UserPanelController@insert')->name('insert');
    Route::any('/insertajax', 'UserPanelController@insertajax')->name('insertajax');
    Route::post('storeUser', 'UserPanelController@storeUser')->name('storeUser');
    Route::any('/updateajax', 'UserPanelController@updateAjax')->name('updateAjax');
    Route::any('login', 'UserPanelController@login')->name('login');
    Route::get('login', 'UserPanelController@logout')->name('logout');
    Route::any('home', 'UserPanelController@home')->name('home');
    Route::any('permission', 'UserPanelController@permission')->name('permission');
    // Route::any('home', 'UserPanelController@home')->name('home');
});

/*
|--------------------------------------------------------------------------
//GroupController
|--------------------------------------------------------------------------
*/

Route::prefix('panel/group')->name('panel.group.')->namespace('panel')->group(function () {
    Route::get('insert/', 'GroupController@insert')->name('insert')->middleware('permission:GroupController@insert');
    Route::post('insert/', 'GroupController@store')->name('store')->middleware('permission:GroupController@store');
    Route::put('edit/', 'GroupController@update')->name('update')->middleware('permission:GroupController@update');
    Route::delete('delete/', 'GroupController@delete')->name('delete')->middleware('permission:GroupController@delete');
    Route::get('show/{Group}', 'GroupController@show')->name('show')->middleware('permission:GroupController@show');
});

/*
|--------------------------------------------------------------------------
| UserGroupController
|--------------------------------------------------------------------------
*/

Route::prefix('panel/user-group')->name('panel.usergroup.')->namespace('panel')->group(function () {
    Route::get('insert/', 'UserGroupController@insert')->name('insert')->middleware('permission:UserGroupController@insert');
    Route::post('insert/', 'UserGroupController@store')->name('store')->middleware('permission:UserGroupController@store');
    Route::put('edit/', 'UserGroupController@update')->name('update')->middleware('permission:UserGroupController@update');
    Route::delete('delete/', 'UserGroupController@delete')->name('delete')->middleware('permission:UserGroupController@delete');
    Route::get('show/{UserGroup}', 'UserGroupController@show')->name('show')->middleware('permission:UserGroupController@show');
});

/*
|--------------------------------------------------------------------------
//MethodController
|--------------------------------------------------------------------------
*/

Route::prefix('panel/method')->name('panel.method.')->namespace('panel')->group(function () {
    Route::get('insert/', 'MethodController@insert')->name('insert')->middleware('permission:MethodController@insert');
    Route::post('insert/', 'MethodController@store')->name('store');
    Route::put('edit/', 'MethodController@update')->name('update');
    Route::delete('delete/', 'MethodController@delete')->name('delete');
    Route::get('show/', 'MethodController@show')->name('show');
});


/*
|--------------------------------------------------------------------------
/PermissionGroupController
|--------------------------------------------------------------------------
*/

Route::prefix('panel/permission-group')->name('panel.permissiongroup.')->namespace('panel')->group(function () {
    Route::any('insert/', 'PermissionGroupController@insert')->name('insert')->middleware('permission:PermissionGroupController@insert');
    Route::post('insert/', 'PermissionGroupController@store')->name('store')->middleware('permission:PermissionGroupController@insert');
    Route::put('edit/', 'PermissionGroupController@update')->name('update')->middleware('permission:PermissionGroupController@insert');
    Route::delete('delete/', 'PermissionGroupController@delete')->middleware('permission:PermissionGroupController@insert');
    Route::get('show/', 'PermissionGroupController@show')->middleware('permission:PermissionGroupController@insert');
    Route::any('/ajax', 'PermissionGroupController@ajax')->name('ajax');
    Route::any('insertajax/', 'PermissionGroupController@ajaxStore')->name('ajaxStore')->middleware('permission:PermissionGroupController@ajaxStore');
});


/*
|--------------------------------------------------------------------------
//PermissionUserController
|--------------------------------------------------------------------------
*/

Route::prefix('panel/permission-user')->name('panel.permissionuser.')->namespace('panel')->group(function () {
    Route::any('insert/', 'PermissionUserController@insert')->name('insert');
    Route::post('insert/', 'PermissionUserController@store')->name('store');
    Route::put('edit/', 'PermissionUserController@update');
    Route::delete('delete/', 'PermissionUserController@delete');
    Route::get('show/', 'PermissionUserController@show');
    Route::any('/ajax', 'PermissionUserController@ajax')->name('ajax');

});

/*
 * //TaxiController

 */
Route::prefix('panel/taxi')->name('panel.taxi.')->namespace('panel')->group(function () {
    Route::any('requestTaxi/', 'TaxiController@request')->name('request');
    Route::any('chooseSelectedPlace/','TaxiController@chooseSelectedPlace')->name('chooseSelectedPlace');
    Route::any('calculatePrice/', 'TaxiController@calculatePrice')->name('calculatePrice');
    Route::any('requestService/', 'TaxiController@requestService')->name('requestService');
    Route::any('requestRefresh/', 'TaxiController@requestRefresh')->name('requestRefresh');
    Route::any('search/', 'TaxiController@search')->name('search');
    Route::any('storeTrip/', 'TaxiController@storeTrip')->name('storeTrip');
    Route::any('storeSelectedPlace/','TaxiController@storeSelectedPlace')->name('storeSelectedPlace');
    Route::any('currentRequest/','TaxiController@currentRequest')->name('currentRequest');
    Route::any('countRequest/','TaxiController@countRequest')->name('countRequest');
});
/*
|--------------------------------------------------------------------------
|  //tourism
|--------------------------------------------------------------------------
*/

Route::prefix('panel/tourism')->name('panel.tourism.')->namespace('panel')->group(function () {
    Route::any('storeTypeCategory/', 'tourismController@storeTypeCategory')->name('storeTypeCategory');
    Route::any('typeCategory/', 'tourismController@typeCategory')->name('typeCategory');
    Route::any('listTypeCategory', 'tourismController@listTypeCategory')->name('listTypeCategory');
    Route::any('listCity', 'tourismController@listCity')->name('listCity');
    Route::any('homeTourism/{id}/{cityId}', 'tourismController@homeTourism')->name('homeTourism');
    Route::any('placeInfo/{id?}', 'tourismController@placeInfo')->name('placeInfo');
    Route::any('storeCategoryPlace', 'tourismController@storeCategoryPlace')->name('storeCategoryPlace');
    Route::any('editCategoryPlace', 'tourismController@editCategoryPlace')->name('editCategoryPlace');
});
/*
|--------------------------------------------------------------------------
| Fast Test
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/','HomeController@index');







/*
|--------------------------------------------------------------------------
| Finotech
|--------------------------------------------------------------------------
*/
    Route::get('finotech/panel-finotech-cc', 'FinoTechController@panelFinotechCc')->name('panelFinotech');
    Route::get('finotech/panel-finotech-ac', 'FinoTechController@panelFinotechAc')->name('panelFinotechAc');
    Route::get('bank-confirmation', 'FinoTechController@bankConfirmation');
    Route::get('finotech/get_wages', 'FinoTechController@getBankInfo');
    Route::get('finotech/get-authorization-code', 'FinoTechController@getAuthorizationCode');
    Route::get('finotech/deposit-to-card', 'FinoTechController@depositToCard');

/*
|--------------------------------------------------------------------------
| Charity
|--------------------------------------------------------------------------
*/

    Route::resource('charity',CharityController::class)->except('index');
    Route::any('charity','CharityController@index')->name('charity.index');

/*
|--------------------------------------------------------------------------
| AirFare
|--------------------------------------------------------------------------
*/
    Route::get('airfare','AirFareController@selectCity');
    Route::get('airfare/edit','AirFareController@editCity');
    Route::post('airfare/update','AirFareController@updateCity');
    Route::post('airfare/delete','AirFareController@deleteCity');
    Route::post('airfare/save/{Abbreviation}','AirFareController@saveCity')->name('airfare.save.city');



