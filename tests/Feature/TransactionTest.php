<?php

namespace Tests\Feature;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    public $typeOfTransactionMinus = '0';
    public $typeOfTransactionPlus = '1';
    public $statusPre = '0';
    public $statusAccept = '1';
    public $statusCancel = '2';
    public $categoryBuy = '1';
    public $categoryOnline = '2';
    public $statusCode = 200;

    /**
     * @test
     */
    public function transaction_store_buy()
    {
        $user = User::all()->random(1)->first();
        $amount = rand(1000, 10000);
        $apiToken = $user->api_token;
        $response = $this->geResponseStoreTransaction($apiToken, $amount, $this->categoryBuy);
        $response->assertOk();
    }

    /**
     * @test
     */
    public function transaction_store_online()
    {
        $user = User::all()->random(1)->first();
        $amount = rand(1000, 10000);
        $apiToken = $user->api_token;
        $response = $this->geResponseStoreTransaction($apiToken, $amount, $this->categoryOnline);
        $response->assertOk();
    }

    /**
     * @test
     */
    public function transaction_update()
    {
        $user = User::all()->random(1)->first();
        $transaction = Transaction::where('user_id' , $user->user_id)->first();
        $apiToken = $user->api_token;
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $apiToken,
        ])->json('POST', 'api/transaction/update', [
            'status' => 1,
            'transaction_id' => $transaction->transaction_id,
            'amount' => $transaction->amount,
            'category_id' => $transaction->category_id
        ]);
        $response->assertOk();
    }

//    /**
//     * @test
//     */
//    public function transaction_sum()
//    {
//        for ($i = 0; $i < 10000; $i++) {
//            $transaction = Transaction::where(['user_id' => 1, 'status' => '1'])->sum('amount');
//        }
//    }


    public function geResponseStoreTransaction($apiToken, $amount, $categoryId)
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $apiToken,
        ])->json('POST', 'api/transaction/store', [
                'category_id' => $categoryId,
                'amount' => $amount
            ]);
        return $response;
    }


}
